package com.shreecement.shree.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.CheckUserExists;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.PERMISSION_ALL;
import static com.shreecement.shree.Utlility.Constant.PREF_PHONE_FOR_REG;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.hasPermissions;

/**
 * Created by  on 1/3/18.
 */

public class MobileInputActivity extends Activity {
    Button btnSubmit;

    EditText edMobileNumber;

    LinearLayout layout;
    String user_exists;
    String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setTheme(R.style.MyRandomTheme);
        /*if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorWhite));
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }*/
        setContentView(R.layout.activity_mobile_input);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        layout = (LinearLayout) findViewById(R.id.layoutmain);

        btnSubmit = findViewById(R.id.btnSubmit);
        edMobileNumber = findViewById(R.id.edMobileNumber);

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent ev) {
                hideKeyboard(view);
                return false;
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!hasPermissions(MobileInputActivity.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(MobileInputActivity.this, PERMISSIONS, PERMISSION_ALL);
                }
                else {
                    if (edMobileNumber.getText().toString().length() == 0) {
                        Toast.makeText(MobileInputActivity.this, "Mobile  field is required!", Toast.LENGTH_LONG).show();

                    } else {
                        String mobileNumber = edMobileNumber.getText().toString().trim();
                        StorePrefs.setDefaults(PREF_PHONE_FOR_REG, mobileNumber, MobileInputActivity.this);

                        //  String valu2e = b("Abhi","sheik");
                        // Log.e("--------", "valu2e : - " + valu2e);
                        hideKeyboard(view);
                        new PostClass_checkMobileNumber(MobileInputActivity.this, mobileNumber).execute();
                   /* Intent intent = new Intent(MobileInputActivity.this, OtpInputActivity.class);
                    startActivity(intent);*/
                    }
                }
            }
        });

    }

    protected void hideKeyboard(View view)
    {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    class PostClass_checkMobileNumber extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String mobileNumber = "";

        public PostClass_checkMobileNumber(Context c, String mobileNumber) {
            this.context = c;
            this.mobileNumber = mobileNumber;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    Log.d("mobileNumber", "=======send otp==========" + mobileNumber);

                    OkHttpClient client = new OkHttpClient();

                    RequestBody formBody = new FormEncodingBuilder()
                            .add("user_name", mobileNumber)
                           // .add("page_module", MODULE_COMMON)
                            .build();
                    Request request = new Request.Builder()
                            .url(CheckUserExists)
                            .post(formBody)
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .addHeader("Content-Type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("Postman-Token", "34f4897c-795c-4826-97b7-da13434f3797")
                            .build();

                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======mobileNumber=========" + CheckUserExists);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                             user_exists = jObject.getString("user_exists").trim();

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {

                if (user_exists.equalsIgnoreCase("y")) {
                Intent intent = new Intent(MobileInputActivity.this, LoginActivity.class);
                startActivity(intent);
                }
                if (user_exists.equalsIgnoreCase("n")) {
                   // showToastMessage(MobileInputActivity.this,"No user associated with this number");
                Intent intent = new Intent(MobileInputActivity.this, OtpInputActivity.class);
                startActivity(intent);
                }
                user_exists = "";
            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    private String b(String str, String str2) {
        String str3 = "SHA-512";
        byte[] bytes = (MobileInputActivity.this + "|" + "encCuvora-" + str + "|" + MobileInputActivity.this + "|" + str2).getBytes();

        StringBuffer stringBuffer = new StringBuffer();
        try {
            MessageDigest instance = MessageDigest.getInstance(str3);
            instance.reset();
            instance.update(bytes);
            bytes = instance.digest();
            for (byte b : bytes) {
                String toHexString = Integer.toHexString(b & 255);
                if (toHexString.length() == 1) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(toHexString);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }
}
