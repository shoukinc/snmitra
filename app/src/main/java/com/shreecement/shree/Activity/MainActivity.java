package com.shreecement.shree.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.shreecement.shree.Fragment.Conversations.Fragment_Conversion_Formulas;
import com.shreecement.shree.Fragment.Dealers.Fragment_Dealers;
import com.shreecement.shree.Fragment.Events.Fragment_Events;
import com.shreecement.shree.Fragment.FeedBack.Fragment_Feedback;
import com.shreecement.shree.Fragment.Enquiry.Fragment_Customer_add_Enquiry;
import com.shreecement.shree.Fragment.Fragment_Customers;
import com.shreecement.shree.Fragment.Fragment_Dashboard;
import com.shreecement.shree.Fragment.Fragment_Orders;
import com.shreecement.shree.Fragment.Fragment_Receipts;
import com.shreecement.shree.Fragment.Help.Fragment_Help;
import com.shreecement.shree.Fragment.MaterialTransaction.Fragment_Material_Transaction;
import com.shreecement.shree.Fragment.Mitra.Fragment_Mitras;
import com.shreecement.shree.Fragment.Rating.Fragment_RateApp;
import com.shreecement.shree.Fragment.Referral.Fragment_Referral;
import com.shreecement.shree.Fragment.RelailerLocater.Fragment_Dealer_Retailer_Locater;
import com.shreecement.shree.Fragment.Retailer.Fragment_Retailers;
import com.shreecement.shree.Fragment.TVC.Fragment_TVCS;
import com.shreecement.shree.Fragment.Tips.Fragment_Tips;
import com.shreecement.shree.Fragment.fragment_AboutUs;
import com.shreecement.shree.NavgationDrawerUtils.FragmentDrawer;
import com.shreecement.shree.Notifcation.Config;
import com.shreecement.shree.Notifcation.NotificationUtils;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import io.realm.Realm;

import static com.shreecement.shree.Activity.LoginActivity.mRegistrationBroadcastReceiver;
import static com.shreecement.shree.Utlility.Constant.PREF_DISCLAIMER_DESC;


public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    boolean doubleBackToExitPressedOnce = false;
    private static String TAG = MainActivity.class.getSimpleName();
    public DrawerLayout drawer;
    private FragmentDrawer drawerFragment;
    public Toolbar mToolbar;
    public Fragment fragment;
    Context context;
    String clickLayout = "";
    String message = "Fragment_Dashboard";

    private ActionBarDrawerToggle mDrawerToggle;
    private ActionBar mActionBar;
    private boolean mToolBarNavigationListenerIsRegistered = false;
    Dialog dialog;
    Button btnOk;
    TextView tvDisclaimerDesc;
    LinearLayout LLHeader;

    public final static int PERM_REQUEST_CODE_DRAW_OVERLAYS = 1234;

    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificationBuilder;
    private String notificationTitle;
    private String notificationText;
    private Bitmap icon;
    private int currentNotificationID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        LLHeader = findViewById(R.id.LLHeader);
        LLHeader.setClickable(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setSupportActionBar(mToolbar);
        mActionBar = getSupportActionBar();

        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        dialog = new Dialog(getApplicationContext());

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);



        permissionToDrawOverlays();
        Realm.init(this);

        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, drawer, mToolbar);
        drawerFragment.setDrawerListener(this);

        fragment = new Fragment_Dashboard();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment).commit();

        //Initializing our broadcast receiver
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {

            //When the broadcast received

            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                   /* String message = intent.getStringExtra("message");
                    String type = intent.getStringExtra("type");
                     Toast.makeText(getApplicationContext(), "Note : " + message, Toast.LENGTH_LONG).show();*/

                }
            }
        };

     /*   // Check if Android M or higher
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Show alert dialog to the user saying a separate permission is needed
            // Launch the settings activity if the user prefers
            Intent myIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
            startActivity(myIntent);
        }*/

    }

    private void setDataForSimpleNotification() {
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText);
        sendNotification();
    }

    private void sendNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(contentIntent);
        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        currentNotificationID++;
        int notificationId = currentNotificationID;
        if (notificationId == Integer.MAX_VALUE - 1)
            notificationId = 0;
        notificationManager.notify(notificationId, notification);
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Log.e("Main Activity", "Firebase reg id: " + regId);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();

            if (backStackEntryCount > 0) {
                getFragmentManager().popBackStackImmediate();

                if (backStackEntryCount == 1) {
                    showUpButton(false);
                    super.onBackPressed();
                }
                else
                    super.onBackPressed();

            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
                }
            }
    }


    //Registering receiver on activity resume
    @Override
    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    //Unregister receiver on activity paused
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        Log.w("MainActivity", "onPause");


        super.onPause();
    }


    public void onDrawerItemSelected(View view, int position) {
//        displayView(position);
    }

    private void displayView(int position) {
        fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_home))) {
                    clickLayout = getResources().getString(R.string.nav_item_home);
                    fragment = new Fragment_Dashboard();
                }

                break;
            case 1:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_customers))) {
                    clickLayout = getResources().getString(R.string.nav_item_customers);
                    fragment = new Fragment_Customers();
                }
                break;

            case 2:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_dealers))) {
                    clickLayout = getResources().getString(R.string.nav_item_dealers);
                    fragment = new Fragment_Dealers();
                }

                break;



            case 3:

                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_retailers))) {
                    clickLayout = getResources().getString(R.string.nav_item_retailers);
                    fragment = new Fragment_Retailers();
                }

                break;

            case 4:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_dealer_retailer_locater))) {
                    clickLayout = getResources().getString(R.string.nav_item_dealer_retailer_locater);
                    fragment = new Fragment_Dealer_Retailer_Locater();
                }

                break;
            case 5:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_mitras))) {
                    clickLayout = getResources().getString(R.string.nav_item_mitras);
                    fragment = new Fragment_Mitras();

                }
                break;
            case 6:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_material_trasaction))) {
                    clickLayout = getResources().getString(R.string.nav_item_material_trasaction);
                    fragment = new Fragment_Material_Transaction();

                }
                break;
            case 7:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_orders))) {
                    clickLayout = getResources().getString(R.string.nav_item_orders);
                    fragment = new Fragment_Orders();
                }
                break;
            case 8:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_receipts))) {
                    clickLayout = getResources().getString(R.string.nav_item_receipts);
                    fragment = new Fragment_Receipts();
                }
                break;
            case 9:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_customer_enquiry))) {
                    clickLayout = getResources().getString(R.string.nav_item_customer_enquiry);
                    fragment = new Fragment_Customer_add_Enquiry();
                }
                break;
              case 10:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_referral))) {
                    clickLayout = getResources().getString(R.string.nav_item_referral);
                    fragment = new Fragment_Referral();
                }
                break;
                case 11:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_events))) {
                    clickLayout = getResources().getString(R.string.nav_item_events);
                    fragment = new Fragment_Events();
                }
                break;
            case 12:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_conversion_formulas))) {
                    clickLayout = getResources().getString(R.string.nav_item_conversion_formulas);
                    fragment = new Fragment_Conversion_Formulas();
                }
                break;

                case 13:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_tvcs))) {
                    clickLayout = getResources().getString(R.string.nav_item_tvcs);
                    fragment = new Fragment_TVCS();
                }
                break;
            case 14:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_tips))) {
                    clickLayout = getResources().getString(R.string.nav_item_tips);
                    fragment = new Fragment_Tips();
                }

                break;
            case 15:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_feedaback))) {
                    clickLayout = getResources().getString(R.string.nav_item_feedaback);
                    fragment = new Fragment_Feedback();
                }
                break;
                case 16:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_help))) {
                    clickLayout = getResources().getString(R.string.nav_item_help);
                    fragment = new Fragment_Help();
                }
                break;

                case 17:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_rate_app))) {
                    clickLayout = getResources().getString(R.string.nav_item_rate_app);
                    fragment = new Fragment_RateApp();
                }
                break;
            case 18:
                if (!clickLayout.equalsIgnoreCase(getResources().getString(R.string.nav_item_share))) {
                    clickLayout = getResources().getString(R.string.nav_item_share);

                    String shareBody = "ShreeCement Android \n http://www.shreecement.com";
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "APP NAME (Open it in Google Play Store to Download the Application)");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));

                }

                break;
            case 19:
                if (!clickLayout.equalsIgnoreCase("AboutUs")) {
                    clickLayout = "AboutUs";
                    fragment = new fragment_AboutUs();

                }
                break;

            case 20:
                if (!clickLayout.equals("Disclaimer")) {
                    clickLayout = "Disclaimer";
                    ViewDialog alert = new ViewDialog();
                    Toast.makeText(context, StorePrefs.getDefaults(PREF_DISCLAIMER_DESC, MainActivity.this), Toast.LENGTH_SHORT).show();
                    alert.showDialog(MainActivity.this, StorePrefs.getDefaults(PREF_DISCLAIMER_DESC, MainActivity.this));
                }

                break;

            case 21:
                logout();
                break;

            default:
                break;
        }

        if (fragment != null) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                            R.anim.enter_from_right, R.anim.exit_to_left);
                    fragmentTransaction.replace(R.id.container_body, fragment).commit();
                }
            }, 275);
        }
    }

    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(MainActivity.this)
                .setMessage("Are you sure you want to Logout?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        StorePrefs.clearDefaults("LoggedIn", MainActivity.this);
                        StorePrefs.clearDefaults("regId", MainActivity.this);
                        MainActivity.this.finish();
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    public void disclaimer(){
        dialog.getWindow();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.fragment_disclaimer);
        dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;

        btnOk = (Button) dialog.findViewById(R.id.btnOk);

        dialog.show();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void closeDrawer() {
        drawer.closeDrawer(GravityCompat.START);
    }

    public void showUpButton(boolean show) {
        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if (show) {
            // Remove hamburger
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            // Show back button
            mActionBar.setDisplayHomeAsUpEnabled(true);
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if (!mToolBarNavigationListenerIsRegistered) {
                mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });

                mToolBarNavigationListenerIsRegistered = true;
            }

        } else {
            // Remove back button
            mActionBar.setDisplayHomeAsUpEnabled(false);
            // Show hamburger
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            // Remove the/any drawer toggle listener
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.openDrawer(GravityCompat.START);
                }
            });
            mToolBarNavigationListenerIsRegistered = false;
        }

        // So, one may think "Hmm why not simplify to:
        // .....
        // getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
        // mDrawer.setDrawerIndicatorEnabled(!enable);
        // ......
        // To re-iterate, the order in which you enable and disable views IS important #dontSimplify.
    }
    public class ViewDialog {

        public void showDialog(Activity activity, String msg){
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.fragment_disclaimer);

            tvDisclaimerDesc = dialog.findViewById(R.id.tvDisclaimerDesc);
            tvDisclaimerDesc.setText(msg);
             btnOk =dialog.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        }
    }

    public void permissionToDrawOverlays() {
        if (android.os.Build.VERSION.SDK_INT >= 23) {   //Android M Or Over
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, PERM_REQUEST_CODE_DRAW_OVERLAYS);
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PERM_REQUEST_CODE_DRAW_OVERLAYS) {
            if (android.os.Build.VERSION.SDK_INT >= 23) {   //Android M Or Over
                if (!Settings.canDrawOverlays(this)) {
                    // ADD UI FOR USER TO KNOW THAT UI for SYSTEM_ALERT_WINDOW permission was not granted earlier...
                }
            }
        }
    }
}
