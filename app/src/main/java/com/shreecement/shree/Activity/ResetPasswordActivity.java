package com.shreecement.shree.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.ACTION_EDIT;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.ResetPassUrl;

/**
 * Created by  on 1/3/18.
 */

public class ResetPasswordActivity extends Activity {
    Button btnSubmit;

    EditText ed_otpCode,ed_password,ed_Conformpassword;


    String Otp_Code = "";
    String password = "";
    String ConformPassword = "";
    String from_mobile = "1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        btnSubmit = findViewById(R.id.btnSubmit);


        ed_otpCode = findViewById(R.id.ed_otpCode);
        ed_password = findViewById(R.id.ed_password);
        ed_Conformpassword = findViewById(R.id.ed_Conformpassword);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Otp_Code=ed_otpCode.getText().toString();
                password=ed_password.getText().toString();
                ConformPassword=ed_Conformpassword.getText().toString();

                if (ed_otpCode.getText().toString().length()< 4){

                    Toast.makeText(ResetPasswordActivity.this, "Enter 4 digit code", Toast.LENGTH_LONG).show();
                }
                else if (ed_password.getText().toString().trim().length()==0) {
                    Toast.makeText(getApplicationContext(),"Password is required!",Toast.LENGTH_SHORT).show();
                }else if (ed_password.getText().toString().trim().length()<6) {
                    Toast.makeText(getApplicationContext(),"Enter password length at least 6  ",Toast.LENGTH_SHORT).show();
                }
                else if (!ConformPassword.equals(password)) {
                    Toast.makeText(getApplicationContext(),"Password not match ",Toast.LENGTH_SHORT).show();
                }
                else {

                       new PostClass_ResetPassword(ResetPasswordActivity.this,Otp_Code, password,ConformPassword,from_mobile).execute();
                }
            }
        });


    }



    class PostClass_ResetPassword extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String Email = "";
        String Otp_Code = "";
        String Password = "";
        String ConformPassword = "";
        String From_mobile;
        String jsonreplyMsg ;


        public PostClass_ResetPassword(Context c, String otp_Code, String password,String conformPassword, String from_mobile) {
            this.context = c;
            this.Otp_Code = otp_Code;
            this.Password = password;
            this.ConformPassword = conformPassword;
            this.From_mobile = from_mobile;

            Email= StorePrefs.getDefaults("email",ResetPasswordActivity.this);

            Log.d("===Email=======",Email);
            Log.d("===Otp_Code=======",Otp_Code);
            Log.d("===Password=======",Password);
            Log.d("=ConformPassword==",ConformPassword);
            Log.d("=From_mobile==",From_mobile);



        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject  jsonObject = new JSONObject();
                    jsonObject.put("email",Email);
                    jsonObject.put("key_code",Otp_Code);
                    jsonObject.put("password",Password);
                    jsonObject.put("cpassword",ConformPassword);
                    jsonObject.put("from_mobile",From_mobile);

                    RequestBody body = RequestBody.create(mediaType,jsonObject.toString());

                    Request request = new Request.Builder()
                            .url(ResetPassUrl)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_EDIT)
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======ResetPasswordUrl=========" + ResetPassUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        jsonreplyMsg= jObject.getString("replyMsg");
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyCode");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                Toast.makeText(context, jsonreplyMsg.toString(), Toast.LENGTH_SHORT).show();
                StorePrefs.clearDefaults("email",ResetPasswordActivity.this);

                Intent i=new Intent(ResetPasswordActivity.this,LoginActivity.class);
                startActivity(i);
                finish();

            } else {
                Toast.makeText(context, s+jsonreplyMsg.toString(), Toast.LENGTH_SHORT).show();

            }

        }
    }

}


