package com.shreecement.shree.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.shreecement.shree.R;

/**
 * Created by  on 1/3/18.
 */

public class InternetCheck_activity extends Activity {


    TextView tvheader;

    TextView btn_tryAgain;

    String Internet="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_internet_connection);
        btn_tryAgain = findViewById(R.id.btn_tryAgain);
        tvheader = findViewById(R.id.tvheader);




        Internet="Oops!\n" +
                "No Internet";
        tvheader.setText(Internet);
        btn_tryAgain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();

                if (netInfo != null && netInfo.isConnected()) {
                   //
                    Intent intent = new Intent(InternetCheck_activity.this, SplashActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });



    }



}


