package com.shreecement.shree.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.Wave;
import com.shreecement.shree.BuildConfig;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.shreecement.shree.networkUtils.BackGroundTask;
import com.shreecement.shree.networkUtils.OnTaskCompleted;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.AboutUsDiscription;
import static com.shreecement.shree.Utlility.Constant.ApkVersion;
import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_DESCRIPTION;
import static com.shreecement.shree.Utlility.Constant.PREF_DISCLAIMER_DESC;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;

/**
 * Created by  on 1/3/18.
 */

public class SplashActivity extends Activity {
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        ProgressBar progressBar = findViewById(R.id.spin_kit);
        Wave wave = new Wave();
       // CubeGrid wave = new CubeGrid();
        progressBar.setIndeterminateDrawable(wave);

        //funDiscreption();

        new PostClass_CheckAppVersion(SplashActivity.this, BuildConfig.VERSION_NAME).execute();


       /* new Handler().postDelayed(new Runnable() {
            *//*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             *//*
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();

                if (netInfo != null && netInfo.isConnected()) {
                    //

                if (StorePrefs.getDefaults("LoggedIn",SplashActivity.this) != null) {
                    Log.d("USER_NAME", "" + StorePrefs.getDefaults("USER_NAME",SplashActivity.this));
                    *//*Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);*//*
                    new PostClass_CheckAppVersion(SplashActivity.this, BuildConfig.VERSION_NAME).execute();
                }
                else{

                    //Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    Intent intent = new Intent(SplashActivity.this, MobileInputActivity.class);
                  //  Intent intent = new Intent(SplashActivity.this, ForgetPasswordActivity.class);
                    startActivity(intent);
                }

                }
                else {
                    Intent intent = new Intent(SplashActivity.this, InternetCheck_activity.class);
                    startActivity(intent);
                }

                finish();
            }
        }, SPLASH_TIME_OUT);*/
    }

    public void funDiscreption(){
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("org_id", ORG_ID);


            Log.d("Tag", "Params ----------->" + jsonObject.toString());
            new BackGroundTask.makeServiceCallPOSTWithCommonHeader(SplashActivity.this, AboutUsDiscription, jsonObject.toString(),
                    new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String response) {
                            // exerciseJsonResponse = response;
                            //parseJsonData(response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                                    JSONObject jsonData = jsonObject.getJSONObject("data");

                                    StorePrefs.setDefaults(PREF_DESCRIPTION, jsonData.getString("description"), SplashActivity.this);
                                    StorePrefs.setDefaults(PREF_DISCLAIMER_DESC, jsonData.getString("disclaimer"), SplashActivity.this);


                                } else if (jsonObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                                    //Utils.displayToastMessage(SplashActivity.this, jsonObject.getString("replyCode"));
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    }).execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class PostClass_CheckAppVersion extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String apk_version = "";


        public PostClass_CheckAppVersion(Context c, String apk_version) {
            this.context = c;
            this.apk_version = apk_version;


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

           /* ProgressBar progressBar = (ProgressBar)findViewById(R.id.spin_kit);
            Wave wave = new Wave();
            progressBar.setIndeterminateDrawable(wave);*/

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    Log.d("apk_version", "=======apk_version==========" + apk_version);

                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject  jsonObject = new JSONObject();


                    jsonObject.put("version", apk_version);
                    jsonObject.put("client_key", CLIENT_KEY);
                    jsonObject.put("client_secret", CLIENT_SECRET);

                    RequestBody body = RequestBody.create(mediaType,jsonObject.toString());

                    System.out.println("jsonObject.toString()===============" + jsonObject.toString());
                    System.out.println("ApkVersion===============" + ApkVersion);
                    Request request = new Request.Builder()
                            .url(ApkVersion)
                            .post(body)
                            .addHeader("page_module", MODULE_COMMON)
                            //.addHeader("client_key", CLIENT_KEY)
                            //.addHeader("client_secret", CLIENT_SECRET)
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            String replyMsg = jObject.getString("replyMsg").trim();


                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            }
            else if (s.equals("success")) {

                if (StorePrefs.getDefaults("LoggedIn",SplashActivity.this) != null) {
                    Log.d("USER_NAME", "" + StorePrefs.getDefaults("USER_NAME",SplashActivity.this));
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else{

                    //Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    Intent intent = new Intent(SplashActivity.this, MobileInputActivity.class);
                    //  Intent intent = new Intent(SplashActivity.this, ForgetPasswordActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
            else if (s.equals("error")) {

                Log.e("In error","In error");
                android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(SplashActivity.this);

                alert.setTitle("Please Update App");

                alert.setCancelable(false);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            Intent viewIntent =
                                    new Intent("android.intent.action.VIEW",
                                            Uri.parse("https://play.google.com/store/apps/details?id=com.shreecement.shree"));
                            startActivity(viewIntent);
                        }catch(Exception e) {
                            Toast.makeText(getApplicationContext(),"Unable to Connect Try Again...",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }
                });

                alert.show();

            }
            else {
                //Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

                android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(SplashActivity.this);

                alert.setTitle("UPDATE APPLICATION");
                alert.setMessage("Please update the application.");
                alert.setCancelable(false);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            Intent viewIntent =
                                    new Intent("android.intent.action.VIEW",
                                            Uri.parse("https://play.google.com/store/apps/details?id=com.shreecement.shree"));
                            startActivity(viewIntent);
                        }catch(Exception e) {
                            Toast.makeText(getApplicationContext(),"Unable to Connect Try Again...", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                });

                alert.show();

            }
        }
    }
}
