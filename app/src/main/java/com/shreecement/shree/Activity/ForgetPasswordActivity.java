package com.shreecement.shree.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.shreecement.shree.Utlility.Constant.ForgotPassUrl;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;

/**
 * Created by  on 1/3/18.
 */

public class ForgetPasswordActivity extends Activity {
    Button btnSend;

    EditText ed_email, ed_phoneNum;
    RadioButton radioUsername, radioPassword;
    RadioButton radioByEmail, radioByPhone;
    RadioGroup radioGroup, radioGroupResetBy;
    String forgot_type = "";
    String from_mobile = "";
    TextView tvTitle;

    Boolean isEmail = true;

    String emailToSend , phoneToSend;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        btnSend = (Button) findViewById(R.id.btnSend);

        radioUsername = (RadioButton) findViewById(R.id.radioUsername);
        radioPassword = (RadioButton) findViewById(R.id.radioPassword);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        radioByEmail = (RadioButton) findViewById(R.id.radioByEmail);
        radioByPhone = (RadioButton) findViewById(R.id.radioByPhone);
        radioGroupResetBy = (RadioGroup) findViewById(R.id.radioGroupResetBy);

        ed_email = (EditText) findViewById(R.id.ed_email);
        ed_phoneNum = (EditText) findViewById(R.id.ed_phoneNum);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        radioPassword.isChecked();

        radioPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                tvTitle.setText(R.string.forgot_passwordd);
            }
        });

        radioUsername.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                tvTitle.setText(R.string.forgot_username);
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioPassword:
                        tvTitle.setText(R.string.forgot_passwordd);
                        break;

                    case R.id.radioUsername:
                        tvTitle.setText(R.string.forgot_username);
                        break;
                    default:
                        break;
                }
            }

        });

        radioGroupResetBy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioByEmail:

                        ed_email.setVisibility(View.VISIBLE);
                        ed_phoneNum.setVisibility(View.GONE);
                        isEmail = true;
                         phoneToSend = "";

                        break;

                    case R.id.radioByPhone:

                        ed_email.setVisibility(View.GONE);
                        ed_phoneNum.setVisibility(View.VISIBLE);
                        isEmail = false;
                        emailToSend  = "";
                        break;
                    default:
                        break;
                }
            }

        });


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             /*   if (isEmail) {
                    if (ed_email.getText().toString().length() == 0) {

                        Toast.makeText(ForgetPasswordActivity.this, "Email Address field is required!", Toast.LENGTH_LONG).show();

                    } else if (!emailValidator(ed_email.getText().toString().trim())) {
                        Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
                    }
                }
                else  if (!isEmail){
                    Toast.makeText(getApplicationContext(), "Phone Number Selected ", Toast.LENGTH_SHORT).show();
                }


                else {*/
                    if (radioPassword.isChecked()) {
                        Log.d("===radioPassword====", "");

                         emailToSend = ed_email.getText().toString();
                         phoneToSend = ed_phoneNum.getText().toString();
                        forgot_type = "forgotpass";
                        from_mobile = "1";
                        new PostClass_ForgetPassword(ForgetPasswordActivity.this, emailToSend, phoneToSend, forgot_type, from_mobile).execute();
                    } else if (radioUsername.isChecked()) {
                        Log.d("===radioUsername====", "");

                         emailToSend = ed_email.getText().toString();
                         phoneToSend = ed_phoneNum.getText().toString();
                        forgot_type = "forgotusername";
                        from_mobile = "1";
                        new PostClass_ForgetPassword(ForgetPasswordActivity.this, emailToSend, phoneToSend, forgot_type, from_mobile).execute();

                    }


               /* }*/
            }
        });


    }

    public static boolean emailValidator(final String mailAddress) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(mailAddress);
        return matcher.matches();
    }

    class PostClass_ForgetPassword extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject = new JSONObject();
        String Email = "";
        String phone = "";
        String Forgot_type = "";
        String From_mobile;
        String jsonreplyMsg;


        public PostClass_ForgetPassword(Context c, String email, String phone, String forgot_type, String from_mobile) {
            this.context = c;
            this.Email = email;
            this.phone = phone;
            this.Forgot_type = forgot_type;
            this.From_mobile = from_mobile;
            Log.d("===Curr_password===", Email);
            Log.d("===Password===", Forgot_type);
            Log.d("===Cpassword===", From_mobile);


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("email", Email);
                    jsonObject.put("mobile_no", phone);
                    jsonObject.put("forgot_type", Forgot_type);
                    jsonObject.put("from_mobile", From_mobile);



                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());
                    System.out.println("Parameters===============" + jsonObject.toString());

                    Request request = new Request.Builder()
                            .url(ForgotPassUrl)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            //.addHeader("action", ACTION_EDIT)
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======ForgotPasswordUrl=========" + ForgotPassUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        jsonreplyMsg = jObject.getString("replyMsg");
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyCode");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();

            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                Toast.makeText(context, jsonreplyMsg.toString(), Toast.LENGTH_SHORT).show();

                Log.d("==jsonreplyMsg==", "" + jsonreplyMsg.toString());
                StorePrefs.setDefaults("email", Email, ForgetPasswordActivity.this);
                Intent i = new Intent(context, ResetPasswordActivity.class);
                startActivity(i);
                finish();

            } else {
                Log.d("==jsonreplyMsg==", "" + jsonreplyMsg.toString());
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

    private void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
        editText.setBackgroundColor(Color.TRANSPARENT);
    }

    private void enableEditText(EditText editText) {
        editText.setFocusable(true);
        editText.setEnabled(true);
        editText.setCursorVisible(true);
        editText.setKeyListener(null);
        editText.setBackgroundColor(Color.WHITE);
    }

}


