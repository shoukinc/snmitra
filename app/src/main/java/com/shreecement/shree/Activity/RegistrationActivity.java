package com.shreecement.shree.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chuross.library.ExpandableLayout;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.Citylist;
import static com.shreecement.shree.Utlility.Constant.Districtlist;
import static com.shreecement.shree.Utlility.Constant.Lookup_data;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.PREFS_DISCLAMER;
import static com.shreecement.shree.Utlility.Constant.PREF_DISCLAIMER_DESC;
import static com.shreecement.shree.Utlility.Constant.PREF_PHONE_FOR_REG;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.Statelist;
import static com.shreecement.shree.Utlility.Constant.Talukalist;
import static com.shreecement.shree.Utlility.Constant.orders_listUrl;

/**
 * Created by  on 1/3/18.
 */

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener{
    Button btnRegister;

    LinearLayout layout;

    TextView tvTitle, tvMitraType, tvState, tvDistrict,
            tvTaluka, tvCity, tvMobileNumber, spnGender,tvDateOfBirthMitra;

    EditText  edFisrtName, edMiddleName, edLastName, edEmail, edConfirmPassword, edPassword, edPostal, edAddressLineOne;

    ExpandableLayout layout_Address;
    RelativeLayout relAddresssInformation;

    CheckBox cbShowPwd;

    String   strFisrtName, strMiddleName, strLastName, strMobileNumber, strEmail, strConfirmPassword, strPassword;
    String mitraCode = "";
    String titleCode = "";
    String stateCode = "";
    String districtCode = "";
    String talukaCode = "";
    String cityCode = "";
    String genderCode = "";
    String date_dobOfMitra= "";
    String dateButtonType = "";

    TextView dialogTitle;
    ListView listView;
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog builder3;

    public ArrayList<LookupCategory> Titlelist;
    public ArrayList<LookupCategory> MitraTypeList;
    public ArrayList<LookupCategory> StateList;
    public ArrayList<LookupCategory> DistrictList;
    public ArrayList<LookupCategory> TalukaList;
    public ArrayList<LookupCategory> CityList;
    public ArrayList<LookupCategory> GenderList;

    public SpinnerDialogAdapter spinnerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registration);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //button
        btnRegister = findViewById(R.id.btnRegister);

        //edittext
        edFisrtName = findViewById(R.id.edFisrtName);
        edFisrtName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        edMiddleName = findViewById(R.id.edMiddleName);
        edMiddleName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        edLastName = findViewById(R.id.edLastName);
        edLastName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        tvMobileNumber = findViewById(R.id.tvMobileNumber);
        edEmail = findViewById(R.id.edEmail);
        edEmail.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        edPassword = findViewById(R.id.edPassword);
        edConfirmPassword = findViewById(R.id.edConfirmPassword);
        cbShowPwd = (CheckBox) findViewById(R.id.cbShowPwd);
        spnGender = (TextView) findViewById(R.id.spnGender);
        tvDateOfBirthMitra= (TextView) findViewById(R.id.tvDateOfBirthMitra);
        edAddressLineOne = (EditText) findViewById(R.id.edAddressLineOne);
        edAddressLineOne.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        edPostal = (EditText) findViewById(R.id.edPostal);
        edPostal.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        layout_Address = (ExpandableLayout)findViewById(R.id.layout_Address);
        relAddresssInformation = findViewById(R.id.relAddresssInformation);
        relAddresssInformation.setOnClickListener(this);

        // add onCheckedListener on checkbox
        // when user clicks on this checkbox, this is the handler.
        cbShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // checkbox status is changed from uncheck to checked.
                if (!isChecked) {
                    // show password
                    edPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    edPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    edConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        //  Log.e("Token:==> ", StorePrefs.getDefaults("token", RegistrationActivity.this));
        tvMobileNumber.setText(StorePrefs.getDefaults(PREF_PHONE_FOR_REG,RegistrationActivity.this));

        //textview
        tvTitle = findViewById(R.id.tvTitle);
        tvMitraType = findViewById(R.id.tvMitraType);
        tvState = findViewById(R.id.tvState);
        tvDistrict = findViewById(R.id.tvDistrict);
        tvTaluka = findViewById(R.id.tvTaluka);
        tvCity = findViewById(R.id.tvCity);

        //clickListner
        btnRegister.setOnClickListener(this);
        tvTitle.setOnClickListener(this);
        tvMitraType.setOnClickListener(this);
        tvState.setOnClickListener(this);
        tvDistrict.setOnClickListener(this);
        tvTaluka.setOnClickListener(this);
        tvCity.setOnClickListener(this);
        spnGender.setOnClickListener(this);
        tvDateOfBirthMitra.setOnClickListener(this);

        Titlelist = new ArrayList<LookupCategory>();
        MitraTypeList = new ArrayList<LookupCategory>();
        StateList = new ArrayList<LookupCategory>();
        DistrictList = new ArrayList<LookupCategory>();
        TalukaList = new ArrayList<LookupCategory>();
        CityList = new ArrayList<LookupCategory>();
        GenderList = new ArrayList<LookupCategory>();


        new PostClassLookUpData(RegistrationActivity.this).execute();

        showDialog(0);

      /*  btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    //String mobileNumber = edMobileNumber.getText().toString().trim();
                 *//*Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(intent);*//*

            }
        });*/

    }

    protected void hideKeyboard(View view)
    {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onClick(View view) {

        if (view == relAddresssInformation) {
            layout_Address.expand();// expand with animation
            layout_Address.collapse();// collapse with animation
        }

        if (view == tvDateOfBirthMitra) {
            showDialog();
            dateButtonType = "tvDateOfBirthMitra";
        }

        if (view == btnRegister) {


            strFisrtName =  edFisrtName.getText().toString();
            strMiddleName =  edMiddleName.getText().toString();
            strLastName =  edLastName.getText().toString();
            strMobileNumber =  tvMobileNumber.getText().toString();
            strEmail =  edEmail.getText().toString();
            strConfirmPassword =  edConfirmPassword.getText().toString();
            strPassword =  edPassword.getText().toString();


            if (mitraCode.trim().length() == 0) {
                Toast.makeText(RegistrationActivity.this, "Profession field is required!", Toast.LENGTH_LONG).show();

            } else if (titleCode.trim().length() == 0) {
                Toast.makeText(RegistrationActivity.this, "Title field is required!", Toast.LENGTH_LONG).show();

            }
            else if (strFisrtName.length() == 0) {
                Toast.makeText(RegistrationActivity.this, "FirstName field is required!", Toast.LENGTH_LONG).show();

            }  /*else if (strLastName.length() == 0) {
                Toast.makeText(RegistrationActivity.this, "LastName field is required!", Toast.LENGTH_LONG).show();

            }*/
            else if (strMobileNumber.length() == 0) {
                Toast.makeText(RegistrationActivity.this, "Mobile field is required!", Toast.LENGTH_LONG).show();

            }
            else if (genderCode.trim().length() == 0) {
                Toast.makeText(getApplicationContext(), "Gender field is required!", Toast.LENGTH_LONG).show();

            }
            else if (tvState.getText().toString().length() == 0) {
                Toast.makeText(getApplicationContext(), "State field is required!", Toast.LENGTH_LONG).show();

            } else if (tvDistrict.getText().toString().length() == 0) {
                Toast.makeText(getApplicationContext(), "District field is required!", Toast.LENGTH_LONG).show();

            } else if (tvTaluka.getText().toString().length() == 0) {
                Toast.makeText(getApplicationContext(), "Taluka field is required!", Toast.LENGTH_LONG).show();

            } else if (tvCity.getText().toString().length() == 0) {
                Toast.makeText(getApplicationContext(), "City field is required!", Toast.LENGTH_LONG).show();

            }
            /*else if (strEmail.length() == 0) {
                Toast.makeText(RegistrationActivity.this, "Email field is required!", Toast.LENGTH_LONG).show();

            }*/
            else if (strPassword.length() == 0) {
                Toast.makeText(getApplicationContext(), "" + "Password is required!", Toast.LENGTH_LONG).show();
            } else if (strPassword.length() < 6) {
                Toast.makeText(getApplicationContext(), "" + "Please use minimum 6 character is password!", Toast.LENGTH_LONG).show();
            }
            else if (!strPassword.equals(strConfirmPassword)) {
                Toast.makeText(getApplicationContext(), "" + "Confirm password doesn't match!", Toast.LENGTH_LONG).show();
            }


            else {

                Log.d("===strMobileNumber===", "" + strMobileNumber);
                Log.d("===mitraCode===", "" + mitraCode);
                Log.d("===titleCode===", "" + titleCode);
                Log.d("===strFisrtName===", "" + strFisrtName);
                Log.d("===strMiddleName===", "" + strMiddleName);
                Log.d("===strLastName===", "" + strLastName);
                Log.d("===genderCode===", "" + genderCode);
                Log.d("===date_dobOfMitra===", "" + date_dobOfMitra);
                Log.d("===tvState===", "" + tvState.getText().toString().trim());
                Log.d("===tvDistrict===", "" + tvDistrict.getText().toString().trim());
                Log.d("===tvTaluka===", "" + tvTaluka.getText().toString().trim());
                Log.d("===tvCity===", "" + tvCity.getText().toString().trim());
                Log.d("===edAddressLineOne===", "" + edAddressLineOne.getText().toString().trim());
                Log.d("===edPostal===", "" + edPostal.getText().toString().trim());
                Log.d("===strEmail===", "" + strEmail);
                Log.d("===strPassword===", "" + strPassword);
                Log.d("strConfirmPassword=", "" + strConfirmPassword);




                String strFullName = titleCode +" "+strFisrtName+" "+strMiddleName+" "+strLastName;

                new PostClassCreateUser(RegistrationActivity.this,
                        tvState.getText().toString().trim(),
                        tvDistrict.getText().toString().trim(),
                        tvCity.getText().toString().trim(),
                        strFullName.trim(),
                        mitraCode.trim(),
                        strMiddleName.trim(),
                        tvTaluka.getText().toString().trim(), strLastName.trim(),
                        strFisrtName.trim(),
                        strPassword.trim(),
                        titleCode.trim(),
                        strMobileNumber.trim(),
                        strEmail.trim(),
                        "M",
                        genderCode,
                        date_dobOfMitra.trim(),
                        edAddressLineOne.getText().toString().trim(),
                        edPostal.getText().toString().trim()

                ).execute();

            }


        } else if (view == tvMitraType) {
            showSpinnerDialog(MitraTypeList, "Mitra Type");

        }
        else if (view == tvTitle) {
            showSpinnerDialog(Titlelist, "Title");
        }

        else if (view == tvState) {
            showSpinnerDialog(StateList, "State");
        }
        else if (view == tvDistrict) {
            showSpinnerDialog(DistrictList, "District");
        }
        else if (view == tvTaluka) {
            showSpinnerDialog(TalukaList, "Taluka");
        }
        else if (view == tvCity) {
            showSpinnerDialog(CityList, "City");
        }
        else if (view == spnGender) {
            showSpinnerDialog(GenderList, "Gender");
        }

    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    private void showSpinnerDialog(ArrayList<LookupCategory> list, String title) {
        dialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);
        LayoutInflater inflater = RegistrationActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        listView = (ListView) dialogView.findViewById(R.id.listCatecory);
        dialogTitle = (TextView) dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerAdapter = new SpinnerDialogAdapter(RegistrationActivity.this, list, title);
        listView.setAdapter(spinnerAdapter);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }

    public class SpinnerDialogAdapter extends BaseAdapter {
        Context context;
        List<LookupCategory> rowItems;
        String Title = "";


        public SpinnerDialogAdapter(Context context, List<LookupCategory> items, String title) {
            this.context = context;
            this.rowItems = items;
            this.Title = title;
            Log.d("===rowItems.zize===", "" + rowItems.size());


        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;

        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new ViewHolder();
                holder.textName = convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();

            }

            holder.textName.setText(rowItems.get(position).getDESCRIPTION());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String meaning = rowItems.get(position).getDESCRIPTION();
                    switch (Title) {

                        case "Mitra Type":
                            builder3.dismiss();
                            tvMitraType.setText(meaning);
                            mitraCode = rowItems.get(position).getCODE();
                            break;

                        case "Title":
                            builder3.dismiss();
                            tvTitle.setText(meaning);
                            titleCode = rowItems.get(position).getCODE();
                            break;

                        case "State":
                            builder3.dismiss();
                            tvState.setText(meaning);
                            stateCode = rowItems.get(position).getDESCRIPTION();
                            tvDistrict.setText("");
                            districtCode = "";
                            tvTaluka.setText("");
                            talukaCode = "";
                            tvCity.setText("");
                            new PostClassDistrictList(RegistrationActivity.this, rowItems.get(position).getDESCRIPTION()).execute();
                            break;
                        case "District":
                            builder3.dismiss();
                            tvDistrict.setText(meaning);
                            districtCode = rowItems.get(position).getDESCRIPTION();
                            tvTaluka.setText("");
                            talukaCode = "";
                            tvCity.setText("");
                            new PostClassTalukaList(RegistrationActivity.this, rowItems.get(position).getDESCRIPTION(),stateCode).execute();
                            break;
                        case "Taluka":
                            builder3.dismiss();
                            tvTaluka.setText(meaning);
                            talukaCode = rowItems.get(position).getDESCRIPTION();
                            tvCity.setText("");
                            new PostClassCityList(RegistrationActivity.this, rowItems.get(position).getDESCRIPTION(),stateCode,districtCode).execute();
                            break;
                        case "City":
                            builder3.dismiss();
                            tvCity.setText(meaning);
                            break;

                        case "Gender":
                            builder3.dismiss();
                            spnGender.setText(meaning);
                            genderCode= rowItems.get(position).getCODE();
                            break;

                    }


                }
            });
            return convertView;
        }


        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }


    }

    // Spinners Async

    public class PostClassLookUpData extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;

        public ArrayList<LookupCategory> tempMitraTypeList;
        public ArrayList<LookupCategory> tempTitlelist;
        public ArrayList<LookupCategory> tempGenderList;

        public PostClassLookUpData(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

            tempMitraTypeList = new ArrayList<LookupCategory>();
            tempTitlelist = new ArrayList<LookupCategory>();
            tempGenderList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");

                   /* JSONObject jsonObject_page_module = new JSONObject();

                    try {
                        jsonObject_page_module.put("page_module", MODULE_COMMON);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody body = RequestBody.create(mediaType, jsonObject_page_module.toString());

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .build();*/
                    // .addHeader("token", StorePrefs.getDefaults("token", context))

                    RequestBody body = RequestBody.create(null, new byte[]{});

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .addHeader("page_module", MODULE_COMMON)
                            // .addHeader("action", ACTION_VIEW)
                            // .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();

                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();


                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONObject category = jObject.getJSONObject("data");

                            System.out.println("======Lookup_data=========" + Lookup_data);


                            JSONArray jsonArrayMitraType = category.getJSONArray("mitra_type");
                            for (int i = 0; i < jsonArrayMitraType.length(); i++) {
                                JSONObject jsonPacking = jsonArrayMitraType.getJSONObject(i);
                                tempMitraTypeList.add(new LookupCategory(jsonPacking.getString("lookup_type"),
                                        jsonPacking.getString("code"), jsonPacking.getString("description")));

                            }
                            MitraTypeList.addAll(tempMitraTypeList);

                            JSONArray jsonArrayTitle = category.getJSONArray("title");
                            for (int i = 0; i < jsonArrayTitle.length(); i++) {
                                JSONObject jsonPacking = jsonArrayTitle.getJSONObject(i);
                                tempTitlelist.add(new LookupCategory(jsonPacking.getString("lookup_type"),
                                        jsonPacking.getString("code"), jsonPacking.getString("description")));

                            }
                            Titlelist.addAll(tempTitlelist);

                            JSONArray jsonSex = category.getJSONArray("sex");
                            for (int i = 0; i < jsonSex.length(); i++) {
                                JSONObject jsonObject = jsonSex.getJSONObject(i);
                                tempGenderList.add(new LookupCategory(jsonObject.getString("lookup_type"),
                                        jsonObject.getString("code"), jsonObject.getString("description")));

                            }
                            GenderList.addAll(tempGenderList);


                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
              /*  spinTradeadapter = new SpinCalegory_Adapter(getActivity(), tempCategorylist);
                spnCategory.setAdapter(spinTradeadapter);*/
                new PostClassStateList(context).execute();

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }
    }

    class PostClassStateList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempStateList;

        PostClassStateList(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempStateList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Statelist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            // .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Statelist=========" + Statelist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempStateList.add(new LookupCategory("",
                                            "", jsonObject.getString("state")));

                                }
                                StateList.clear();
                                StateList.addAll(tempStateList);

                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            } else {


            }

        }
    }

    class PostClassDistrictList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempDistrictList;
        String state;


        PostClassDistrictList(Context c, String state) {
            this.context = c;
            this.state = state;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempDistrictList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("state",state);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Districtlist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            //   .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======orders_listUrl=========" + orders_listUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempDistrictList.add(new LookupCategory("",
                                            "", jsonObject.getString("district")));

                                }
                                DistrictList.clear();
                                DistrictList.addAll(tempDistrictList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


            } else {


            }

        }
    }

    class PostClassTalukaList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempTalukaList;
        String district;
        String state ;

        PostClassTalukaList(Context c, String district, String state) {
            this.context = c;
            this.district = district;
            this.state = state;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempTalukaList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("district", district);
                    json.put("state", state);

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Talukalist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            //  .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======orders_listUrl=========" + orders_listUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempTalukaList.add(new LookupCategory("",
                                            "", jsonObject.getString("taluka")));

                                }
                                TalukaList.clear();
                                TalukaList.addAll(tempTalukaList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            } else {


            }

        }
    }

    class PostClassCityList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempCityList;
        String taluka;
        String state, district;

        PostClassCityList(Context c, String taluka, String state, String district) {
            this.context = c;
            this.taluka = taluka;
            this.state = state;
            this.district = district;
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempCityList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("taluka", taluka);
                    json.put("state", state);
                    json.put("district", district);
                   /* json.put("registration_source", "S");
                    json.put("mobile_number_verified_flag", "Y");*/

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Citylist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            // .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======orders_listUrl=========" + orders_listUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempCityList.add(new LookupCategory("",
                                            "", jsonObject.getString("city")));

                                }
                                CityList.clear();
                                CityList.addAll(tempCityList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            } else {

            }

        }
    }


    protected Dialog onCreateDialog(int id){
        // show disclaimer....
        // for example, you can show a dialog box...
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //builder.setMessage("LEGAL DISCLAIMER: ... ")
        builder.setTitle(R.string.legal_des_title);
        builder.setMessage(StorePrefs.getDefaults(PREF_DISCLAIMER_DESC, RegistrationActivity.this))
                .setCancelable(false)
                .setPositiveButton("Agree", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // and, if the user accept, you can execute something like this:
                        // We need an Editor object to make preference changes.
                        // All objects are from android.context.Context
                        SharedPreferences settings = getSharedPreferences(PREFS_DISCLAMER, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putBoolean("accepted", true);
                        // Commit the edits!
                        editor.commit();
                    }
                })
                .setNegativeButton("Disagree", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //nm.cancel(R.notification.running); // cancel the NotificationManager (icon)
                        System.exit(0);
                    }
                });
        AlertDialog alert = builder.create();
        return alert;
    }

    private void showDialog() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        cal.add(Calendar.DAY_OF_MONTH, 1);
        // Create the DatePickerDialog instance
        DatePickerDialog datePicker = new DatePickerDialog(RegistrationActivity.this, datePickerListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.setCancelable(false);
        datePicker.setTitle("Select the date");
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePicker.show();
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            String year = String.valueOf(selectedYear);
            String month = String.valueOf(selectedMonth + 1);
            String day = String.valueOf(selectedDay);
            String time = "";
            try {
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("hh:mm aa");
                time = df.format(cal.getTime());

                // tvDate.setText(time);

            } catch (Exception e) {
                e.printStackTrace();
            }

            String validity = String.valueOf(new StringBuilder().append(day).append("-")
                    .append(month).append("-").append(year));
            DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
            Date date1 = null;
            Calendar cal = Calendar.getInstance();
            try {
                date1 = readFormat1.parse(validity);
                String dateformatt = formatter1.format(date1);

                if (dateButtonType.equalsIgnoreCase("tvDateOfBirthMitra")) {
                    tvDateOfBirthMitra.setText(dateformatt);
                    date_dobOfMitra = validity;
                }


            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };

}
