package com.shreecement.shree.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.shreecement.shree.Notifcation.Config;
import com.shreecement.shree.Notifcation.NotificationUtils;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.PERMISSION_ALL;
import static com.shreecement.shree.Utlility.Constant.PREF_CUSTOMER_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_EMPLOYEE_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_CITY;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_DISTRICT;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_STATE;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_TALUKA;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_TYPE;
import static com.shreecement.shree.Utlility.Constant.PREF_PHONE_FOR_REG;
import static com.shreecement.shree.Utlility.Constant.PREF_ROLE_TYPE;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.hasPermissions;
import static com.shreecement.shree.Utlility.Constant.loginUrl;
import static com.shreecement.shree.Utlility.Constant.nullCheckFunction;

/**
 * Created by Choudhary on 1/3/18.
 */

public class LoginActivity extends Activity {
    Button btnLogin;
    TextView tvForget;
    EditText edUsername, edpassword;
    CheckBox checkbox;
    LinearLayout layout;

    public static BroadcastReceiver mRegistrationBroadcastReceiver;
    String device_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setTheme(R.style.MyRandomTheme);
        /*if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorWhite));
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorWhite));
        }*/
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        layout = findViewById(R.id.layoutmain);
        tvForget = findViewById(R.id.tvForget);
        btnLogin = findViewById(R.id.btnLogin);
        edUsername = findViewById(R.id.ed_userName);
        edpassword = findViewById(R.id.ed_password);
        checkbox = findViewById(R.id.checkbox);

        edUsername.setText(StorePrefs.getDefaults(PREF_PHONE_FOR_REG,LoginActivity.this));

        String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE,};
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


        tvForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(i);
            }
        });
        layout.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent ev)
            {
                hideKeyboard(view);
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edUsername.getText().toString().length() == 0) {
                    Toast.makeText(LoginActivity.this, "User Name field is required!", Toast.LENGTH_LONG).show();



                } else if (edpassword.getText().toString().length() == 0) {
                    Toast.makeText(LoginActivity.this, "Password field is required!", Toast.LENGTH_LONG).show();

                    YoYo.with(Techniques.Shake)
                            .duration(100)
                            .repeat(1)
                            .playOn(edpassword);
                } else {
                    String userName = edUsername.getText().toString().trim();
                    String password = edpassword.getText().toString().trim();

                    new PostClass_Login(LoginActivity.this, userName, password, displayFirebaseRegId()).execute();

                }
            }
        });

        //Initializing our broadcast receiver
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            //When the broadcast received
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    protected void hideKeyboard(View view)
    {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //Registering receiver on activity resume
    @Override
    protected void onResume() {
        super.onResume();
        Log.w("Login Activity", "onResume");
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }


    //Unregistering receiver on activity paused
    @Override
    protected void onPause() {
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        Log.w("Login Activity", "onPause");
        super.onPause();
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        device_token = Constant.nullCheckFunction(regId);
        Log.e("Login Activity", "Firebase reg id=======" + regId);

        return regId;
    }

    class PostClass_Login extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String Username = "";
        String Password = "";
        String Device_token;

        public PostClass_Login(Context c, String username, String password, String device_token) {
            this.context = c;
            this.Username = username;
            this.Password = password;
            this.Device_token = device_token;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

           /* ProgressBar progressBar = (ProgressBar)findViewById(R.id.spin_kit);
            Wave wave = new Wave();
            progressBar.setIndeterminateDrawable(wave);*/

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    Log.d("username", "=======login==========" + Username);
                    Log.d("password", "=================" + Password);
                    Log.d("Device_token", "=================" + Device_token);
                    OkHttpClient client = new OkHttpClient();

                    RequestBody formBody = new FormEncodingBuilder()
                            .add("username", Username)
                            .add("password", Password)
                            .add("gcm", Device_token)
                            .build();
                    Request request = new Request.Builder()
                            .url(loginUrl)
                            .post(formBody)
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .addHeader("page_module", MODULE_COMMON)
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======loginUrl=========" + loginUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            String token = jObject.getString("token").trim();
                            JSONObject jsonData = jObject.getJSONObject("data");
                            String USER_TYPE = jsonData.getString("user_type").trim();
                            String IMAGE = jsonData.getString("image").trim();
                            String NAME = jsonData.getString("name").trim();
                            String USER_NAME = jsonData.getString("user_name").trim();
                            String EMAIL = jsonData.getString("email").trim();
                            Log.d("===Email====", "-------------" + EMAIL);
                            String CITY = jsonData.getString("city").trim();
                            String STATE = jsonData.getString("state").trim();
                            String ADDRESS = jsonData.getString("address").trim();
                            String PERSON_ID = jsonData.getString("customer_id").trim();
                            String EMPLOYEE_ID = jsonData.getString("employee_id").trim();
                            String SUPPLIER_ID = jsonData.getString("supplier_id").trim();
                            String STATUS = jsonData.getString("status").trim();
                            String EXPIRE_FROM_DATE = jsonData.getString("expire_from_date").trim();
                            String set_login_expire = jsonData.getString("set_login_expire").trim();
                            String EXPIRE_TO_DATE = jsonData.getString("expire_to_date").trim();

                            StorePrefs.setDefaults(PREF_MITRA_STATE, nullCheckFunction(jsonData.getString("state").trim()), context);
                            StorePrefs.setDefaults(PREF_MITRA_DISTRICT, jsonData.getString("district").trim(), context);
                            StorePrefs.setDefaults(PREF_MITRA_TALUKA, jsonData.getString("taluka").trim(), context);
                            StorePrefs.setDefaults(PREF_MITRA_CITY, jsonData.getString("city").trim(), context);
                            StorePrefs.setDefaults(PREF_MITRA_TYPE, jsonData.getString("mitra_type").trim(), context);



                       /*     String Login = "yes";

                            StorePrefs.setDefaults("LoggedIn", Login, context);*/
                            StorePrefs.setDefaults("token", token, context);
                            StorePrefs.setDefaults("USER_TYPE", USER_TYPE, context);
                            StorePrefs.setDefaults(PREF_USER_TYPE, USER_TYPE, context);
                            StorePrefs.setDefaults(PREF_CUSTOMER_ID, jsonData.getString("customer_id").trim(), context);
                            StorePrefs.setDefaults(PREF_MITRA_ID, jsonData.getString("mitra_id").trim(), context);
                            StorePrefs.setDefaults(PREF_EMPLOYEE_ID, jsonData.getString("employee_id").trim(), context);
                            StorePrefs.setDefaults(PREF_ROLE_TYPE, jsonData.getString("role").trim(), context);
                            StorePrefs.setDefaults("IMAGE", IMAGE, context);
                            StorePrefs.setDefaults("NAME", NAME, context);
                            StorePrefs.setDefaults("USER_NAME", USER_NAME, context);
                            StorePrefs.setDefaults("EMAIL", EMAIL, context);
                            StorePrefs.setDefaults("CITY", CITY, context);
                            StorePrefs.setDefaults("STATE", STATE, context);
                            StorePrefs.setDefaults("ADDRESS", ADDRESS, context);
                            StorePrefs.setDefaults("PERSON_ID", PERSON_ID, context);
                            StorePrefs.setDefaults("EMPLOYEE_ID", EMPLOYEE_ID, context);
                            StorePrefs.setDefaults("SUPPLIER_ID", SUPPLIER_ID, context);
                            StorePrefs.setDefaults("STATUS", STATUS, context);
                            StorePrefs.setDefaults("EXPIRE_FROM_DATE", EXPIRE_FROM_DATE, context);
                            StorePrefs.setDefaults("SET_LOGIN_EXPIRE", set_login_expire, context);
                            StorePrefs.setDefaults("EXPIRE_TO_DATE", EXPIRE_TO_DATE, context);



                            Gson gson = new Gson();
                            String json = gson.toJson(jsonData.getJSONArray("groupmoduls"));
                            StorePrefs.setDefaults("userPermissions", json, context);

                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                if (checkbox.isChecked()) {
                    StorePrefs.setDefaults("LoggedIn", "yes", getApplicationContext());
                }
                Toast.makeText(context, "Successfully login", Toast.LENGTH_LONG).show();
                Intent i = new Intent(context, MainActivity.class);
                startActivity(i);
                finish();

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
}