package com.shreecement.shree.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;
import android.widget.Toast;

import com.shreecement.shree.R;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.UserRegistration;

/**
 * Created by Abhishek Punia on 15-03-2018.
 */


public class PostClassCreateUser extends AsyncTask<String, Void, String> {
    private  Context context;
    ProgressDialog progress;
    String jsonreplyMsg;
    JSONObject jObject;

    String state, district, city, full_name, mitra_type_code, middle_names, taluka, last_name, first_name, password, title, user_name,email_address, user_type;
    String genderCode,type_date,address1,postal;

    public PostClassCreateUser(Context context, String state, String district, String city, String full_name, String mitra_type_code, String middle_names, String taluka, String last_name, String first_name, String password, String title, String user_name, String email_address, String user_type, String genderCode, String type_date, String address1, String postal) {
        this.context = context;
        this.state = state;
        this.district = district;
        this.city = city;
        this.full_name = full_name;

        this.mitra_type_code = mitra_type_code;
        this.middle_names = middle_names;
        this.taluka = taluka;
        this.last_name = last_name;

        this.first_name = first_name;
        this.password = password;
        this.title = title;
        this.user_name = user_name;
        this.email_address = email_address;
        this.user_type = user_type;

        this.genderCode = genderCode;
        this.type_date = type_date;
        this.address1 = address1;
        this.postal = postal;
    }



    protected void onPreExecute() {

        progress = ProgressDialog.show(context, null, null);
        progress.setTitle("Loading...");
        Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
        d.setAlpha(200);
        progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        progress.getWindow().setBackgroundDrawable(d);
        progress.setContentView(R.layout.progress_dialog);
        progress.show();

    }

    @Override
    protected String doInBackground(String... params) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnected()) {
            try {
                OkHttpClient client = new OkHttpClient();
                MediaType mediaType = MediaType.parse("application/json");
                JSONObject  jsonObject = new JSONObject();

                jsonObject.put("state", state);
                jsonObject.put("district", district);
                jsonObject.put("city", city);
                jsonObject.put("full_name", full_name);
                // jsonObject.put("page_module", MODULE_COMMON);
                jsonObject.put("mitra_type_code", mitra_type_code);
                jsonObject.put("middle_names", middle_names);
                jsonObject.put("taluka", taluka);
                jsonObject.put("last_name", last_name);
                jsonObject.put("first_name", first_name);
                jsonObject.put("password", password);
                jsonObject.put("title", title);
                jsonObject.put("user_name", user_name);
                jsonObject.put("mobile_number", user_name);
                jsonObject.put("email_address", email_address);
                jsonObject.put("user_type", user_type);

                jsonObject.put("date_of_birth", type_date);
                jsonObject.put("sex_code", genderCode);
                jsonObject.put("address_line1",address1);
                jsonObject.put("postal_code",postal);


                jsonObject.put("registration_source", "S");
                jsonObject.put("mobile_number_verified_flag", "Y");
                jsonObject.put("status", "N");

                RequestBody body = RequestBody.create(mediaType,jsonObject.toString());
                System.out.println("jsonObject.toString()===============" + jsonObject.toString());
                Request request = new Request.Builder()
                        .url(UserRegistration)
                        .post(body)
                        .addHeader("content-type", "application/json")
                        .addHeader("page_module", MODULE_COMMON)
                        //  .addHeader("token",  StorePrefs.getDefaults("token", context))
                        .build();
                Response response = client.newCall(request).execute();
                String reqBody = response.body().string();
                System.out.println("response===============" + reqBody);
                response.message();

                try {
                    jObject = new JSONObject(reqBody);

                    if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                        jsonreplyMsg = jObject.getString("replyMsg");
                        return jObject.getString("replyCode");

                    } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                        return jObject.getString("replyMsg");
                    }
                } catch (JSONException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }

            } catch (IOException e) {
                progress.dismiss();
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    progress.dismiss();
                    Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            });
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progress.dismiss();
        if (s == null) {
            Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
            return;
        } else if (s.equals("success")) {
            android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(
                    context);
            alert.setTitle(jsonreplyMsg);
            alert.setCancelable(false);
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent= new Intent(context, LoginActivity.class);
                    context.startActivity(intent);
                    // ((Activity)context).finish();
                    if(context instanceof Activity){
                        ((Activity)context).finish(); }

                }
            });

            alert.show();

        } else {
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

        }

    }
}
