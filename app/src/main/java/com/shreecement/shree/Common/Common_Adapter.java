package com.shreecement.shree.Common;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.FragmentName;


/**
 * Created by TIA on 12-08-2016.
 */

public class Common_Adapter extends RecyclerView.Adapter<Common_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanCommon> rowItems;

    public Common_Adapter(Context context, ArrayList<BeanCommon> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_common_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        BeanCommon beanCommon = rowItems.get(position);
        JSONObject jsonObject = beanCommon.getJsonObject();

        holder.llStarIcon.setVisibility(View.GONE);

        if (FragmentName.equalsIgnoreCase("ratings")){
            holder.llStarIcon.setVisibility(View.VISIBLE);
            try {
                holder.tvHeading.setText(Constant.nullCheckFunction(jsonObject.getString("title")));
                holder.tvDesc1.setText(jsonObject.getString("rating_number"));
                holder.tvDesc2.setText(jsonObject.getString("long_description"));

                String ratingNumber = jsonObject.getString("rating_number");
                if (ratingNumber.equalsIgnoreCase("1")){
                    holder.btnStar1.setVisibility(View.VISIBLE);
                    holder.btnStar2.setVisibility(View.GONE);
                    holder.btnStar3.setVisibility(View.GONE);
                    holder.btnStar4.setVisibility(View.GONE);
                    holder.btnStar5.setVisibility(View.GONE);
                }

                if (ratingNumber.equalsIgnoreCase("2")){
                    holder.btnStar1.setVisibility(View.VISIBLE);
                    holder.btnStar2.setVisibility(View.VISIBLE);
                    holder.btnStar3.setVisibility(View.GONE);
                    holder.btnStar4.setVisibility(View.GONE);
                    holder.btnStar5.setVisibility(View.GONE);
                }

                if (ratingNumber.equalsIgnoreCase("3")){
                    holder.btnStar1.setVisibility(View.VISIBLE);
                    holder.btnStar2.setVisibility(View.VISIBLE);
                    holder.btnStar3.setVisibility(View.VISIBLE);
                    holder.btnStar4.setVisibility(View.GONE);
                    holder.btnStar5.setVisibility(View.GONE);
                }

                if (ratingNumber.equalsIgnoreCase("4")){
                    holder.btnStar1.setVisibility(View.VISIBLE);
                    holder.btnStar2.setVisibility(View.VISIBLE);
                    holder.btnStar3.setVisibility(View.VISIBLE);
                    holder.btnStar4.setVisibility(View.VISIBLE);
                    holder.btnStar5.setVisibility(View.GONE);
                }

                if (ratingNumber.equalsIgnoreCase("5")){
                    holder.btnStar1.setVisibility(View.VISIBLE);
                    holder.btnStar2.setVisibility(View.VISIBLE);
                    holder.btnStar3.setVisibility(View.VISIBLE);
                    holder.btnStar4.setVisibility(View.VISIBLE);
                    holder.btnStar5.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /*try {
            holder.tvHeading.setText(jsonObject.getString("mobile_number"));
            holder.tvDesc1.setText(jsonObject.getString("referral_name"));
            holder.tvDesc2.setText(jsonObject.getString("comments"));
            holder.tvFirst.setText(jsonObject.getString("referral_name").substring(0,1));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        holder.tvFirst.setBackgroundResource(R.drawable.rounder_date_red_image);

    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public void add(List<BeanCommon> items) {
        int previousDataSize = this.rowItems.size();
        this.rowItems.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvFirst, tvHeading, tvDesc1 ,tvDesc2;
        LinearLayout llStarIcon;
        Button btnStar1,btnStar2,btnStar3,btnStar4,btnStar5;

        ViewHolder(View convertView) {
            super(convertView);
            convertView.setOnClickListener(this);

            tvFirst = (TextView) convertView.findViewById(R.id.tvFirst);
            tvHeading = (TextView) convertView.findViewById(R.id.tvHeading);
            tvDesc1 = (TextView) convertView.findViewById(R.id.tvDesc1);
            tvDesc2 = (TextView) convertView.findViewById(R.id.tvDesc2);
            llStarIcon = (LinearLayout) convertView.findViewById(R.id.llStarIcon);

            btnStar1 = (Button) convertView.findViewById(R.id.btnStar1);
            btnStar2 = (Button) convertView.findViewById(R.id.btnStar2);
            btnStar3 = (Button) convertView.findViewById(R.id.btnStar3);
            btnStar4 = (Button) convertView.findViewById(R.id.btnStar4);
            btnStar5 = (Button) convertView.findViewById(R.id.btnStar5);

        }

        @Override
        public void onClick(View v) {



            /*BeanMitra beanMitra_item = rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanMitra_item);
            StorePrefs.setDefaults(PREF_BEAN_MITRA_ITEM, json, context);

            Fragment_Mitra_Details fragment = new Fragment_Mitra_Details();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            ((MainActivity) context).showUpButton(true);*/


        }


    }
}
