package com.shreecement.shree.Common;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.shreecement.shree.Utlility.Constant;

public class HeadersClass {



    public static GlideUrl getUrlWithHeaders(String url){
        return new GlideUrl(url, new LazyHeaders.Builder()
                .addHeader("token", Constant.token_from_server)
                .build());
    }
}
