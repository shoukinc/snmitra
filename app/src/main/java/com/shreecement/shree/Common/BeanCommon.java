package com.shreecement.shree.Common;


import org.json.JSONObject;

public class BeanCommon {

    String id;

   JSONObject jsonObject;

   String name ;


    public BeanCommon() {
    }

    public BeanCommon(String id, JSONObject jsonObject) {
        this.id = id;
        this.jsonObject = jsonObject;
    }

    public BeanCommon(String id, JSONObject jsonObject, String name) {
        this.id = id;
        this.jsonObject = jsonObject;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
