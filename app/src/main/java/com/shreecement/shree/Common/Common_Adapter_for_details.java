package com.shreecement.shree.Common;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by TIA on 12-08-2016.
 */

public class Common_Adapter_for_details extends RecyclerView.Adapter<Common_Adapter_for_details.ViewHolder> {
    Context context;
    ArrayList<LookupCategory> rowItems;

    public Common_Adapter_for_details(Context context, ArrayList<LookupCategory> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_common_details_row, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        LookupCategory category = rowItems.get(position);

                holder.tvHeading.setText(category.getLOOKUP_TYPE());
                holder.tvDesc.setText(category.getDESCRIPTION());

       /*  if (Fragment_Mitra_Details.PageValue.equalsIgnoreCase("edit")) {

             holder.BtnRowCLick.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {

                     if (holder.tvHeading.getText().toString().equalsIgnoreCase("Full Name") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Email Address") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Mobile Number") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Gender") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Blood Type") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Date Of Birth") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Mitra Number") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Aadhaar Number") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Voter Id") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("PAN Card") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();

                     }else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Driving Licence") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();

                     }else if (holder.tvHeading.getText().toString().equalsIgnoreCase("GST Number") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Marital Status") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Spouse Name") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Anniversary") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Child 1 Name") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Child 1 Date Of Birth") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Child 2 Name") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Child 2 Date Of Birth") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Address Line 1") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Address Line 2") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Address Line 3") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("State") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("District") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Taluka") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("City") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }
                     else if (holder.tvHeading.getText().toString().equalsIgnoreCase("Postal") ) {
                         Toast.makeText(context, "" + holder.tvHeading.getText().toString(), Toast.LENGTH_SHORT).show();
                     }


                     else {
                          Toast.makeText(context, "did not match" , Toast.LENGTH_SHORT).show();
                     }
                 }
             });
         }else {
             Toast.makeText(context, "View Page" , Toast.LENGTH_SHORT).show();
         }*/

    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public void add(List<LookupCategory> items) {
        int previousDataSize = this.rowItems.size();
        this.rowItems.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvHeading, tvDesc;
        Button BtnRowCLick;

        ViewHolder(View convertView) {
            super(convertView);
            convertView.setOnClickListener(this);

            tvHeading = (TextView) convertView.findViewById(R.id.tvHeading);
            tvDesc = (TextView) convertView.findViewById(R.id.tvDesc);
            BtnRowCLick = (Button) convertView.findViewById(R.id.BtnRowCLick);


        }

        @Override
        public void onClick(View v) {
           // int nt=v.getId();
           // LookupCategory category = rowItems.get(nt);
           // Toast.makeText(context, "" + tvDesc.getText().toString(), Toast.LENGTH_SHORT).show();
        }


    }
}
