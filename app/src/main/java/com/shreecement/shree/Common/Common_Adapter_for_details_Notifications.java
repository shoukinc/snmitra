package com.shreecement.shree.Common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.StatusAccept;
import static com.shreecement.shree.Utlility.Constant.StatusReject;
import static com.shreecement.shree.Utlility.Constant.UpdateMaterialTransactionStatus;
import static com.shreecement.shree.Utlility.Constant.UpdateSourceStatus;


/**
 * Created by TIA on 12-08-2016.
 */

public class Common_Adapter_for_details_Notifications extends RecyclerView.Adapter<Common_Adapter_for_details_Notifications.ViewHolder> {
    Context context;
    ArrayList<LookupCategory> rowItems;
    int details = 0;
    int action = 1;
    String source_type = "";
    String source_id = "";

    public Common_Adapter_for_details_Notifications(Context context, ArrayList<LookupCategory> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    public void setData(String sourceId, String sourceType){
            source_id = sourceId;
            source_type = sourceType;
    }

    @Override
    public int getItemViewType(int position) {

        LookupCategory lookupCategory = rowItems.get(position);
        if (position == rowItems.size() -1  && lookupCategory.getLOOKUP_TYPE().equalsIgnoreCase("action")
                && !lookupCategory.getDESCRIPTION().equalsIgnoreCase("Approved")) {
            return action;
        } else {
            return details;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == action) {
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_common_buton_row, parent, false));
        }
        else  {
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_common_details_row, parent, false));
        }
     }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        LookupCategory lookupCategory = rowItems.get(position);

        if (position == rowItems.size() -1  && lookupCategory.getLOOKUP_TYPE().equalsIgnoreCase("action")
                && !lookupCategory.getDESCRIPTION().equalsIgnoreCase("Approved")) {

            holder.btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                      //Toast.makeText(context, ""+source_id , Toast.LENGTH_SHORT).show();

                    if (source_type.equalsIgnoreCase("ORDER")) {
                       // new PostClassUpdateStatus(context,source_id,source_type,StatusAccept,MODULE_ORDERS,UpdateSourceStatus).execute();
                        new PostClassUpdateStatus(context,source_id,source_type,StatusAccept,MODULE_COMMON,UpdateSourceStatus).execute();
                        Toast.makeText(context, ""+source_type , Toast.LENGTH_SHORT).show();

                    } else if (source_type.equalsIgnoreCase("RECEIPT")) {
                        //new PostClassUpdateStatus(context,source_id,source_type,StatusAccept,MODULE_RECEIPTS,UpdateSourceStatus).execute();
                        new PostClassUpdateStatus(context,source_id,source_type,StatusAccept,MODULE_COMMON,UpdateSourceStatus).execute();
                        Toast.makeText(context, ""+source_type , Toast.LENGTH_SHORT).show();


                    } else if (source_type.equalsIgnoreCase("MATERIAL_TRANSACTION")) {
                       // new PostClassUpdateStatus(context,source_id,source_type,StatusAccept,MODULE_MATERIALTRANSACTIONS,UpdateMaterialTransactionStatus).execute();
                        new PostClassUpdateStatus(context,source_id,source_type,StatusAccept,MODULE_COMMON,UpdateMaterialTransactionStatus).execute();
                      //  Toast.makeText(context, ""+source_type , Toast.LENGTH_SHORT).show();

                    }
                    else {
                       // Toast.makeText(context, ""+source_id , Toast.LENGTH_SHORT).show();
                    }
                }

            });

            holder.btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                      Toast.makeText(context, ""+source_id , Toast.LENGTH_SHORT).show();
                    //   Toast.makeText(context, ""+source_type , Toast.LENGTH_SHORT).show();

                    if (source_type.equalsIgnoreCase("ORDER")) {
                        //new PostClassUpdateStatus(context,source_id,source_type,StatusReject,MODULE_ORDERS,UpdateSourceStatus).execute();
                        new PostClassUpdateStatus(context,source_id,source_type,StatusReject,MODULE_COMMON,UpdateSourceStatus).execute();

                    } else if (source_type.equalsIgnoreCase("RECEIPT")) {
                        //new PostClassUpdateStatus(context,source_id,source_type,StatusReject,MODULE_RECEIPTS,UpdateSourceStatus).execute();
                        new PostClassUpdateStatus(context,source_id,source_type,StatusReject,MODULE_COMMON,UpdateSourceStatus).execute();


                    } else if (source_type.equalsIgnoreCase("MATERIAL_TRANSACTION")) {
                        //new PostClassUpdateStatus(context,source_id,source_type,StatusReject,MODULE_MATERIALTRANSACTIONS,UpdateMaterialTransactionStatus).execute();
                        new PostClassUpdateStatus(context,source_id,source_type,StatusReject,MODULE_COMMON,UpdateMaterialTransactionStatus).execute();

                    }
                }

            });

        }else {
            holder.tvHeading.setText(lookupCategory.getLOOKUP_TYPE());
            holder.tvDesc.setText(lookupCategory.getDESCRIPTION());
        }
    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public void add(List<LookupCategory> items) {
        int previousDataSize = this.rowItems.size();
        this.rowItems.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvHeading, tvDesc;
        Button btnReject, btnAccept;

        ViewHolder(View convertView) {
            super(convertView);
            convertView.setOnClickListener(this);

            tvHeading = (TextView) convertView.findViewById(R.id.tvHeading);
            tvDesc = (TextView) convertView.findViewById(R.id.tvDesc);

            btnReject  = (Button) convertView.findViewById(R.id.btnReject);
            btnAccept  = (Button) convertView.findViewById(R.id.btnAccept);


        }

        @Override
        public void onClick(View v) {


        }


    }

    public class PostClassUpdateStatus extends AsyncTask<String, Void, String> {
        private  Context context;
        ProgressDialog progress;
        String jsonreplyMsg;
        JSONObject jObject;

       String source_id,source_type,status, page_module,page_url;

        public PostClassUpdateStatus(Context context,String source_id, String source_type, String status, String page_module, String page_url) {
            this.context = context;
            this.source_id = source_id;
            this.source_type = source_type;
            this.status = status;
            this.page_module = page_module;
            this.page_url = page_url;

        }


        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject  jsonObject = new JSONObject();
                    //jsonObject.put("page_module", page_module);

                    jsonObject.put("source_id",source_id);
                    jsonObject.put("source_type",source_type);
                    jsonObject.put("status",status);

                    RequestBody body = RequestBody.create(mediaType,jsonObject.toString());
                    System.out.println("jsonObject.toString()===============" + jsonObject.toString());
                    Request request = new Request.Builder()
                            .url(page_url)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", page_module)
                           // .addHeader("action", ACTION_EDIT)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))

                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    System.out.println("page_url===============" + page_url);
                    response.message();

                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(
                        context);
                alert.setTitle(jsonreplyMsg);
                alert.setCancelable(false);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    /*Fragment fragment = new Fragment_Mitras();
                    FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_left);
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();*/
                        ((Activity)context).onBackPressed();
                    }
                });

                alert.show();

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

}
