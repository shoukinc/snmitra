package com.shreecement.shree.PostClass;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;
import android.widget.Toast;

import com.shreecement.shree.Fragment.Fragment_Receipts;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.ACTION_ADD;
import static com.shreecement.shree.Utlility.Constant.ACTION_EDIT;
import static com.shreecement.shree.Utlility.Constant.Create_receipt;
import static com.shreecement.shree.Utlility.Constant.MODULE_RECEIPTS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.Update_receipt;

/**
 * Created by Abhishek Punia on 15-03-2018.
 */


 public class PostClassAddMaterialrecepit extends AsyncTask<String, Void, String> {
    private  Context context;
    ProgressDialog progress;
    String jsonreplyMsg ;
    JSONObject jObject;
    String Organization_id;
    String Delivery_id;
    String Transporter;
    String Di_quantity;
    String Truck_no;
    String Rec_qunatity;
    String Packing_type;
    String Short_qty;
    String Egp_date;
    String Cut_qty;
    String Product;
    String Damage_qty;
    String Bag_type;
    String Total_quantity;
    String Egp_no ;
    String Receipt_date;
    String addEdit;

    public PostClassAddMaterialrecepit(Context context, String addEdit, String organization_id, String delivery_id,
                                       String transporter, String di_quantity, String truck_no,
                                       String rec_qunatity, String packing_type, String short_qty,
                                       String egp_date, String cut_qty, String product, String damage_qty,
                                       String bag_type, String total_quantity, String egp_no,
                                       String receipt_date) {
        this.context = context;
        this.addEdit = addEdit;
        Organization_id = organization_id;
        Delivery_id = delivery_id;
        Transporter = transporter;
        Di_quantity = di_quantity;
        Truck_no = truck_no;
        Rec_qunatity = rec_qunatity;
        Packing_type = packing_type;
        Short_qty = short_qty;
        Egp_date = egp_date;
        Cut_qty = cut_qty;
        Product = product;
        Damage_qty = damage_qty;
        Bag_type = bag_type;
        Total_quantity = total_quantity;
        Egp_no = egp_no;
        Receipt_date = receipt_date;
    }

    protected void onPreExecute() {

        progress = ProgressDialog.show(context, null, null);
        progress.setTitle("Loading...");
        Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
        d.setAlpha(200);
        progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        progress.getWindow().setBackgroundDrawable(d);
        progress.setContentView(R.layout.progress_dialog);
        progress.show();

    }

    @Override
    protected String doInBackground(String... params) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnected()) {
            try {
                OkHttpClient client = new OkHttpClient();
                MediaType mediaType = MediaType.parse("application/json");
                JSONObject  jsonObject = new JSONObject();
                jsonObject.put("organization_id",Organization_id);
                jsonObject.put("delivery_id",Delivery_id);
                jsonObject.put("transporter",Transporter);
                jsonObject.put("di_quantity",Di_quantity);
                jsonObject.put("truck_no",Truck_no);
                jsonObject.put("rec_qunatity",Rec_qunatity);
                jsonObject.put("packing_type",Packing_type);
                jsonObject.put("short_qty",Short_qty);
                jsonObject.put("egp_date",Egp_date);
                jsonObject.put("cut_qty",Cut_qty);
                jsonObject.put("product",Product);
                jsonObject.put("damage_qty",Damage_qty);
                jsonObject.put("bag_type",Bag_type);
                jsonObject.put("total_quantity",Total_quantity);
                jsonObject.put("egp_no",Egp_no);
                jsonObject.put("receipt_date",Receipt_date);

                RequestBody body = RequestBody.create(mediaType,jsonObject.toString());

                Request request = new Request.Builder()
                        .url(addEdit.equals("edit")? Update_receipt:Create_receipt)
                        .post(body)
                        .addHeader("content-type", "application/json")
                        .addHeader("token",  StorePrefs.getDefaults("token", context))
                        .addHeader("page_module", MODULE_RECEIPTS)
                        .addHeader("action",  addEdit.equals("edit") ? ACTION_EDIT : ACTION_ADD)
                        .build();
                Response response = client.newCall(request).execute();
                String reqBody = response.body().string();
                System.out.println("response===============" + reqBody);
                response.message();

                    System.out.println("======Create_receipt=========" + Create_receipt);



                try {
                    jObject = new JSONObject(reqBody);

                    if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                        jsonreplyMsg = jObject.getString("replyMsg");
                        return jObject.getString("replyCode");

                    } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                        return jObject.getString("replyMsg");
                    }
                } catch (JSONException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }

            } catch (IOException e) {
                progress.dismiss();
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    progress.dismiss();
                    Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            });
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progress.dismiss();
        if (s == null) {
            Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
            return;
        } else if (s.equals("success")) {
           // Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(
                    context);
            alert.setTitle(jsonreplyMsg);
            alert.setCancelable(false);


            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Fragment fragment = new Fragment_Receipts();
                    FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_left);
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();
                }
            });

            alert.show();

        } else {
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

        }

    }
}
