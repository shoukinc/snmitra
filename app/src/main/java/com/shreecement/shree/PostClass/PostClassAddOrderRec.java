package com.shreecement.shree.PostClass;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.ACTION_ADD;
import static com.shreecement.shree.Utlility.Constant.ACTION_EDIT;
import static com.shreecement.shree.Utlility.Constant.CreateOrder;
import static com.shreecement.shree.Utlility.Constant.MODULE_ORDERS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.UpdateOrder;

/**
 * Created by Abhishek Punia on 15-03-2018.
 */


 public class PostClassAddOrderRec extends AsyncTask<String, Void, String> {
    private  Context context;
    ProgressDialog progress;
    String jsonreplyMsg;
    JSONObject jObject;
    String OrderId;
    String Category;
    String Brand;
    String User_type;
    String Customer;
    String Consignee;
    String Product;
    String Packing;
    String Quantity;
    String Mm_name;
    String Vendor_id;
    String Rate;
    String Validity;
    String Dispatch_from;
    String Remark ;
    String Address;
    String City;
    String State;
    String District;

    public PostClassAddOrderRec(Context context,String orderId, String category, String brand, String user_type, String customer,
                                String consignee, String product, String packing, String quantity,
                                String mm_name, String vendor_id, String rate, String validity, String dispatch_from,String remark
                                ,String address, String city, String state, String district) {
        this.context = context;
        this.OrderId = orderId;
        this.Category = category;
        this.Brand = brand;
        this.User_type = user_type;
        this.Customer = customer;
        this.Consignee = consignee;
        this.Product = product;
        this.Packing = packing;
        this.Quantity = quantity;
        this.Mm_name = mm_name;
        this.Vendor_id = vendor_id;
        this.Rate = rate;
        this.Validity = validity;
        this.Dispatch_from = dispatch_from;
        this.Remark = remark;
        this.Address = address;
        this.City = city;
        this.State = state;
        this.District = district;

        Log.d("======OrderId=======",""+OrderId);
        Log.d("======Category=======",""+Category);
        Log.d("======brand=======",""+Brand);
        Log.d("======User_type=======",""+User_type);
        Log.d("======Customer=======",""+Customer);
        Log.d("======Consignee=======",""+Consignee);
        Log.d("======Product=======",""+Product);
        Log.d("======Packing=======",""+Packing);
        Log.d("======Quantity=======",""+Quantity);
        Log.d("======Mm_name=======",""+Mm_name);
        Log.d("======Vendor_id=======",""+Vendor_id);
        Log.d("======Rate=======",""+Rate);
        Log.d("======Validity=======",""+Validity);
        Log.d("====Dispatch_from=====",""+Dispatch_from);
        Log.d("======Remark=======",""+Remark);
        Log.d("======Address=======",""+Address);
        Log.d("======City=======",""+City);
        Log.d("======State=======",""+State);
        Log.d("======District=======",""+District);

    }


    protected void onPreExecute() {

        progress = ProgressDialog.show(context, null, null);
        progress.setTitle("Loading...");
        Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
        d.setAlpha(200);
        progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        progress.getWindow().setBackgroundDrawable(d);
        progress.setContentView(R.layout.progress_dialog);
        progress.show();

    }

    @Override
    protected String doInBackground(String... params) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnected()) {
            try {
                OkHttpClient client = new OkHttpClient();
                MediaType mediaType = MediaType.parse("application/json");
                JSONObject  jsonObject = new JSONObject();
                jsonObject.put("order_id",OrderId);
                jsonObject.put("category",Category);
                jsonObject.put("brand",Brand);
                jsonObject.put("user_type",User_type);
                jsonObject.put("customer",Customer);
                jsonObject.put("consignee",Consignee);
                jsonObject.put("product",Product);
                jsonObject.put("packing",Packing);
                jsonObject.put("quantity",Quantity);
                jsonObject.put("mm_name",Mm_name);
                jsonObject.put("vendor_id",Vendor_id);
                jsonObject.put("rate",Rate);
                jsonObject.put("validity",Validity);
                jsonObject.put("dispatch_from",Dispatch_from);
                jsonObject.put("remark",Remark);
                jsonObject.put("address",Address);
                jsonObject.put("city",City);
                jsonObject.put("state",State);
                jsonObject.put("district",District);
                RequestBody body = RequestBody.create(mediaType,jsonObject.toString());

                Request request = new Request.Builder()
                        .url(OrderId.equals("") ? CreateOrder : UpdateOrder)
                        .post(body)
                        .addHeader("content-type", "application/json")
                        .addHeader("token",  StorePrefs.getDefaults("token", context))
                        .addHeader("action",  OrderId.equals("") ? ACTION_ADD : ACTION_EDIT)
                        .addHeader("page_module", MODULE_ORDERS)
                        .build();
                Response response = client.newCall(request).execute();
                String reqBody = response.body().string();
                System.out.println("response===============" + reqBody);
                response.message();
                if (OrderId.equals("")){
                    System.out.println("======CreateOrder=========" + CreateOrder);
                }
                else {
                    System.out.println("======UpdateOrder=========" + UpdateOrder);
                }


                try {
                    jObject = new JSONObject(reqBody);

                    if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                        jsonreplyMsg = jObject.getString("replyMsg");
                        return jObject.getString("replyCode");

                    } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                        return jObject.getString("replyMsg");
                    }
                } catch (JSONException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }

            } catch (IOException e) {
                progress.dismiss();
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    progress.dismiss();
                    Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            });
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progress.dismiss();
        if (s == null) {
            Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
            return;
        } else if (s.equals("success")) {
            android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(
                    context);
            alert.setTitle(jsonreplyMsg);
            alert.setCancelable(false);
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    /*Fragment fragment = new Fragment_Orders();
                    FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_left);
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();*/
                    ((Activity)context).onBackPressed();
                }
            });

            alert.show();








        } else {
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

        }

    }
}
