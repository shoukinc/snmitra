package com.shreecement.shree.Notifcation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.shreecement.shree.Activity.LoginActivity;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.R;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wakeel on 8/1/17.
 */


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    String title = "";
    String message = "";

    private NotificationUtils notificationUtils;
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog builder3;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

            title = remoteMessage.getNotification().getTitle();
            message = remoteMessage.getNotification().getBody();
            Log.e(TAG, "Notification Title : " + title);
            Log.e(TAG, "Notification Body : " + message);

            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);

        // play notification sound
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.playNotificationSound();


        String type = "";
        String source_type = "";
        String imageUrl = "";


        try {
            JSONObject messageType = new JSONObject(intent.getExtras().getString("PATH"));
            type = messageType.getString("type");
            source_type = messageType.getString("source_type");
            imageUrl = messageType.getString("image_url");


            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date resultdate = new Date(System.currentTimeMillis());
            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);

            String timestamp = String.valueOf(format.format(resultdate));

            // check for image attachment
            if (TextUtils.isEmpty(imageUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

            Handler handler1= new Handler(Looper.getMainLooper());
            handler1.post(new Runnable() {
                @Override
                public void run() {


                    try {
                        @SuppressLint("RestrictedApi") android.support.v7.app.AlertDialog.Builder dialog =
                                new AlertDialog.Builder(new ContextThemeWrapper(getApplicationContext(), R.style.myAlertDialog));
                        dialog.setTitle(title);
                        dialog.setMessage(message);
                        dialog.setCancelable(true);
                        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                /* //    new PostClassClearUserSession(getApplicationContext()).execute();
                            Intent intent = new Intent((getApplicationContext()), LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getApplicationContext().startActivity(intent);*/
                            }
                        });
                        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                     /*  new PostClassClearUserSession(getApplicationContext()).execute();
                            Intent intent = new Intent((getApplicationContext()), LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getApplicationContext().startActivity(intent);
                           dialog.dismiss();*/
                            }
                        });

                        // create alert dialog
                        AlertDialog alertDialog = dialog.create();
                        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                        // show it
                        alertDialog.show();

                    }catch (Exception e){
                        Toast.makeText(MyFirebaseMessagingService.this, "Please Grant App overlay permission in settings for notifications", Toast.LENGTH_SHORT).show();
                    }



                }
            });

            if (source_type.equals("5")) {

                // app is in background, Clear user Details
                //  new PostClassClearUserSession(getApplicationContext(), "auto").execute();

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        @SuppressLint("RestrictedApi") android.support.v7.app.AlertDialog.Builder dialog =
                                new AlertDialog.Builder(new ContextThemeWrapper(getApplicationContext(), R.style.myAlertDialog));
                        dialog.setMessage("You have been logged out due to change of credentials. Please login to resume.");
                        dialog.setCancelable(false);
                        dialog.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
//                                        new PostClassClearUserSession(getApplicationContext()).execute();
                                Intent intent = new Intent((getApplicationContext()), LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getApplicationContext().startActivity(intent);
                            }
                        });
                        // create alert dialog
                        AlertDialog alertDialog = dialog.create();
                        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                        // show it
                        alertDialog.show();
                    }
                });
            }
        }
        else {
            if (source_type.equals("5")) {
                Log.d("Type 5", "FOUND. Delete user details..................");
                // app is in background, Clear user Details
                //new PostClassClearUserSession(getApplicationContext(), "auto").execute();
            }
        }
    }

    private void handleNotification(String message) {

        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date resultdate = new Date(System.currentTimeMillis());

            String type = "";
            String source_type = "";
            String imageUrl = "";

            try {
                JSONObject messageType = json.getJSONObject("PATH");
                //type = messageType.getString("type");
                source_type = messageType.getString("source_type");
                imageUrl = messageType.getString("image_url");

                String timestamp = String.valueOf(format.format(resultdate));
                Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);

                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }



            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                if(source_type.equals("5")){
                    // app is in foreground, broadcast the push message
                   /* Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                    pushNotification.putExtra("message", message);
                    pushNotification.putExtra("type", type);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);*/
                }
                else {
                    // app is in foreground, show the notification in notification tray
                    Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                    resultIntent.putExtra("message", message);
                    resultIntent.putExtra("type", type);
                    String timestamp = String.valueOf(format.format(resultdate));

                    // check for image attachment
                    if (TextUtils.isEmpty(imageUrl)) {
                        showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                    }
                }
            } else {
                if(source_type.equals("5")){
                    Log.d("Type 5", "FOUND. Delete user details..................");
                    // app is in background, Clear user Details
                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                    notificationUtils.clearUserDetails();
                }
                else {
                    Log.d("Type 5", " NOT FOUND........................");
                    // app is in background, show the notification in notification tray
                    Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                    resultIntent.putExtra("message", message);
                    resultIntent.putExtra("type", type);
                    String timestamp = String.valueOf(format.format(resultdate));

                    // check for image attachment
                    if (TextUtils.isEmpty(imageUrl)) {
                        showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                    }
                }
            }
            // app is in background, show the notification in notification tray
            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("type", type);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}