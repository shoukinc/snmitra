package com.shreecement.shree.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shreecement.shree.ModalClass.Dashboard_item;
import com.shreecement.shree.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by user on 6/29/2017.
 */

public class Dashboard_item_adapter extends RecyclerView.Adapter<Dashboard_item_adapter.MyViewHolder> {
    List<Dashboard_item> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public Dashboard_item_adapter(Context context, List<Dashboard_item> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public Dashboard_item_adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_dashboard, parent, false);
        Dashboard_item_adapter.MyViewHolder holder = new Dashboard_item_adapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Dashboard_item_adapter.MyViewHolder holder, int position) {
        Dashboard_item current = data.get(position);

        holder.title.setText(current.getTitle());
        if (position == 0)
            holder.navImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_customer));
        if (position == 1)
            holder.navImage.setImageDrawable(context.getResources().getDrawable(R.drawable.material));
        if (position == 2)
            holder.navImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_orders));
        if (position == 3)
            holder.navImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_invoice));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView navImage;

        public MyViewHolder(View itemView) {
            super(itemView);

        }
    }
}
