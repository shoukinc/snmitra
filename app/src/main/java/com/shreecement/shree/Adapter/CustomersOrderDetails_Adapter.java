package com.shreecement.shree.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shreecement.shree.ModalClass.BeanCustomer_OrderItem;
import com.shreecement.shree.R;

import java.util.ArrayList;


/**
 * Created by TIA on 12-08-2016.
 */

public class CustomersOrderDetails_Adapter extends RecyclerView.Adapter<CustomersOrderDetails_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanCustomer_OrderItem> rowItems;

    public CustomersOrderDetails_Adapter(Context context, ArrayList<BeanCustomer_OrderItem> rowItem) {
        this.context = context;
        this.rowItems = rowItem;

}
    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    @Override
    public int getItemViewType(int position) {
                return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.fragment_customers_order_details_row_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final BeanCustomer_OrderItem rowItem = rowItems.get(position);
        Log.d("----rowItems---1-----",""+rowItems.size());
        holder.tvOrder.setText("Order : "+rowItem.getProduct());
        holder.tvPenddingQty.setText("Quantity : "+rowItem.getQuantity());
        holder.tvHoldQty.setText("Date : "+rowItem.getOrder_date());
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvOrder,tvPenddingQty,tvHoldQty;

        ViewHolder(View convertView) {
            super(convertView);
            convertView.setOnClickListener(this);
            tvPenddingQty = convertView.findViewById(R.id.tvPendingQty);
            tvOrder =convertView.findViewById(R.id.tvOrder);
            tvHoldQty = convertView.findViewById(R.id.tvHoldQuantity);



        }
        @Override
        public void onClick(View v) {
/*
            Fragment_Customers_Order_Details fragment = new Fragment_Customers_Order_Details();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();*/
        }
    }
}