package com.shreecement.shree.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.shreecement.shree.R;

import java.util.ArrayList;
import java.util.List;

import static android.view.Gravity.CENTER;

/**
 * Created by Choudhary on 7/1/2017.
 */

public class Spin_Custom_adapter extends BaseAdapter implements SpinnerAdapter {

    private final Context activity;
    private ArrayList<String> asr;
    private Spinner spinner;

    public Spin_Custom_adapter(Context context, List<String> asr, Spinner nspinner) {
        this.asr = (ArrayList<String>) asr;
        activity = context;
        spinner = nspinner;
    }

    public int getCount() {
        return asr.size();
    }

    public Object getItem(int i) {
        return asr.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);
        txt.setPadding(15, 15, 15, 15);
        txt.setGravity(Gravity.START|CENTER);
        txt.setTextColor(Color.BLACK);
        txt.setTextSize(15);
        txt.setText(asr.get(position));
        txt.setBackgroundResource(R.drawable.background_lineview_gray);
        return txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        if (spinner.getSelectedItemPosition() ==0){
            TextView txt = new TextView(activity);
            txt.setGravity(Gravity.START|Gravity.CENTER);
            txt.setPadding(18, 18, 18, 18);
            txt.setTextSize(15);
            txt.setTextColor(activity.getResources().getColor(R.color.colorDarkGray));
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt.setText(asr.get(i));
            return txt;
        }else {
            TextView txt = new TextView(activity);
            txt.setGravity(Gravity.START|Gravity.CENTER);
            txt.setPadding(18, 18, 18, 18);
            txt.setTextSize(15);
            txt.setTextColor(Color.BLACK);
            txt.setTextColor(activity.getResources().getColor(R.color.colorBlack));
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt.setText(asr.get(i));

            return txt;
        }

    }

}
