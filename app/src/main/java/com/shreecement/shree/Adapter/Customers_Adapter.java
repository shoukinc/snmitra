package com.shreecement.shree.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Fragment_Customer_Tab;
import com.shreecement.shree.ModalClass.BeanCustomer_Item;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import java.util.ArrayList;



/**
 * Created by TIA on 12-08-2016.
 */

public class Customers_Adapter extends RecyclerView.Adapter<Customers_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanCustomer_Item> rowItems;

    public Customers_Adapter(Context context, ArrayList<BeanCustomer_Item> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_customer_row_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String rowItem = rowItems.get(position).getParty_name();


       // holder.tvCustomerId.setText(rowItems.get(position).getAccount_number());
        holder.tvCustomerName.setText(rowItem+"  ("+rowItems.get(position).getAccount_number()+")");
        holder.tvCustomer.setText(rowItems.get(position).getAddress());
        String s=rowItem.substring(0,1);
        holder.tvFirst.setText(s);
        holder.tvFirst.setText(s);
        holder.tvFirst.setBackgroundResource(R.drawable.rounder_dateimage_red);

       /* holder.itemView.setTranslationX(-(50+position*100));
        holder.itemView.setAlpha(0.5f);
        holder.itemView.animate().alpha(1f).translationX(0).setDuration(700).start();*/
    }

    @Override
    public int getItemCount() {
        return rowItems.size();

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvCustomerName,tvCustomer,tvCustomerId;
        TextView tvFirst;
        ViewHolder(View convertView) {
            super(convertView);
            tvCustomer = convertView.findViewById(R.id.tvCustomer);
            tvCustomerId = convertView.findViewById(R.id.tvCustomerId);
            tvFirst = convertView.findViewById(R.id.tvFirst);
            tvCustomerName = convertView.findViewById(R.id.tvCustomerName);
            tvCustomerId.setVisibility(View.GONE);
            convertView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {

            StorePrefs.setDefaults("customer_code",rowItems.get(getPosition()).getParty_id(),context);
            BeanCustomer_Item beanCustomer_item = rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanCustomer_item);
            StorePrefs.setDefaults("beanCustomer_item", json, context);
            Fragment_Customer_Tab fragment = new Fragment_Customer_Tab();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            ((MainActivity) context).showUpButton(true);

        }
    }


}