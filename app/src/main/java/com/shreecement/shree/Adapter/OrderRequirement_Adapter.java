package com.shreecement.shree.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Fragment_Add_Order;
import com.shreecement.shree.Fragment.Fragment_Order_Requirement_TradeView;
import com.shreecement.shree.ModalClass.BeanOrderList;
import com.shreecement.shree.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;


/**
 * Created by TIA on 12-08-2016.
 */

public class OrderRequirement_Adapter extends RecyclerView.Adapter<OrderRequirement_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanOrderList> rowItems;
    String caltegory;

    public OrderRequirement_Adapter(Context context, ArrayList<BeanOrderList> rowItem) {
        this.context = context;
        this.rowItems = rowItem;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_customer_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        BeanOrderList beanOrderList = rowItems.get(position);
        holder.tvCategory.setText(beanOrderList.getCUSTOMER_NAME());

        DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
        Date date1 = null;
        Calendar cal = Calendar.getInstance();
        try {
            date1 = readFormat1.parse(rowItems.get(position).getORDER_DATE());
            String dateformatt = formatter1.format(date1);

            holder.tvOrder.setText(dateformatt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvCustomerName.setText(beanOrderList.getPRODUCT_NAME() + "  (" + beanOrderList.getORDER_ID() + ")");
        holder.tvaddOrder.setText(beanOrderList.getADDRESS());
        holder.tvaddOrder.setVisibility(View.VISIBLE);
        String first = beanOrderList.getCUSTOMER_NAME().substring(0, 1);
        holder.tvFirst.setText(first);

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

        holder.tvFirst.setBackgroundResource(R.drawable.rounder_dateimage_red);

        // Animation


    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public void add(List<BeanOrderList> items) {
        int previousDataSize = this.rowItems.size();
        this.rowItems.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvCategory, tvOrder, tvCustomerName, tvFirst, tvaddOrder;

        ViewHolder(View convertView) {
            super(convertView);
            convertView.setOnClickListener(this);

            tvCategory = convertView.findViewById(R.id.tvCustomerName);
            tvOrder = convertView.findViewById(R.id.tvCustomerId);
            tvaddOrder =convertView.findViewById(R.id.tvaddOrder);
            tvCustomerName = convertView.findViewById(R.id.tvCustomer);
            tvFirst =convertView.findViewById(R.id.tvFirst);


        }

        @Override
        public void onClick(View v) {

            final BeanOrderList beanOrderList = rowItems.get(getLayoutPosition());

            android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(
                    context);
            alert.setTitle(context.getResources().getString(R.string.order_id)+"" +beanOrderList.getORDER_ID());
            alert.setPositiveButton("Update Order", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (beanOrderList.getCATEGORY_NAME().equals("Non Trade") || beanOrderList.getCATEGORY_NAME().equals("Trade")) {
                        Fragment_Add_Order fragment = new Fragment_Add_Order().newInstance("edit");
                        FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                        Bundle bundle = new Bundle();
                        bundle.putString("edit", "edit");
                        bundle.putString("orderId", beanOrderList.getORDER_ID());
                        bundle.putString("Category", beanOrderList.getCATEGORY_NAME());
                        bundle.putString("brandid", beanOrderList.getBRAND());
                        bundle.putString("brand", beanOrderList.getBRAND_NAME());
                        bundle.putString("customer", beanOrderList.getCUSTOMER_NAME());
                        bundle.putString("customerCode", beanOrderList.getCUSTOMER_CODE());
                        bundle.putString("consingee", beanOrderList.getCONSIGNEE());
                        bundle.putString("consingee", beanOrderList.getCONSIGNEE());
                        bundle.putString("address", beanOrderList.getADDRESS());
                        bundle.putString("product", beanOrderList.getPRODUCT());
                        bundle.putString("packing", beanOrderList.getPACKING_TYPE_NAME());
                        bundle.putString("qty", beanOrderList.getQUANTITY());
                        bundle.putString("rs", beanOrderList.getRATE());
                        bundle.putString("valid", beanOrderList.getVALIDITY());
                        bundle.putString("remark", beanOrderList.getREMARK());
                        bundle.putString("consignee_name", beanOrderList.getConsignee_name());
                        bundle.putString("vendor_id", beanOrderList.getVendor_id());
                        bundle.putString("dispatch_from", beanOrderList.getDispatch_from());
                        bundle.putString("mm_name", beanOrderList.getMm_name());
                        fragment.setArguments(bundle);
                        fragmentTransaction.replace(R.id.container_body, fragment);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                        ((MainActivity) context).showUpButton(true);
                    }
                }
            });
            alert.setNegativeButton("View Order", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Fragment_Order_Requirement_TradeView fragment = new Fragment_Order_Requirement_TradeView();
                    FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putString("orderId", beanOrderList.getORDER_ID());
                    fragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                    ((MainActivity) context).showUpButton(true);
                }
            });

            alert.show();

        }


    }
}
