package com.shreecement.shree.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shreecement.shree.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by TIA on 12-08-2016.
 */

public class CustomersOrderDespatch_Adapter extends RecyclerView.Adapter<CustomersOrderDespatch_Adapter.ViewHolder> {
    Context context;
    ArrayList<String> rowItems;

    public CustomersOrderDespatch_Adapter(Context context, ArrayList<String> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    public List<String> getData() {
        return rowItems;
    }

    @Override
    public int getItemCount() {
        return rowItems.size();

    }

    @Override
    public int getItemViewType(int position) {
                return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_customers_orderr_despatch_row_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String rowItem = rowItems.get(position);
        Log.d("=========", String.valueOf(rowItem));
        holder.tvInvoice.setText(rowItem);

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvInvoice,tvInvoiceDate,tvTrunk,tvTranspoter,tvQuantity,tvAmount;

        ViewHolder(View convertView) {
            super(convertView);
            convertView.setOnClickListener(this);
            tvInvoice = (TextView)convertView.findViewById(R.id.tvInvoice);
            tvInvoiceDate = (TextView)convertView.findViewById(R.id.tvInvoiceDate);
            tvTrunk = (TextView)convertView.findViewById(R.id.tvTruck);
            tvTranspoter = (TextView)convertView.findViewById(R.id.tvTranspoter);
            tvQuantity = (TextView)convertView.findViewById(R.id.tvQuantity);
            tvAmount = (TextView)convertView.findViewById(R.id.tvAmount);



        }
        @Override
        public void onClick(View v) {
        }
    }
}