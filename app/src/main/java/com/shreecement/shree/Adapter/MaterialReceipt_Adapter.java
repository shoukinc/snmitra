package com.shreecement.shree.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Fragment_Add_Material_Receipt_Entry;
import com.shreecement.shree.Fragment.Fragment_Material_Receipt_View;
import com.shreecement.shree.ModalClass.BeanReceipt;
import com.shreecement.shree.ModalClass.BeanReceiptDetail;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.MODULE_RECEIPTS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.receiptDetailURL;


/**
 * Created by TIA on 12-08-2016.
 */

public class MaterialReceipt_Adapter extends RecyclerView.Adapter<MaterialReceipt_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanReceipt> rowItems;

    public  MaterialReceipt_Adapter(Context context, ArrayList<BeanReceipt> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    public List<BeanReceipt> getData() {
        return rowItems;
    }

    @Override
    public int getItemCount() {
        return rowItems.size();

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_customer_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final BeanReceipt rowItem = rowItems.get(position);
        Log.d("=========", String.valueOf(rowItem));
        holder.tvDi.setText(rowItem.getDESTINATION_ORGANIZATION());
        holder.tvTruck.setText(rowItem.getTRUCK_NO());
        holder.tvDate.setText(rowItem.getPRODUCT() + " (" +rowItem.getDELIVERY_ID() + ")");
        String first = rowItem.getPRODUCT().substring(0,1);
        holder.tvFirst.setText(first);
        holder.tvFirst.setBackgroundResource(R.drawable.rounder_dateimage_red);

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvDi, tvDate, tvTruck,tvFirst;

        ViewHolder(View convertView) {
            super(convertView);
            convertView.setOnClickListener(this);

            tvDi =  convertView.findViewById(R.id.tvCustomerName);
            tvDate =  convertView.findViewById(R.id.tvCustomerId);
            tvTruck = convertView.findViewById(R.id.tvCustomer);
            tvFirst =  convertView.findViewById(R.id.tvFirst);

        }

        @Override
        public void onClick(View v) {
            final BeanReceipt rowItem = rowItems.get(getLayoutPosition());

            new PostClassReceiptDetail(context, rowItem.getDELIVERY_ID()).execute();


        }
    }

    class PostClassReceiptDetail extends AsyncTask<String, Void, String> {

        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg, deliveryID;
        BeanReceiptDetail receiptDetail;


        public PostClassReceiptDetail(Context c, String deliveryID) {
            this.context = c;
            this.deliveryID = deliveryID;
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    RequestBody body = new FormEncodingBuilder()
                            .add("delivery_id", deliveryID)
                            //.add("page_module", MODULE_RECEIPTS)
                            .build();

                    Request request = new Request.Builder()
                            .url(receiptDetailURL)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_RECEIPTS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONObject jsonData = jObject.getJSONObject("data");
                            if (jsonData != null) {

                                receiptDetail = new BeanReceiptDetail(jsonData.getString("delivery_id"),
                                        jsonData.getString("egp_no"), jsonData.getString("egp_date"),
                                        jsonData.getString("product"), jsonData.getString("inventory_item_id"),
                                        jsonData.getString("source_organization_id"), jsonData.getString("destination_organization_id"),
                                        jsonData.getString("packing_type"), jsonData.getString("source_organization"),
                                        jsonData.getString("destination_organization"), jsonData.getString("receipt_date"),
                                        jsonData.getString("accept_qty"), jsonData.getString("short_qty"),
                                        jsonData.getString("cut_qty"), jsonData.getString("damage_qty"),
                                        jsonData.getString("truck_no"), jsonData.getString("created_by"),
                                        jsonData.getString("last_updated_by"), jsonData.getString("last_update_login"),
                                        jsonData.getString("creation_date"), jsonData.getString("last_update_date"),
                                        jsonData.getString("warehouse"), jsonData.getString("bag_type"),
                                        jsonData.getString("transporter"));

                            }
                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {

                Gson gson = new Gson();
                String json = gson.toJson(receiptDetail);
                StorePrefs.setDefaults("receiptDetail", json, context);

                new AlertDialog.Builder(context)
                        .setTitle(context.getResources().getString(R.string.delivery_id) + receiptDetail.getDelivery_id())
                        .setPositiveButton("View Receipt", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Fragment_Material_Receipt_View fragment = new Fragment_Material_Receipt_View();
                                FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.container_body, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();

                                ((MainActivity) context).showUpButton(true);
                            }
                        })
                        .setNegativeButton("Update Receipt", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Fragment_Add_Material_Receipt_Entry fragment = Fragment_Add_Material_Receipt_Entry.newInstance();
                                FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.replace(R.id.container_body, fragment);
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();

                                ((MainActivity) context).showUpButton(true);
                            }
                        })
                        .show();
            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }
}