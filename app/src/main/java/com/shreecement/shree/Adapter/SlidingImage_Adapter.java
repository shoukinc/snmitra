package com.shreecement.shree.Adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.shreecement.shree.Fragment.BeanSliderImage.BeanSliderImage;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;

import java.util.ArrayList;

/**
 * Created by alliencetech on 6/24/2017.
 */

public class SlidingImage_Adapter extends PagerAdapter {

    private ArrayList<BeanSliderImage> IMAGES;
    private LayoutInflater inflater;
    private Context context;

    public SlidingImage_Adapter(Context context, ArrayList<BeanSliderImage> IMAGES) {
        this.context = context;
        this.IMAGES=IMAGES;
        inflater = LayoutInflater.from(context);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
    @Override
    public int getCount() {
        return IMAGES.size();
    }
    public int getItemPosition(Object object) { return POSITION_NONE; }

    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.pager_item, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);

        BeanSliderImage beanSliderImage =  IMAGES.get(position);

        try {
            Glide.with(context).load(Constant.baseUrlForImage + beanSliderImage.getScheme_url())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }catch (Exception e){
            System.out.print(e);
        }


        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
