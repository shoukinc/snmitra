package com.shreecement.shree.Adapter;

/**
 * Created by TIA on 13-10-2016.
 */

public class SpinnerDialogAdapter {
   /* Context context;
    List<LookupCategory> rowItems;
    String Title ="";
    public static String code ="";




    public SpinnerDialogAdapter(Context context, List<LookupCategory> items, String title) {
        this.context = context;
        this.rowItems = items;
        this.Title = title;
        Log.d("===rowItems.zize===","" +    rowItems.size());


    }

    *//*private view holder class*//*
    public class ViewHolder {
        TextView textName;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
            holder = new ViewHolder();
            holder.textName = (TextView) convertView.findViewById(R.id.txt_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();

        }

        holder.textName.setText(rowItems.get(position).getDESCRIPTION());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String meaning = rowItems.get(position).getDESCRIPTION();

                if (Title.equals("Select Package type")){
                    builder3.dismiss();
                    tvPackingType.setText(meaning);
                    tvPackingType.setTextColor(context.getResources().getColor(R.color.colorBlack));
                }

                else if (Title.equals("Select Brand")){
                    builder3.dismiss();
                    tvBrand.setText(meaning);
                    code = rowItems.get(position).getCODE();
                    tvBrand.setTextColor(context.getResources().getColor(R.color.colorBlack));

                    new PostClassMidalman(context, rowItems.get(position).getCODE()).execute();

                }
                else if (Title.equals("Select Product")){
                    builder3.dismiss();
                    tvProduct.setText(meaning);
                    tvProduct.setTextColor(context.getResources().getColor(R.color.colorBlack));



                }
                else if (Title.equals("Select Dispatch form")){
                    builder3.dismiss();
                    tvDispatch.setText(meaning);
                    tvDispatch.setTextColor(context.getResources().getColor(R.color.colorBlack));
                }
                else if (Title.equals("Select Category")){
                    builder3.dismiss();
                    tvDispatch.setText(meaning);
                    tvDispatch.setTextColor(context.getResources().getColor(R.color.colorBlack));
                }



            }
        });
        return convertView;
    }
    public String getCode(){
        return code;
    }
    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }




    public static class PostClassMidalman extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg="" ;
        String Id="" ;
        String Usertype="" ;
        public static ArrayList<BeanMiddleman> MiddleManList = new ArrayList<BeanMiddleman>();


        public PostClassMidalman(Context c, String id) {
            this.context = c;
            this.Id = id;
            MiddleManList.clear();


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject  jsonObject = new JSONObject();
                    jsonObject.put("org_id",Id);

                    RequestBody body = RequestBody.create(mediaType,jsonObject.toString());
                    Request request = new Request.Builder()
                            .post(body)
                            .url(Middle_man)

                            .addHeader("content-type", "application/json")
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===Middle_man===","======="+Middle_man);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            JSONArray jsonData = jObject.getJSONArray("data");

                            for (int i = 0; i < jsonData.length();i++){
                                JSONObject jCustomer = jsonData.getJSONObject(i);
                                MiddleManList.add(new BeanMiddleman(jCustomer.getString("vendor_id"),
                                        jCustomer.getString("vendor_name"),jCustomer.getString("inactive_date"),
                                        jCustomer.getString("vendor_site_code")));

                            }
                            Log.d("===DiList==","======="+MiddleManList.size());
                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyCode");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
             return;
            } else if (s.equals("success")) {
                Toast.makeText(context, "Okk", Toast.LENGTH_SHORT).show();
                spinMiddleManAdapter = new SpinMiddleMan_Adapter(context,MiddleManList);
                spnMiddleName.setAdapter(spinMiddleManAdapter);
                spinMiddleManAdapter.notifyDataSetChanged();


            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }
    }
*/
}