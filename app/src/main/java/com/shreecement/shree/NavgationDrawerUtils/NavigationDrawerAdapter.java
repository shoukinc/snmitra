package com.shreecement.shree.NavgationDrawerUtils;

/*
 * Created by Chinmay on 12/28/2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shreecement.shree.Activity.MobileInputActivity;
import com.shreecement.shree.Fragment.Conversations.Fragment_Conversion_Formulas;
import com.shreecement.shree.Fragment.Dealers.Fragment_Dealers;
import com.shreecement.shree.Fragment.Documents.Fragment_DocUpload;
import com.shreecement.shree.Fragment.Enquiry.Fragment_Enquiry_List;
import com.shreecement.shree.Fragment.Events.Fragment_Events;
import com.shreecement.shree.Fragment.FeedBack.Fragment_Feedback;
import com.shreecement.shree.Fragment.FeedBack.Fragment_Feedback_List;
import com.shreecement.shree.Fragment.Enquiry.Fragment_Customer_add_Enquiry;
import com.shreecement.shree.Fragment.Fragment_Customers;
import com.shreecement.shree.Fragment.Fragment_Dashboard;
import com.shreecement.shree.Fragment.Fragment_Orders;
import com.shreecement.shree.Fragment.Fragment_Receipts;
import com.shreecement.shree.Fragment.Help.Fragment_Help;
import com.shreecement.shree.Fragment.MaterialTransaction.Fragment_Material_Transaction;
import com.shreecement.shree.Fragment.Mitra.Fragment_MitraRegistration;
import com.shreecement.shree.Fragment.Mitra.Fragment_Mitras;
import com.shreecement.shree.Fragment.Query.Fragment_Query;
import com.shreecement.shree.Fragment.Query.Fragment_Query_List;
import com.shreecement.shree.Fragment.Rating.Fragment_Rating_list;
import com.shreecement.shree.Fragment.Referral.Fragment_Referral;
import com.shreecement.shree.Fragment.RelailerLocater.Fragment_Dealer_Retailer_Locater;
import com.shreecement.shree.Fragment.Retailer.Fragment_Retailers;
import com.shreecement.shree.Fragment.TVC.Fragment_TVCS;
import com.shreecement.shree.Fragment.Tips.Fragment_Tips;
import com.shreecement.shree.Fragment.fragment_AboutUs;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;
import com.shreecement.shree.Utlility.StorePrefs;

import java.util.List;

import static com.shreecement.shree.Utlility.Constant.PREF_DISCLAIMER_DESC;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    private List<NavDrawerItem> data;
    private LayoutInflater inflater;
    private Context context;
    private int currentPosition = 0;
    Fragment fragment;
    Button btnOk;
    TextView tvDisclaimerDesc;
    String UserType = "";

    NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());


        holder.navImage.setImageDrawable(current.getThumbnail());


        if (position == currentPosition) {
            holder.TopLayout.setBackgroundResource(R.drawable.item_pressed);

        } else {
            holder.TopLayout.setBackgroundResource(R.drawable.item_normal);
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        ImageView navImage;
        LinearLayout TopLayout;



        MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            navImage = itemView.findViewById(R.id.navImage);
            TopLayout = itemView.findViewById(R.id.TopLayout);
            title.setOnClickListener(this);
            TopLayout.setOnClickListener(this);
            navImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            UserType = StorePrefs.getDefaults(PREF_USER_TYPE, context);
            currentPosition = getLayoutPosition();
            Log.d("currentPosition", "=======" + currentPosition);
            Log.d("position", "=======" + getLayoutPosition());
            //Reloading
            notifyDataSetChanged();

            fragment = null;
            String slug = data.get(currentPosition).getSlug();

            switch (slug) {

                case "home":
                    fragment = new Fragment_Dashboard();
                    break;
                case "customers":
                    fragment = new Fragment_Customers();
                    break;
                case "dealers":
                    fragment = new Fragment_Dealers();
                    break;
                case "retailers":
                    fragment = new Fragment_Retailers();
                    break;
                case "retailerLocator":
                    fragment = new Fragment_Dealer_Retailer_Locater();
                    //fragment = new Fragment_Dealer_retailer_Locater_list();
                    break;
                case "mitras":
                    if (UserType.equalsIgnoreCase("E")){
                    fragment = new Fragment_Mitras();
                    }
                    else{
                        fragment = new Fragment_MitraRegistration();
                       // Fragment_MitraRegistration.newInstance("add");
                    }

                    break;
                case "materialTransaction":
                    fragment = new Fragment_Material_Transaction();
                    break;
                case "orders":
                    fragment = new Fragment_Orders();
                    break;
                case "receipts":
                    fragment = new Fragment_Receipts();
                    break;
                case "enquiries":
                    if (UserType.equalsIgnoreCase("E")) {
                        fragment = new Fragment_Enquiry_List();
                    }else {
                        fragment = new Fragment_Customer_add_Enquiry();
                    }
                    break;
                case "referral":
                    fragment = new Fragment_Referral();
                    //fragment = new Fragment_Referrals_list();
                    break;
                case "events":
                    fragment = new Fragment_Events();
                    break;
                case "conversionFormulas":
                    fragment = new Fragment_Conversion_Formulas();
                    break;
                case "myDocuments":
                    fragment = new Fragment_DocUpload();
                    break;
                case "tvcs":
                    fragment = new Fragment_TVCS();
                    break;
                case "tips":
                    fragment = new Fragment_Tips();
                    break;
                case "feedback":
                    if (UserType.equalsIgnoreCase("E")) {
                        fragment = new Fragment_Feedback_List();
                    }else {
                        fragment = new Fragment_Feedback();
                    }
                    break;
                case "query":
                    if (UserType.equalsIgnoreCase("E")) {
                        fragment = new Fragment_Query_List();
                    }else {
                        fragment = new Fragment_Query();
                    }
                    break;
                case "help":
                    fragment = new Fragment_Help();
                    break;
                case "rateApp":
                    fragment = new Fragment_Rating_list();
                    Constant.FragmentName = "RATINGS";
                    break;
                case "share":

                   // String shareBody = "ShreeCement Android \n http://www.shreecement.com";
                    String shareBody = "https://play.google.com/store/apps/details?id=com.shreecement.shree";
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "APP NAME (Open it in Google Play Store to Download the Application)");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

                    break;
                case "aboutUs":
                    fragment = new fragment_AboutUs();
                    break;
                case "disclaimer":
                    showDialog(context, StorePrefs.getDefaults(PREF_DISCLAIMER_DESC, context));
                    break;
                case "logout":
                    logout();
                    break;
                default:
                    break;
            }

            if (fragment != null) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                                R.anim.enter_from_right, R.anim.exit_to_left);
                        fragmentTransaction.replace(R.id.container_body, fragment).commit();

                    }
                }, 275);
            }
        }
    }

    public void logout() {
        new android.support.v7.app.AlertDialog.Builder(context)
                .setMessage("Are you sure you want to Logout?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        StorePrefs.clearDefaults("LoggedIn", context);
                        StorePrefs.clearDefaults("regId", context);

                        Intent intent = new Intent(context, MobileInputActivity.class);
                        context.startActivity(intent);
                        ((FragmentActivity) context).finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    public void showDialog(Context activity, String msg) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.fragment_disclaimer);

        tvDisclaimerDesc = dialog.findViewById(R.id.tvDisclaimerDesc);
        tvDisclaimerDesc.setText(msg);
        btnOk = dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
