package com.shreecement.shree.NavgationDrawerUtils;

/**
 * Created by  on 12/28/2016.
 */

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Common.HeadersClass;
import com.shreecement.shree.Fragment.Profile.fragment_MyProfile;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.RecyclerTouchListener;
import com.shreecement.shree.Utlility.StorePrefs;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.shreecement.shree.Utlility.Constant.baseUrlForImage;


public class FragmentDrawer extends Fragment {

    private RecyclerView recyclerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerAdapter adapter;
    private View containerView;
    private String[] titles = null;
    private FragmentDrawerListener drawerListener;
    private ImageView prof_img;
    private TextView tv_NavUsername, tvAppVersion;
    String App_Version = "";

    InputMethodManager inputMethodManager;


    public FragmentDrawer() {

    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;

    }

    public List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();

        String jsonPermission = StorePrefs.getDefaults("userPermissions", getActivity());
        Log.e("Clicked",jsonPermission);

        data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_home),
                getActivity().getResources().getDrawable(R.drawable.ic_home),"home"));

        if (userPermissionExists(jsonPermission, "CUSTOMERS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_customers),
                    getActivity().getResources().getDrawable(R.drawable.ic_customer),"customers"));

        if (userPermissionExistsForDealers(jsonPermission, "DEALERS"))
        //if (userPermissionExists(jsonPermission, "DEALERS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_dealers),
                    getActivity().getResources().getDrawable(R.drawable.ic_dealers),"dealers"));

        if (userPermissionExists(jsonPermission, "DEALERSLOCATORS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_dealer_retailer_locater),
                    getActivity().getResources().getDrawable(R.drawable.ic_delear_retailer_locator),"retailerLocator"));

        if (userPermissionExists(jsonPermission, "RETAILERS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_retailers),
                    getActivity().getResources().getDrawable(R.drawable.ic_retailers),"retailers"));



        if (userPermissionExists(jsonPermission, "MITRAS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_mitras),
                    getActivity().getResources().getDrawable(R.drawable.ic_mitra),"mitras"));

        if (userPermissionExists(jsonPermission, "MATERIALTRANSACTIONS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_material_trasaction),
                    getActivity().getResources().getDrawable(R.drawable.ic_material_transcation),"materialTransaction"));

        if (userPermissionExists(jsonPermission, "ORDERS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_orders),
                    getActivity().getResources().getDrawable(R.drawable.ic_order),"orders"));

        if (userPermissionExists(jsonPermission, "RECEIPTS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_receipts),
                    getActivity().getResources().getDrawable(R.drawable.ic_receipt),"receipts"));


        if (userPermissionExists(jsonPermission, "ENQUIRIES"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_customer_enquiry),
                    getActivity().getResources().getDrawable(R.drawable.ic_customer_enquiry),"enquiries"));

        if (userPermissionExists(jsonPermission, "REFERRALS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_referral),
                    getActivity().getResources().getDrawable(R.drawable.ic_referrals),"referral"));

        if (userPermissionExists(jsonPermission, "EVENTS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_events),
                    getActivity().getResources().getDrawable(R.drawable.ic_event),"events"));

//        data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_conversion_formulas),
//                getActivity().getResources().getDrawable(R.drawable.ic_receipt),"conversionFormulas"));

       /* if (!StorePrefs.getDefaults("USER_TYPE", getActivity()).equalsIgnoreCase("E")) {
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_my_documments),
                    getActivity().getResources().getDrawable(R.drawable.ic_document), "myDocuments"));
        }*/

        if (userPermissionExists(jsonPermission, "TVCS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_tvcs),
                    getActivity().getResources().getDrawable(R.drawable.ic_tvc),"tvcs"));

        if (userPermissionExists(jsonPermission, "TIPS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_tips),
                    getActivity().getResources().getDrawable(R.drawable.ic_tips),"tips"));

       /* if (userPermissionExists(jsonPermission, "FEEDBACKS"))
        data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_feedaback),
                getActivity().getResources().getDrawable(R.drawable.ic_feedback),"feedback"));
*/
        if (userPermissionExists(jsonPermission, "FEEDBACKS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_feedabacks),
                    getActivity().getResources().getDrawable(R.drawable.ic_feedback),"feedback"));
       /* data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_query),
                getActivity().getResources().getDrawable(R.drawable.ic_query),"query"));*/


        if (userPermissionExists(jsonPermission, "QUERIES"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_query),
                    getActivity().getResources().getDrawable(R.drawable.ic_query),"query"));

        if (userPermissionExists(jsonPermission, "HELPTOPICS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_help),
                    getActivity().getResources().getDrawable(R.drawable.ic_help),"help"));

        if (userPermissionExists(jsonPermission, "RATINGS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_rate_app),getActivity().getResources().getDrawable(R.drawable.ic_rate_app),"rateApp"));

        if (userPermissionExists(jsonPermission, "SHARE"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_share),
                    getActivity().getResources().getDrawable(R.drawable.ic_share),"share"));

        if (userPermissionExists(jsonPermission, "ABOUTUS"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_about_us),
                    getActivity().getResources().getDrawable(R.drawable.ic_about_us),"aboutUs"));

        if (userPermissionExists(jsonPermission, "DISCLAIMER"))
            data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_disclaimer),
                    getActivity().getResources().getDrawable(R.drawable.ic_disclaimer),"disclaimer"));

        data.add(new NavDrawerItem(getActivity().getResources().getString(R.string.nav_item_logout),
                getActivity().getResources().getDrawable(R.drawable.ic_logout),"logout"));

        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // drawer labels
      /*  titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);

        List<String> list = new ArrayList<>(Arrays.asList(titles));*/

        inputMethodManager = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

       /* Gson gson = new Gson();
        String json = StorePrefs.getDefaults("customersPermission", getActivity());
        BeanPermission customersPermission = gson.fromJson(json, BeanPermission.class);

        if (customersPermission.getPermission_view().equals("0"))
            list.remove("Customers");

        json = StorePrefs.getDefaults("receiptsPermission", getActivity());
        BeanPermission receiptsPermission = gson.fromJson(json, BeanPermission.class);

        if (receiptsPermission.getPermission_view().equals("0"))
            list.remove("Material Receipt Entry");

        json = StorePrefs.getDefaults("ordersPermission", getActivity());
        BeanPermission ordersPermission = gson.fromJson(json, BeanPermission.class);

        if (ordersPermission.getPermission_view().equals("0"))
            list.remove("Order Requirement Entry");

        titles = list.toArray(new String[0]);*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = layout.findViewById(R.id.drawerList);
        tvAppVersion = layout.findViewById(R.id.tvAppVersion);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            App_Version = pInfo.versionName;
            Log.d("App_Version", "========" + App_Version);

            tvAppVersion.setText("v." + App_Version);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        adapter = new NavigationDrawerAdapter(getActivity(), getData());
        recyclerView.setAdapter(adapter);


        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        prof_img = (ImageView) layout.findViewById(R.id.prof_img);

        prof_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).closeDrawer();
                fragment_MyProfile fragment = new fragment_MyProfile();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.container_body, fragment).addToBackStack(null).commit();
            }
        });

        tv_NavUsername = (TextView) layout.findViewById(R.id.tv_NavUsername);

       /* Glide.with(getActivity())
                // .load(baseUrlForImage + voterId)
                .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + StorePrefs.getDefaults("IMAGE", getActivity())))
                .thumbnail(1.0f)
                .placeholder(R.drawable.profile_icon).dontAnimate()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .crossFade()
                .centerCrop()
                .into(prof_img);*/

        Glide.with(getActivity())
                // .load(baseUrlForImage + StorePrefs.getDefaults("IMAGE", getActivity()))
                .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + StorePrefs.getDefaults("IMAGE", getActivity())))
                .asBitmap()
                .centerCrop()
                .into(new BitmapImageViewTarget(prof_img) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        prof_img.setImageDrawable(circularBitmapDrawable);
                    }
                });

      /*  if (StorePrefs.getDefaults("IMAGE", getActivity()) != null) {

            Log.e("In part", "In image part");
            Glide.with(this)
                   // .load(baseUrlForImage + StorePrefs.getDefaults("IMAGE", getActivity()))
                    .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + StorePrefs.getDefaults("IMAGE", getActivity())))
                    .asBitmap()
                    .error(R.drawable.profile_icon)
                    .centerCrop()
                    .into(new BitmapImageViewTarget(prof_img) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            prof_img.setImageDrawable(circularBitmapDrawable);
                        }
                    });




        }*/
        if (StorePrefs.getDefaults("NAME", getActivity()) != null) {
            tv_NavUsername.setText(StorePrefs.getDefaults("NAME", getActivity()));
        }


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                mDrawerLayout.closeDrawer(containerView);
                drawerListener.onDrawerItemSelected(view, position);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return layout;
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }


    public interface FragmentDrawerListener {
        void onDrawerItemSelected(View view, int position);
    }

    private boolean userPermissionExists(String strJsonArray, String permissionToFind){
        //return strJsonArray.contains(permissionToFind);
        return strJsonArray.toString().contains(permissionToFind);
    }

    private static boolean userPermissionExistsForDealers(String source, String subItem){
        String pattern = "\\b"+subItem+"\\b";
        Pattern p=Pattern.compile(pattern);
        Matcher m=p.matcher(source);
        return m.find();
    }

}
