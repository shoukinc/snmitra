package com.shreecement.shree.ModalClass;

/**
 * Created by Abhishek Punia on 08-02-2018.
 */

public class BeanOrderDetails {
    private String customer_name, consignee_name, order_id, mm_name, customer_code, product, brand,
            packingtype, quantity, consignee, state, district, taluka, city, remark, category, validity,
            address, dispatch_from, rate, created_by, last_updated_by, last_update_login, last_update_date,
            order_date, brand_name, packing_type_name, product_name, category_name;

    public BeanOrderDetails(String customer_name, String consignee_name, String order_id, String mm_name,
                            String customer_code, String product, String brand, String packingtype,
                            String quantity, String consignee, String state, String district, String taluka,
                            String city, String remark, String category, String validity, String address,
                            String dispatch_from, String rate, String created_by, String last_updated_by,
                            String last_update_login, String last_update_date, String order_date,
                            String brand_name, String packing_type_name, String product_name, String category_name) {
        this.customer_name = customer_name;
        this.consignee_name = consignee_name;
        this.order_id = order_id;
        this.mm_name = mm_name;
        this.customer_code = customer_code;
        this.product = product;
        this.brand = brand;
        this.packingtype = packingtype;
        this.quantity = quantity;
        this.consignee = consignee;
        this.state = state;
        this.district = district;
        this.taluka = taluka;
        this.city = city;
        this.remark = remark;
        this.category = category;
        this.validity = validity;
        this.address = address;
        this.dispatch_from = dispatch_from;
        this.rate = rate;
        this.created_by = created_by;
        this.last_updated_by = last_updated_by;
        this.last_update_login = last_update_login;
        this.last_update_date = last_update_date;
        this.order_date = order_date;
        this.brand_name = brand_name;
        this.packing_type_name = packing_type_name;
        this.product_name = product_name;
        this.category_name = category_name;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getConsignee_name() {
        return consignee_name;
    }

    public void setConsignee_name(String consignee_name) {
        this.consignee_name = consignee_name;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getMm_name() {
        return mm_name;
    }

    public void setMm_name(String mm_name) {
        this.mm_name = mm_name;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPackingtype() {
        return packingtype;
    }

    public void setPackingtype(String packingtype) {
        this.packingtype = packingtype;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getTaluka() {
        return taluka;
    }

    public void setTaluka(String taluka) {
        this.taluka = taluka;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDispatch_from() {
        return dispatch_from;
    }

    public void setDispatch_from(String dispatch_from) {
        this.dispatch_from = dispatch_from;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public String getLast_update_login() {
        return last_update_login;
    }

    public void setLast_update_login(String last_update_login) {
        this.last_update_login = last_update_login;
    }

    public String getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(String last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getPacking_type_name() {
        return packing_type_name;
    }

    public void setPacking_type_name(String packing_type_name) {
        this.packing_type_name = packing_type_name;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }
}
