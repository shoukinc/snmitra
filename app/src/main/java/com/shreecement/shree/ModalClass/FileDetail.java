package com.shreecement.shree.ModalClass;

/**
 * Created by alliencetech on 8/10/2017.
 */

public class FileDetail {

    String name;
    String path;
    String isLocal;
    String imgId;

    public FileDetail(String name, String path, String isLocal, String imgId) {
        this.name = name;
        this.path = path;
        this.isLocal = isLocal;
        this.imgId = imgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(String isLocal) {
        this.isLocal = isLocal;
    }

    public String getImgId() {
        return imgId;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }
}
