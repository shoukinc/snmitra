package com.shreecement.shree.ModalClass;

/**
 * Created by Abhishek Punia on 16-03-2018.
 */

public class BeanDi {
  private String delivery_id = "";
  private String egp_no = "";
  private String egp_date = "";
  private String product = "";
  private String ship_quantity = "";
  private String packing_type = "";
  private String bag_type = "";
  private String truck_no = "";
  private String transporter = "";

    public BeanDi(String delivery_id, String egp_no, String egp_date, String product, String ship_quantity,
                  String packing_type, String bag_type, String truck_no, String transporter) {
        this.delivery_id = delivery_id;
        this.egp_no = egp_no;
        this.egp_date = egp_date;
        this.product = product;
        this.ship_quantity = ship_quantity;
        this.packing_type = packing_type;
        this.bag_type = bag_type;
        this.truck_no = truck_no;
        this.transporter = transporter;
    }


    public String getDelivery_id() {
        return delivery_id;
    }

    public void setDelivery_id(String delivery_id) {
        this.delivery_id = delivery_id;
    }

    public String getEgp_no() {
        return egp_no;
    }

    public void setEgp_no(String egp_no) {
        this.egp_no = egp_no;
    }

    public String getEgp_date() {
        return egp_date;
    }

    public void setEgp_date(String egp_date) {
        this.egp_date = egp_date;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getShip_quantity() {
        return ship_quantity;
    }

    public void setShip_quantity(String ship_quantity) {
        this.ship_quantity = ship_quantity;
    }

    public String getPacking_type() {
        return packing_type;
    }

    public void setPacking_type(String packing_type) {
        this.packing_type = packing_type;
    }

    public String getBag_type() {
        return bag_type;
    }

    public void setBag_type(String bag_type) {
        this.bag_type = bag_type;
    }

    public String getTruck_no() {
        return truck_no;
    }

    public void setTruck_no(String truck_no) {
        this.truck_no = truck_no;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }
}
