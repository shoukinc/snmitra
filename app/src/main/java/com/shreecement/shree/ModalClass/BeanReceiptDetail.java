package com.shreecement.shree.ModalClass;

/*
 * Created by digvijay on 3/17/18.
 */

public class BeanReceiptDetail {

    private String delivery_id, egp_no, egp_date, product, inventory_item_id, source_organization_id,
            destination_organization_id, packing_type, source_organization, destination_organization,
            receipt_date, accept_qty, short_qty, cut_qty, damage_qty, truck_no, created_by, last_updated_by,
            last_update_login, creation_date, last_update_date, warehouse, bag_type, transporter;

    public BeanReceiptDetail(String delivery_id, String egp_no, String egp_date, String product,
                             String inventory_item_id, String source_organization_id,
                             String destination_organization_id, String packing_type, String source_organization,
                             String destination_organization, String receipt_date, String accept_qty,
                             String short_qty, String cut_qty, String damage_qty, String truck_no,
                             String created_by, String last_updated_by, String last_update_login,
                             String creation_date, String last_update_date, String warehouse,
                             String bag_type, String transporter) {
        this.delivery_id = delivery_id;
        this.egp_no = egp_no;
        this.egp_date = egp_date;
        this.product = product;
        this.inventory_item_id = inventory_item_id;
        this.source_organization_id = source_organization_id;
        this.destination_organization_id = destination_organization_id;
        this.packing_type = packing_type;
        this.source_organization = source_organization;
        this.destination_organization = destination_organization;
        this.receipt_date = receipt_date;
        this.accept_qty = accept_qty;
        this.short_qty = short_qty;
        this.cut_qty = cut_qty;
        this.damage_qty = damage_qty;
        this.truck_no = truck_no;
        this.created_by = created_by;
        this.last_updated_by = last_updated_by;
        this.last_update_login = last_update_login;
        this.creation_date = creation_date;
        this.last_update_date = last_update_date;
        this.warehouse = warehouse;
        this.bag_type = bag_type;
        this.transporter = transporter;
    }

    public String getDelivery_id() {
        return delivery_id;
    }

    public void setDelivery_id(String delivery_id) {
        this.delivery_id = delivery_id;
    }

    public String getEgp_no() {
        return egp_no;
    }

    public void setEgp_no(String egp_no) {
        this.egp_no = egp_no;
    }

    public String getEgp_date() {
        return egp_date;
    }

    public void setEgp_date(String egp_date) {
        this.egp_date = egp_date;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getInventory_item_id() {
        return inventory_item_id;
    }

    public void setInventory_item_id(String inventory_item_id) {
        this.inventory_item_id = inventory_item_id;
    }

    public String getSource_organization_id() {
        return source_organization_id;
    }

    public void setSource_organization_id(String source_organization_id) {
        this.source_organization_id = source_organization_id;
    }

    public String getDestination_organization_id() {
        return destination_organization_id;
    }

    public void setDestination_organization_id(String destination_organization_id) {
        this.destination_organization_id = destination_organization_id;
    }

    public String getPacking_type() {
        return packing_type;
    }

    public void setPacking_type(String packing_type) {
        this.packing_type = packing_type;
    }

    public String getSource_organization() {
        return source_organization;
    }

    public void setSource_organization(String source_organization) {
        this.source_organization = source_organization;
    }

    public String getDestination_organization() {
        return destination_organization;
    }

    public void setDestination_organization(String destination_organization) {
        this.destination_organization = destination_organization;
    }

    public String getReceipt_date() {
        return receipt_date;
    }

    public void setReceipt_date(String receipt_date) {
        this.receipt_date = receipt_date;
    }

    public String getAccept_qty() {
        return accept_qty;
    }

    public void setAccept_qty(String accept_qty) {
        this.accept_qty = accept_qty;
    }

    public String getShort_qty() {
        return short_qty;
    }

    public void setShort_qty(String short_qty) {
        this.short_qty = short_qty;
    }

    public String getCut_qty() {
        return cut_qty;
    }

    public void setCut_qty(String cut_qty) {
        this.cut_qty = cut_qty;
    }

    public String getDamage_qty() {
        return damage_qty;
    }

    public void setDamage_qty(String damage_qty) {
        this.damage_qty = damage_qty;
    }

    public String getTruck_no() {
        return truck_no;
    }

    public void setTruck_no(String truck_no) {
        this.truck_no = truck_no;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public String getLast_update_login() {
        return last_update_login;
    }

    public void setLast_update_login(String last_update_login) {
        this.last_update_login = last_update_login;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(String last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getBag_type() {
        return bag_type;
    }

    public void setBag_type(String bag_type) {
        this.bag_type = bag_type;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }
}
