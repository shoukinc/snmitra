package com.shreecement.shree.ModalClass;

/**
 * Created by Abhishek Punia on 14-03-2018.
 */

public class BeanMiddleman {

    String vendor_id = "";
    String vendor_name = "";
    String inactive_date = "";
    String vendor_site_code = "";


    public BeanMiddleman(String vendor_id, String vendor_name, String inactive_date, String vendor_site_code) {
        this.vendor_id = vendor_id;
        this.vendor_name = vendor_name;
        this.inactive_date = inactive_date;
        this.vendor_site_code = vendor_site_code;
    }


    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getInactive_date() {
        return inactive_date;
    }

    public void setInactive_date(String inactive_date) {
        this.inactive_date = inactive_date;
    }

    public String getVendor_site_code() {
        return vendor_site_code;
    }

    public void setVendor_site_code(String vendor_site_code) {
        this.vendor_site_code = vendor_site_code;
    }
}
