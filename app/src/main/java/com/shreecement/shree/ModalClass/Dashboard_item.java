package com.shreecement.shree.ModalClass;

/**
 * Created by user on 6/29/2017.
 */

public class Dashboard_item {

    private boolean showNotify;
    private String title;

    public Dashboard_item(boolean showNotify, String title) {
        this.showNotify = showNotify;
        this.title = title;
    }

    public Dashboard_item() {

    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
