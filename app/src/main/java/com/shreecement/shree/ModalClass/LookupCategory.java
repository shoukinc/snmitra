package com.shreecement.shree.ModalClass;

/**
 * Created by Abhishek Punia on 05-02-2018.
 */

public class LookupCategory {

    String LOOKUP_TYPE ="";
    String CODE ="";
    String DESCRIPTION ="";
    String district = "";
    String city = "";

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public LookupCategory(String LOOKUP_TYPE, String CODE, String DESCRIPTION) {
        this.LOOKUP_TYPE = LOOKUP_TYPE;
        this.CODE = CODE;
        this.DESCRIPTION = DESCRIPTION;
    }
    public LookupCategory(String LOOKUP_TYPE, String CODE, String DESCRIPTION, String district, String city) {
        this.LOOKUP_TYPE = LOOKUP_TYPE;
        this.CODE = CODE;
        this.DESCRIPTION = DESCRIPTION;
        this.district = district;
        this.city = city;
    }

    public String getLOOKUP_TYPE() {
        return LOOKUP_TYPE;
    }

    public void setLOOKUP_TYPE(String LOOKUP_TYPE) {
        this.LOOKUP_TYPE = LOOKUP_TYPE;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }
}
