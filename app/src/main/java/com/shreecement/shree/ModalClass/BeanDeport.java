package com.shreecement.shree.ModalClass;

/**
 * Created by Abhishek Punia on 16-03-2018.
 */

public class BeanDeport {
  private String name = "";
  private String organization_id = "";

    public BeanDeport(String name, String organization_id) {
        this.name = name;
        this.organization_id = organization_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }
}
