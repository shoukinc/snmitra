package com.shreecement.shree.ModalClass;

/**
 * Created by Abhishek Punia on 05-02-2018.
 */

public class LookUpPacking {
    String LOOKUP_TYPE = "";
    String CODE = "";
    String MEANING = "";
    String DESCRIPTION = "";


    public LookUpPacking(String LOOKUP_TYPE, String CODE, String MEANING, String DESCRIPTION) {
        this.LOOKUP_TYPE = LOOKUP_TYPE;
        this.CODE = CODE;
        this.MEANING = MEANING;
        this.DESCRIPTION = DESCRIPTION;
    }


    public String getLOOKUP_TYPE() {
        return LOOKUP_TYPE;
    }

    public void setLOOKUP_TYPE(String LOOKUP_TYPE) {
        this.LOOKUP_TYPE = LOOKUP_TYPE;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getMEANING() {
        return MEANING;
    }

    public void setMEANING(String MEANING) {
        this.MEANING = MEANING;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }
}
