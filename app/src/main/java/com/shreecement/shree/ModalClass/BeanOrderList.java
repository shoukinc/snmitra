package com.shreecement.shree.ModalClass;

import io.realm.RealmObject;

/**
 * Created by Abhishek Punia on 07-02-2018.
 */

public class BeanOrderList extends RealmObject {
   String ORDER_DATE = "";
   String CUSTOMER_NAME = "";
   String Consignee_name = "";
   String Vendor_id = "";
   String dispatch_from = "";
   String mm_name = "";
    String BRAND_NAME = "";
   String CATEGORY_NAME = "";
   String PACKING_TYPE_NAME = "";
   String PRODUCT_NAME = "";
 /*  @PrimaryKey*/
   String ORDER_ID = "";
   String CATEGORY = "";
   String CUSTOMER_CODE = "";
   String PRODUCT = "";
   String BRAND = "";
   String PACKINGTYPE = "";
   String BAGTYPE = "";
   String QUANTITY = "";
   String CONSIGNEE = "";
   String STATE = "";
   String DISTRICT = "";
   String TALUKA = "";
   String CITY = "";
   String RATE = "";
   String VALIDITY = "";
   String ADDRESS = "";
   String EMPLOYEE_CODE = "";
   String TRANSFER_FLAG = "";
   String REMARK = "";
   String LAST_UPDATED_BY = "";
   String LAST_UPDATE_LOGIN = "";
   String LAST_UPDATE_DATE = "";
   String ORDER_DATE_NEW = "";

    public BeanOrderList() {
    }

    public BeanOrderList(String ORDER_DATE, String CUSTOMER_NAME, String consignee_name, String vendor_id, String dispatch_from, String mm_name, String BRAND_NAME, String CATEGORY_NAME, String PACKING_TYPE_NAME, String PRODUCT_NAME, String ORDER_ID, String CATEGORY, String CUSTOMER_CODE, String PRODUCT, String BRAND, String PACKINGTYPE, String BAGTYPE, String QUANTITY, String CONSIGNEE, String STATE, String DISTRICT, String TALUKA, String CITY, String RATE, String VALIDITY, String ADDRESS, String EMPLOYEE_CODE, String TRANSFER_FLAG, String REMARK, String LAST_UPDATED_BY, String LAST_UPDATE_LOGIN, String LAST_UPDATE_DATE, String ORDER_DATE_NEW) {
        this.ORDER_DATE = ORDER_DATE;
        this.CUSTOMER_NAME = CUSTOMER_NAME;
        Consignee_name = consignee_name;
        Vendor_id = vendor_id;
        this.dispatch_from = dispatch_from;
        this.mm_name = mm_name;
        this.BRAND_NAME = BRAND_NAME;
        this.CATEGORY_NAME = CATEGORY_NAME;
        this.PACKING_TYPE_NAME = PACKING_TYPE_NAME;
        this.PRODUCT_NAME = PRODUCT_NAME;
        this.ORDER_ID = ORDER_ID;
        this.CATEGORY = CATEGORY;
        this.CUSTOMER_CODE = CUSTOMER_CODE;
        this.PRODUCT = PRODUCT;
        this.BRAND = BRAND;
        this.PACKINGTYPE = PACKINGTYPE;
        this.BAGTYPE = BAGTYPE;
        this.QUANTITY = QUANTITY;
        this.CONSIGNEE = CONSIGNEE;
        this.STATE = STATE;
        this.DISTRICT = DISTRICT;
        this.TALUKA = TALUKA;
        this.CITY = CITY;
        this.RATE = RATE;
        this.VALIDITY = VALIDITY;
        this.ADDRESS = ADDRESS;
        this.EMPLOYEE_CODE = EMPLOYEE_CODE;
        this.TRANSFER_FLAG = TRANSFER_FLAG;
        this.REMARK = REMARK;
        this.LAST_UPDATED_BY = LAST_UPDATED_BY;
        this.LAST_UPDATE_LOGIN = LAST_UPDATE_LOGIN;
        this.LAST_UPDATE_DATE = LAST_UPDATE_DATE;
        this.ORDER_DATE_NEW = ORDER_DATE_NEW;
    }

    public String getORDER_DATE() {
        return ORDER_DATE;
    }

    public void setORDER_DATE(String ORDER_DATE) {
        this.ORDER_DATE = ORDER_DATE;
    }

    public String getCUSTOMER_NAME() {
        return CUSTOMER_NAME;
    }

    public void setCUSTOMER_NAME(String CUSTOMER_NAME) {
        this.CUSTOMER_NAME = CUSTOMER_NAME;
    }

    public String getConsignee_name() {
        return Consignee_name;
    }

    public void setConsignee_name(String consignee_name) {
        Consignee_name = consignee_name;
    }

    public String getVendor_id() {
        return Vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        Vendor_id = vendor_id;
    }

    public String getDispatch_from() {
        return dispatch_from;
    }

    public void setDispatch_from(String dispatch_from) {
        this.dispatch_from = dispatch_from;
    }

    public String getMm_name() {
        return mm_name;
    }

    public void setMm_name(String mm_name) {
        this.mm_name = mm_name;
    }

    public String getBRAND_NAME() {
        return BRAND_NAME;
    }

    public void setBRAND_NAME(String BRAND_NAME) {
        this.BRAND_NAME = BRAND_NAME;
    }

    public String getCATEGORY_NAME() {
        return CATEGORY_NAME;
    }

    public void setCATEGORY_NAME(String CATEGORY_NAME) {
        this.CATEGORY_NAME = CATEGORY_NAME;
    }

    public String getPACKING_TYPE_NAME() {
        return PACKING_TYPE_NAME;
    }

    public void setPACKING_TYPE_NAME(String PACKING_TYPE_NAME) {
        this.PACKING_TYPE_NAME = PACKING_TYPE_NAME;
    }

    public String getPRODUCT_NAME() {
        return PRODUCT_NAME;
    }

    public void setPRODUCT_NAME(String PRODUCT_NAME) {
        this.PRODUCT_NAME = PRODUCT_NAME;
    }

    public String getORDER_ID() {
        return ORDER_ID;
    }

    public void setORDER_ID(String ORDER_ID) {
        this.ORDER_ID = ORDER_ID;
    }

    public String getCATEGORY() {
        return CATEGORY;
    }

    public void setCATEGORY(String CATEGORY) {
        this.CATEGORY = CATEGORY;
    }

    public String getCUSTOMER_CODE() {
        return CUSTOMER_CODE;
    }

    public void setCUSTOMER_CODE(String CUSTOMER_CODE) {
        this.CUSTOMER_CODE = CUSTOMER_CODE;
    }

    public String getPRODUCT() {
        return PRODUCT;
    }

    public void setPRODUCT(String PRODUCT) {
        this.PRODUCT = PRODUCT;
    }

    public String getBRAND() {
        return BRAND;
    }

    public void setBRAND(String BRAND) {
        this.BRAND = BRAND;
    }

    public String getPACKINGTYPE() {
        return PACKINGTYPE;
    }

    public void setPACKINGTYPE(String PACKINGTYPE) {
        this.PACKINGTYPE = PACKINGTYPE;
    }

    public String getBAGTYPE() {
        return BAGTYPE;
    }

    public void setBAGTYPE(String BAGTYPE) {
        this.BAGTYPE = BAGTYPE;
    }

    public String getQUANTITY() {
        return QUANTITY;
    }

    public void setQUANTITY(String QUANTITY) {
        this.QUANTITY = QUANTITY;
    }

    public String getCONSIGNEE() {
        return CONSIGNEE;
    }

    public void setCONSIGNEE(String CONSIGNEE) {
        this.CONSIGNEE = CONSIGNEE;
    }

    public String getSTATE() {
        return STATE;
    }

    public void setSTATE(String STATE) {
        this.STATE = STATE;
    }

    public String getDISTRICT() {
        return DISTRICT;
    }

    public void setDISTRICT(String DISTRICT) {
        this.DISTRICT = DISTRICT;
    }

    public String getTALUKA() {
        return TALUKA;
    }

    public void setTALUKA(String TALUKA) {
        this.TALUKA = TALUKA;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getRATE() {
        return RATE;
    }

    public void setRATE(String RATE) {
        this.RATE = RATE;
    }

    public String getVALIDITY() {
        return VALIDITY;
    }

    public void setVALIDITY(String VALIDITY) {
        this.VALIDITY = VALIDITY;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getEMPLOYEE_CODE() {
        return EMPLOYEE_CODE;
    }

    public void setEMPLOYEE_CODE(String EMPLOYEE_CODE) {
        this.EMPLOYEE_CODE = EMPLOYEE_CODE;
    }

    public String getTRANSFER_FLAG() {
        return TRANSFER_FLAG;
    }

    public void setTRANSFER_FLAG(String TRANSFER_FLAG) {
        this.TRANSFER_FLAG = TRANSFER_FLAG;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public String getLAST_UPDATED_BY() {
        return LAST_UPDATED_BY;
    }

    public void setLAST_UPDATED_BY(String LAST_UPDATED_BY) {
        this.LAST_UPDATED_BY = LAST_UPDATED_BY;
    }

    public String getLAST_UPDATE_LOGIN() {
        return LAST_UPDATE_LOGIN;
    }

    public void setLAST_UPDATE_LOGIN(String LAST_UPDATE_LOGIN) {
        this.LAST_UPDATE_LOGIN = LAST_UPDATE_LOGIN;
    }

    public String getLAST_UPDATE_DATE() {
        return LAST_UPDATE_DATE;
    }

    public void setLAST_UPDATE_DATE(String LAST_UPDATE_DATE) {
        this.LAST_UPDATE_DATE = LAST_UPDATE_DATE;
    }

    public String getORDER_DATE_NEW() {
        return ORDER_DATE_NEW;
    }

    public void setORDER_DATE_NEW(String ORDER_DATE_NEW) {
        this.ORDER_DATE_NEW = ORDER_DATE_NEW;
    }
}

