package com.shreecement.shree.ModalClass;

/**
 * Created by Abhishek Punia on 05-02-2018.
 */

public class CustomerSearch {

    String id = "";
    String name = "";
    String city = "";
    String state = "";
    String address = "";

    public CustomerSearch(String id, String name, String city, String state, String address) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.state = state;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}


