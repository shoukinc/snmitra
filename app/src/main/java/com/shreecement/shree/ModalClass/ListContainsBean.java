package com.shreecement.shree.ModalClass;

public class ListContainsBean {

    boolean exist;
    LookupCategory lookupCategory;


    public ListContainsBean(boolean exist, LookupCategory lookupCategory) {
        this.exist = exist;
        this.lookupCategory = lookupCategory;
    }

    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }

    public LookupCategory getLookupCategory() {
        return lookupCategory;
    }

    public void setLookupCategory(LookupCategory lookupCategory) {
        this.lookupCategory = lookupCategory;
    }
}
