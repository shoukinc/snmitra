package com.shreecement.shree.ModalClass;

public class BeanPermission {

    private String order_sequence, permission_name, permission_code, permission_add, permission_edit,
            permission_delete, permission_view;

    public BeanPermission(String order_sequence, String permission_name, String permission_code,
                          String permission_add, String permission_edit, String permission_delete,
                          String permission_view) {
        this.order_sequence = order_sequence;
        this.permission_name = permission_name;
        this.permission_code = permission_code;
        this.permission_add = permission_add;
        this.permission_edit = permission_edit;
        this.permission_delete = permission_delete;
        this.permission_view = permission_view;
    }

    public String getOrder_sequence() {
        return order_sequence;
    }

    public void setOrder_sequence(String order_sequence) {
        this.order_sequence = order_sequence;
    }

    public String getPermission_name() {
        return permission_name;
    }

    public void setPermission_name(String permission_name) {
        this.permission_name = permission_name;
    }

    public String getPermission_code() {
        return permission_code;
    }

    public void setPermission_code(String permission_code) {
        this.permission_code = permission_code;
    }

    public String getPermission_add() {
        return permission_add;
    }

    public void setPermission_add(String permission_add) {
        this.permission_add = permission_add;
    }

    public String getPermission_edit() {
        return permission_edit;
    }

    public void setPermission_edit(String permission_edit) {
        this.permission_edit = permission_edit;
    }

    public String getPermission_delete() {
        return permission_delete;
    }

    public void setPermission_delete(String permission_delete) {
        this.permission_delete = permission_delete;
    }

    public String getPermission_view() {
        return permission_view;
    }

    public void setPermission_view(String permission_view) {
        this.permission_view = permission_view;
    }
}
