package com.shreecement.shree.Utlility;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.shreecement.shree.R;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by ivan.s.petrov on 22.3.2016.
 */
public class Utils {
    private static final String TAG = "Utils";
    public static final String Please_Check_Internet_Connection = "Please Check Internet Connection";


    /**
     * Calculates a previous date based on a number of days back
     * from the current date.
     *
     * @param numberOfDaysBack the number of days back from the current date
     * @return a previous date
     */
    public static String getPreviousDate(int numberOfDaysBack) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -numberOfDaysBack);

        return dateFormat.format(cal.getTime());
    }

    /**
     * Rounds a double value.
     *
     * @param number        the number that should be rounded
     * @param decimalPlaces the number of decimal places that are taken into consideration
     *                      when rounding
     * @return a rounded double value
     */
    public static double roundDouble(double number, int decimalPlaces) {
        if (decimalPlaces < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal numberToRound = new BigDecimal(number);
        numberToRound = numberToRound.setScale(decimalPlaces, RoundingMode.HALF_UP);
        return numberToRound.doubleValue();
    }

    /**
     * Displays a warning toast.
     *
     * @param message the message to be displayed
     */
    public static void displayWarningToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        View toastView = toast.getView();
        toastView.setBackgroundResource(R.color.colorAccent);
        toast.show();
    }

    /**
     * Display a warning toast.
     * <p>
     * The method should be called from background threads for
     * displaying warning messages.
     *
     * @param context an Activity context
     * @param message
     */
    public static void displayWarningToastFromBackgroundThread(final Context context, final String message) {
        try {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
                    View toastView = toast.getView();
                    toastView.setBackgroundResource(R.color.colorAccent);
                    toast.show();
                }
            });
        } catch (ClassCastException ex) {
            Log.e(TAG, "The given context could not be cast to Activity context", ex);
        }

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public static boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }




    /* My Utility Functions */
    public static void displayToastMessage(final Context context, final String message) {
//        Toast.makeText(context, message, duration).show();

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static String checkStringNull(String text) {

        if (text.trim().length() == 0 || text.trim().equalsIgnoreCase("null") || text.isEmpty()) {
            return "Not Available";
        }
        return text;
    }

    public static String resetNullString(String text) {

        if (text.trim().length() == 0 || text.trim().equalsIgnoreCase("null")) {
            return "";
        }
        return text;
    }

    public static String resetEmptyString(String text) {

        if (text.trim().length() == 0) {
            return "0";
        }

        return text;
    }

    public static String capitalizeFirstLetter(String capString) {
        capString = capString.substring(0, 1).toUpperCase() + capString.substring(1).toLowerCase();
        return capString;
    }

    public static String capitalizeFirstLetterOfWords(String capString) {
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()) {
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    public static String checkSharedPrefsNull(Context context, String text) {
        String prefs = StorePrefs.getDefaults(text, context);

        if (prefs != null) {
            return prefs;
        }
        return "";
    }

    @SuppressWarnings("deprecation")
    public static String fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY).toString().trim();
        } else {
            return Html.fromHtml(source).toString().trim();
        }
    }

    public static String dateFormatWithTime(String date) {

        String formattedString = "";

        if (date.trim().length() == 0 || date.trim().equalsIgnoreCase("null")) {
            return "Not Available";
        }

        DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        Date date1 = null;

        try {
            date1 = readFormat.parse(date);
            formattedString = formatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }

        return formattedString;
    }

    public static String dateFormatToPost(String date) {

        String formattedString = "";

        if (date.trim().length() == 0 || date.trim().equalsIgnoreCase("null")) {
            return "Not Available";
        }

        DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;

        try {
            date1 = readFormat.parse(date);
            formattedString = formatter.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }

        return formattedString;
    }

    public static Date stringToDateFormat(String date) {

        DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date1 = null;

        try {
            date1 = readFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            // return date;
        }

        return date1;
    }

    public static String dateFormatToString(Date date) {

        String formattedString = "";

        if (date == null) {
            return "";
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedString = formatter.format(date);
        return formattedString;
    }



    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }



}
