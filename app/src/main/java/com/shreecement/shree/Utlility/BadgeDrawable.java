package com.shreecement.shree.Utlility;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.shreecement.shree.R;


/**
 * Drawable for display badge number in menu icon
 *
 * @author Togi
 * @version 2016.07.04
 * @since 1.0
 */

public class BadgeDrawable extends Drawable {

    private Paint mBadgePaint;
    private Paint mBadgePaint1;
    private Paint mTextPaint;
    private Rect mTxtRect = new Rect();

    private String mCount = "";
    private boolean mWillDraw;
    private Boolean mIsRight = true;
    private float mTextSize;

    public BadgeDrawable(Context context, Boolean isRight) {
        mTextSize = context.getResources().getDimension(R.dimen.badge_text_size);
        this.mIsRight = isRight;

        mBadgePaint = new Paint();
        mBadgePaint.setColor(Color.WHITE);
        mBadgePaint.setAntiAlias(true);
        mBadgePaint.setStyle(Paint.Style.FILL);
        mBadgePaint1 = new Paint();
        mBadgePaint1.setColor(ContextCompat.getColor(context.getApplicationContext(), R.color.colorBlack));
        mBadgePaint1.setAntiAlias(true);
        mBadgePaint1.setStyle(Paint.Style.FILL);

        mTextPaint = new Paint();
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setTypeface(Typeface.DEFAULT);
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setAntiAlias(true);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    public void draw(Canvas canvas) {

        if (!mWillDraw) {
            return;
        }
        Rect bounds = getBounds();
        float width = bounds.right - bounds.left;
        float height = bounds.bottom - bounds.top;

        float radius = mTextSize / 2;
        float centerX = (mIsRight ? width - radius / 2 : radius / 2);
        float centerY = height - radius / 2;//(mIsRight ? radius / 2: height - radius / 2);
        if (mCount.length() <= 2) {
            // Draw badge circle.
            canvas.drawCircle(centerX, centerY, (int) (radius + 7.5), mBadgePaint1);
            canvas.drawCircle(centerX, centerY, (int) (radius + 5.5), mBadgePaint);
        } else {
            canvas.drawCircle(centerX, centerY, (int) (radius + 8.5), mBadgePaint1);
            canvas.drawCircle(centerX, centerY, (int) (radius + 6.5), mBadgePaint);
//	        	canvas.drawRoundRect(radius, radius, radius, radius, 10, 10, mBadgePaint);
        }
        // Draw badge count text inside the circle.
        mTextPaint.getTextBounds(mCount, 0, mCount.length(), mTxtRect);
        float textHeight = mTxtRect.bottom - mTxtRect.top;
        float textY = centerY + (textHeight / 2f);
        if (mCount.length() > 2)
            canvas.drawText("99+", centerX, textY, mTextPaint);
        else
            canvas.drawText(mCount, centerX, textY, mTextPaint);
    }

    /*
    Sets the count (i.e notifications) to display.
     */
    public void setCount(String count) {
        mCount = count;

        // Only draw a badge if there are notifications.
        mWillDraw = !count.equalsIgnoreCase("0");
        invalidateSelf();
    }

    @Override
    public void setAlpha(int alpha) {
        // do nothing
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        // do nothing
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }
}