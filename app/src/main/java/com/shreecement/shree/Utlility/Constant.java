package com.shreecement.shree.Utlility;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Fragment.fragment_CheckInternet;
import com.shreecement.shree.ModalClass.ListContainsBean;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;


/**
 * Created by Choudhary on 6/15/2017.
 */

     public class Constant {
    public static String tokan = "";

    public static String color = "#000";
    public static final String RESPONSE_TYPE_SUCCESS = "success";
    public static final String RESPONSE_TYPE_ERROR = "error";
    public static  String token_from_server = "";


    //Url................
    public static final String baseUrl = "http://api.shreecement.com/v1/";
    //public static final String baseUrl = "http://api.shreecementltd.com/v1/";

    public static final String ImageShowUrl = "http://104.194.231.20:3000/";
    //public static final String baseUrlForImage = "http://api.shreecementltd.com/";
    //public static final String baseUrlForImage = "http://web.shreecement.com/";
    public static final String baseUrlForImage = "http://shree.shreecement.com/";


    public static final String loginUrl = baseUrl + "login";
    public static final String ApkVersion = baseUrl + "apk_version";
    public static final String ForgotPassUrl = baseUrl + "forgot_password";
    public static final String ResetPassUrl = baseUrl + "reset_password";
    public static final String ChangePasswordUrl = baseUrl + "change_password";
    public static final String customersUrl = baseUrl + "customers";
    public static final String ProfileChangeUrl = baseUrl + "change_profile_image";
    public static final String CustomerSearchUrl = baseUrl + "customers_search";
    public static final String ConsineeSearchearchUrl = baseUrl + "consinee_search";
    public static final String orders_listUrl = baseUrl + "orders_list";
    public static final String OrderdetailUrl = baseUrl + "order_detail";
    public static final String Lookup_data = baseUrl + "lookup_data";
    public static final String Middle_man = baseUrl + "middle_man";
    public static final String CreateOrder = baseUrl + "create_order";
    public static final String UpdateOrder = baseUrl + "update_order";
    public static final String Deport = baseUrl + "deport";
    public static final String Di = baseUrl + "get_di";
    public static final String receiptsURL = baseUrl + "receipts_list";
    public static final String Create_receipt = baseUrl + "create_receipt";
    public static final String Update_receipt = baseUrl + "update_receipt";
    public static final String receiptDetailURL = baseUrl + "receipt_detail";
    public static final String customerOrdersUrl = baseUrl + "customer_orders";
    public static final String dashboardCount = baseUrl + "dashboard";

    public static final String notifications = baseUrl + "notifications";

    public static final String Eventslist = baseUrl + "events_list";
    public static final String Mitralist = baseUrl + "mitra_list";
    public static final String CreateMitra = baseUrl + "create_mitra";


    public static final String Statelist = baseUrl + "state_list";
    public static final String Districtlist = baseUrl + "district_list";
    public static final String Talukalist = baseUrl + "taluka_list";
    public static final String Citylist = baseUrl + "city_list";
    public static final String Schemeslist = baseUrl + "schemes_list";
    public static final String Announcements = baseUrl + "announcements";
    public static final String Getstock = baseUrl + "getstock";


    public static final String help_listUrl = baseUrl + "help_list";
    public static final String tips_listUrl = baseUrl + "tips_list";
    public static final String dealersUrl = baseUrl + "dealers";
    public static final String retailersUrl = baseUrl + "retailers";
    public static final String create_ratingUrl = baseUrl + "create_rating";
    public static final String Tvcslist = baseUrl + "tvcs_list";

    public static final String CheckUserExists = baseUrl + "check_user_exists";
    public static final String SendOtp = baseUrl + "send_otp";

    public static final String AboutUsDiscription = baseUrl + "aboutus";
    public static final String LinkList = baseUrl + "link_list";

    public static final String UploadDocuments = baseUrl + "upload_documents";
    public static final String MitraDocuments = baseUrl + "mitra_documents";
    public static final String UserRegistration = baseUrl + "user_registration";
    public static final String ReferralListUrl = baseUrl + "referral_list";
    public static final String ReferralCreate = baseUrl + "referral_create";
    public static final String RatingList = baseUrl + "rating_list";
    public static final String RatingCreate = baseUrl + "rating_create";
    public static final String GetpackingBagType = baseUrl + "get_packing_bag_type";

    public static final String MaterialDealersList = baseUrl + "material_dealers_list";
    public static final String MaterialRetailerslist = baseUrl + "material_retailers_list";
    public static final String MaterialMitraList = baseUrl + "material_mitra_list";
    public static final String Architectlist = baseUrl + "architect_list";
    public static final String Create_Materialtransaction = baseUrl + "create_materialtransaction";
    public static final String MitraDetail = baseUrl + "mitra_detail";
    public static final String ProfileDetail = baseUrl + "profile_detail";
    public static final String Materialtransactionslist = baseUrl + "materialtransactions_list";
    public static final String NotificationDetail = baseUrl + "notification_detail";
    public static final String UpdateMaterialTransactionStatus = baseUrl + "update_materialtransaction_status";
    public static final String UpdateSourceStatus = baseUrl + "update_source_status";

    public static final String ValidateAadhaarPhone = baseUrl + "mitras_validate";

    public static final String RetailerDetail = baseUrl + "retailer_detail";
    public static final String DelearDetail = baseUrl + "dealer_detail";
    public static final String UploadNewMitraDocuments = baseUrl + "upload_new_mitra_documents/";
    public static final String UpdateMitra = baseUrl + "update_mitra";
    public static final String UpdateMitraProfile = baseUrl + "update_mitra_profile";
    public static final String Dealerslocators = baseUrl + "dealerslocators";

    public static final String GetDistrict = baseUrl + "get_district";
    public static final String GetArchiect = baseUrl + "get_district";
    public static final String VerifyMitraMobile = baseUrl + "verify_mitra_mobile";
    public static final String ResendMitraMobileOtp = baseUrl + "resend_mitra_mobile_otp";
    public static final String VerifyMitraMobileOtp = baseUrl + "verify_mitra_mobile_otp";

    public static final String Feedback_list = baseUrl + "feedback_list";
    public static final String Create_feedback = baseUrl + "create_feedback";
    public static final String Feedback_comment = baseUrl + "feedback_comment";
    //query
    public static final String Query_list = baseUrl + "query_list";
    public static final String Create_Query = baseUrl + "create_query";
    public static final String Query_comment = baseUrl + "query_comment";
   //Enquiry
    public static final String Enquiry_list = baseUrl + "enquiry_list";
    public static final String Create_enquiry = baseUrl + "create_enquiry";
    public static final String Enquiry_comment = baseUrl + "enquiry_comment";





    //public static String BaseUrl="http://172.16.25.95/emp/rest_api/";
         //public static String BaseUrl="http://centerserv.co.in/presto/rest_api/";3

    //public static String CLIENT_KEY = "ANDROIDAPP";
    //public static String CLIENT_SECRET = "ANRDOIDAPPZ10000000000000000000000000000000000000000";

    public static String CLIENT_KEY = "4923BD9F798E5390E455CEFDCD8474BFEEC6B828C8F585F34ABC47D7BBDA153E";
    public static String CLIENT_SECRET = "4923BD9F798E5390E455CEFDCD8474BFF0E1CBA1F65DED717A9E0E86F069BBAA";

    public static String PREF_NOTIFICATION_ID = "notification_id";
    public static String PREF_DESCRIPTION = "description";
    public static String PREF_FACEBOOk = "Facebook";
    public static String PREF_TWITTER = "Twitter";
    public static String PREF_YOUTUBE = "Youtube";
    public static String PREF_BEAN_MITRA_ITEM = "BeanMitra_Item";
    public static String PREF_DISCLAIMER_DESC = "disclaimer_desc";
    public static String PREF_USER_TYPE = "user_type";
    public static String PREF_CUSTOMER_ID = "customer_id";
    public static String PREF_MITRA_ID= "mitra_id";
    public static String PREF_EMPLOYEE_ID= "employee_id";
    public static String PREF_BEAN_MaterialTransaction = "beanMaterialTransaction_item";
    public static String PREF_NotificationHeader= "beanNotificationHeader";
    public static String PREFS_DISCLAMER= "prefs_disclamer";

    public static String PREF_MITRA_STATE= "state";
    public static String PREF_MITRA_DISTRICT= "district";
    public static String PREF_MITRA_TALUKA= "taluka";
    public static String PREF_MITRA_CITY= "city";
    public static String PREF_MITRA_TYPE= "mitra_type";
    public static String PREF_ROLE_TYPE= "role";





    public static String StatusAccept = "A";
    public static String StatusReject = "R";

    public static String ACTION_VIEW = "view";
    public static String ACTION_EDIT = "edit";
    public static String ACTION_ADD = "add";


    public static String MODULE_ORDERS = "ORDERS";
   // public static String MODULE_RECEIPTS = "MATERIALTRANSACTIONS";
    public static String MODULE_RECEIPTS = "RECEIPTS";
    public static String MODULE_CUSTOMERS = "CUSTOMERS";
    public static String MODULE_NOTIFICATIONS = "NOTIFICATIONS";
    public static String MODULE_EVENTS = "EVENTS";
    public static String MODULE_MITRAS = "MITRAS";
    public static String MODULE_COMMON = "COMMON";
    public static String MODULE_SCHEMES = "SCHEMES";
    public static String MODULE_REFERRALS = "REFERRALS";
    public static String MODULE_ANNOUNCEMENTS = "ANNOUNCEMENTS";


    public static String MODULE_TIPS = "TIPS";
    public static String MODULE_HELP = "HELPTOPICS";
    public static String MODULE_RATINGS = "RATINGS";
    public static String PREF_PHONE_FOR_REG = "mobile_number";
    public static String MODULE_TVCS = "TVCS";
    public static String MODULE_DEALERS = "DEALERS";
    public static String MODULE_RETAILERS = "RETAILERS";

    public static String MODULE_REGISTRATION_OTP = "REGISTRATION_OTP";

    public static String MODULE_MATERIALTRANSACTIONS = "MATERIALTRANSACTIONS";
    public static String MODULE_PAGE = "page_module";
    public static String FEEDBACKS = "FEEDBACKS";
    public static String MODULE_FEEDBACKS = "FEEDBACKS";
    public static String MODULE_QUERYS = "QUERIES";
    public static String MODULE_ENQUIRIES = "ENQUIRIES";


    public static String AadhaarImage = "aadhar_image";
    public static String AadhaarImage_back = "aadhar_image_back";
    public static String PanCard = "pan_card";
    public static String DrivingLicence = "driving_licence";
    public static String VoterId = "voter_id";

    public static String FragmentName = "";

    public static String ORG_ID = "102";

          public static void showToastMessage(Context context, String message){
         LayoutInflater inflater = LayoutInflater.from(context);
         View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) ((Activity) context).findViewById(R.id.custom_toast_layout));

        // set a message
        if (message.equals("Invalid Login Details"))
        {
            layout.setBackgroundResource(R.drawable.button_backgroundr);
        }else if (message.equals("please check Internet connection")){
            layout.setBackgroundColor(Color.DKGRAY);
        }
        TextView text = layout.findViewById(R.id.custom_toast_message);
        text.setText(message);
        // Toast.............
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();

    }
    public static   int PERMISSION_ALL = 999;

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    public static String dateFuncation(String date){

        String dateformatt = "";

        if (date.trim().length() == 0 || date.trim().equalsIgnoreCase("null")){
            return  "";
        }

        //DateFormat readFormat1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
        Date date1 = null;
        Calendar cal = Calendar.getInstance();

        try {
            date1 = readFormat1.parse(date);
            dateformatt = formatter1.format(date1);

        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }


        return  dateformatt;
    }

    public static String nullCheckFunction(String text){

        String value = "";

       /* if (text.length() == 0 || text.equalsIgnoreCase("null") || TextUtils.isEmpty(text) ||
                text.isEmpty() || TextUtils.equals(text ,"null") || text == null){
            return  "";
        }*/

        if (text.trim().length() == 0 || text.trim().equalsIgnoreCase("null") || TextUtils.isEmpty(text) ||
                text.isEmpty() || TextUtils.equals(text ,"null") || text == null){
            return  "";
        }

        try{

            value = text;

        }catch (Exception e){
            e.printStackTrace();
            return text;
        }

        return value;
    }

    public static void hideKeyboard(Activity context) {
        try {
            if (context == null) return;
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(context.getWindow().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static ListContainsBean checkListContains (ArrayList<LookupCategory> list, String name){
            for (LookupCategory item : list) {
                if (item.getCODE().equals(name)) {
                    return new ListContainsBean(true, item);
                }
            }
            return  new ListContainsBean(false, null);
        }

    public static void CheckInternet(Context context ) {

        FragmentActivity activity = (FragmentActivity) context;
        Fragment fragment = new fragment_CheckInternet();
        FragmentTransaction fragmentTransaction =activity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);
        fragmentTransaction.replace(R.id.container_body, fragment)
                .addToBackStack(null)
                .commit();
    }
}
