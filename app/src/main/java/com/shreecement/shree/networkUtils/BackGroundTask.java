package com.shreecement.shree.networkUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.WindowManager;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Utils;

/**
 * Created by chinmay on 15/01/18.
 */

public class BackGroundTask {

    public static class BackGroundTaskWithoutParam extends AsyncTask<Object, Void, ServiceResponse> {

        public OnTaskCompleted listener;
        private String url;
        private int method;
        private Context context;
        private ProgressDialog progressDialog;
        private Boolean network = true;

        public BackGroundTaskWithoutParam(Context context1, String url, int method, OnTaskCompleted listener) {
            context = context1;
            this.url = url;
            this.method = method;
            this.listener = listener;   //Assigning call back interface  through constructor
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialog.show(context, null, null);

            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.chart_transparent));
            d.setAlpha(200);
            progressDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progressDialog.getWindow().setBackgroundDrawable(d);
            progressDialog.setContentView(R.layout.progress_dialog);
            progressDialog.show();
        }

        @Override
        protected ServiceResponse doInBackground(Object... params) {

            if (NetworkUtil.isNetworkAvailable(context)) {
                network = true;
                synchronized (this) {

                    url = url.replaceAll(" ", "%20");
                    Log.i("Url Without Params: ", " ------> " + url);

                    try {
                        ServiceResponse serviceResponse = ServiceHandler.makeServiceCallWithoutParams(context, url, method);
                        Log.i("Response: ", " ------> " + serviceResponse.getResponse());
                        return serviceResponse;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.displayToastMessage(context, e.getMessage());
                        return null;
                    }
                }
            } else {
                Utils.displayToastMessage(context, context.getResources().getString(R.string.no_internet_retry));
                return null;
            }
        }

        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {

                if (result.getStatus().equals(ServiceHandler.STATUS_SUCCESS))
                    listener.onTaskCompleted(result.getResponse());
                else {
                    listener.onError(result.getResponse());
                }
            }
        }
    }

    public static class BackGroundTaskWithParam extends AsyncTask<Object, Void, ServiceResponse> {

        public OnTaskCompleted listener;
        private String url;
        private int method;
        private String parameter;
        private Context context;
        private ProgressDialog progressDialog;
        private Boolean network = true;

        public BackGroundTaskWithParam(Context context1, String url, int method, String parameter, int pageNumber, OnTaskCompleted listener) {
            this.context = context1;
            this.url = url;
            this.method = method;
            this.parameter = parameter;
            this.listener = listener;   //Assigning call back interface  through constructor
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, null, null);

            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.chart_transparent));
            d.setAlpha(200);
            progressDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progressDialog.getWindow().setBackgroundDrawable(d);
            progressDialog.setContentView(R.layout.progress_dialog);
            progressDialog.show();
        }

        @Override
        protected ServiceResponse doInBackground(Object... params) {

            if (NetworkUtil.isNetworkAvailable(context)) {
                network = true;
                synchronized (this) {
                    url = url.replaceAll(" ", "%20");
                    Log.i("Url With Parameters: ", " ------> " + url);
                    Log.i("Parameters: ", " ------> " + parameter);

                    try {
                        ServiceResponse serviceResponse = ServiceHandler.makeServiceCall(context, url, method, parameter);
                        Log.i("Response: ", " ------> " + serviceResponse.getResponse());
                        return serviceResponse;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.displayToastMessage(context, e.getMessage());
                        return null;
                    }

                }
            } else {
                Utils.displayToastMessage(context, context.getResources().getString(R.string.no_internet_retry));
                return null;
            }
        }

        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {
                if (result.getStatus().equals(ServiceHandler.STATUS_SUCCESS))
                    listener.onTaskCompleted(result.getResponse());
                else {
                    listener.onError(result.getResponse());
                }
            }
        }
    }

    public static class BackGroundTaskWithRawData extends AsyncTask<Object, Void, ServiceResponse> {

        public OnTaskCompleted listener;
        private String url;
        private String parameters;
        private Context context;
        private ProgressDialog progressDialog;
        private Boolean network = true;

        public BackGroundTaskWithRawData(Context context, String url, String parameters, OnTaskCompleted listener) {
            this.context = context;
            this.url = url;
            this.parameters = parameters;
            this.listener = listener;   //Assigning call back interface  through constructor
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, null, null);

            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.chart_transparent));
            d.setAlpha(200);
            progressDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progressDialog.getWindow().setBackgroundDrawable(d);
            progressDialog.setContentView(R.layout.progress_dialog);
            progressDialog.show();
        }

        @Override
        protected ServiceResponse doInBackground(Object... params) {

            if (NetworkUtil.isNetworkAvailable(context)) {
                network = true;
                synchronized (this) {

                    url = url.replaceAll(" ", "%20");
                    Log.i("Url With Raw: ", " ------> " + url);

                    try {
                        ServiceResponse serviceResponse = ServiceHandler.makeServiceCallWithRawData(context, url, parameters);
                        Log.i("Response: ", " ------> " + serviceResponse.getResponse());
                        return serviceResponse;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.displayToastMessage(context, e.getMessage());
                        return null;
                    }
                }
            } else {
                Utils.displayToastMessage(context, context.getResources().getString(R.string.no_internet_retry));
                return null;
            }
        }

        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {

                if (result.getStatus().equals(ServiceHandler.STATUS_SUCCESS))
                    listener.onTaskCompleted(result.getResponse());
                else {
                    listener.onError(result.getResponse());
                }
            }
        }
    }

    public static class BackGroundTaskGETWithHeader extends AsyncTask<Object, Void, ServiceResponse> {

        public OnTaskCompleted listener;
        private String url;
        private Context context;
        private ProgressDialog progressDialog;

        public BackGroundTaskGETWithHeader(Context context, String url, OnTaskCompleted listener) {
            this.context = context;
            this.url = url;
            this.listener = listener;   //Assigning call back interface  through constructor
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, null, null);

            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.chart_transparent));
            d.setAlpha(200);
            progressDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progressDialog.getWindow().setBackgroundDrawable(d);
            progressDialog.setContentView(R.layout.progress_dialog);
            progressDialog.show();
        }

        @Override
        protected ServiceResponse doInBackground(Object... params) {

            if (NetworkUtil.isNetworkAvailable(context)) {
                synchronized (this) {

                    url = url.replaceAll(" ", "%20");
                    Log.i("Url With HEADER: ", " ------> " + url);

                    try {
                        ServiceResponse serviceResponse = ServiceHandler.makeServiceCallGETWithHeader(context, url);
                        Log.i("Response: ", " ------> " + serviceResponse.getResponse());
                        return serviceResponse;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.displayToastMessage(context, e.getMessage());
                        return null;
                    }
                }
            } else {
                Utils.displayToastMessage(context, context.getResources().getString(R.string.no_internet_retry));
                return null;
            }
        }

        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {

                if (result.getStatus().equals(ServiceHandler.STATUS_SUCCESS))
                    listener.onTaskCompleted(result.getResponse());
                else {
                    listener.onError(result.getResponse());
                }
            }
        }
    }

    public static class BackGroundTaskPOSTWithHeader extends AsyncTask<Object, Void, ServiceResponse> {

        public OnTaskCompleted listener;
        private String url;
        private String params;
        private Context context;
        private ProgressDialog progressDialog;

        public BackGroundTaskPOSTWithHeader(Context context, String url, String params, OnTaskCompleted listener) {
            this.context = context;
            this.url = url;
            this.params = params;
            this.listener = listener;   //Assigning call back interface  through constructor
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, null, null);

            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.chart_transparent));
            d.setAlpha(200);
            progressDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progressDialog.getWindow().setBackgroundDrawable(d);
            progressDialog.setContentView(R.layout.progress_dialog);
            progressDialog.show();
        }

        @Override
        protected ServiceResponse doInBackground(Object... params) {

            if (NetworkUtil.isNetworkAvailable(context)) {
                synchronized (this) {

                    url = url.replaceAll(" ", "%20");
                    Log.i("Url With HEADER: ", " ------> " + url);

                    try {
                        ServiceResponse serviceResponse = ServiceHandler.makeServiceCallPOSTWithHeader(context, url, this.params);
                        Log.i("Response: ", " ------> " + serviceResponse.getResponse());
                        return serviceResponse;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.displayToastMessage(context, e.getMessage());
                        return null;
                    }
                }
            } else {
                Utils.displayToastMessage(context, context.getResources().getString(R.string.no_internet_retry));
                return null;
            }
        }

        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {

                if (result.getStatus().equals(ServiceHandler.STATUS_SUCCESS))
                    listener.onTaskCompleted(result.getResponse());
                else {
                    listener.onError(result.getResponse());
                }
            }
        }
    }

    public static class makeServiceCallPOSTWithCommonHeader extends AsyncTask<Object, Void, ServiceResponse> {

        public OnTaskCompleted listener;
        private String url;
        private String params;
        private Context context;
        private ProgressDialog progressDialog;

        public makeServiceCallPOSTWithCommonHeader(Context context, String url, String params, OnTaskCompleted listener) {
            this.context = context;
            this.url = url;
            this.params = params;
            this.listener = listener;   //Assigning call back interface  through constructor
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, null, null);

            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.chart_transparent));
            d.setAlpha(200);
            progressDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progressDialog.getWindow().setBackgroundDrawable(d);
            progressDialog.setContentView(R.layout.progress_dialog);
            progressDialog.show();
        }

        @Override
        protected ServiceResponse doInBackground(Object... params) {

            if (NetworkUtil.isNetworkAvailable(context)) {
                synchronized (this) {

                    url = url.replaceAll(" ", "%20");
                    Log.i("Url With HEADER: ", " ------> " + url);

                    try {
                        ServiceResponse serviceResponse = ServiceHandler.makeServiceCallPOSTWithCommonHeader(context, url, this.params);
                        Log.i("Response: ", " ------> " + serviceResponse.getResponse());
                        return serviceResponse;
                    } catch (Exception e) {
                        e.printStackTrace();
                       // Utils.displayToastMessage(context, e.getMessage());
                        return null;
                    }
                }
            } else {
                Utils.displayToastMessage(context, context.getResources().getString(R.string.no_internet_retry));
                return null;
            }
        }

        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {

                if (result.getStatus().equals(ServiceHandler.STATUS_SUCCESS))
                    listener.onTaskCompleted(result.getResponse());
                else {
                    listener.onError(result.getResponse());
                }
            }
        }
    }

    public static class makeServiceCallPOSTWithCustomHeader extends AsyncTask<Object, Void, ServiceResponse> {

        public OnTaskCompleted listener;
        private String url;
        private String params;
        private String page_module;
        private String action;
        private Context context;
        private ProgressDialog progressDialog;

        public makeServiceCallPOSTWithCustomHeader(Context context, String url, String params, String page_module, String action,OnTaskCompleted listener) {
            this.context = context;
            this.url = url;
            this.params = params;
            this.page_module = page_module;
            this.action = action;
            this.listener = listener;   //Assigning call back interface  through constructor
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, null, null);

            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.chart_transparent));
            d.setAlpha(200);
            progressDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progressDialog.getWindow().setBackgroundDrawable(d);
            progressDialog.setContentView(R.layout.progress_dialog);
            progressDialog.show();
        }

        @Override
        protected ServiceResponse doInBackground(Object... params) {

            if (NetworkUtil.isNetworkAvailable(context)) {
                synchronized (this) {

                    url = url.replaceAll(" ", "%20");
                    Log.i("Url With HEADER: ", " ------> " + url);

                    try {
                        ServiceResponse serviceResponse = ServiceHandler.makeServiceCallPOSTWithCustomHeader(context, url, this.params, this.page_module, this.action);
                        Log.i("Response: ", " ------> " + serviceResponse.getResponse());
                        return serviceResponse;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.displayToastMessage(context, e.getMessage());
                        return null;
                    }
                }
            } else {
                Utils.displayToastMessage(context, context.getResources().getString(R.string.no_internet_retry));
                return null;
            }
        }

        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {

                if (result.getStatus().equals(ServiceHandler.STATUS_SUCCESS))
                    listener.onTaskCompleted(result.getResponse());
                else {
                    listener.onError(result.getResponse());
                }
            }
        }
    }

    public static class BackGroundTaskWithMultiPart extends AsyncTask<Object, Void, ServiceResponse> {

        public OnTaskCompleted listener;
        String filePath;
        String fileKey;
        private String url;
        private String parameters;
        private Context context;
        private ProgressDialog progressDialog;
        private Boolean network = true;

        public BackGroundTaskWithMultiPart(Context context, String url, String parameters, String filePath, String fileKey, OnTaskCompleted listener) {
            this.context = context;
            this.url = url;
            this.parameters = parameters;
            this.filePath = filePath;
            this.fileKey = fileKey;
            this.listener = listener;   //Assigning call back interface  through constructor
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, null, null);

            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.chart_transparent));
            d.setAlpha(200);
            progressDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progressDialog.getWindow().setBackgroundDrawable(d);
            progressDialog.setContentView(R.layout.progress_dialog);
            progressDialog.show();
        }

        @Override
        protected ServiceResponse doInBackground(Object... params) {

            if (NetworkUtil.isNetworkAvailable(context)) {
                network = true;
                synchronized (this) {

                    url = url.replaceAll(" ", "%20");
                    Log.i("Url With Raw: ", " ------> " + url);

                    try {
                        ServiceResponse serviceResponse = ServiceHandler.makeServiceCallMultiPart(context, url, parameters, filePath, fileKey);
                        Log.i("Response: ", " ------> " + serviceResponse.getResponse());
                        return serviceResponse;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.displayToastMessage(context, e.getMessage());
                        return null;
                    }
                }
            } else {
                Utils.displayToastMessage(context, context.getResources().getString(R.string.no_internet_retry));
                return null;
            }
        }

        @Override
        protected void onPostExecute(ServiceResponse result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {
                if (result.getStatus().equals(ServiceHandler.STATUS_SUCCESS))
                    listener.onTaskCompleted(result.getResponse());
                else {
                    listener.onError(result.getResponse());
                }
            }
        }
    }
}