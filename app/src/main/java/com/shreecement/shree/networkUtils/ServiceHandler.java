package com.shreecement.shree.networkUtils;


import android.content.Context;
import android.util.Log;

import com.shreecement.shree.Utlility.StorePrefs;
import com.shreecement.shree.Utlility.Utils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;


/**
 * Created by chinmay on 15/01/18.
 */

public class ServiceHandler {

    public static final String TAG = "ServiceHandler";

    public final static int GET = 1;
    public final static int POST = 2;
    public final static int NO_PARAMS = 1;
    public final static int HAS_PARAMS = 2;

    public final static String STATUS_SUCCESS = "success";
    public final static String STATUS_ERROR = "error";
    public final static String RESPONSE_STATUS = "status";
    public final static String RESPONSE_MESSAGE = "message";
    public final static String RESPONSE_DATA = "data";
    public final static String RESPONSE_ID = "id";


    public ServiceHandler() {

    }

    /**
     * Making service call
     *
     * @url - url to make request
     * @method - http request method
     */

    public static ServiceResponse makeServiceCallWithoutParams(Context context, String url, int method) {
        // http client
        OkHttpClient client = new OkHttpClient();
        Request request = null;
        String responseBody;
        int responseCode = 0;

        try {
            // Checking http request method type
            if (method == POST) {

                RequestBody body = RequestBody.create(null, new byte[0]);
                request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .addHeader("content-type", "application/x-www-form-urlencoded")
                        .addHeader("cache-control", "no-cache")
                        .addHeader("postman-token", "453daeff-8e17-3822-0857-2a80cc717bc3")
                        .build();

            } else if (method == GET) {

                request = new Request.Builder()
                        .url(url)
                        .get()
                        .addHeader("content-type", "application/x-www-form-urlencoded")
                        .addHeader("cache-control", "no-cache")
                        .addHeader("postman-token", "b1bb6a03-f53a-9735-3483-72e087e93907")
                        .build();

            }
        } catch (Exception e) {
            e.printStackTrace();
            Utils.displayToastMessage(context, e.getMessage());
            return null;
        }


        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
            responseCode = response.code();

        } catch (IOException e) {
            e.printStackTrace();
            return new ServiceResponse(responseCode, STATUS_ERROR, e.getMessage());
        }
        return new ServiceResponse(responseCode, STATUS_SUCCESS, responseBody);

    }

    /**
     * Making service call
     *
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     */
    public static ServiceResponse makeServiceCall(Context context, String url, int method, String params) {
        // http client
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        Request request = null;
        String responseBody;
        int responseCode = 0;

        try {

            // Checking http request method type
            if (method == POST) {
                RequestBody body = RequestBody.create(mediaType, params);
                request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .addHeader("content-type", "application/x-www-form-urlencoded")
                        .addHeader("cache-control", "no-cache")
                        .addHeader("postman-token", "453daeff-8e17-3822-0857-2a80cc717bc3")
                        .build();

            } else if (method == GET) {

                request = new Request.Builder()
                        .url(url + params)
                        .get()
                        .addHeader("content-type", "application/x-www-form-urlencoded")
                        .addHeader("cache-control", "no-cache")
                        .addHeader("postman-token", "b1bb6a03-f53a-9735-3483-72e087e93907")
                        .build();
            }
        } catch (Exception e) {
            e.printStackTrace();

            Utils.displayToastMessage(context, e.getMessage());
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }

        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
            responseCode = response.code();

        } catch (IOException e) {
            e.printStackTrace();
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }
        return new ServiceResponse(responseCode, STATUS_SUCCESS, responseBody);
    }

    public static ServiceResponse makeServiceCallWithRawData(Context context, String url, String params) {
        // http client
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        Request request;
        String responseBody;
        int responseCode = 0;

        try {
            RequestBody body = RequestBody.create(mediaType, params);
            request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .addHeader("content-type", "application/json")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "e599cda7-feb5-25f8-f9d9-a7ef22b44f65")
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
            Utils.displayToastMessage(context, e.getMessage());
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }

        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
            responseCode = response.code();

        } catch (IOException e) {
            e.printStackTrace();
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }
        return new ServiceResponse(responseCode, STATUS_SUCCESS, responseBody);
    }

    public static ServiceResponse makeServiceCallGETWithHeader(Context context, String url) {
        // http client
        OkHttpClient client = new OkHttpClient();
        Request request;
        String responseBody;
        int responseCode = 0;

        try {
            request = new Request.Builder()
                    .url(url)
                    .get()
                    .addHeader("content-type", "application/json")
                    //.addHeader("x-app-id", Constant.nutritionAppId)
                    //.addHeader("x-app-key", Constant.nutritionAppKey)
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "6fbba77b-07fb-bf44-2302-391e21c5c584")
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            Utils.displayToastMessage(context, e.getMessage());
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }

        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
            responseCode = response.code();

        } catch (IOException e) {
            e.printStackTrace();
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }
        return new ServiceResponse(responseCode, STATUS_SUCCESS, responseBody);
    }

    public static ServiceResponse makeServiceCallPOSTWithHeader(Context context, String url, String params) {
        // http client
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        Request request;
        String responseBody;
        int responseCode = 0;

        try {
            RequestBody body = RequestBody.create(mediaType, params);
            request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .addHeader("content-type", "application/json")
                    //.addHeader("x-app-id", Constant.nutritionAppId)
                    //.addHeader("x-app-key", Constant.nutritionAppKey)
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "6fbba77b-07fb-bf44-2302-391e21c5c584")
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            Utils.displayToastMessage(context, e.getMessage());
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }

        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
            responseCode = response.code();

        } catch (IOException e) {
            e.printStackTrace();
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }
        return new ServiceResponse(responseCode, STATUS_SUCCESS, responseBody);
    }

    public static ServiceResponse makeServiceCallPOSTWithCommonHeader(Context context, String url, String params) {
        // http client
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        Request request;
        String responseBody;
        int responseCode = 0;

        try {
            RequestBody body = RequestBody.create(mediaType, params);
            request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .addHeader("content-type", "application/json")
                    //.addHeader("x-app-id", Constant.nutritionAppId)
                    .addHeader("page_module", MODULE_COMMON)
                    .addHeader("token",  StorePrefs.getDefaults("token", context))
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "6fbba77b-07fb-bf44-2302-391e21c5c584")
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            //Utils.displayToastMessage(context, e.getMessage());
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }

        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
            responseCode = response.code();

        } catch (IOException e) {
            e.printStackTrace();
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }
        return new ServiceResponse(responseCode, STATUS_SUCCESS, responseBody);
    }

    public static ServiceResponse makeServiceCallPOSTWithCustomHeader(Context context, String url, String params, String page_module, String action) {
        // http client
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        Request request;
        String responseBody;
        int responseCode = 0;

        try {
            RequestBody body = RequestBody.create(mediaType, params);
            request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .addHeader("content-type", "application/json")
                    //.addHeader("x-app-id", Constant.nutritionAppId)
                    .addHeader("page_module", page_module)
                    .addHeader("action", action)
                    .addHeader("token",  StorePrefs.getDefaults("token", context))
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "6fbba77b-07fb-bf44-2302-391e21c5c584")
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            Utils.displayToastMessage(context, e.getMessage());
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }

        try {
            Response response = client.newCall(request).execute();
            responseBody = response.body().string();
            responseCode = response.code();

        } catch (IOException e) {
            e.printStackTrace();
            return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
        }
        return new ServiceResponse(responseCode, STATUS_SUCCESS, responseBody);
    }

    public static ServiceResponse makeServiceCallMultiPart(Context context, String url, String params, String filePath, String fileKey) {
        // http client
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(120, java.util.concurrent.TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(120, java.util.concurrent.TimeUnit.SECONDS);
        client.setWriteTimeout(120, java.util.concurrent.TimeUnit.SECONDS);
        String path = filePath.replaceAll(" ", "%20");
        File uploadFile = new File(path);
        String fileName = uploadFile.getName();
        Log.d(TAG, " File Path ----------> " + path);
        Log.d(TAG, " File Name ----------> " + fileName);
        Log.d(TAG, " File Exists ----------> " + uploadFile.exists());
        final MediaType MEDIA_TYPE = MediaType.parse("application/pdf");
        RequestBody requestBody;
        Request request;
        String responseBody;
        int responseCode = 0;

        if (uploadFile.exists()) {
            try {
                requestBody = new MultipartBuilder()
                        .type(MultipartBuilder.FORM)
                        .addFormDataPart(fileKey, fileName,
                                RequestBody.create(MEDIA_TYPE, uploadFile))
                       // .addFormDataPart("user_id", StorePrefs.getDefaults(StorePrefs.PREFS_USER_ID, context))
                        .build();

                request = new Request.Builder()
                        .url(url)
                        .post(requestBody)
                        .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                        .addHeader("Content-Type", "application/x-www-form-urlencoded")
                        .addHeader("Cache-Control", "no-cache")
                        .addHeader("Postman-Token", "2c126362-1839-818d-1b67-d0b1eb53719e")
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.displayToastMessage(context, e.getMessage());
                return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
            }

            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
                responseCode = response.code();

            } catch (IOException e) {
                e.printStackTrace();
                return new ServiceResponse(responseCode, STATUS_ERROR, e.toString());
            }
            return new ServiceResponse(responseCode, STATUS_SUCCESS, responseBody);
        }
        Utils.displayToastMessage(context, "File Not Found!");
        return new ServiceResponse(responseCode, STATUS_ERROR, "File Not Found!");
    }
}