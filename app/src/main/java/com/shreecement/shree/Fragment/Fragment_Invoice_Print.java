package com.shreecement.shree.Fragment;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.shreecement.shree.Adapter.Spin_Custom_adapter;
import com.shreecement.shree.Adapter.SpinnerDialogAdapter;
import com.shreecement.shree.R;

import java.util.ArrayList;


/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Invoice_Print extends Fragment {
    public static AlertDialog.Builder dialogBuilder;
    public static AlertDialog builder4;
    public static ListView CategoryList;
    public static SpinnerDialogAdapter spinnerAdapter;
    String className = "Fragment_Invoice_Print";
    TextView textview2;
    Button btnPrintView, btnPrint;
    TextView dialogTitle;
    TextView spnDi;
    ArrayList<String> diList = new ArrayList<String>();
    Spin_Custom_adapter spinCustomAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_print_invoice, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        spnDi = (TextView) view.findViewById(R.id.spnDi);
        getActivity().setTitle(getActivity().getString(R.string.invoice_printing));

        btnPrintView = (Button) view.findViewById(R.id.btnPrintView);
        btnPrint = (Button) view.findViewById(R.id.btnPrint);


        btnPrintView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new Fragment_PrintView();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new Fragment_ThannkYou();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });


        spnDi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                diList.clear();
                diList.add("Di #1");
                diList.add("Di #2");
                diList.add("Di #3");
                // showSpinnerDialog(diList,"Select DI",spnDi);
            }
        });


        return view;
    }

    private void showSpinnerDialog(ArrayList<String> list, String title, TextView spnType) {
     /*   dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        listView = (ListView) dialogView.findViewById(R.id.listCatecory);
        dialogTitle = (TextView)dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerAdapter = new SpinnerDialogAdapter(getActivity(), list,spnType,className);
        listView.setAdapter(spinnerAdapter);
        builder4 = dialogBuilder.create();
        builder4.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder4.show();*/
    }
}
