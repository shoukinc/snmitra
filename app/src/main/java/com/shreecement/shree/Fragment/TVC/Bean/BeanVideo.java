package com.shreecement.shree.Fragment.TVC.Bean;


public class BeanVideo {
    String application_id, long_description, short_description, status_code,title, tvcs_id, tvcs_number, youtube_url;

    public BeanVideo() {
    }

    public BeanVideo(String application_id, String long_description, String short_description, String status_code, String title, String tvcs_id, String tvcs_number, String youtube_url) {
        this.application_id = application_id;
        this.long_description = long_description;
        this.short_description = short_description;
        this.status_code = status_code;
        this.title = title;
        this.tvcs_id = tvcs_id;
        this.tvcs_number = tvcs_number;
        this.youtube_url = youtube_url;
    }

    public String getApplication_id() {
        return application_id;
    }

    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }

    public String getLong_description() {
        return long_description;
    }

    public void setLong_description(String long_description) {
        this.long_description = long_description;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTvcs_id() {
        return tvcs_id;
    }

    public void setTvcs_id(String tvcs_id) {
        this.tvcs_id = tvcs_id;
    }

    public String getTvcs_number() {
        return tvcs_number;
    }

    public void setTvcs_number(String tvcs_number) {
        this.tvcs_number = tvcs_number;
    }

    public String getYoutube_url() {
        return youtube_url;
    }

    public void setYoutube_url(String youtube_url) {
        this.youtube_url = youtube_url;
    }
}
