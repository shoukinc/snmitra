package com.shreecement.shree.Fragment.TVC;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Fragment.TVC.Bean.BeanVideo;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;


/**
 * Created by Choudhary on 6/29/2017.
 */

public class Fragment_TVCSVideoPlayer extends Fragment {
    TextView tvName,tvEmail;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_video_player, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getActivity().setTitle(getActivity().getString(R.string.nav_item_tvcs));

        Gson gson = new Gson();
        String json = StorePrefs.getDefaults("beanVideo", getActivity());
        BeanVideo beanVideo = gson.fromJson(json, BeanVideo.class);

       /* tvNameNumber.setText( beanCustomer_item.getCust_account_id()   + " & "+beanCustomer_item.getParty_name());
        tvCityState.setText(beanCustomer_item.getCity()+ " & "+ beanCustomer_item.getState());*/

      String strUrl = "https://www.youtube.com/embed/"+beanVideo.getYoutube_url();


       /* String urlVideo = strUrl;
        VideoView video = (VideoView)view.findViewById(R.id.VideoView01);
        Log.e("Url-" , urlVideo);
        video.setVideoURI(Uri.parse(urlVideo));
        MediaController mc = new MediaController(getActivity());
        video.setMediaController(mc);
        video.requestFocus();
        video.start();
        mc.show();
*/
        WebView webview = (WebView) view.findViewById(R.id.mWebView);
        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadUrl(strUrl);

        return view;

    }



}
