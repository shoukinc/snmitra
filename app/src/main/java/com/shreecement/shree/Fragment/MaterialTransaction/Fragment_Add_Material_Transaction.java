package com.shreecement.shree.Fragment.MaterialTransaction;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Fragment.MaterialTransaction.Bean.MitraBeanSearch;
import com.shreecement.shree.Fragment.MaterialTransaction.PostClass.PostClassCreateMaterialTransaction;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.Architectlist;
import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.Districtlist;
import static com.shreecement.shree.Utlility.Constant.GetDistrict;
import static com.shreecement.shree.Utlility.Constant.GetpackingBagType;
import static com.shreecement.shree.Utlility.Constant.Getstock;
import static com.shreecement.shree.Utlility.Constant.Lookup_data;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.MODULE_MATERIALTRANSACTIONS;
import static com.shreecement.shree.Utlility.Constant.MaterialDealersList;
import static com.shreecement.shree.Utlility.Constant.MaterialMitraList;
import static com.shreecement.shree.Utlility.Constant.MaterialRetailerslist;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_CUSTOMER_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.orders_listUrl;
import static com.shreecement.shree.Utlility.Constant.showToastMessage;

/*
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Add_Material_Transaction extends Fragment
        implements View.OnClickListener{

    String keyword = "";
    int page = 1;
    EditText  edComments;
    boolean isPagination = false;
    Boolean isEditable = false;
    Boolean isNameMandotry = false;
    Boolean showCode = false;
    public CustomerSearchAdapter customerSearchAdapter;


    TextView tvUserType,tvRMNameTitle,tvRMName,tvProduct,tvPackingType,tvDate,
            tvDistrict, tvIsArchitect,
            tvOnHandStock, tvQty;
    CheckBox cbIsArchitect;
    Button btnSave;
    String date = "";

    TextView dialogTitle;
    ListView listView;
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog builder3;

    public ArrayList<LookupCategory> TransactionTypeList;
    public ArrayList<LookupCategory> ProductList;
    public ArrayList<LookupCategory> PackingBagTypeList;
    public ArrayList<LookupCategory> DataList;
    public ArrayList<LookupCategory> DistrictList;
    public ArrayList<LookupCategory> ArchitectList;
    public ArrayList<MitraBeanSearch> mitraSearchList;

    ListView cusSearchList;
    ArrayList<MitraBeanSearch> tempmitraSearchList;

    public SpinnerDialogAdapter spinnerAdapter;

    String userTypeCode = "";
    String architectCode = "";
    String productCode = "";
    String packingBagCode = "";
    String partyIdCode = " ";
    String out_district = "";
    String out_state = "";
    String nameHeading = "";

    String urlToPass;

    String customer_id, mitra_id, strStock;
    String architect_involved_flag = "N";
    Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_material_transaction_entery, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        dialog = new Dialog(getActivity());

        keyword = "";

        if(!StorePrefs.getDefaults(PREF_MITRA_ID,getActivity()).equalsIgnoreCase("null")){
            mitra_id = StorePrefs.getDefaults(PREF_MITRA_ID,getActivity());
        }
        else
        {
            mitra_id = "";
        }
        if(!StorePrefs.getDefaults(PREF_CUSTOMER_ID,getActivity()).equalsIgnoreCase("null")){
            customer_id = StorePrefs.getDefaults(PREF_CUSTOMER_ID,getActivity());
        }
        else
        {
            customer_id = "";
        }


        getActivity().setTitle(getActivity().getString(R.string.add_material_transaction));


        edComments = view.findViewById(R.id.edComments);
        cbIsArchitect = view.findViewById(R.id.cbIsArchitect);

        tvQty = view.findViewById(R.id.tvQty);
        tvUserType = view.findViewById(R.id.tvUserType);
        tvRMNameTitle = view.findViewById(R.id.tvRMNameTitle);
        tvRMName = view.findViewById(R.id.tvRMName);
        tvProduct = view.findViewById(R.id.tvProduct);
        tvPackingType = view.findViewById(R.id.tvPackingType);
        tvDate = view.findViewById(R.id.tvDate);
        tvDistrict = view.findViewById(R.id.tvDistrict);
        tvIsArchitect = view.findViewById(R.id.tvIsArchitect);
        tvOnHandStock = view.findViewById(R.id.tvOnHandStock);

        btnSave = view.findViewById(R.id.btnSave);

        //clickListner
        tvUserType.setOnClickListener(this);
        tvRMName.setOnClickListener(this);
        tvProduct.setOnClickListener(this);
        tvPackingType.setOnClickListener(this);
        tvDate.setOnClickListener(this);
        tvDistrict.setOnClickListener(this);
        tvIsArchitect.setOnClickListener(this);
        tvQty.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        TransactionTypeList = new ArrayList<LookupCategory>();
        ProductList = new ArrayList<LookupCategory>();
        PackingBagTypeList = new ArrayList<LookupCategory>();
        DataList = new ArrayList<LookupCategory>();
        DistrictList = new ArrayList<LookupCategory>();
        ArchitectList = new ArrayList<LookupCategory>();

        mitraSearchList = new ArrayList<MitraBeanSearch>();
        tempmitraSearchList = new ArrayList<MitraBeanSearch>();

        new PostClassLookUpData(getActivity()).execute();
        //new PostClassPackingBagTypeList(getActivity()).execute();


        cbIsArchitect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // checkbox status is changed from uncheck to checked.
                if (!isChecked) {
                    // hide
                    tvIsArchitect.setVisibility(View.GONE);
                    architectCode = "";
                    tvIsArchitect.setText("");
                    architect_involved_flag = "N";
                } else {
                    // show
                    tvIsArchitect.setVisibility(View.VISIBLE);
                    architect_involved_flag = "Y";
                   // new PostClassGettingArchitect(getActivity()).execute();
                }
            }
        });

        return view;
    }

    @Override
    public void onClick(View view) {
        if (view == tvUserType) {

            showSpinnerDialog(TransactionTypeList, "Transaction Type");
            showCode = false;
        }
        if (view == tvProduct) {
            showSpinnerDialog(ProductList, "Product");
            showCode = false;
        }
        if (view == tvPackingType) {
            showSpinnerDialog(PackingBagTypeList, "Packing Bag Type");
            showCode = false;
        }
        if (view == tvDistrict) {
            if(isEditable) {
                Toast.makeText(getActivity(), "Editable", Toast.LENGTH_SHORT).show();
                showSpinnerDialog(DistrictList, "District");
                showCode = false;
            }else {
                Toast.makeText(getActivity(), "Can not edit.", Toast.LENGTH_SHORT).show();
            }
        }
        if (view == tvRMName) {
           // showSpinnerDialog(DataList, "Name");

           // showCustomerSearchDialog(DataList,nameHeading);
            if(nameHeading.equalsIgnoreCase("Mitras")){
                showCustomerSearchDialog("Mitras",MaterialMitraList);
                showCode = true;
            }
            else {
                showCustomerSearchDialog(DataList, "Name");
                showCode = true;
            }

        }
        if (view == tvIsArchitect) {

            //showSpinnerDialog(ArchitectList, "Architect");
            showCustomerSearchDialog("Architect",Architectlist);
            showCode = false;

        }
        if (view == tvQty){

            dialog.getWindow();

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_with_edittext_and_button);
            dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

            TextView txtHeading;
            Button btnCancelDialogTop , btnDialogOk;
            final EditText edInputField;

            btnCancelDialogTop = (Button) dialog.findViewById(R.id.btnCancelDialogTop);
            btnDialogOk = (Button) dialog.findViewById(R.id.btnDialogOk);
            txtHeading  = dialog.findViewById(R.id.txtHeading);
            edInputField  = dialog.findViewById(R.id.edInputField);
            txtHeading.setText(R.string.quantity_bags);
            btnDialogOk.setText(R.string.submit);


            int maxLength = 10;
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            edInputField.setFilters(FilterArray);

            dialog.show();

            btnCancelDialogTop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });
            btnDialogOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    tvQty.setText(edInputField.getText().toString().trim());

                    if (tvUserType.getText().toString().length() == 0) {
                        Toast.makeText(getActivity(), "Transaction type is required!", Toast.LENGTH_LONG).show();

                    } else if (userTypeCode.trim().length() == 0) {
                        Toast.makeText(getActivity(), "Title field is required!", Toast.LENGTH_LONG).show();

                    }
                    /*else if(isNameMandotry) {
                          if (tvRMName.getText().toString().length() == 0) {
                            Toast.makeText(getActivity(), "Name field is required!", Toast.LENGTH_LONG).show();

                        }
                    }*/


                    else if (tvProduct.getText().toString().length() == 0) {
                        Toast.makeText(getActivity(), "Product field is required!", Toast.LENGTH_LONG).show();

                    } else if (tvPackingType.getText().toString().length() == 0) {
                        Toast.makeText(getActivity(), "Packing Type field is required!", Toast.LENGTH_LONG).show();

                    } else if (tvDate.getText().toString().length() == 0) {
                        Toast.makeText(getActivity(), "Date field is required!", Toast.LENGTH_LONG).show();

                    } else if (tvQty.getText().toString().length() == 0) {
                        Toast.makeText(getActivity(), "Quantity field is required!", Toast.LENGTH_LONG).show();

                    } else {
                        new PostClassGetStock(getActivity(), userTypeCode,
                                StorePrefs.getDefaults(PREF_USER_TYPE, getActivity()), tvProduct.getText().toString().trim(),
                                userTypeCode, partyIdCode, productCode,
                                tvPackingType.getText().toString().trim(), edInputField.getText().toString().trim(),
                                date.trim(), mitra_id,
                                customer_id, edComments.getText().toString().trim(),
                                Constant.nullCheckFunction(tvDistrict.getText().toString())
                        ).execute();

                    }
                    dialog.dismiss();

                }
            });
        }

        if (view == btnSave){

            Log.e("Save button clicked = >"," SAVE");
           /* new PostClassCreateMaterialTransaction(getActivity(),userTypeCode,
                    StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()), tvProduct.getText().toString().trim(),
                    userTypeCode, partyIdCode,  productCode,
                    tvPackingType.getText().toString().trim(), edQty.getText().toString().trim(),
                    date.trim(),  mitra_id,
                    customer_id,edComments.getText().toString().trim(),
                    Constant.nullCheckFunction(tvDistrict.getText().toString())
            ).execute();*/


           /* if (architect_involved_flag.equalsIgnoreCase("Y")) {
                if (tvIsArchitect.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Architect field is required!", Toast.LENGTH_LONG).show();
                    Log.e("In if part","If");

                }
                else if (architectCode.trim().length() == 0) {
                    Toast.makeText(getActivity(), "Architect field is required!", Toast.LENGTH_LONG).show();
                    Log.e("In else part","else");

                }
            }*/

            if (cbIsArchitect.isChecked()){

                Log.e("1==> ","1");
                if (tvIsArchitect.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Architect field is required!", Toast.LENGTH_LONG).show();
                    Log.e("In if part==> ","If");
                    Log.e("2==> ","2");
                }

                else if (tvUserType.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Transaction type is required!", Toast.LENGTH_LONG).show();
                    Log.e("3=> ","4");

                } else if (userTypeCode.trim().length() == 0) {
                    Toast.makeText(getActivity(), "Title field is required!", Toast.LENGTH_LONG).show();
                    Log.e("5==> ","5");

                }/*else if (tvRMName.getText().toString().length() == 0) {
                Toast.makeText(getActivity(), "Name field is required!", Toast.LENGTH_LONG).show();

            }*/
           /* else if(isNameMandotry) {
                if (tvRMName.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Name field is required!", Toast.LENGTH_LONG).show();

                }
            }*/
                else if (tvProduct.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Product field is required!", Toast.LENGTH_LONG).show();
                    Log.e("6==> ","6");

                } else if (tvPackingType.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Packing Type field is required!", Toast.LENGTH_LONG).show();
                    Log.e("7==> ","7");

                } else if (tvDate.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Date field is required!", Toast.LENGTH_LONG).show();
                    Log.e("8==> ","8");

                } else if (tvQty.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Quantity field is required!", Toast.LENGTH_LONG).show();
                    Log.e("9==> ","9");
                }
                else {
                    Log.e("In else part==> ","else");
                    Log.e("10==> ","10");

                    new PostClassCreateMaterialTransaction(getActivity(), userTypeCode,
                            StorePrefs.getDefaults(PREF_USER_TYPE, getActivity()), tvProduct.getText().toString().trim(),
                            userTypeCode, partyIdCode, productCode,
                            tvPackingType.getText().toString().trim(), tvQty.getText().toString().trim(),
                            date.trim(), mitra_id,
                            customer_id, edComments.getText().toString().trim(),
                            Constant.nullCheckFunction(tvDistrict.getText().toString()), architect_involved_flag, architectCode
                    ).execute();
                    showCode = false;
                }

            }

            // without Architect

            else {

            if (tvUserType.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Transaction type is required!", Toast.LENGTH_LONG).show();
                    Log.e("else 1=> ", "1");

                } else if (userTypeCode.trim().length() == 0) {
                    Toast.makeText(getActivity(), "Title field is required!", Toast.LENGTH_LONG).show();
                    Log.e("else 2==> ", "2");

                }/*else if (tvRMName.getText().toString().length() == 0) {
                Toast.makeText(getActivity(), "Name field is required!", Toast.LENGTH_LONG).show();

            }*/
           /* else if(isNameMandotry) {
                if (tvRMName.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Name field is required!", Toast.LENGTH_LONG).show();

                }
            }*/
                else if (tvProduct.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Product field is required!", Toast.LENGTH_LONG).show();
                    Log.e("else 3==> ", "3");

                } else if (tvPackingType.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Packing Type field is required!", Toast.LENGTH_LONG).show();
                    Log.e("else 4==> ", "4");

                } else if (tvDate.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Date field is required!", Toast.LENGTH_LONG).show();
                    Log.e("else 5==> ", "5");

                } else if (tvQty.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Quantity field is required!", Toast.LENGTH_LONG).show();
                    Log.e("else 6==> ", "6");
                } else {
                    Log.e("In else part==> ", "else");
                    Log.e("else 7==> ", "7");

                    new PostClassCreateMaterialTransaction(getActivity(), userTypeCode,
                            StorePrefs.getDefaults(PREF_USER_TYPE, getActivity()), tvProduct.getText().toString().trim(),
                            userTypeCode, partyIdCode, productCode,
                            tvPackingType.getText().toString().trim(), tvQty.getText().toString().trim(),
                            date.trim(), mitra_id,
                            customer_id, edComments.getText().toString().trim(),
                            Constant.nullCheckFunction(tvDistrict.getText().toString()), architect_involved_flag, architectCode
                    ).execute();
                    showCode = false;
                }
            }
        }
        if (view == tvDate) {
            showDialog();
            showCode = false;
        }
    }

    private void showCustomerSearchDialog(String title, String UrlToPassInSearchList ) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_customer_search_listview, null);
        dialogBuilder.setView(dialogView);
        final EditText edCustomerSearch = dialogView.findViewById(R.id.customSearch);
        Button btnCancel = dialogView.findViewById(R.id.btnCancle);
        Button btnSubmit = dialogView.findViewById(R.id.btnSubmit);
        cusSearchList = dialogView.findViewById(R.id.searchlist);
        final String Url = UrlToPassInSearchList;
        final String titleToPass = title;

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String keyword = edCustomerSearch.getText().toString().toUpperCase();
                if (!keyword.equals("null")) {
                    new PostClassCustomerSearch(getActivity(), keyword, Url, titleToPass).execute();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder3.dismiss();
            }
        });

        listView = dialogView.findViewById(R.id.listCatecory);
        dialogTitle = dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }

    private void showSpinnerDialog(ArrayList<LookupCategory> list, String title) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        listView = (ListView) dialogView.findViewById(R.id.listCatecory);
        dialogTitle = (TextView) dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerAdapter = new SpinnerDialogAdapter(getActivity(), list, title);
        listView.setAdapter(spinnerAdapter);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }

    public class SpinnerDialogAdapter extends BaseAdapter {
        Context context;
        List<LookupCategory> rowItems;
        String Title = "";


        public SpinnerDialogAdapter(Context context, List<LookupCategory> items, String title) {
            this.context = context;
            this.rowItems = items;
            this.Title = title;
            Log.d("===rowItems.zize===", "" + rowItems.size());


        }

        public void updateData(List<LookupCategory> rowItem) {
            this.rowItems = rowItem;
            notifyDataSetChanged();
        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;

        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new ViewHolder();
                holder.textName = convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();

            }

            if(showCode) {
                holder.textName.setText(rowItems.get(position).getDESCRIPTION()
                        +" ("+rowItems.get(position).getCODE()+")"
                        +" , "+rowItems.get(position).getDistrict()
                        +" - "+rowItems.get(position).getCity()
                );
            }
            else {
                holder.textName.setText(rowItems.get(position).getDESCRIPTION());
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String meaning = rowItems.get(position).getDESCRIPTION();
                    switch (Title) {

                        case "Transaction Type":
                            builder3.dismiss();
                            tvUserType.setText(meaning);
                            userTypeCode = rowItems.get(position).getCODE();
                            Log.e("userTypeCode and Desc", userTypeCode +" , "+rowItems.get(position).getDESCRIPTION());
                            //Toast.makeText(context, "under process", Toast.LENGTH_SHORT).show();
                            tvDistrict.setText("");
                            tvRMName.setText("");

                            if(userTypeCode.endsWith("M")){
                                urlToPass = MaterialMitraList;
                                nameHeading = "Mitras";
                                tvRMNameTitle.setVisibility(View.VISIBLE);
                                tvRMName.setVisibility(View.VISIBLE);

                                if (userTypeCode.equalsIgnoreCase("RM")) {
                                  //  new PostClassGettingDistrict(getActivity(), GetDistrict, StorePrefs.getDefaults(PREF_USER_TYPE, getActivity()), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity())).execute();
                                    cbIsArchitect.setVisibility(View.VISIBLE);
                                    isEditable = true;
                                    //isNameMandotry = true;
                                }
                                if (userTypeCode.equalsIgnoreCase("DM")) {
                                  //  new PostClassGettingDistrict(getActivity(), GetDistrict, StorePrefs.getDefaults(PREF_USER_TYPE, getActivity()), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity())).execute();
                                    cbIsArchitect.setVisibility(View.VISIBLE);
                                    isEditable = true;
                                  //  isNameMandotry = true;
                                }
                            }
                          if(userTypeCode.endsWith("R")){
                                urlToPass = MaterialRetailerslist;
                              nameHeading = "Retailers";
                              tvRMNameTitle.setVisibility(View.VISIBLE);
                                tvRMName.setVisibility(View.VISIBLE);

                              if (userTypeCode.equalsIgnoreCase("DR")) {
                                //  new  PostClassGettingDistrict(getActivity(),GetDistrict,StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()),StorePrefs.getDefaults(PREF_CUSTOMER_ID,getActivity())).execute();

                                  cbIsArchitect.setVisibility(View.GONE);
                                  tvIsArchitect.setVisibility(View.GONE);
                                  cbIsArchitect.setChecked(false);
                                  isEditable = false;
                                  tvIsArchitect.setText("");
                                  architect_involved_flag = "N";
                                 // isNameMandotry = true;
                              }

                          }
                            if(userTypeCode.endsWith("D")){
                                urlToPass = MaterialDealersList;
                                nameHeading = "Dealers";
                                tvRMNameTitle.setVisibility(View.VISIBLE);
                                tvRMName.setVisibility(View.VISIBLE);

                                if (userTypeCode.equalsIgnoreCase("RD")) {
                                   // new PostClassGettingDistrict(getActivity(), GetDistrict, StorePrefs.getDefaults(PREF_USER_TYPE, getActivity()), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity())).execute();
                                    cbIsArchitect.setVisibility(View.VISIBLE);
                                    isEditable = false;
                                 //   isNameMandotry = true;
                                }


                            }
                             if (userTypeCode.equalsIgnoreCase("DD"))
                            {
                                tvRMNameTitle.setVisibility(View.GONE);
                                tvRMName.setVisibility(View.GONE);
                                cbIsArchitect.setVisibility(View.VISIBLE);

                              new  PostClassGettingDistrict(getActivity(),GetDistrict,StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()),StorePrefs.getDefaults(PREF_CUSTOMER_ID,getActivity())).execute();
                               // isEditable = false;
                                isEditable = true;
                              //  isNameMandotry = false;
                            }


                            new PostClassGettingName(getActivity(), urlToPass).execute();

                            break;

                        case "Name":
                            builder3.dismiss();
                            //tvRMName.setText(meaning+" "+rowItems.get(position).getLOOKUP_TYPE());
                            tvRMName.setText(meaning+" "+rowItems.get(position).getCODE());
                            //partyIdCode = rowItems.get(position).getCODE();
                            partyIdCode = rowItems.get(position).getLOOKUP_TYPE();

                            if (userTypeCode.equalsIgnoreCase("DM")) {
                                //  new PostClassGettingDistrict(getActivity(), GetDistrict, "M", rowItems.get(position).getCODE()).execute();
                                new  PostClassGettingDistrict(getActivity(),GetDistrict,StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()),StorePrefs.getDefaults(PREF_CUSTOMER_ID,getActivity())).execute();

                                isEditable = true;
                            }
                            if (userTypeCode.equalsIgnoreCase("DR")) {
                                new PostClassGettingDistrict(getActivity(), GetDistrict, "R", rowItems.get(position).getLOOKUP_TYPE()).execute();
                                isEditable = false;
                            }
                            if (userTypeCode.equalsIgnoreCase("RM")) {
                                // Old new PostClassGettingDistrict(getActivity(), GetDistrict, StorePrefs.getDefaults(PREF_USER_TYPE, getActivity()), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity())).execute();
                               //Old 1 new PostClassGettingDistrict(getActivity(), GetDistrict, "M", rowItems.get(position).getCODE()).execute();
                                new  PostClassGettingDistrict(getActivity(),GetDistrict,StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()),StorePrefs.getDefaults(PREF_CUSTOMER_ID,getActivity())).execute();

                                // isEditable = true;
                                isEditable = false;
                            }

                            Log.e("getLOOKUP_TYPE",""+rowItems.get(position).getLOOKUP_TYPE());
                            Log.e("getCODE",""+rowItems.get(position).getCODE());
                            Log.e("getDESCRIPTION",""+rowItems.get(position).getDESCRIPTION());

                           // Toast.makeText(context, ""+partyIdCode, Toast.LENGTH_SHORT).show();
                            break;

                        case "Product":
                            builder3.dismiss();
                            tvProduct.setText(meaning);
                            productCode = rowItems.get(position).getCODE();
                            Log.e("Product code",""+productCode);
                            packingBagCode = "";
                            tvPackingType.setText("");
                            tvQty.setText("");
                            tvOnHandStock.setText("");
                            new PostClassPackingBagTypeList(getActivity(),rowItems.get(position).getDESCRIPTION()).execute();
                            break;

                        case "Architect":
                            builder3.dismiss();
                            tvIsArchitect.setText(meaning);
                            architectCode = rowItems.get(position).getCODE();

                            break;

                        case "Packing Bag Type":
                            builder3.dismiss();
                            tvPackingType.setText(meaning);
                            tvQty.setText("");
                            tvOnHandStock.setText("");
                            packingBagCode = rowItems.get(position).getDESCRIPTION();
                            Log.e("packingBag code",""+packingBagCode);
                             break;

                        case "District":
                            builder3.dismiss();
                            tvDistrict.setText(meaning);
                           // districtCode = rowItems.get(position).getDESCRIPTION();
                            break;

                    }


                }
            });
            return convertView;
        }


        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }


    }

    private void showCustomerSearchDialog(ArrayList<LookupCategory> list,String title) {

        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_customer_search_listview_with_cancel_button, null);
        dialogBuilder.setView(dialogView);
        final EditText edCustomerSearch = dialogView.findViewById(R.id.customSearch);
        Button btnCancel = dialogView.findViewById(R.id.btnCancle);

        listView = (ListView) dialogView.findViewById(R.id.searchlist);
        dialogTitle = (TextView) dialogView.findViewById(R.id.dialogTitle);

       /* if(nameHeading.equalsIgnoreCase("Mitras")) {
            dialogTitle.setText("Mitras");

        } else*/ if (nameHeading.equalsIgnoreCase("Retailers")){
            dialogTitle.setText("Retailers");
        }
        else if (nameHeading.equalsIgnoreCase("Dealers")){
            dialogTitle.setText("Dealers");
        }
        else
            dialogTitle.setText(title);

        spinnerAdapter = new SpinnerDialogAdapter(getActivity(), list, title);
        listView.setAdapter(spinnerAdapter);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();

        /*dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_customer_search_listview, null);
        dialogBuilder.setView(dialogView);
        final EditText edCustomerSearch = dialogView.findViewById(R.id.customSearch);
        Button btnCancel = dialogView.findViewById(R.id.btnCancle);
        Button btnSubmit = dialogView.findViewById(R.id.btnSubmit);
        listView = dialogView.findViewById(R.id.searchlist);*/



        edCustomerSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // filter your list from your input
                filter(s.toString());
               // Toast.makeText(getActivity(), ""+s.toString(), Toast.LENGTH_SHORT).show();
                //you can use runnable postDelayed like 500 ms to delay search text
            }
        });




        /*btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String keyword = edCustomerSearch.getText().toString().toUpperCase();
                if (!keyword.equals("null")) {
                    new PostClassCustomerSearch(getActivity(), keyword, "C").execute();
                }
            }
        });*/

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder3.dismiss();
            }
        });

        /*listView = dialogView.findViewById(R.id.listCatecory);
        spinnerAdapter = new SpinnerDialogAdapter(getActivity(), list, title);
        listView.setAdapter(spinnerAdapter);
        dialogTitle = dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();*/
    }

    // Spinners Async

    public class PostClassLookUpData extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;

        public ArrayList<LookupCategory> tempTransactionTypeList;
        public ArrayList<LookupCategory> tempProductList;
        public ArrayList<LookupCategory> tempArchitList;

        public PostClassLookUpData(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

            tempTransactionTypeList = new ArrayList<LookupCategory>();
            tempProductList = new ArrayList<LookupCategory>();
            tempArchitList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");

                    /*JSONObject jsonObject_page_module = new JSONObject();

                    try {
                        jsonObject_page_module.put("page_module", MODULE_COMMON);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    RequestBody body = RequestBody.create(null, new byte[]{});

                   // RequestBody body = RequestBody.create(mediaType, jsonObject_page_module.toString());

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .addHeader("page_module", MODULE_COMMON)
                            //.addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    // .addHeader("token", StorePrefs.getDefaults("token", context))
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    System.out.println("Lookup_data===============" + Lookup_data);
                    response.message();


                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONObject category = jObject.getJSONObject("data");

                            JSONArray jsonArrayTransactionType = category.getJSONArray("transaction_type");
                            for (int j = 0; j < jsonArrayTransactionType.length(); j++) {
                                JSONObject jsonTransactionType = jsonArrayTransactionType.getJSONObject(j);

                                System.out.println("PREF_USER_TYPE===============" + StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()));

                                if(jsonTransactionType.getString("code").startsWith(StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()))) {
                                        tempTransactionTypeList.add(new LookupCategory(jsonTransactionType.getString("lookup_type"),
                                                jsonTransactionType.getString("code"), jsonTransactionType.getString("description")));
                                }
                            }
                            TransactionTypeList.addAll(tempTransactionTypeList);

                            JSONArray jsonArrayProduct = category.getJSONArray("product");
                            for (int i = 0; i < jsonArrayProduct.length(); i++) {
                                JSONObject jsonProduct = jsonArrayProduct.getJSONObject(i);
                                tempProductList.add(new LookupCategory(jsonProduct.getString("lookup_type"),
                                        jsonProduct.getString("code"), jsonProduct.getString("description")));
                                Log.e("Product", ""+jsonProduct.getString("lookup_type") +" = "+jsonProduct.getString("code") +" = "+
                                        jsonProduct.getString("description") );

                            }
                            ProductList.addAll(tempProductList);

                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
              /*  spinTradeadapter = new SpinCalegory_Adapter(getActivity(), tempCategorylist);
                spnCategory.setAdapter(spinTradeadapter);*/
             //   new PostClassStateList(context).execute();

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }
    }

    class PostClassPackingBagTypeList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempPackingBagTypeList;
        String item_name;


        PostClassPackingBagTypeList(Context c, String item_name) {
            this.context = c;
            this.item_name = item_name;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempPackingBagTypeList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();

                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("item_name", item_name);
                    RequestBody body = RequestBody.create(mediaType, json.toString());
                    System.out.println("RequestBody===============" + json.toString());

                   // RequestBody body = RequestBody.create(null, new byte[]{});
                    Request request = new Request.Builder()
                            .url(GetpackingBagType)
                            .post(body)
                            .addHeader("content-type", "application/json")
                          //  .addHeader("page_module", MODULE_MATERIALTRANSACTIONS)
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======GetpackingBagType=========" + GetpackingBagType);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempPackingBagTypeList.add(new LookupCategory("",
                                            "", jsonObject.getString("pack_cond")));

                                }
                                PackingBagTypeList.clear();
                                PackingBagTypeList.addAll(tempPackingBagTypeList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


            } else {


            }

        }
    }

    class PostClassGettingName extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempDataList;
        String urlTopass;

        PostClassGettingName(Context c, String urlTopass) {
            this.context = c;
            this.urlTopass = urlTopass;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempDataList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("user_type", StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()));

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(urlTopass)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_MATERIALTRANSACTIONS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======urlTopass=========" + urlTopass);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {

                                if (urlTopass.equalsIgnoreCase(MaterialDealersList)) {
                                    for (int i = 0; i < jsonArraydata.length(); i++) {
                                        JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                       /* tempDataList.add(new LookupCategory(jsonObject.getString("account_number"),
                                                jsonObject.getString("party_id"), jsonObject.getString("party_name")));*/

                                        tempDataList.add(new LookupCategory(jsonObject.getString("party_id"),
                                                jsonObject.getString("account_number"), jsonObject.getString("party_name")));

                                        // Log.e("MaterialDealersList:- ",""+jsonObject.getString("party_id")+" = "+ jsonObject.getString("party_name"));

                                    }

                                    DataList.clear();
                                    DataList.addAll(tempDataList);
                                }
                                if (urlTopass.equalsIgnoreCase(MaterialRetailerslist)) {
                                    for (int i = 0; i < jsonArraydata.length(); i++) {
                                        JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                       /* tempDataList.add(new LookupCategory(jsonObject.getString("account_number"),
                                                jsonObject.getString("party_id"), jsonObject.getString("party_name")));*/

                                        tempDataList.add(new LookupCategory(jsonObject.getString("party_id"),
                                                jsonObject.getString("account_number"), jsonObject.getString("party_name"),
                                                jsonObject.getString("district"), jsonObject.getString("city")));

                                        Log.e("MaterialRetailIst:- ",""+jsonObject.getString("party_id")+" = "+ jsonObject.getString("party_name")+" = "
                                                +" = "+ jsonObject.getString("account_number")
                                                +" = "+ jsonObject.getString("district")
                                                +" = "+ jsonObject.getString("city"));

                                    }

                                    DataList.clear();
                                    DataList.addAll(tempDataList);
                                }
                                if (urlTopass.equalsIgnoreCase(MaterialMitraList)) {
                                    for (int i = 0; i < jsonArraydata.length(); i++) {
                                        JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                      /*  tempDataList.add(new LookupCategory("",
                                                jsonObject.getString("mitra_id"), jsonObject.getString("full_name")));*/

                                        tempDataList.add(new LookupCategory(jsonObject.getString("mitra_id"),
                                                jsonObject.getString("mitra_id"), jsonObject.getString("full_name")));

                                    }

                                    DataList.clear();
                                    DataList.addAll(tempDataList);
                                }

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


            } else {


            }

        }
    }

    class PostClassGettingArchitect extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempArchitectList;
        String urlTopass;

        PostClassGettingArchitect(Context c) {
            this.context = c;


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempArchitectList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();

                    json.put("user_type", StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()));

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Architectlist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                          //  .addHeader("page_module", MODULE_MATERIALTRANSACTIONS)
                           .addHeader("page_module", MODULE_COMMON)
                         //   .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Architectlist=========" + Architectlist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {

                                    for (int i = 0; i < jsonArraydata.length(); i++) {
                                        JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                        tempArchitectList.add(new LookupCategory(jsonObject.getString("mitra_id"),
                                                jsonObject.getString("mitra_id"), jsonObject.getString("full_name")));

                                    }

                                ArchitectList.clear();
                                ArchitectList.addAll(tempArchitectList);
                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


            } else {


            }

        }
    }

    class PostClassGettingDistrict extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempDataList;
        String urlTopass,userTypeCode,id;

        PostClassGettingDistrict(Context c, String urlTopass, String userTypeCode, String id) {
            this.context = c;
            this.urlTopass = urlTopass;
            this.userTypeCode = userTypeCode;
            this.id= id;
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempDataList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("user_type", userTypeCode);
                    json.put("id", id);

                   // json.put("user_type", StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()));

                    RequestBody body = RequestBody.create(mediaType, json.toString());
                    System.out.println("Parameters===============" + json.toString());

                    Request request = new Request.Builder()
                            .url(urlTopass)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            //.addHeader("page_module", MODULE_MATERIALTRANSACTIONS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======PostClassGettingDistrict=========" + urlTopass);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONObject jsonData = jObject.getJSONObject("data");
                            if (jsonData != null) {
                                out_district = jsonData.getString("out_district").trim();
                                out_state = jsonData.getString("out_state").trim();

                            }

                           /* JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {

                                    for (int i = 0; i < jsonArraydata.length(); i++) {
                                        JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                        tempDataList.add(new LookupCategory(jsonObject.getString("account_number"),
                                                jsonObject.getString("party_id"), jsonObject.getString("party_name")));



                                }


                            }*/

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();
                tvDistrict.setText(out_district);
                new PostClassDistrictList(getActivity(), out_state).execute();


            } else {


            }

        }
    }

    class PostClassDistrictList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempDistrictList;
        String state;


        PostClassDistrictList(Context c, String state) {
            this.context = c;
            this.state = state;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempDistrictList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("state",state);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Districtlist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======orders_listUrl=========" + orders_listUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempDistrictList.add(new LookupCategory("",
                                            "", jsonObject.getString("district")));

                                }
                                DistrictList.clear();
                                DistrictList.addAll(tempDistrictList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


            } else {


            }

        }
    }

    class PostClassGetStock extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;

        String transactionTypeCode = "";
        String partyIdToPost = "";


        String user_type,product,transaction_type,inventory_item_id,
                packing_type,transaction_quantity,transaction_date,mitra_id,customer_id,comments,district;



        public PostClassGetStock(Context context, String transactionTypeCode, String user_type, String product, String transaction_type, String partyIdToPost, String inventory_item_id, String packing_type,String transaction_quantity,String transaction_date, String mitra_id, String customer_id, String comments, String district) {
            this.context = context;
            this.transactionTypeCode = transactionTypeCode;
            this.user_type = user_type;
            this.product = product;
            this.transaction_type = transaction_type;
            this.partyIdToPost = partyIdToPost;
            this.inventory_item_id = inventory_item_id;
            this.packing_type = packing_type;
            this.transaction_quantity= transaction_quantity;
            this.transaction_date = transaction_date;
            this.mitra_id = mitra_id;
            this.customer_id = customer_id;
            this.comments=comments;
            this.district = district;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();


        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject  jsonObject = new JSONObject();
                    //jsonObject.put("page_module", MODULE_MATERIALTRANSACTIONS);

                    jsonObject.put("user_type",user_type);
                    jsonObject.put("product",product);
                    jsonObject.put("transaction_type",transaction_type);

                    if(transaction_type.equalsIgnoreCase("DM")){
                        jsonObject.put("mitra_to_party_id",partyIdToPost);
                    }
                    if(transaction_type.equalsIgnoreCase("DR")){
                        jsonObject.put("retailer_to_party_id",partyIdToPost);
                    }
                    if(transaction_type.equalsIgnoreCase("DD")){

                    }
                    if(transaction_type.equalsIgnoreCase("RD")){
                        jsonObject.put("dealer_to_party_id",partyIdToPost);
                    }
                    if(transaction_type.equalsIgnoreCase("RM")){
                        jsonObject.put("mitra_to_party_id",partyIdToPost);
                    }
                    if(transaction_type.equalsIgnoreCase("MD")){
                        jsonObject.put("dealer_to_party_id",partyIdToPost);
                    }
                    if(transaction_type.equalsIgnoreCase("MR")){
                        jsonObject.put("retailer_to_party_id",partyIdToPost);
                    }

                    jsonObject.put("inventory_item_id",inventory_item_id);
                    jsonObject.put("packing_type",packing_type);
                    jsonObject.put("transaction_quantity",transaction_quantity);
                    jsonObject.put("transaction_date",transaction_date);
                    jsonObject.put("mitra_id",mitra_id);
                    jsonObject.put("customer_id",customer_id);
                    jsonObject.put("comments",comments);
                    jsonObject.put("district",district);


                    RequestBody body = RequestBody.create(mediaType,jsonObject.toString());
                    System.out.println("Params>===============>" + jsonObject.toString());
                    System.out.println("Getstock>===============>" + Getstock);
                    Request request = new Request.Builder()
                            .url(Getstock)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                           // .addHeader("action", ACTION_ADD)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))

                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Getstock=========" + Getstock);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            jObject = new JSONObject(reqBody);
                            if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                                final JSONObject jsonData = jObject.getJSONObject("data");
                                if(jsonData.getString("l_return_status").equalsIgnoreCase("E")){

                                    ((Activity) context).runOnUiThread(new Runnable() {
                                        public void run() {
                                            progress.dismiss();
                                            try {
                                                showToastMessage(getActivity(),Constant.nullCheckFunction(jsonData.getString("l_msg_data")));
                                                strStock ="";
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                }
                                else {
                                    strStock = Constant.nullCheckFunction(jsonData.getString("out_par"));
                                }
                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

                tvOnHandStock.setText(strStock);

            } else {


            }

        }
    }

    class PostClassCustomerSearch extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";
        String Keyword = "";
        String tvClickedItem = "" ;
        String UrlToPassInSearchList = "";


        public PostClassCustomerSearch(Context c, String keyword, String UrlToPassInSearchList, String tvClickedItem) {
            this.context = c;
            this.Keyword = keyword;
            this.UrlToPassInSearchList = UrlToPassInSearchList;
            this.tvClickedItem = tvClickedItem;
            mitraSearchList.clear();
            tempmitraSearchList.clear();
            Log.d("====keyword=====", "" + Keyword);

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("keyword", Keyword);
                    //jsonObject.put("user_type", Usertype);
                    //jsonObject.put("page_module", MODULE_ORDERS);
                    jsonObject.put("user_type", StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()));

                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());
                    Request request = new Request.Builder()
                            .post(body)
                            .url(UrlToPassInSearchList)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_MATERIALTRANSACTIONS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===Url In Search===", "=======" + UrlToPassInSearchList);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONArray jsonData = jObject.getJSONArray("data");

                            for (int i = 0; i < jsonData.length(); i++) {
                                JSONObject jCustomer = jsonData.getJSONObject(i);
                                mitraSearchList.add(new MitraBeanSearch(
                                        Constant.nullCheckFunction(jCustomer.getString("mitra_id")),
                                        Constant.nullCheckFunction(jCustomer.getString("full_name")),
                                        Constant.nullCheckFunction(jCustomer.getString("mobile_number")),
                                        Constant.nullCheckFunction(jCustomer.getString("mitra_type"))));

                            }
                            tempmitraSearchList.addAll(mitraSearchList);

                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                if (mitraSearchList != null) {
                    customerSearchAdapter = new CustomerSearchAdapter(getActivity(), mitraSearchList,  tvClickedItem);
                    cusSearchList.setAdapter(customerSearchAdapter);

                }

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }
    }

    private void showDialog() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
       // cal.add(Calendar.DAY_OF_YEAR, -7);
        cal.add(Calendar.DAY_OF_MONTH, 1);

        //Date newDate = cal.getTime();
        // Create the DatePickerDialog instance
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), datePickerListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.setCancelable(false);
        datePicker.setTitle("Select the date");
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
      // datePicker.getDatePicker().setMinDate(newDate.getTime());
        datePicker.show();


        /*Calendar calendar = Calendar.getInstance();
        calendar.setTime(myDate);
        calendar.add(Calendar.DAY_OF_YEAR, -7);
        Date newDate = calendar.getTime();*/

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            String year = String.valueOf(selectedYear);
            String month = String.valueOf(selectedMonth + 1);
            String day = String.valueOf(selectedDay);
            String time = "";
            try {
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("hh:mm aa");
                time = df.format(cal.getTime());

                // tvDate.setText(time);

            } catch (Exception e) {
                e.printStackTrace();
            }

            String validity = String.valueOf(new StringBuilder().append(day).append("-")
                    .append(month).append("-").append(year));

            DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
            Date date1 = null;
            Calendar cal = Calendar.getInstance();
            try {
                date1 = readFormat1.parse(validity);
                String dateformatt = formatter1.format(date1);


                tvDate.setText(dateformatt);
                date = dateformatt;
                //date = validity;


            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };

    void filter(String text){
        List<LookupCategory> temp = new ArrayList();
        for(LookupCategory d: DataList){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getDESCRIPTION().toLowerCase().contains(text.toLowerCase())){
                temp.add(d);

            }
            if(d.getCODE().toLowerCase().contains(text.toLowerCase())){
                temp.add(d);

            }
        }
        //update recyclerview
        spinnerAdapter.updateData(temp);
    }

    public class CustomerSearchAdapter extends BaseAdapter {
        Context context;
        List<MitraBeanSearch> rowItems;
        String Title = "";
        String tvClickedItem = "";


        public CustomerSearchAdapter(Context context, List<MitraBeanSearch> items, String tvClickedItem) {
            this.context = context;
            this.rowItems = items;
            this.tvClickedItem = tvClickedItem;
            Log.d("===rowItems.zize===", "" + rowItems.size());
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            CustomerSearchAdapter.ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new CustomerSearchAdapter.ViewHolder();
                holder.textName = (TextView) convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (CustomerSearchAdapter.ViewHolder) convertView.getTag();

            }
            Log.d("=====search list=====", "" + rowItems.get(position).getId());

            holder.textName.setText(rowItems.get(position).getName() +" ("+rowItems.get(position).getId()+") "+rowItems.get(position).getMobile_number()+" - "+rowItems.get(position).getMitra_type());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tvClickedItem.equalsIgnoreCase("mitras")) {
                        partyIdCode = rowItems.get(position).getId();

                        final String meaning = rowItems.get(position).getName();

                        final String code = rowItems.get(position).getId();
                        tvRMName.setText(meaning);

                        if (userTypeCode.equalsIgnoreCase("DM")) {
                            new PostClassGettingDistrict(getActivity(), GetDistrict, StorePrefs.getDefaults(PREF_USER_TYPE, getActivity()), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity())).execute();

                            isEditable = true;
                        }

                        if (userTypeCode.equalsIgnoreCase("RM")) {
                            new PostClassGettingDistrict(getActivity(), GetDistrict, StorePrefs.getDefaults(PREF_USER_TYPE, getActivity()), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity())).execute();

                            // isEditable = true;
                            isEditable = false;
                        }
                    }

                    if (tvClickedItem.equalsIgnoreCase("Architect")) {
                        architectCode = rowItems.get(position).getId();

                        final String meaning = rowItems.get(position).getName();

                        final String code = rowItems.get(position).getId();
                        tvIsArchitect.setText(meaning);

                    }

                    builder3.dismiss();

                }
            });
            return convertView;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;
        }

    }
}
