package com.shreecement.shree.Fragment.MaterialTransaction;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Common.BeanCommon;
import com.shreecement.shree.Fragment.MaterialTransaction.Adapter.MaterialTransaction_Adapter;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.shreecement.shree.Utlility.Utils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.CheckInternet;
import static com.shreecement.shree.Utlility.Constant.MODULE_MATERIALTRANSACTIONS;
import static com.shreecement.shree.Utlility.Constant.Materialtransactionslist;
import static com.shreecement.shree.Utlility.Constant.PREF_CUSTOMER_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Material_Transaction extends Fragment {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    MaterialTransaction_Adapter materialEntry_adapter;
    //ArrayList<BeanMaterialTransaction> materiaList = new ArrayList<>();
    ArrayList<BeanCommon> materiaList;
    String keyword = "";
    int page = 1;
    String reqBody;
    int total_record = 0;
    int recordCount = 0;


    FloatingActionButton addMaterilEntry;

    TextView tv_no_record_found;
    TextView tv_count_item;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_material_entry, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setHasOptionsMenu(true);
        tv_no_record_found = view.findViewById(R.id.tv_no_record_found);
        tv_count_item = view.findViewById(R.id.tv_count_item);

        addMaterilEntry = view.findViewById(R.id.addMaterilEntry);
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        getActivity().setTitle(getActivity().getString(R.string.nav_item_material_trasaction));

        keyword = "";

        materiaList = new ArrayList<>();
        materialEntry_adapter = new MaterialTransaction_Adapter(getActivity(), materiaList);
        recyclerView.setAdapter(materialEntry_adapter);
        materialEntry_adapter.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();

        addMaterilEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = new Fragment_Add_Material_Transaction();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                ((MainActivity) getActivity()).showUpButton(true);
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    loadMoreItems();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new PostClassMaterialTransactions(getActivity(), "0", keyword, true,false).execute();
            }
        }, 450);


        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();


        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                materiaList.clear();
                keyword = "";
                page = 1;
                new PostClassMaterialTransactions(getActivity(), "1", "", true,false).execute();

                return false;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                keyword = query;
                materiaList.clear();
                new PostClassMaterialTransactions(getActivity(), "", keyword, true,true).execute();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0) {
                    keyword = "";
                    materiaList.clear();
                    new PostClassMaterialTransactions(getActivity(), "1", keyword, true,true).execute();
                }
                return true;
            }
        });

    }

    private void loadMoreItems() {
        new PostClassMaterialTransactions(getActivity(), String.valueOf(++page), keyword, true,true).execute();

    }
    private void refreshItems() {

        page = 1;
        materiaList.clear();
        swipeRefreshLayout.setRefreshing(false);
        // Load items
        // ...
        //new PostClassMaterialTransactions(getActivity(), String.valueOf(page), keyword, true).execute();

        new PostClassMaterialTransactions(getActivity(), String.valueOf(page), keyword, true,false).execute();

/*
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new PostClassMaterialTransactions(getActivity(), String.valueOf(page), keyword, true).execute();
            }
        }, 450);*/


    }

    class PostClassMaterialTransactions extends AsyncTask<String, Void, String> {

        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        boolean showLoader;
        String Keyword = "";
        List<BeanCommon> tempMateriaList = new ArrayList<>();
        boolean   isPagination ;

        public PostClassMaterialTransactions(Context c, String pageNo, String keyword, boolean showLoader, boolean   isPagination) {
            this.context = c;
            this.pageNo = pageNo;
            this.Keyword = keyword;
            this.showLoader = showLoader;
            this.isPagination = isPagination;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempMateriaList.clear();


        }

        @Override
        protected String doInBackground(String... params) {

            System.out.println("pageNo===============" + pageNo);


            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    client.setConnectTimeout(15, TimeUnit.SECONDS); // connect timeout
                    client.setReadTimeout(15, TimeUnit.SECONDS);
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("keyword", Keyword);
                    json.put("page", pageNo);
                    json.put("user_type", StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()));

                    if(StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()).equalsIgnoreCase("M")){
                        json.put("id", StorePrefs.getDefaults(PREF_MITRA_ID,getActivity()));
                    }
                    if(StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()).equalsIgnoreCase("R")){
                        json.put("id", StorePrefs.getDefaults(PREF_CUSTOMER_ID,getActivity()));
                    }
                    if(StorePrefs.getDefaults(PREF_USER_TYPE,getActivity()).equalsIgnoreCase("D")){
                        json.put("id", StorePrefs.getDefaults(PREF_CUSTOMER_ID,getActivity()));
                    }

                   // json.put("page_module", MODULE_MATERIALTRANSACTIONS);

                    RequestBody body = RequestBody.create(mediaType,  json.toString());
                    System.out.println("body===============" + json.toString());
                    Request request = new Request.Builder()
                            .url(Materialtransactionslist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_MATERIALTRANSACTIONS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                     reqBody = response.body().string();
                    System.out.println("======Materialtransactionslist=========" + Materialtransactionslist);
                    System.out.println("response===============" + reqBody);
                    response.message();


                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            total_record = Integer.parseInt(jObject.getString("total_record"));
                            recordCount = Integer.parseInt(jObject.getString("recordCount"));
                            JSONArray jsonArrayData = jObject.getJSONArray("data");


                            if (jsonArrayData != null) {
                              //  JSONObject jsonObject;


                                for (int i = 0; i < jsonArrayData.length(); i++) {
                                    JSONObject   jsonObject = jsonArrayData.getJSONObject(i);
                                   // System.out.println("======jsonObject=========" + jsonObject);

                                    tempMateriaList .add(new BeanCommon(
                                            jsonObject.getString("transaction_id"),
                                            jsonObject));
                                }

                            }
                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();


                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                /*((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });*/
               return Utils.Please_Check_Internet_Connection;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                --page;
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
               // showToastMessage(getActivity(),reqBody);
                return;
            } else if (s.equals("success")) {

                if (!isPagination) {
                    materiaList.clear();
                    materiaList.addAll(tempMateriaList);

                } else {

                    if (tempMateriaList.size() > 0) {
                        // mitraList.clear();
                        materiaList.addAll(tempMateriaList);

                    } else {
                        Toast.makeText(context, "This is last page", Toast.LENGTH_SHORT).show();
                    }
                }

                materialEntry_adapter.notifyDataSetChanged();


            }
            else if (s.equalsIgnoreCase(Utils.Please_Check_Internet_Connection)) {
                progress.dismiss();
                CheckInternet(getContext());
            }

            else {
                --page;
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

            materialEntry_adapter.notifyDataSetChanged();

            recordCount = materiaList.size();
            if (recordCount==0){
                tv_no_record_found.setVisibility(View.VISIBLE);

                tv_count_item.setText("Showing "+recordCount+"   of "+total_record);
            }else {
                tv_no_record_found.setVisibility(View.GONE);
                tv_count_item.setText("Showing "+recordCount+"   of "+total_record);
            }
        }

    }
}
