package com.shreecement.shree.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.shreecement.shree.Adapter.SlidingImage_Adapter;
import com.shreecement.shree.Fragment.BeanSliderImage.BeanSliderImage;
import com.shreecement.shree.Fragment.BeanSliderImage.ImagePagerAdapter;
import com.shreecement.shree.Fragment.NotificationHeader.Fragment_Notifications;
import com.shreecement.shree.ModalClass.BeanDi;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.BadgeDrawable;
import com.shreecement.shree.Utlility.Constant;
import com.shreecement.shree.Utlility.StorePrefs;
import com.shreecement.shree.Utlility.Utils;
import com.shreecement.shree.networkUtils.BackGroundTask;
import com.shreecement.shree.networkUtils.OnTaskCompleted;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import static com.shreecement.shree.Activity.MainActivity.PERM_REQUEST_CODE_DRAW_OVERLAYS;
import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.AboutUsDiscription;
import static com.shreecement.shree.Utlility.Constant.Announcements;
import static com.shreecement.shree.Utlility.Constant.LinkList;
import static com.shreecement.shree.Utlility.Constant.MODULE_ANNOUNCEMENTS;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.MODULE_NOTIFICATIONS;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_DESCRIPTION;
import static com.shreecement.shree.Utlility.Constant.PREF_DISCLAIMER_DESC;
import static com.shreecement.shree.Utlility.Constant.PREF_FACEBOOk;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_CITY;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_DISTRICT;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_STATE;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_TALUKA;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_TYPE;
import static com.shreecement.shree.Utlility.Constant.PREF_TWITTER;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;
import static com.shreecement.shree.Utlility.Constant.PREF_YOUTUBE;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.baseUrlForImage;
import static com.shreecement.shree.Utlility.Constant.dashboardCount;
import static com.shreecement.shree.Utlility.Constant.notifications;


/**
 * Created by Choudhary on 6/29/2017.
 */

public class Fragment_Dashboard extends Fragment implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener , View.OnClickListener {
    private static final Integer[] IMAGES = {R.drawable.scl_logo, R.drawable.scl_logo, R.drawable.scl_logo, R.drawable.scl_logo};

    public static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    TextView tvOreCount, tvMreCount;
    String order_count = "";
    String material_count = "";
    ImageView imgBack;
    Context mContext;
    View view;
    ArrayList<BeanSliderImage> ImageUrlList = new ArrayList<BeanSliderImage>();
    ArrayList<BeanSliderImage> tempImageUrlList = new ArrayList<BeanSliderImage>();
    SlidingImage_Adapter slidingImage_adapter;
    ViewPager pager;
    ImagePagerAdapter imagePagerAdapter;
    SliderLayout sliderLayout;
    HashMap<String, String> HashMapForURL;
    TextSliderView textSliderView;
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    Dialog dialog;
    int notificaionCount;

    ImageButton btnGplus, btnTwitter, btnFb, btnYoutube;
    private LayerDrawable mBasketIcon;
    static Button notifCount;
    static int mNotifCount ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        setHasOptionsMenu(true);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        inItdisplay(view);
        dialog = new Dialog(getActivity());

        Log.e("Token", StorePrefs.getDefaults("token", getActivity()));

        //pager = view.findViewById(R.id.pager);
        /*slidingImage_adapter =  new SlidingImage_Adapter(getActivity(), ImageUrlList);
        mPager.setAdapter(imagePagerAdapter);*/
        //mPager.setAdapter(new SlidingImage_Adapter(getActivity(),ImageUrlList));

        tvOreCount = view.findViewById(R.id.tvOreCount);
        tvMreCount = view.findViewById(R.id.tvMreCount);
        imgBack = view.findViewById(R.id.imgBack);
        btnTwitter = view.findViewById(R.id.btnTwitter);
        btnFb = view.findViewById(R.id.btnFb);
        btnYoutube = view.findViewById(R.id.btnYoutube);
        btnGplus = view.findViewById(R.id.btnGplus);

        btnTwitter.setOnClickListener(this);
        btnFb.setOnClickListener(this);
        btnYoutube.setOnClickListener(this);
        btnGplus.setOnClickListener(this);

        sliderLayout = (SliderLayout) view.findViewById(R.id.slider);
        textSliderView = new TextSliderView(getActivity());




        //funNotificaionCount(getActivity(),"","");

        getActivity().setTitle(getActivity().getString(R.string.nav_item_home));
        permissionToDrawOverlays();




        return view;

    }

    public void permissionToDrawOverlays() {
        if (android.os.Build.VERSION.SDK_INT >= 23) { //Android M Or Over
            if (!Settings.canDrawOverlays(getActivity())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getActivity().getPackageName()));
                startActivityForResult(intent, PERM_REQUEST_CODE_DRAW_OVERLAYS);
            }else {
                funNotificaionCount(getActivity(),"","");
            }
        }
    }

    private void inItdisplay(View view) {
        mPager =   view.findViewById(R.id.pager);

        Constant.token_from_server = StorePrefs.getDefaults("token", getActivity());


        funLinkList();

    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_notification, menu);

        MenuItem item = menu.findItem(R.id.badge);
        MenuItemCompat.setActionView(item, R.layout.feed_update_count);
        View view = MenuItemCompat.getActionView(item);
        notifCount = (Button)view.findViewById(R.id.notif_count);

        notifCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, new Fragment_Notifications()).commit();
            }
        });

        }
       // notifCount.setText(String.valueOf(mNotifCount));


    private void setNotifCount(int count){
        mNotifCount = count;
        getActivity().supportInvalidateOptionsMenu();
    }


    @Override
    public void onStop() {

        sliderLayout.stopAutoCycle();

        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
          //Toast.makeText(getActivity(),slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();


        dialog.getWindow();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setContentView(R.layout.dialog_notification);
        dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        TextView txtHeading;
        Button btnCancelDialogTop , btnDialogOk;
        final TextView tvdesc1,tvTitle;

        btnCancelDialogTop = (Button) dialog.findViewById(R.id.btnCancelDialogTop);
        btnDialogOk = (Button) dialog.findViewById(R.id.btnDialogOk);
        tvTitle  = dialog.findViewById(R.id.tvTitle);
        tvTitle.setVisibility(View.INVISIBLE);
        tvdesc1  = dialog.findViewById(R.id.tvdesc1);
        //edInputField  = dialog.findViewById(R.id.edInputField);
        tvdesc1.setText(slider.getBundle().get("extra") + "" );


        dialog.show();

        btnCancelDialogTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        btnDialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void init() {

        //  mPager.setAdapter(new SlidingImage_Adapter(getActivity(),ImageUrlList));

        CirclePageIndicator indicator = (CirclePageIndicator)
                view.findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = ImageUrlList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }

        });

    }

    public void initSlider() {
        for (String name : HashMapForURL.keySet()) {
            Log.e("name:-",""+name);
            textSliderView = new TextSliderView(getActivity());

            textSliderView
//                    .description(name)
                    .image(baseUrlForImage + HashMapForURL.get(name))
                    .error(R.drawable.scl_logo)
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            Log.e("ImageUrlForSlider:-",""+baseUrlForImage + HashMapForURL.get(name));
            textSliderView.bundle(new Bundle());

            textSliderView.getBundle()
                    .putString("extra", name);

            try{
            sliderLayout.addSlider(textSliderView);}
            catch (Exception e){
                e.printStackTrace();
            }
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.DepthPage);

        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

        sliderLayout.setCustomAnimation(new DescriptionAnimation());

        sliderLayout.setDuration(3000);

        sliderLayout.addOnPageChangeListener(this);
    }

    void parseJsonData(String response) {

    }

    @Override
    public void onClick(View view) {
        if (view == btnFb) {
            Uri uri = Uri.parse(StorePrefs.getDefaults(PREF_FACEBOOk, getActivity())); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
        if (view == btnTwitter) {
            Uri uri = Uri.parse(StorePrefs.getDefaults(PREF_TWITTER, getActivity())); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
        if (view == btnYoutube) {
            Uri uri = Uri.parse(StorePrefs.getDefaults(PREF_YOUTUBE, getActivity())); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
        if (view == btnGplus) {
            Uri uri = Uri.parse("https://plus.google.com/discover"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
    }

    public class PostClassDashBoardCount extends AsyncTask<String, Void, String> {

        private final Context context;
        public ArrayList<BeanDi> tempDiArrayList = new ArrayList<BeanDi>();
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";
        String Organization_id = "";
        String Usertype = "";


        public PostClassDashBoardCount(Context c) {
            this.context = c;


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                   /* MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("page_module", MODULE_ORDERS);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());*/
                    RequestBody body = RequestBody.create(null, new byte[]{});

                    Request request = new Request.Builder()
                            .post(body)
                            .url(dashboardCount)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                           // .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    System.out.println("dashboardCount===============" + dashboardCount);
                    response.message();
                    //Log.d("===Di===", "=======" + Di);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                           // JSONObject jsonData = jObject.getJSONObject("data");
                          //  material_count = jsonData.getString("material_count");
                           // order_count = jsonData.getString("order_count");

                            JSONArray jsonArray = jObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                Log.d("===count_type==", "=======" + jsonObject.getString("count_type"));
                                Log.d("===counts==", "=======" + jsonObject.getString("counts"));
                                Log.d("xxxxxxx", "xxxxxxxx" );

                            }

                           // Log.d("===DiList==", "=======" + tempDiArrayList.size());




                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyCode");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                tvOreCount.setText(order_count);
                tvMreCount.setText(material_count);

            } else {
//                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            }

        }
    }

    class PostClass_SliderImages extends AsyncTask<String, Void, String> {

        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        List<BeanSliderImage> tempImageUrlList;
        private Context context;

        PostClass_SliderImages(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {
            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempImageUrlList = new ArrayList<>();
            HashMapForURL = new HashMap<String, String>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    // connect timeout
                    OkHttpClient client = new OkHttpClient();
                    client.setConnectTimeout(5, TimeUnit.MINUTES);

                    client.setReadTimeout(5, TimeUnit.MINUTES);
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("org_id", ORG_ID);

                    json.put("state", StorePrefs.getDefaults(PREF_MITRA_STATE, context));
                    json.put("district", StorePrefs.getDefaults(PREF_MITRA_DISTRICT, context));
                    json.put("taluka", StorePrefs.getDefaults(PREF_MITRA_TALUKA, context));
                    json.put("city", StorePrefs.getDefaults(PREF_MITRA_CITY, context));
                    json.put("mitra_type", StorePrefs.getDefaults(PREF_MITRA_TYPE, context));
                    json.put("user_type", StorePrefs.getDefaults(PREF_USER_TYPE, context));



                    // json.put("page_module", MODULE_SCHEMES);

                    RequestBody body = RequestBody.create(mediaType, json.toString());
                    System.out.println("======json.toString()=========" + json.toString());

                    Request request = new Request.Builder()
                            .url(Announcements)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            // .addHeader("page_module", MODULE_SCHEMES)
                            .addHeader("page_module", MODULE_ANNOUNCEMENTS)
                            // .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))

                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Announcements=========" + Announcements);

                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            System.out.println("======jsonArraydata=========" + jsonArraydata);
                            if (jsonArraydata != null) {
                                JSONObject jsonObject;


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    jsonObject = jsonArraydata.getJSONObject(i);
                                    System.out.println("======jsonObject=========" + jsonObject);

                                    //Scheme sliders image
                                   /* tempImageUrlList.add(new BeanSliderImage(
                                            jsonObject.getString("scheme_name"),
                                            jsonObject.getString("scheme_short_description"),
                                            jsonObject.getString("scheme_long_description"),
                                            jsonObject.getString("scheme_url")));

                                    HashMapForURL.put(jsonObject.getString("scheme_short_description"), jsonObject.getString("scheme_url"));*/

                                   //Announancements sliders image
                                    tempImageUrlList.add(new BeanSliderImage(
                                            Constant.nullCheckFunction(jsonObject.getString("title")),
                                            Constant.nullCheckFunction(jsonObject.getString("short_description")),
                                            Constant.nullCheckFunction(jsonObject.getString("long_description")),
                                            Constant.nullCheckFunction(jsonObject.getString("announcement_url"))));

                                    HashMapForURL.put(Constant.nullCheckFunction(jsonObject.getString("short_description")), Constant.nullCheckFunction(jsonObject.getString("announcement_url")));

                                }
                                ImageUrlList.clear();
                                ImageUrlList.addAll(tempImageUrlList);
                                if (ImageUrlList.size() > 0) {
                                    // mPager.setAdapter(new SlidingImage_Adapter(getActivity(), ImageUrlList));
                                    /*slidingImage_adapter.notifyDataSetChanged();*/
                                }
                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                // mPager.setAdapter(new SlidingImage_Adapter(getActivity(),ImageUrlList));
                /*if (ImageUrlList.size() > 0) {
                    init();
                }*/

                try{
                initSlider();}
                catch (Exception e){
                    e.printStackTrace();
                }

            }


        }
    }

    public class PostClassDiscription extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";

        public PostClassDiscription(Context c) {
            this.context = c;


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    try {
                        // jsonObject.put("page_module", MODULE_COMMON);
                        jsonObject.put("org_id", ORG_ID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());
                    Request request = new Request.Builder()
                            .post(body)
                            .url(AboutUsDiscription)
                            .addHeader("content-type", "application/json")
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .addHeader("page_module", MODULE_COMMON)
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===AboutUsDiscription==", "=======" + AboutUsDiscription);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            JSONObject jsonData = jObject.getJSONObject("data");

                            StorePrefs.setDefaults(PREF_DESCRIPTION, jsonData.getString("description"), getActivity());
                            StorePrefs.setDefaults(PREF_DISCLAIMER_DESC, jsonData.getString("disclaimer"), getActivity());

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyCode");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //  Toast.makeText(context, StorePrefs.getDefaults(PREF_DISCLAIMER_DESC, getActivity()), Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

    public class PostLinkList extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";

        public PostLinkList(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    try {
                        //jsonObject.put("page_module", MODULE_COMMON);
                        jsonObject.put("org_id", ORG_ID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());
                    Request request = new Request.Builder()
                            .post(body)
                            .url(LinkList)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            //.addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===LinkList==", "=======" + LinkList);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            System.out.println("======jsonArraydata=========" + jsonArraydata);

                            // StorePrefs.setDefaults(PREF_DESCRIPTION, jsonData.getString("description"), getActivity());

                            if (jsonArraydata != null) {
                                JSONObject jsonObjectData;
                                System.out.println("======jsonObject=========" + jsonObject);

                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    jsonObjectData = jsonArraydata.getJSONObject(i);

                                    if (jsonObjectData.getString("link_name").equalsIgnoreCase("Facebook")) {
                                        StorePrefs.setDefaults(PREF_FACEBOOk, jsonObjectData.getString("link_url"), getActivity());
                                    }

                                    if (jsonObjectData.getString("link_name").equalsIgnoreCase("Twitter")) {
                                        StorePrefs.setDefaults(PREF_TWITTER, jsonObjectData.getString("link_url"), getActivity());
                                    }

                                    if (jsonObjectData.getString("link_name").equalsIgnoreCase("Youtube")) {
                                        StorePrefs.setDefaults(PREF_YOUTUBE, jsonObjectData.getString("link_url"), getActivity());
                                    }
                                }

                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyCode");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

    public void funDiscreption(){
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("org_id", ORG_ID);


            Log.d("Tag", "Params ----------->" + jsonObject.toString());
            new BackGroundTask.makeServiceCallPOSTWithCommonHeader(getActivity(), AboutUsDiscription, jsonObject.toString(),
                    new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String response) {
                            // exerciseJsonResponse = response;
                            //parseJsonData(response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                                    JSONObject jsonData = jsonObject.getJSONObject("data");

                                    StorePrefs.setDefaults(PREF_DESCRIPTION, jsonData.getString("description"), getActivity());
                                    StorePrefs.setDefaults(PREF_DISCLAIMER_DESC, jsonData.getString("disclaimer"), getActivity());


                                } else if (jsonObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                                    Utils.displayToastMessage(getActivity(), jsonObject.getString("replyCode"));
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    }).execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void funLinkList(){
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("org_id", ORG_ID);


            Log.d("Tag", "Params ----------->" + jsonObject.toString());
            new BackGroundTask.makeServiceCallPOSTWithCommonHeader(getActivity(), LinkList, jsonObject.toString(),
                    new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String response) {
                            // exerciseJsonResponse = response;
                            //parseJsonData(response);
                            try {
                                JSONObject jObject = new JSONObject(response);

                                if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                                    JSONArray jsonArraydata = jObject.getJSONArray("data");
                                    System.out.println("======jsonArraydata=========" + jsonArraydata);

                                    // StorePrefs.setDefaults(PREF_DESCRIPTION, jsonData.getString("description"), getActivity());

                                    if (jsonArraydata != null) {
                                        JSONObject jsonObjectData;

                                        for (int i = 0; i < jsonArraydata.length(); i++) {
                                            jsonObjectData = jsonArraydata.getJSONObject(i);

                                            if (jsonObjectData.getString("link_name").equalsIgnoreCase("Facebook")) {
                                                StorePrefs.setDefaults(PREF_FACEBOOk, Constant.nullCheckFunction(jsonObjectData.getString("link_url")), getActivity());
                                            }

                                            if (jsonObjectData.getString("link_name").equalsIgnoreCase("Twitter")) {
                                                StorePrefs.setDefaults(PREF_TWITTER, Constant.nullCheckFunction(jsonObjectData.getString("link_url")), getActivity());
                                            }

                                            if (jsonObjectData.getString("link_name").equalsIgnoreCase("Youtube")) {
                                                StorePrefs.setDefaults(PREF_YOUTUBE, Constant.nullCheckFunction(jsonObjectData.getString("link_url")), getActivity());
                                            }
                                        }

                                    }

                                }else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                                    Utils.displayToastMessage(getActivity(), jObject.getString("replyCode"));
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    }).execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void funNotificaionCount(Context c, String pageNo, String keyword) {
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("page", pageNo);
            jsonObject.put("keyword", keyword);

            Log.d("Tag", "Params ----------->" + jsonObject.toString());
            new BackGroundTask.makeServiceCallPOSTWithCustomHeader(getActivity(), notifications, jsonObject.toString(),MODULE_NOTIFICATIONS,ACTION_VIEW,
                    new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String response) {
                            // exerciseJsonResponse = response;
                            //parseJsonData(response);
                            Log.d("response", "response ----------->" + response);
                            try {
                                JSONObject jObject = new JSONObject(response);

                                if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                                     notificaionCount = Integer.parseInt(Constant.nullCheckFunction(jObject.getString("unreadCount")));
                                  //  setBadgeCount(getActivity(), mBasketIcon, notificaionCount, false);
                                   // mNotifCount = notificaionCount;
                                    if(notificaionCount > 0) {
                                        try {
                                            //notifCount.setText(String.valueOf(notificaionCount));
                                            notifCount.setText(String.valueOf(notificaionCount));
                                        }catch (Exception e){
                                            notifCount.setText("0");

                                        }
                                    }else{
                                        try {
                                            notifCount.setText("0");
                                        }catch (Exception e){
                                            notifCount.setText("0");

                                        }

                                    }


                                } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                                    Utils.displayToastMessage(getActivity(), jObject.getString("replyCode"));
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                notifCount.setText("0");

                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    }).execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, int count, Boolean isRight) {

        Log.e("Value Count "," "+count+" "+isRight);

        if (count <= 0) {
            icon.mutate();
            icon.setDrawableByLayerId(R.id.ic_badge, null);
        } else {
            BadgeDrawable badge;

// Reuse drawable if possible
            Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
            if (reuse != null && reuse instanceof BadgeDrawable) {
                badge = (BadgeDrawable) reuse;
            } else {
                badge = new BadgeDrawable(context, isRight);
            }

            badge.setCount("" + count);
            icon.mutate();
            icon.setDrawableByLayerId(R.id.ic_badge, badge);
        }
    }


}
