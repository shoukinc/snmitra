package com.shreecement.shree.Fragment.MaterialTransaction.PostClass;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.ACTION_ADD;
import static com.shreecement.shree.Utlility.Constant.Create_Materialtransaction;
import static com.shreecement.shree.Utlility.Constant.MODULE_MATERIALTRANSACTIONS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;

/**
 * Created by Abhishek Punia on 15-03-2018.
 */


 public class PostClassCreateMaterialTransaction extends AsyncTask<String, Void, String> {
    private  Context context;

    ProgressDialog progress;
    String jsonreplyMsg;
    JSONObject jObject;

    String transactionTypeCode = "";
    String partyIdToPost = "";


    String user_type,product,transaction_type,inventory_item_id,
            packing_type,transaction_quantity,transaction_date,mitra_id,customer_id,comments,district;

    String architect_involved_flag, architect_id;


    public PostClassCreateMaterialTransaction(Context context, String transactionTypeCode, String user_type, String product, String transaction_type, String partyIdToPost, String inventory_item_id, String packing_type,String transaction_quantity,String transaction_date, String mitra_id, String customer_id, String comments, String district, String architect_involved_flag, String architect_id) {
        this.context = context;
        this.transactionTypeCode = transactionTypeCode;
        this.user_type = user_type;
        this.product = product;
        this.transaction_type = transaction_type;
        this.partyIdToPost = partyIdToPost;
        this.inventory_item_id = inventory_item_id;
        this.packing_type = packing_type;
        this.transaction_quantity= transaction_quantity;
        this.transaction_date = transaction_date;
        this.mitra_id = mitra_id;
        this.customer_id = customer_id;
        this.comments=comments;
        this.district = district;
        this.architect_involved_flag = architect_involved_flag;
        this.architect_id = architect_id;
    }



    protected void onPreExecute() {

        progress = ProgressDialog.show(context, null, null);
        progress.setTitle("Loading...");
        Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
        d.setAlpha(200);
        progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        progress.getWindow().setBackgroundDrawable(d);
        progress.setContentView(R.layout.progress_dialog);
        progress.show();

    }

    @Override
    protected String doInBackground(String... params) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnected()) {
            try {

                OkHttpClient client = new OkHttpClient();
                client.setConnectTimeout(15, java.util.concurrent.TimeUnit.SECONDS); // connect timeout
                client.setReadTimeout(15, java.util.concurrent.TimeUnit.SECONDS);   // socket timeout


                MediaType mediaType = MediaType.parse("application/json");
                JSONObject  jsonObject = new JSONObject();
                //jsonObject.put("page_module", MODULE_MATERIALTRANSACTIONS);

                jsonObject.put("user_type",user_type);
                jsonObject.put("product",product);
                jsonObject.put("transaction_type",transaction_type);

                if(transaction_type.equalsIgnoreCase("DM")){
                    jsonObject.put("mitra_to_party_id",partyIdToPost);
                }
                if(transaction_type.equalsIgnoreCase("DR")){
                    jsonObject.put("retailer_to_party_id",partyIdToPost);
                }
                if(transaction_type.equalsIgnoreCase("DD")){

                }
                if(transaction_type.equalsIgnoreCase("RD")){
                    jsonObject.put("dealer_to_party_id",partyIdToPost);
                }
                if(transaction_type.equalsIgnoreCase("RM")){
                    jsonObject.put("mitra_to_party_id",partyIdToPost);
                }
                if(transaction_type.equalsIgnoreCase("MD")){
                    jsonObject.put("dealer_to_party_id",partyIdToPost);
                }
                if(transaction_type.equalsIgnoreCase("MR")){
                    jsonObject.put("retailer_to_party_id",partyIdToPost);
                }

                jsonObject.put("inventory_item_id",inventory_item_id);
                jsonObject.put("packing_type",packing_type);
                jsonObject.put("transaction_quantity",transaction_quantity);
                jsonObject.put("transaction_date",transaction_date);
                jsonObject.put("mitra_id",mitra_id);
                jsonObject.put("customer_id",customer_id);
                jsonObject.put("comments",comments);
                jsonObject.put("district",district);
                jsonObject.put("architect_involved_flag",architect_involved_flag);
                jsonObject.put("architect_id",architect_id);


                RequestBody body = RequestBody.create(mediaType,jsonObject.toString());
                System.out.println("Params>===============>" + jsonObject.toString());
                System.out.println("Create_Materialtransaction>===============>" + Create_Materialtransaction);
                Request request = new Request.Builder()
                        .url(Create_Materialtransaction)
                        .post(body)
                        .addHeader("content-type", "application/json")
                        .addHeader("page_module", MODULE_MATERIALTRANSACTIONS)
                        .addHeader("action", ACTION_ADD)
                        .addHeader("token",  StorePrefs.getDefaults("token", context))

                        .build();
                Response response = client.newCall(request).execute();
                String reqBody = response.body().string();
                System.out.println("response===============" + reqBody);
                response.message();

                try {
                    jObject = new JSONObject(reqBody);

                    if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                        jsonreplyMsg = jObject.getString("replyMsg");
                        return jObject.getString("replyCode");

                    } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                        return jObject.getString("replyMsg");
                    }
                } catch (JSONException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }

            } catch (IOException e) {
                progress.dismiss();
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    progress.dismiss();
                    Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            });
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progress.dismiss();
        if (s == null) {
            Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
            return;
        } else if (s.equals("success")) {
            android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(
                    context);

            //alert.setTitle(jsonreplyMsg);
            alert.setMessage(jsonreplyMsg);
            alert.setCancelable(false);
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                   /* Fragment fragment = new Fragment_Material_Transaction();
                    FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_left);
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();*/
                    ((Activity)context).onBackPressed();
                }
            });

            alert.show();

        } else {
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

        }

    }
}
