package com.shreecement.shree.Fragment.Referral;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Referral.Adapter.Referral_Adapter;
import com.shreecement.shree.Fragment.Referral.Bean.BeanReferral;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.shreecement.shree.Utlility.Utils;
import com.shreecement.shree.networkUtils.BackGroundTask;
import com.shreecement.shree.networkUtils.OnTaskCompleted;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.MODULE_REFERRALS;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.ReferralListUrl;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Referrals_list extends Fragment {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Referral_Adapter referralAdapter;
    ArrayList<BeanReferral> ReferralList;

    FloatingActionButton btnFabAddBtn;
    String keyword = "";
    int page = 1;
    TextView tv_no_record_found;
    TextView tv_count_item;
    int total_record = 0;
    int recordCount = 0;


    public static Fragment_Referrals_list newInstance(String text) {

        Fragment_Referrals_list f = new Fragment_Referrals_list();
        Bundle b = new Bundle();
        b.putString("edit", text);

        f.setArguments(b);
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_referral_list, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        tv_no_record_found = view.findViewById(R.id.tv_no_record_found);
        tv_count_item = view.findViewById(R.id.tv_count_item);


        btnFabAddBtn = view.findViewById(R.id.btnFabAddBtn);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);

        getActivity().setTitle(getActivity().getString(R.string.referral_list));
        keyword = "";

        ReferralList = new ArrayList<>();
        referralAdapter = new Referral_Adapter(getActivity(), ReferralList);
        recyclerView.setAdapter(referralAdapter);
        referralAdapter.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    loadMoreItems();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            public void onRefresh() {
                refreshItems();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new PostClassReferralList(getActivity(), "1", keyword, true, false).execute();
             //   funReferralList(getActivity(), "1", keyword);

            }
        }, 450);

        return view;
    }



    private void loadMoreItems() {


        new PostClassReferralList(getActivity(), String.valueOf(++page), keyword, true,true).execute();


    }

    private void refreshItems() {
        page = 1;
        ReferralList.clear();
        swipeRefreshLayout.setRefreshing(false);
        // Load items
        // ...
        new PostClassReferralList(getActivity(), String.valueOf(page), keyword, true,false).execute();

    }

    class PostClassReferralList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        String Keyword;
        boolean showLoader;
        List<BeanReferral> tempReferralList = new ArrayList<>();
        boolean   isPagination ;


        PostClassReferralList(Context c, String pageNo, String keyword, boolean showLoader, boolean   isPagination) {
            this.context = c;
            this.pageNo = pageNo;
            this.Keyword = keyword;
            this.showLoader = showLoader;
            this.isPagination = isPagination;
            Log.d("-----Keyword------", "" + Keyword);
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

            tempReferralList.clear();

            }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            String   User_type= StorePrefs.getDefaults(PREF_USER_TYPE, context);
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("page", pageNo);
                    json.put("user_type",User_type);
                    json.put("keyword", Keyword);
                    System.out.println("Param===============" + json.toString());
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(ReferralListUrl)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_REFERRALS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======ReferralListUrl=========" + ReferralListUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");
                            total_record = Integer.parseInt(jObject.getString("total_record"));
                            recordCount = Integer.parseInt(jObject.getString("recordCount"));

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {

                                /*if(jsonArraydata.length()==0){
                                    --page;
                                }
                                */

                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);
                                    tempReferralList.add(new BeanReferral(
                                            jsonObject.getString("referral_id"),
                                            jsonObject));
                                }

                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                //page--;
                --page;
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            if (!isPagination) {
                ReferralList.clear();
                ReferralList.addAll(tempReferralList);

            } else {

                if (tempReferralList.size() > 0) {
                   // mitraList.clear();
                    ReferralList.addAll(tempReferralList);

                } else {
                    Toast.makeText(context, "This is last page", Toast.LENGTH_SHORT).show();
                }
            }

                referralAdapter.notifyDataSetChanged();


            } else {
              //  page--;
                --page;
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }
            referralAdapter.notifyDataSetChanged();

            recordCount=ReferralList.size();
            if (recordCount==0){
                tv_no_record_found.setVisibility(View.VISIBLE);

                //tv_count_item.setText("Showing  "+recordCount+"   of  "+total_record);
            }else {
                tv_no_record_found.setVisibility(View.GONE);
                tv_count_item.setText("Showing  "+recordCount+"   of  "+total_record);
            }

        }
    }

    /*private void funReferralList(Context c, String pageNo, String keyword) {
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("page", pageNo);
            jsonObject.put("keyword", keyword);

            Log.d("Tag", "Params ----------->" + jsonObject.toString());
            new BackGroundTask.makeServiceCallPOSTWithCustomHeader(getActivity(), ReferralListUrl, jsonObject.toString(),MODULE_REFERRALS,ACTION_VIEW,
                    new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String response) {
                            // exerciseJsonResponse = response;
                            //parseJsonData(response);
                            try {
                                JSONObject jObject = new JSONObject(response);

                                if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {





                                } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                                    Utils.displayToastMessage(getActivity(), jObject.getString("replyCode"));
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    }).execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/
}
