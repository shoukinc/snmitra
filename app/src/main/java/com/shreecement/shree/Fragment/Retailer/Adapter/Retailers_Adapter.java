package com.shreecement.shree.Fragment.Retailer.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Retailer.Bean.BeanRetailer_Item;
import com.shreecement.shree.Fragment.Retailer.Fragment_Retailer_Tab;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import java.util.ArrayList;


/**
 * Created by TIA on 12-08-2016.
 */

public class Retailers_Adapter extends RecyclerView.Adapter<Retailers_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanRetailer_Item> rowItems;

    public Retailers_Adapter(Context context, ArrayList<BeanRetailer_Item> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_retailer_row_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String rowItem = rowItems.get(position).getParty_name();

       // holder.tvCustomerId.setText(rowItems.get(position).getAccount_number());
        holder.tvRetailerName.setText(rowItem+"  ("+rowItems.get(position).getAccount_number()+")");
        holder.tvRetailer.setText(rowItems.get(position).getAddress());
        String s=rowItem.substring(0,1);
        holder.tvFirst.setText(s);
        holder.tvFirst.setText(s);
        holder.tvFirst.setBackgroundResource(R.drawable.rounder_dateimage_red);
    }

    @Override
    public int getItemCount() {
        return rowItems.size();

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvRetailerName,tvRetailer,tvRetailerId;
        TextView tvFirst;
        ViewHolder(View convertView) {
            super(convertView);
            tvRetailer = convertView.findViewById(R.id.tvRetailer);
            tvRetailerId = convertView.findViewById(R.id.tvRetailerId);
            tvFirst = convertView.findViewById(R.id.tvFirst);
            tvRetailerName = convertView.findViewById(R.id.tvRetailerName);
            tvRetailerId.setVisibility(View.GONE);
            convertView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {


            BeanRetailer_Item beanRetailer_item = rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanRetailer_item);
            StorePrefs.setDefaults("beanRetailer_item", json, context);
            Fragment_Retailer_Tab fragment = new Fragment_Retailer_Tab();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            ((MainActivity) context).showUpButton(true);

        }
    }


}