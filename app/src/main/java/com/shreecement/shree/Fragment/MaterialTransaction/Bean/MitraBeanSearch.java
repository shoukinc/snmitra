package com.shreecement.shree.Fragment.MaterialTransaction.Bean;

/**
 * Created by chinmay on 15/06/18.
 */

public class MitraBeanSearch {

    String id , name , mobile_number, mitra_type;

    public String getMitra_type() {
        return mitra_type;
    }

    public void setMitra_type(String mitra_type) {
        this.mitra_type = mitra_type;
    }

    public MitraBeanSearch(String id, String name, String mobile_number, String mitra_type) {
        this.id = id;
        this.name = name;
        this.mobile_number = mobile_number;
        this.mitra_type = mitra_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }
}
