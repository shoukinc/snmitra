package com.shreecement.shree.Fragment.Tips.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Tips.Bean.BeanTips_Item;
import com.shreecement.shree.Fragment.Tips.Fragment_Tips_Details;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import java.util.ArrayList;


/**
 * Created by TIA on 12-08-2016.
 */

public class Tips_Adapter extends RecyclerView.Adapter<Tips_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanTips_Item> rowItems;

    public Tips_Adapter(Context context, ArrayList<BeanTips_Item> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_tips_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String rowItem = rowItems.get(position).getTitle();

       // holder.tvCustomerId.setText(rowItems.get(position).getAccount_number());
        holder.tvTips.setText(rowItem);

         }

    @Override
    public int getItemCount() {
        return rowItems.size();

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTips;
        ViewHolder(View convertView) {
            super(convertView);
            tvTips = convertView.findViewById(R.id.tvTips);

            convertView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {

           // StorePrefs.setDefaults("tips_code",rowItems.get(getPosition()).getTip_id(),context);
            BeanTips_Item beanTips_item = rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanTips_item);
            StorePrefs.setDefaults("beanTips_item", json, context);
            Fragment_Tips_Details fragment = new Fragment_Tips_Details();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            ((MainActivity) context).showUpButton(true);

        }
    }


}