package com.shreecement.shree.Fragment.FeedBack;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;

import java.util.ArrayList;
import java.util.List;

import static android.view.Gravity.CENTER;

/**
 * Created by Choudhary on 7/1/2017.
 */

public class Spin_Feedback_adapter extends BaseAdapter implements SpinnerAdapter {

    private final Context activity;
    private ArrayList<LookupCategory> asr;


    public Spin_Feedback_adapter(Context context, List<LookupCategory> asr) {
        this.asr = (ArrayList<LookupCategory>) asr;
        activity = context;

    }

    public int getCount() {
        return asr.size();
    }

    public Object getItem(int i) {
        return asr.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);
        txt.setPadding(16, 16, 16, 16);
        txt.setGravity(Gravity.START|CENTER);
        txt.setTextColor(Color.BLACK);
        txt.setTextSize(15);
        txt.setText(asr.get(position).getDESCRIPTION());
        txt.setBackgroundResource(R.drawable.background_lineview_gray);
        return txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {

            TextView txt = new TextView(activity);
            txt.setGravity(Gravity.START|Gravity.CENTER);
            txt.setPadding(18, 18, 18, 18);
            txt.setTextSize(15);
            txt.setTextColor(activity.getResources().getColor(R.color.colorDarkGray));
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txt.setText(asr.get(i).getDESCRIPTION());
            return txt;


    }

}
