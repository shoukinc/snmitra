package com.shreecement.shree.Fragment.BeanSliderImage;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.shreecement.shree.R;

import java.util.ArrayList;

import static com.shreecement.shree.Utlility.Constant.baseUrlForImage;

public class ImagePagerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    ArrayList<BeanSliderImage> arrayList;
    ImageView   imageView;

    public ImagePagerAdapter(Context context, ArrayList<BeanSliderImage> arrayList) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        if(arrayList != null){
            return arrayList.size();
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }
    public int getItemPosition(Object object) { return POSITION_NONE; }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.custom_layout, container, false);
        BeanSliderImage beanSliderImage = arrayList.get(position);

         imageView = (ImageView) itemView.findViewById(R.id.viewPagerItem_image1);

        /*Picasso.with(context).load(arrayList.get(position))
                .placeholder(R.drawable.image_uploading)
                .error(R.drawable.image_not_found).into(imageView);*/

        Glide.with(context).load(baseUrlForImage + beanSliderImage.getScheme_url())
                .asBitmap()
                .centerCrop()
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
