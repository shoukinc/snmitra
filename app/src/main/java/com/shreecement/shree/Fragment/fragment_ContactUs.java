package com.shreecement.shree.Fragment;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;


/**
 * Created by Choudhary on 6/29/2017.
 */

public class fragment_ContactUs extends Fragment {
    TextView tvName, tvEmail;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_contactus, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        tvName = view.findViewById(R.id.tvName);
        tvEmail = view.findViewById(R.id.tvEmail);

        tvName.setText(StorePrefs.getDefaults("USER_NAME", getActivity()));
        tvEmail.setText(StorePrefs.getDefaults("EMAIL", getActivity()));

        getActivity().setTitle(getActivity().getString(R.string.help));
        return view;

    }


}
