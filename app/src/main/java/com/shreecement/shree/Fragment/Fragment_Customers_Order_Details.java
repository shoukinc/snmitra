package com.shreecement.shree.Fragment;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Adapter.CustomersOrderDespatch_Adapter;
import com.shreecement.shree.ModalClass.BeanCustomer_Item;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import java.util.ArrayList;


/**
 * Created by TIA on 11-08-2017.
 */

public class Fragment_Customers_Order_Details extends Fragment {

    RecyclerView recyclerView;
    CustomersOrderDespatch_Adapter customersOrderDespatch_adapter;
    ArrayList<String> OrderDispatchList = new ArrayList<String>();

    TextView tvNameNumber, tvCityState;
    // TextView txtOrderDetails, txtConsignee, txtDispatchDetails;

    TextView tvCustomerNumber, tvCustomerCity, tvCustomerName, tvCustomerState, tvCustomerAddress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customers_order_details, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        tvCustomerNumber = view.findViewById(R.id.tvCustomerNumber);
        tvCustomerCity = view.findViewById(R.id.tvCustomerCity);
        tvCustomerName = view.findViewById(R.id.tvCustomerName);
        tvCustomerState = view.findViewById(R.id.tvCustomerState);
        tvCustomerAddress = view.findViewById(R.id.tvCustomerAddress);


        getActivity().setTitle(getActivity().getString(R.string.customer_details));


        Gson gson = new Gson();
        String json = StorePrefs.getDefaults("beanCustomer_item", getActivity());
        BeanCustomer_Item beanCustomer_item = gson.fromJson(json, BeanCustomer_Item.class);

       /* tvNameNumber.setText( beanCustomer_item.getCust_account_id()   + " & "+beanCustomer_item.getParty_name());
        tvCityState.setText(beanCustomer_item.getCity()+ " & "+ beanCustomer_item.getState());*/

        tvCustomerNumber.setText(beanCustomer_item.getCust_account_id());
        tvCustomerName.setText(beanCustomer_item.getParty_name());
        tvCustomerCity.setText(beanCustomer_item.getCity());
        tvCustomerState.setText(beanCustomer_item.getState());
        tvCustomerAddress.setText(beanCustomer_item.getAddress());

        OrderDispatchList.clear();
        intiDisplay(view);

        return view;
    }

    private void intiDisplay(View view) {
        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        customersOrderDespatch_adapter = new CustomersOrderDespatch_Adapter(getActivity(), OrderDispatchList);
        recyclerView.setAdapter(customersOrderDespatch_adapter);
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);


        OrderDispatchList.add("Dispatch123");
        OrderDispatchList.add("Dispatch543");
        OrderDispatchList.add("Dispatch5984");
        OrderDispatchList.add("Dispatch5875");
        OrderDispatchList.add("Dispatch5345");
        OrderDispatchList.add("Dispatch575");

        customersOrderDespatch_adapter.notifyDataSetChanged();
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();
    }

}
