package com.shreecement.shree.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.shreecement.shree.ModalClass.BeanOrderDetails;
import com.shreecement.shree.ModalClass.BeanOrderList;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.MODULE_ORDERS;
import static com.shreecement.shree.Utlility.Constant.OrderdetailUrl;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Order_Requirement_TradeView extends Fragment {
    TextView tvCategory, tvBrand, tvCustomer, tvConsingee, tvAddress, tvProduct, tvPacking, tvRs, tvValid, tvDispatch, tvMiddleName,
            tvQty, tvRemark;
    BeanOrderList beanOrderList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_order_requirement_tradeview, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        tvCategory = (TextView) view.findViewById(R.id.tvCategory);
        tvCustomer = (TextView) view.findViewById(R.id.tvCustomer);
        tvConsingee = (TextView) view.findViewById(R.id.tvConsingee);
        tvAddress = (TextView) view.findViewById(R.id.tvAddress);
        tvProduct = (TextView) view.findViewById(R.id.tvProduct);
        tvPacking = (TextView) view.findViewById(R.id.tvPacking);
        tvBrand = (TextView) view.findViewById(R.id.tvBrand);

        tvQty = (TextView) view.findViewById(R.id.tvQty);
        tvRemark = (TextView) view.findViewById(R.id.tvRemark);

        tvRs = (TextView) view.findViewById(R.id.tvRs);
        tvValid = (TextView) view.findViewById(R.id.tvValid);
        tvDispatch = (TextView) view.findViewById(R.id.tvDispatch);
        tvMiddleName = (TextView) view.findViewById(R.id.tvMiddleName);

        //Get a data..........
        Gson gson = new Gson();
        String json = StorePrefs.getDefaults("beanOrderList", getActivity());
        beanOrderList = gson.fromJson(json, BeanOrderList.class);

        String orderId = getArguments().getString("orderId");

        //Post Class Calll..........
        new PostClassOrdeeDetails(getActivity(), orderId).execute();

        return view;
    }

    class PostClassOrdeeDetails extends AsyncTask<String, Void, String> {

        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        BeanOrderDetails beanOrderDetails = null;
        String Id = "";
        private Context context;


        public PostClassOrdeeDetails(Context c, String id) {
            this.context = c;
            this.Id = id;
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("id", Id);
                    //json.put("page_module", MODULE_ORDERS);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(OrderdetailUrl)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_ORDERS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======OrderdetailUrl=========" + OrderdetailUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONObject jsonObject = jObject.getJSONObject("data");
                            if (jsonObject != null) {
                                beanOrderDetails = new BeanOrderDetails(
                                        jsonObject.getString("customer_name"),
                                        jsonObject.getString("consignee_name"),
                                        jsonObject.getString("order_id"),
                                        jsonObject.getString("mm_name"),
                                        jsonObject.getString("customer_code"),
                                        jsonObject.getString("product"),
                                        jsonObject.getString("brand"),
                                        jsonObject.getString("packingtype"),
                                        jsonObject.getString("quantity"),
                                        jsonObject.getString("consignee"),
                                        jsonObject.getString("state"),
                                        jsonObject.getString("district"),
                                        jsonObject.getString("taluka"),
                                        jsonObject.getString("city"),
                                        jsonObject.getString("remark"),
                                        jsonObject.getString("category"),
                                        jsonObject.getString("validity"),
                                        jsonObject.getString("address"),
                                        jsonObject.getString("dispatch_from"),
                                        jsonObject.getString("rate"),
                                        jsonObject.getString("created_by"),
                                        jsonObject.getString("last_updated_by"),
                                        jsonObject.getString("last_update_login"),
                                        jsonObject.getString("last_update_date"),
                                        jsonObject.getString("order_date"),
                                        jsonObject.getString("brand_name"),
                                        jsonObject.getString("packing_type_name"),
                                        jsonObject.getString("product_name"),
                                        jsonObject.getString("category_name")

                                );


                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {

                if (beanOrderDetails.getCategory_name().equals("Trade")) {
                    getActivity().setTitle(context.getResources().getString(R.string.order_number));
                    tvRs.setVisibility(View.GONE);
                    tvValid.setVisibility(View.GONE);
                    tvDispatch.setVisibility(View.GONE);
                    tvMiddleName.setVisibility(View.GONE);
                    tvCategory.setText(beanOrderDetails.getCategory());
                    tvBrand.setText(beanOrderDetails.getBrand());
                    tvCustomer.setText(beanOrderDetails.getCustomer_name());
                    tvConsingee.setText(beanOrderDetails.getConsignee_name());
                    tvAddress.setText(beanOrderDetails.getAddress());
                    tvPacking.setText(beanOrderDetails.getPackingtype());
                    tvQty.setText(beanOrderDetails.getQuantity());
                    tvRemark.setText(beanOrderDetails.getRemark());
                    tvProduct.setText(beanOrderDetails.getProduct_name());

                } else if (beanOrderDetails.getCategory_name().equals("Non Trade")) {
                    tvRs.setVisibility(View.VISIBLE);
                    tvValid.setVisibility(View.VISIBLE);
                    tvDispatch.setVisibility(View.VISIBLE);
                    tvMiddleName.setVisibility(View.VISIBLE);
                    getActivity().setTitle(context.getResources().getString(R.string.order_number));
                    tvCategory.setText(beanOrderDetails.getCategory_name());
                    tvBrand.setText(beanOrderDetails.getBrand_name());
                    tvCustomer.setText(beanOrderDetails.getCustomer_name());
                    tvConsingee.setText(beanOrderDetails.getConsignee_name());
                    tvAddress.setText(beanOrderDetails.getAddress());
                    tvPacking.setText(beanOrderDetails.getPackingtype());
                    tvQty.setText(beanOrderDetails.getQuantity());
                    tvRemark.setText(beanOrderDetails.getRemark());
                    tvRs.setText(beanOrderDetails.getRate());

                    DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
                    Date date1 = null;
                    Calendar cal = Calendar.getInstance();
                    try {
                        date1 = readFormat1.parse(beanOrderDetails.getValidity());
                        String dateformatt = formatter1.format(date1);

                        tvValid.setText(dateformatt);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    tvDispatch.setText(beanOrderDetails.getDispatch_from());
                    tvMiddleName.setText(beanOrderDetails.getMm_name());
                    tvProduct.setText(beanOrderDetails.getProduct_name());
                }


            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }


}
