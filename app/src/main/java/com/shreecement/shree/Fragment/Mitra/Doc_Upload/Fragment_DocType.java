package com.shreecement.shree.Fragment.Mitra.Doc_Upload;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.R;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_DocType extends Fragment implements View.OnClickListener {

    Button btnAadhaar,btnVoterId,btnPanCard,btnDrivingLicence;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_doc_type, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        btnAadhaar = (Button) view.findViewById(R.id.btnAadhaar);
        btnAadhaar.setOnClickListener(this);

        btnVoterId = (Button) view.findViewById(R.id.btnVoterId);
        btnVoterId.setOnClickListener(this);

        btnPanCard = (Button) view.findViewById(R.id.btnPanCard);
        btnPanCard.setOnClickListener(this);

        btnDrivingLicence = (Button) view.findViewById(R.id.btnDrivingLicence);
        btnDrivingLicence.setOnClickListener(this);

        return view;

    }

    @Override
    public void onClick(View view) {
        if (view == btnAadhaar){
            Fragment_DocUpload fragment = new Fragment_DocUpload();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            ((MainActivity) getActivity()).showUpButton(true);
        }
    }
}
