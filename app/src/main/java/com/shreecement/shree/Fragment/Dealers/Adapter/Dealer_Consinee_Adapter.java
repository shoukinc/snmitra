package com.shreecement.shree.Fragment.Dealers.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shreecement.shree.Fragment.Dealers.Bean.BeanDealer_OrderItem;
import com.shreecement.shree.R;

import java.util.ArrayList;


/**
 * Created by TIA on 12-08-2016.
 */

public class Dealer_Consinee_Adapter extends RecyclerView.Adapter<Dealer_Consinee_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanDealer_OrderItem> rowItems;

    public Dealer_Consinee_Adapter(Context context, ArrayList<BeanDealer_OrderItem> rowItem) {
        this.context = context;
        this.rowItems = rowItem;

}
    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    @Override
    public int getItemViewType(int position) {
                return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.fragment_customers_order_details_row_list, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final BeanDealer_OrderItem rowItem = rowItems.get(position);
        Log.d("----rowItems---1-----",""+rowItems.size());
        holder.tvOrder.setText("Order : "+rowItem.getProduct());
        holder.tvPenddingQty.setText("Quantity : "+rowItem.getQuantity());
        holder.tvHoldQty.setText("Date : "+rowItem.getOrder_date());

        holder.itemView.setTranslationX(-(50+position*100));
        holder.itemView.setAlpha(0.5f);
        holder.itemView.animate().alpha(1f).translationX(0).setDuration(700).start();


    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvOrder,tvPenddingQty,tvHoldQty;

        ViewHolder(View convertView) {
            super(convertView);
            convertView.setOnClickListener(this);
            tvPenddingQty = convertView.findViewById(R.id.tvPendingQty);
            tvOrder =convertView.findViewById(R.id.tvOrder);
            tvHoldQty = convertView.findViewById(R.id.tvHoldQuantity);

        }
        @Override
        public void onClick(View v) {

        }
    }
}