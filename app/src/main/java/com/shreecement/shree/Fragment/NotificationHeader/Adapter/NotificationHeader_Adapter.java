package com.shreecement.shree.Fragment.NotificationHeader.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Common.BeanCommon;
import com.shreecement.shree.Fragment.NotificationHeader.Fragment_NotficationHeader_DataView;
import com.shreecement.shree.R;

import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by TIA on 12-08-2016.
 */

public class NotificationHeader_Adapter extends RecyclerView.Adapter<NotificationHeader_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanCommon> rowItems;

    public NotificationHeader_Adapter(Context context, ArrayList<BeanCommon> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_notification_header_row_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        BeanCommon beanMitra = rowItems.get(position);

        JSONObject jsonObject = beanMitra.getJsonObject();

        try {
            holder.tvStatus.setText(jsonObject.getString("status_text"));
            holder.tvCreationDate.setText(jsonObject.getString("creation_date"));
            holder.tvNotification.setText(jsonObject.getString("notification"));
        }catch (Exception e){

        }
       /* holder.tvStatus.setText(rowItems.get(position).getStatus_text());
        holder.tvCreationDate.setText(rowItems.get(position).getCreation_date());
        holder.tvNotification.setText(rowItems.get(position).getNotification());*/

       // Random rnd = new Random();

        //int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

       // holder.tvFirst.setBackgroundResource(R.drawable.rounder_dateimage);
     /*   GradientDrawable drawable = (GradientDrawable) holder.tvFirst.getBackground();
        drawable.setColor(context.getResources().getColor(R.color.colorPrimary));*/
        //RotateAnimation rotate= (RotateAnimation) AnimationUtils.loadAnimation(context,R.anim.rotate_bnimation);
        /*RotateAnimation rotate= new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(3000);
        rotate.setInterpolator(new LinearInterpolator());
        holder.tvFirst.setAnimation(rotate);*/

    }

    @Override
    public int getItemCount() {
        return rowItems.size();

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNotification,tvCreationDate,tvStatus;
       // ImageView tvFirst;
        ViewHolder(View convertView) {
            super(convertView);
            tvNotification = convertView.findViewById(R.id.tvNotification);
            tvCreationDate = convertView.findViewById(R.id.tvCreationDate);
            tvStatus = convertView.findViewById(R.id.tvStatus);
          //  tvFirst = convertView.findViewById(R.id.tvFirst);

            convertView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {

           /* StorePrefs.setDefaults(PREF_NOTIFICATION_ID,rowItems.get(getPosition()).getNotification_id(),context);
            BeanNotificationHeader beanNotificationHeader = rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanNotificationHeader);
            StorePrefs.setDefaults("beanNotificationHeader", json, context);

            Fragment_NotficationHeader_DataView fragment = new Fragment_NotficationHeader_DataView();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            ((MainActivity) context).showUpButton(true);*/

            ((Activity) context).onBackPressed();

            BeanCommon beanNotification_item = rowItems.get(getLayoutPosition());

            Fragment_NotficationHeader_DataView fragment = new Fragment_NotficationHeader_DataView();
            fragment.notificationId =beanNotification_item.getId();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            ((MainActivity) context).showUpButton(true);

        }
    }



}