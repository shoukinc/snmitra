package com.shreecement.shree.Fragment.NotificationHeader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.shreecement.shree.Common.Common_Adapter_for_details_Notifications;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.MODULE_NOTIFICATIONS;
import static com.shreecement.shree.Utlility.Constant.NotificationDetail;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.dateFuncation;
import static com.shreecement.shree.Utlility.Constant.showToastMessage;


/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_NotficationHeader_DataView extends Fragment {



    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Common_Adapter_for_details_Notifications commonAdapterForDetails;
    ArrayList<LookupCategory> NotificationList;
    public String notificationId = "";
    String reqBody;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_common_details_list, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getActivity().setTitle(getActivity().getString(R.string.notifications_details));

        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);

        NotificationList = new ArrayList<>();
        commonAdapterForDetails = new Common_Adapter_for_details_Notifications(getActivity(), NotificationList);
        recyclerView.setAdapter(commonAdapterForDetails);
        commonAdapterForDetails.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();

        new PostClassNotificationDetail(getActivity(), notificationId).execute();

        return view;
    }

    class PostClassNotificationDetail extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempNotificationList;
        String id;

        PostClassNotificationDetail(Context c, String id) {
            this.context = c;
            this.id = id;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

            tempNotificationList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();

                    json.put("id", id);
                    //json.put("page_module", MODULE_NOTIFICATIONS);
                    RequestBody body = RequestBody.create(mediaType, json.toString());
                    System.out.println("param===============>" + json.toString());

                    Request request = new Request.Builder()
                            .url(NotificationDetail)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_NOTIFICATIONS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                     reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======NotificationDetail=========" + NotificationDetail);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONObject jsonData = jObject.getJSONObject("data");


                            JSONObject jsonObjMaterial_transaction_detail = jsonData.getJSONObject("material_transaction_detail");

                            if (jsonObjMaterial_transaction_detail != null) {

                                if (jsonObjMaterial_transaction_detail.toString().equals("{}")) ;

                                else {
                                    Log.e("In part", "in part");

                                   /* tempNotificationList.add(new LookupCategory("Party Name",
                                            "", jsonObjMaterial_transaction_detail.getString("to_party_name")));*/

                                    tempNotificationList.add(new LookupCategory("Transaction Type",
                                            "", jsonObjMaterial_transaction_detail.getString("transaction_type_disp")));

                                    tempNotificationList.add(new LookupCategory("Transaction Date",
                                            "", dateFuncation(jsonObjMaterial_transaction_detail.getString("transaction_date"))));

                                    tempNotificationList.add(new LookupCategory("District",
                                            "", jsonObjMaterial_transaction_detail.getString("district")));


                                    tempNotificationList.add(new LookupCategory("Product Name",
                                            "", jsonObjMaterial_transaction_detail.getString("inventory_item_name")));

                                    tempNotificationList.add(new LookupCategory("Packing Type",
                                            "", jsonObjMaterial_transaction_detail.getString("packing_type")));

                                    tempNotificationList.add(new LookupCategory("Transaction Quantity",
                                            "", jsonObjMaterial_transaction_detail.getString("transaction_quantity")));


                                    if (jsonObjMaterial_transaction_detail.getString("architect_involved_flag").equalsIgnoreCase("y")) {

                                        tempNotificationList.add(new LookupCategory("Architect",
                                                "", jsonObjMaterial_transaction_detail.getString("architect_name")));
                                    }

                                }
                            }

                            if (jObject != null) {

                                tempNotificationList.add(new LookupCategory("Source Id",
                                        "", jsonData.getString("source_id")));

                                tempNotificationList.add(new LookupCategory("Sender Name",
                                        "", jsonData.getString("sender_name")));

                                tempNotificationList.add(new LookupCategory("Source Type",
                                        "", jsonData.getString("source_type")));



                                tempNotificationList.add(new LookupCategory("Notification",
                                        "", jsonData.getString("notification")));



                               /* tempNotificationList.add(new LookupCategory("Date",
                                        "", Constant.dateFuncation(jsonData.getString("last_update_date"))));*/

                                tempNotificationList.add(new LookupCategory("Creation Date",
                                        "", dateFuncation(jsonData.getString("creation_date"))));

                                tempNotificationList.add(new LookupCategory("Status",
                                        "", jsonData.getString("status_text")));



                                if (!jsonData.getString("status_text").equalsIgnoreCase("Approved"))
                                    tempNotificationList.add(new LookupCategory("action",
                                            "",  jsonData.getString("status_text")));
                            }

                            commonAdapterForDetails.setData(jsonData.getString("source_id"),
                                    jsonData.getString("source_type"));

                            NotificationList.addAll(tempNotificationList);


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                //Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                showToastMessage(getActivity(),reqBody);

                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


                commonAdapterForDetails.notifyDataSetChanged();

            } else {

                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

}
