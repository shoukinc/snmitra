package com.shreecement.shree.Fragment.Help.Bean;

/**
 * Created by kamesh on 2/1/18.
 */

public class BeanHelp_Item {

    private String help_topic_id, help_topic_number, title, short_description,long_description;

    public BeanHelp_Item(String help_topic_id,
                         String help_topic_number,
                         String title,
                         String short_description,
                         String long_description) {
        this.help_topic_id = help_topic_id;
        this.help_topic_number = help_topic_number;
        this.title = title;
        this.short_description = short_description;
        this.long_description = long_description;
    }

    public String getHelp_topic_id() {
        return help_topic_id;
    }

    public void setHelp_topic_id(String help_topic_id) {
        this.help_topic_id = help_topic_id;
    }

    public String getHelp_topic_number() {
        return help_topic_number;
    }

    public void setHelp_topic_number(String help_topic_number) {
        this.help_topic_number = help_topic_number;
    }

    public String getTitle() {
        return title;
    }

    public void getTitle(String title) {
        this.title = title;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getLong_description() {
        return long_description;
    }

    public void setLong_description(String long_description) {
        this.long_description = long_description;
    }

}
