package com.shreecement.shree.Fragment.MaterialTransaction;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.shreecement.shree.Common.Common_Adapter_for_details;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.MODULE_MITRAS;
import static com.shreecement.shree.Utlility.Constant.MitraDetail;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;


/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_MaterialTransaction_Details extends Fragment {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Common_Adapter_for_details commonAdapterForDetails;
    ArrayList<LookupCategory> materialTransactionList;
    public JSONObject jsonObject = new JSONObject();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_common_details_list, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getActivity().setTitle(getActivity().getString(R.string.material_transaction_details));


       /* Gson gson = new Gson();
        String json = StorePrefs.getDefaults(PREF_BEAN_MaterialTransaction, getActivity());
        BeanCommon beanMaterialTransaction = gson.fromJson(json, BeanCommon.class);

        JSONObject jsonObject = beanMaterialTransaction.getJsonObject();
*/
       // Toast.makeText(getActivity(), ""+beanMaterialTransaction.getId(), Toast.LENGTH_SHORT).show();

        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);

        materialTransactionList = new ArrayList<>();
        commonAdapterForDetails = new Common_Adapter_for_details(getActivity(), materialTransactionList);
        recyclerView.setAdapter(commonAdapterForDetails);
        commonAdapterForDetails.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();

       // new PostClassMaterailTransactionDetail(getActivity(),beanMaterialTransaction.getId()).execute();

        if (jsonObject != null) {

            try {

                materialTransactionList.add(new LookupCategory("Type",
                        "", Constant.nullCheckFunction(jsonObject.getString("transaction_type_disp"))));

                materialTransactionList.add(new LookupCategory("To Party",
                        "", Constant.nullCheckFunction(jsonObject.getString("to_party_name"))));

                materialTransactionList.add(new LookupCategory("District",
                        "", Constant.nullCheckFunction(jsonObject.getString("district"))));

                materialTransactionList.add(new LookupCategory("Product",
                        "", Constant.nullCheckFunction(jsonObject.getString("inventory_item_name"))));

                materialTransactionList.add(new LookupCategory("Packing Type",
                        "", Constant.nullCheckFunction(jsonObject.getString("packing_type"))));
                /*
                materialTransactionList.add(new LookupCategory("Transaction Id",
                        "", jsonObject.getString("transaction_id")));*/

                materialTransactionList.add(new LookupCategory("Transaction Date",
                        "", Constant.dateFuncation(jsonObject.getString("transaction_date"))));

                materialTransactionList.add(new LookupCategory("Quantity",
                        "", Constant.nullCheckFunction(jsonObject.getString("transaction_quantity"))));

                if (Constant.nullCheckFunction(jsonObject.getString("architect_involved_flag")).equalsIgnoreCase("y")){

                    materialTransactionList.add(new LookupCategory("Architect",
                            "", Constant.nullCheckFunction(jsonObject.getString("architect_name"))));
                }

                materialTransactionList.add(new LookupCategory("Creation Date",
                        "", Constant.dateFuncation(jsonObject.getString("creation_date"))));

                materialTransactionList.add(new LookupCategory("From Party",
                        "", Constant.nullCheckFunction(jsonObject.getString("from_party_name"))));

                materialTransactionList.add(new LookupCategory("comments",
                        "", Constant.nullCheckFunction(jsonObject.getString("comments"))));

                materialTransactionList.add(new LookupCategory("Status",
                        "", Constant.nullCheckFunction(jsonObject.getString("current_status_disp"))));



            }catch (Exception e){

            }

        }
       // materialTransactionList.addAll(materialTransactionList);
        commonAdapterForDetails.notifyDataSetChanged();

        return view;
    }

    class PostClassMaterailTransactionDetail extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> materialTransactionList;
        String id;

        PostClassMaterailTransactionDetail(Context c, String id) {
            this.context = c;
            this.id = id;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

            materialTransactionList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();

                    json.put("id",id );
                    //json.put("page_module", MODULE_MITRAS);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(MitraDetail)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_MITRAS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======MitraDetail=========" + MitraDetail);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONObject jsonData = jObject.getJSONObject("data");
                            if (jObject != null) {


                                materialTransactionList.add(new LookupCategory("Full Name",
                                        "", jsonData.getString("full_name")));

                                materialTransactionList.add(new LookupCategory("Email Address",
                                        "", jsonData.getString("email_address")));

                                materialTransactionList.add(new LookupCategory("Mobile Number",
                                        "", jsonData.getString("mobile_number")));

                                materialTransactionList.add(new LookupCategory("Gender",
                                        "", jsonData.getString("sex_code")));

                                materialTransactionList.add(new LookupCategory("Blood Type",
                                        "", jsonData.getString("blood_type_code")));

                                materialTransactionList.add(new LookupCategory("Date Of Birth",
                                        "", Constant.dateFuncation( jsonData.getString("date_of_birth"))));

                                materialTransactionList.add(new LookupCategory("Mitra Number",
                                        "", jsonData.getString("mitra_number")));

                                materialTransactionList.add(new LookupCategory("Aadhaar Number",
                                        "", jsonData.getString("adhaar_number")));

                                materialTransactionList.add(new LookupCategory("Voter Id",
                                        "", jsonData.getString("voter_id")));

                                materialTransactionList.add(new LookupCategory("PAN Card",
                                        "", jsonData.getString("pan")));

                                materialTransactionList.add(new LookupCategory("Driving Licence",
                                        "", jsonData.getString("driving_licence_number")));

                                materialTransactionList.add(new LookupCategory("GST Number",
                                        "", jsonData.getString("gst_number")));

                                materialTransactionList.add(new LookupCategory("Marital Status",
                                        "", jsonData.getString("marital_status_code")));

                                materialTransactionList.add(new LookupCategory("Spouse Name",
                                        "", jsonData.getString("spouse_name")));

                                materialTransactionList.add(new LookupCategory("Anniversary",
                                        "", Constant.dateFuncation( jsonData.getString("date_of_wedding"))));

                                materialTransactionList.add(new LookupCategory("Child 1 Name",
                                        "", jsonData.getString("child1_name")));

                                materialTransactionList.add(new LookupCategory("Date Of Birth",
                                        "", Constant.dateFuncation( jsonData.getString("date_of_birth_child1"))));

                                materialTransactionList.add(new LookupCategory("Child 2 Name",
                                        "", jsonData.getString("child2_name")));

                                materialTransactionList.add(new LookupCategory("Date Of Birth",
                                        "",  Constant.dateFuncation(jsonData.getString("date_of_birth_child2"))));

                                materialTransactionList.add(new LookupCategory("Address Line 1",
                                        "", jsonData.getString("address_line1")));

                                materialTransactionList.add(new LookupCategory("Address Line 2",
                                        "", jsonData.getString("address_line2")));

                                materialTransactionList.add(new LookupCategory("Address Line 3",
                                        "", jsonData.getString("address_line3")));

                                materialTransactionList.add(new LookupCategory("City",
                                        "", jsonData.getString("city")));

                                materialTransactionList.add(new LookupCategory("Taluka",
                                        "", jsonData.getString("taluka")));

                                materialTransactionList.add(new LookupCategory("District",
                                        "", jsonData.getString("district")));

                                materialTransactionList.add(new LookupCategory("State",
                                        "", jsonData.getString("state")));


                            }
                            materialTransactionList.addAll(materialTransactionList);


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


                commonAdapterForDetails.notifyDataSetChanged();

            } else {

                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }


}
