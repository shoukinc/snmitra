package com.shreecement.shree.Fragment.Referral;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.ModalClass.BeanCustomer_Item;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.ACTION_ADD;
import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.Lookup_data;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.MODULE_REFERRALS;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.ReferralCreate;



/*
 * Created by kiran on 13-04-2018
 */

public class Fragment_Referral extends Fragment {
    Button btnSave;
    EditText edReferralName, edMobileNumber,edComment;
    ListView listView;

    public AlertDialog.Builder dialogBuilder;
    public AlertDialog builder3;

    SpinnerDialogAdapter spinnerAdapter;

    String mitraCode = "";
    public TextView tvProfession;
    public ArrayList<LookupCategory> ProfessionList;

    FloatingActionButton btnFabAddBtn;

    public static Fragment_Referral newInstance(String text) {

        Fragment_Referral f = new Fragment_Referral();
        Bundle b = new Bundle();
        b.putString("edit", text);

        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_referral_entery, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setHasOptionsMenu(true);
        getActivity().setTitle(getActivity().getString(R.string.nav_item_referral));

        btnFabAddBtn = view.findViewById(R.id.btnFabAddBtn);
        btnSave = view.findViewById(R.id.btnSave);
        tvProfession = view.findViewById(R.id.tvProfession);
        edReferralName = view.findViewById(R.id.edReferralName);
        edMobileNumber = view.findViewById(R.id.edMobileNumber);
        edComment = view.findViewById(R.id.edComment);

        btnFabAddBtn.setVisibility(View.GONE);

        ProfessionList = new ArrayList<LookupCategory>();



        tvProfession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSpinnerDialog(ProfessionList, "Profession");
                new PostClass_LookUpData(getActivity(), "1", "", true).execute();

            }
        });

        btnFabAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);
                fragmentTransaction.replace(R.id.container_body, Fragment_Referrals_list.newInstance("add")).addToBackStack(null);
                fragmentTransaction.commit();

                ((MainActivity) getActivity()).showUpButton(true);

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edReferralName.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Referral name is required!", Toast.LENGTH_LONG).show();

                } else if (edMobileNumber.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Mobile number field is required!", Toast.LENGTH_LONG).show();

                } else if (tvProfession.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Profession field is required!", Toast.LENGTH_LONG).show();
                }
                else if (edComment.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Pleae leave any comment.", Toast.LENGTH_LONG).show();
                }
                else{
                     new PostClassSendReferral(getActivity(),edReferralName.getText().toString(),
                             edMobileNumber.getText().toString(),
                             mitraCode,
                             edComment.getText().toString()
                             ).execute();
                }
            }
        });

        return view;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_common, menu);

        MenuItem item = menu.findItem(R.id.action_button_basket);
        item.setIcon(R.drawable.ic_list);
        View view1 = MenuItemCompat.getActionView(item);


        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);
                fragmentTransaction.replace(R.id.container_body, Fragment_Referrals_list.newInstance("add")).addToBackStack(null);
                fragmentTransaction.commit();


                ((MainActivity) getActivity()).showUpButton(true);


                return false;
            }
        });


    }

    private void showSpinnerDialog(ArrayList<LookupCategory> list, String title) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        listView = (ListView) dialogView.findViewById(R.id.listCatecory);
        TextView dialogTitle = (TextView) dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerAdapter = new SpinnerDialogAdapter(getActivity(), list, title);
        listView.setAdapter(spinnerAdapter);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }


    public class SpinnerDialogAdapter extends BaseAdapter {
        Context context;
        List<LookupCategory> rowItems;
        String Title = "";


        public SpinnerDialogAdapter(Context context, List<LookupCategory> items, String title) {
            this.context = context;
            this.rowItems = items;
            this.Title = title;
            Log.d("===rowItems.zize===", "" + rowItems.size());


        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;

        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new ViewHolder();
                holder.textName = convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();

            }

            holder.textName.setText(rowItems.get(position).getDESCRIPTION());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String meaning = rowItems.get(position).getDESCRIPTION();

                            builder3.dismiss();
                    tvProfession.setText(meaning);
                    mitraCode = rowItems.get(position).getCODE();
                }
            });
            return convertView;
        }


        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }


    }

    class PostClass_LookUpData extends AsyncTask<String, Void, String> {

        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;

        String pageNo;
        String Keyword;
        boolean showLoader;
        List<BeanCustomer_Item> tempcustomerlist;

        PostClass_LookUpData(Context c, String pageNo, String keyword, boolean showLoader) {
            this.context = c;
            this.pageNo = pageNo;
            this.Keyword = keyword;
            this.showLoader = showLoader;
            Log.d("====keyword=====", "" + Keyword);
        }

        protected void onPreExecute() {
            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            ProfessionList = new ArrayList<>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                  /*  MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("keyword", Keyword);
                    json.put("page", pageNo);
                    json.put("page_module", MODULE_COMMON);

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Lookup_data)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();*/

                    RequestBody body = RequestBody.create(null, new byte[]{});

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .addHeader("page_module", MODULE_COMMON)
                           // .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Lookup_data=========" + Lookup_data);

                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONObject jsonObjectData=jObject.getJSONObject("data");
                            JSONArray jsonArraycategory = jsonObjectData.getJSONArray("mitra_type");
                            System.out.println("======jsonArraycategory=========" + jsonArraycategory);
                            if (jsonArraycategory != null) {
                                JSONObject jsonObject;
                                for (int i = 0; i < jsonArraycategory.length(); i++) {
                                    jsonObject = jsonArraycategory.getJSONObject(i);
                                    System.out.println("======jsonObject=========" + jsonObject);

                                    ProfessionList.add(new LookupCategory(
                                            jsonObject.getString("lookup_type"),
                                            jsonObject.getString("code"),
                                            jsonObject.getString("description")
                                            ));

                                }

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {

                spinnerAdapter = new SpinnerDialogAdapter(getActivity(), ProfessionList, "Profession");
                listView.setAdapter(spinnerAdapter);

            } else {


            }
            spinnerAdapter.notifyDataSetChanged();
        }

    }

    class PostClassSendReferral extends AsyncTask<String, Void, String> {
        private  Context context;
        ProgressDialog progress;
        String jsonreplyMsg;
        JSONObject jObject;

        String referral_name, mobile_number, mitra_type_code, comments;

        public PostClassSendReferral(Context context, String referral_name, String mobile_number, String mitra_type_code, String comments) {
            this.context = context;
            this.referral_name= referral_name;
            this.mobile_number = mobile_number;
            this.mitra_type_code = mitra_type_code;
            this.comments = comments;

        }



        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            String   User_type= StorePrefs.getDefaults(PREF_USER_TYPE, context);
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject  jsonObject = new JSONObject();

                    jsonObject.put("referral_name", referral_name);
                    jsonObject.put("mobile_number", mobile_number);
                    jsonObject.put("mitra_type", mitra_type_code);
                    jsonObject.put("comments", comments);
                    jsonObject.put("org_id", ORG_ID);

                    RequestBody body = RequestBody.create(mediaType,jsonObject.toString());
                    System.out.println("jsonObject.toString()===============" + jsonObject.toString());
                    Request request = new Request.Builder()
                            .url(ReferralCreate)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_REFERRALS)
                            .addHeader("action", ACTION_ADD)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();

                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                AlertDialog.Builder alert = new AlertDialog.Builder(
                        context);
                alert.setTitle(jsonreplyMsg);
                alert.setCancelable(false);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       /* Fragment fragment = new Fragment_Referrals_list();
                        FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_left);
                        fragmentTransaction.replace(R.id.container_body, fragment);
                        fragmentTransaction.commit();*/

                        getActivity().onBackPressed();
                    }
                });

                alert.show();

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

}