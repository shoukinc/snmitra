package com.shreecement.shree.Fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.ModalClass.BeanMiddleman;
import com.shreecement.shree.ModalClass.CustomerSearch;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.PostClass.PostClassAddOrderRec;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.ConsineeSearchearchUrl;
import static com.shreecement.shree.Utlility.Constant.CustomerSearchUrl;
import static com.shreecement.shree.Utlility.Constant.Lookup_data;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.MODULE_ORDERS;
import static com.shreecement.shree.Utlility.Constant.Middle_man;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Add_Order extends Fragment implements View.OnClickListener {
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog builder3;
    public SpinnerDialogAdapter spinnerAdapter;
    public CustomerSearchAdapter customerSearchAdapter;
    public ConsinggeAdapter consignee_adapter;
    public MiddleManAdapter middleManAdapter;
    public TextView tvCategory;
    public TextView tvBrand;
    public TextView tvProduct;
    public TextView tvPackingType;
    public TextView tvDispatch;
    public TextView tvValid;
    public TextView tvConsignee;
    public TextView tvMiddleName;
    public EditText edAddress;
    public EditText edQty;
    public EditText edRate;
    public EditText edRemark;
    public TextView tvCustomer;
    public String brandCode = "";
    public String customerId = "";
    public String consigneeId = "";
    public String vendor_id = "";
    public String City = "";
    public String State = "";
    public ArrayList<CustomerSearch> customerSearchList;
    public ArrayList<CustomerSearch> consineeList;
    public ArrayList<LookupCategory> Categorylist;
    public ArrayList<LookupCategory> packingList;
    public ArrayList<LookupCategory> brandList;
    public ArrayList<LookupCategory> dispatchfromList;
    public ArrayList<LookupCategory> productList;
    public ArrayList<LookupCategory> user_typeList;
    public ArrayList<LookupCategory> userstatusList;
    public ArrayList<LookupCategory> validity_type;
    public ArrayList<BeanMiddleman> MiddleManList;
    Button btnSave;
    TextView dialogTitle;
    ListView listView;
    ListView cusSearchList;
    String category = "";
    String validity;
    String orderId = "";
    ArrayList<CustomerSearch> tempCustomerSearchList;
    Boolean flag;
    String edEdit = "aa";

    Vibrator v;
    int intQty;
    int intRate;
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            String year = String.valueOf(selectedYear);
            String month = String.valueOf(selectedMonth + 1);
            String day = String.valueOf(selectedDay);
            String time = "";
            try {
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("hh:mm aa");
                time = df.format(cal.getTime());

                // tvDate.setText(time);

            } catch (Exception e) {
                e.printStackTrace();
            }

            validity = String.valueOf(new StringBuilder().append(day).append("-")
                    .append(month).append("-").append(year));
            DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
            Date date1 = null;
            Calendar cal = Calendar.getInstance();
            try {
                date1 = readFormat1.parse(validity);
                String dateformatt = formatter1.format(date1);

                tvValid.setText(dateformatt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };

    public static Fragment_Add_Order newInstance(String text) {

        Fragment_Add_Order f = new Fragment_Add_Order();
        Bundle b = new Bundle();
        b.putString("edit", text);

        f.setArguments(b);

        return f;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            edEdit = bundle.getString("edit");
            Log.d("--------", "edit" + edEdit);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_order, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        btnSave = (Button) view.findViewById(R.id.btnSave);
        v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        // tvTitle.setText(getActivity().getString(R.string.add_order_requirement_entry));
        // spnCategory = (Spinner)view.findViewById(R.id.spnTrade);


        edQty = view.findViewById(R.id.edQty);
        edRate = view.findViewById(R.id.edRate);
        edRemark = view.findViewById(R.id.edRemark);
        edAddress = view.findViewById(R.id.ed_Address);

        tvCategory = view.findViewById(R.id.tvCategory);
        tvMiddleName = view.findViewById(R.id.tvMiddleName);
        tvValid = view.findViewById(R.id.edValid);
        tvDispatch = view.findViewById(R.id.edDispatch);
        tvPackingType = view.findViewById(R.id.spnPackingType);
        tvProduct = view.findViewById(R.id.spnProduct);
        tvConsignee = view.findViewById(R.id.tvConsignee);

        tvBrand = view.findViewById(R.id.tvBrand);
        tvCustomer = view.findViewById(R.id.spnCustomer);
        tvCustomer.setOnClickListener(this);
        tvProduct.setOnClickListener(this);
        tvPackingType.setOnClickListener(this);
        tvBrand.setOnClickListener(this);
        tvDispatch.setOnClickListener(this);
        tvCategory.setOnClickListener(this);
        tvConsignee.setOnClickListener(this);
        tvMiddleName.setOnClickListener(this);
        tvValid.setOnClickListener(this);
        readBundle(getArguments());

        customerSearchList = new ArrayList<CustomerSearch>();
        tempCustomerSearchList = new ArrayList<CustomerSearch>();

        consineeList = new ArrayList<CustomerSearch>();
        Categorylist = new ArrayList<LookupCategory>();
        packingList = new ArrayList<LookupCategory>();
        brandList = new ArrayList<LookupCategory>();
        dispatchfromList = new ArrayList<LookupCategory>();
        productList = new ArrayList<LookupCategory>();
        user_typeList = new ArrayList<LookupCategory>();
        userstatusList = new ArrayList<LookupCategory>();
        validity_type = new ArrayList<LookupCategory>();
        MiddleManList = new ArrayList<BeanMiddleman>();


        new PostClassLookUpData(getActivity()).execute();

        /*spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                category = Categorylist.get(i).getCODE();


                    spnCategory.setVisibility(View.VISIBLE);
                    tvBrand.setVisibility(View.VISIBLE);
                    tvCustomer.setVisibility(View.VISIBLE);
                    spnConsignee.setVisibility(View.VISIBLE);
                    edAddress.setVisibility(View.VISIBLE);
                    tvProduct.setVisibility(View.VISIBLE);
                    tvPackingType.setVisibility(View.VISIBLE);
                    edQty.setVisibility(View.VISIBLE);
                    spnMiddleName.setVisibility(View.GONE);
                    tvValid.setVisibility(View.GONE);
                    edRate.setVisibility(View.GONE);
                    tvDispatch.setVisibility(View.GONE);
                    edRemark.setVisibility(View.VISIBLE);
                }
                else
                    spnCategory.setVisibility(View.VISIBLE);
                    tvBrand.setVisibility(View.VISIBLE);
                    tvCustomer.setVisibility(View.VISIBLE);
                    spnConsignee.setVisibility(View.VISIBLE);
                    edAddress.setVisibility(View.VISIBLE);
                    tvProduct.setVisibility(View.VISIBLE);
                    tvPackingType.setVisibility(View.VISIBLE);
                    edQty.setVisibility(View.VISIBLE);
                    spnMiddleName.setVisibility(View.VISIBLE);
                    tvValid.setVisibility(View.VISIBLE);
                    edRate.setVisibility(View.VISIBLE);
                    tvDispatch.setVisibility(View.VISIBLE);
                    edRemark.setVisibility(View.VISIBLE);



            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        */


        if (edEdit.equals("edit")) {
            orderId = getArguments().getString("orderId");
            category = getArguments().getString("Category");

            brandCode = getArguments().getString("brandid");
            String brand = getArguments().getString("brand");
            String customer = getArguments().getString("customer");
            customerId = getArguments().getString("customerCode");
            consigneeId = getArguments().getString("consingee");
            String address = getArguments().getString("address");
            String product = getArguments().getString("product");
            String packing = getArguments().getString("packing");
            String qty = getArguments().getString("qty");
            String rs = getArguments().getString("rs");
            String valid = getArguments().getString("valid");
            String remark = getArguments().getString("remark");
            String consignee_name = getArguments().getString("consignee_name");
            vendor_id = getArguments().getString("vendor_id");
            String dispatch_from = getArguments().getString("dispatch_from");
            String mm_name = getArguments().getString("mm_name");


            DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
            Date date = null;
            Calendar cal = Calendar.getInstance();

            try {
                date = readFormat.parse(valid);
                String dateformat = formatter.format(date);
                tvValid.setText(dateformat);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            tvCategory.setText(category);
            tvConsignee.setText(consignee_name);
            tvMiddleName.setText(mm_name);
            tvDispatch.setText(dispatch_from);
            tvBrand.setText(brand);
            new PostClassMidalman(getActivity(), brandCode).execute();
            tvCustomer.setText(customer);

            new PostClassConsingeeSearch(getActivity(), "", "c", customerId).execute();
            edAddress.setText(address);
            tvProduct.setText(product);
            tvPackingType.setText(packing);
            edQty.setText(qty);
            edRate.setText(rs);


            edRemark.setText(remark);
            // getActivity().setTitle(getActivity().getString(R.string.add_material_recepit_entry));
            flag = false;
        } else
            flag = true;
        // getActivity().setTitle(getActivity().getString(R.string.add_material_recepit_entry));

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    intQty = Integer.valueOf(edQty.getText().toString());
                    intRate = Integer.valueOf(edRate.getText().toString());
                } catch (Exception e) {

                }

                if (tvCategory.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Category field is required!", Toast.LENGTH_LONG).show();

                } else if (tvBrand.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Brand field is required!", Toast.LENGTH_LONG).show();

                } else if (tvCustomer.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Customer field is required!", Toast.LENGTH_LONG).show();

                } else if (tvConsignee.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Consignee field is required!", Toast.LENGTH_LONG).show();

                } else if (edAddress.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Address field is required!", Toast.LENGTH_LONG).show();

                } else if (tvProduct.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Product field is required!", Toast.LENGTH_LONG).show();

                } else if (tvPackingType.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Product field is required!", Toast.LENGTH_LONG).show();

                }
                //
                else if (edQty.getText().toString().length() == 0 || intQty <= 0) {
                    Toast.makeText(getActivity(), "Quantity field is required!", Toast.LENGTH_LONG).show();

                    // Vibrate for 400 milliseconds
                    v.vibrate(400);


                } else if (tvMiddleName.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Middle Name field is required!", Toast.LENGTH_LONG).show();

                } else if (edRate.getText().toString().length() == 0 || intRate <= 0) {
                    Toast.makeText(getActivity(), "Rate field is required!", Toast.LENGTH_LONG).show();
                    v.vibrate(400);

                } else if (tvValid.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Validity field is required!", Toast.LENGTH_LONG).show();

                } else if (tvDispatch.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Dispatch field is required!", Toast.LENGTH_LONG).show();

                } else if (edRemark.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Remark field is required!", Toast.LENGTH_LONG).show();

                } else {

                    //
                    String strCategoryValue = tvCategory.getText().toString();
                    if (strCategoryValue.equalsIgnoreCase("non trade")) {
                        category = "NT";
                    }
                    if (strCategoryValue.equalsIgnoreCase("trade")) {
                        category = "TR";
                    }
                    String product = tvProduct.getText().toString().trim();
                    String packing = tvPackingType.getText().toString().trim();
                    String quantity = edQty.getText().toString().trim();
                    String mm_name = tvMiddleName.getText().toString();
                    String rate = edRate.getText().toString().trim();
                    validity = tvValid.getText().toString().trim();
                    String dispatch_from = tvDispatch.getText().toString().trim();
                    String remark = edRemark.getText().toString().trim();
                    String address = edAddress.getText().toString().trim();
                    String district = "";

                    new PostClassAddOrderRec(getActivity(), orderId, category, brandCode, "C", customerId, consigneeId, product, packing, quantity, mm_name, vendor_id, rate, validity,
                            dispatch_from, remark, address, City, State, district).execute();

                }
            }
        });


        return view;
    }

    @Override
    public void onClick(View view) {

        if (view == tvBrand) {

            showSpinnerDialog(brandList, "Brand");
            // showSpinnerDialog(packegeTypeeList,"Select package type",tvPackingType);
        } else if (view == tvProduct) {

            showSpinnerDialog(productList, "Product");
        } else if (view == tvPackingType) {

            showSpinnerDialog(packingList, "Packing Type");

        } else if (view == tvDispatch) {
            showSpinnerDialog(dispatchfromList, "Dispatch From");
        } else if (view == tvCategory) {
            showSpinnerDialog(Categorylist, "Category");
        } else if (view == tvConsignee) {
            showConsigneeDialog(consineeList, "Consignee");
        } else if (view == tvMiddleName) {
            showMiddleManDialog(MiddleManList, "Middle Man");
        } else if (view == tvCustomer) {
            showCustomerSearchDialog("Customer");
        } else if (view == tvValid) {
            showDialog();
        }

    }

    private void showSpinnerDialog(ArrayList<LookupCategory> list, String title) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        listView = (ListView) dialogView.findViewById(R.id.listCatecory);
        dialogTitle = (TextView) dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerAdapter = new SpinnerDialogAdapter(getActivity(), list, title);
        listView.setAdapter(spinnerAdapter);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }

    private void showConsigneeDialog(ArrayList<CustomerSearch> list, String title) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        final EditText edCustomerSearch = (EditText) dialogView.findViewById(R.id.customSearch);
        cusSearchList = (ListView) dialogView.findViewById(R.id.searchlist);
        listView = (ListView) dialogView.findViewById(R.id.listCatecory);
        dialogTitle = (TextView) dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        consignee_adapter = new ConsinggeAdapter(getActivity(), list);
        listView.setAdapter(consignee_adapter);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }

    private void showMiddleManDialog(ArrayList<BeanMiddleman> list, String title) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        //  final EditText edCustomerSearch = (EditText) dialogView.findViewById(R.id.customSearch);
        cusSearchList = dialogView.findViewById(R.id.searchlist);
        listView = dialogView.findViewById(R.id.listCatecory);
        dialogTitle = dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        middleManAdapter = new MiddleManAdapter(getActivity(), list);
        listView.setAdapter(middleManAdapter);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }

    private void showCustomerSearchDialog(String title) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_customer_search_listview, null);
        dialogBuilder.setView(dialogView);
        final EditText edCustomerSearch = dialogView.findViewById(R.id.customSearch);
        Button btnCancel = dialogView.findViewById(R.id.btnCancle);
        Button btnSubmit = dialogView.findViewById(R.id.btnSubmit);
        cusSearchList = dialogView.findViewById(R.id.searchlist);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String keyword = edCustomerSearch.getText().toString().toUpperCase();
                if (!keyword.equals("null")) {
                    new PostClassCustomerSearch(getActivity(), keyword, "C").execute();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder3.dismiss();
            }
        });

        listView = dialogView.findViewById(R.id.listCatecory);
        dialogTitle = dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }

    private void showDialog() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        cal.add(Calendar.DAY_OF_MONTH, 1);
// Create the DatePickerDialog instance
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), datePickerListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.setCancelable(false);
        datePicker.setTitle("Select the date");

        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePicker.show();
    }

    class PostClassCustomerSearch extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";
        String Keyword = "";
        String Usertype = "";


        public PostClassCustomerSearch(Context c, String keyword, String usertype) {
            this.context = c;
            this.Keyword = keyword;
            this.Usertype = usertype;
            customerSearchList.clear();
            tempCustomerSearchList.clear();
            Log.d("====keyword=====", "" + Keyword);
            Log.d("====usertype=====", "" + Usertype);

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("keyword", Keyword);
                    jsonObject.put("user_type", Usertype);
                    jsonObject.put("page_module", MODULE_ORDERS);

                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());
                    Request request = new Request.Builder()
                            .post(body)
                            .url(CustomerSearchUrl)
                            .addHeader("content-type", "application/json")
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===CustomerSearchUrl===", "=======" + CustomerSearchUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONArray jsonData = jObject.getJSONArray("data");

                            for (int i = 0; i < jsonData.length(); i++) {
                                JSONObject jCustomer = jsonData.getJSONObject(i);
                                customerSearchList.add(new CustomerSearch(jCustomer.getString("id"),
                                        jCustomer.getString("name"), jCustomer.getString("city"),
                                        jCustomer.getString("state"), jCustomer.getString("address")));

                            }
                            tempCustomerSearchList.addAll(customerSearchList);

                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                if (customerSearchList != null) {
                    customerSearchAdapter = new CustomerSearchAdapter(getActivity(), customerSearchList);
                    cusSearchList.setAdapter(customerSearchAdapter);

                }

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }
    }

    public class PostClassLookUpData extends AsyncTask<String, Void, String> {

        private final Context context;
        public ArrayList<LookupCategory> tempCategorylist;
        public ArrayList<LookupCategory> tempPackingList;
        public ArrayList<LookupCategory> tempBrandList;
        public ArrayList<LookupCategory> tempDispatchfromList;
        public ArrayList<LookupCategory> tempProductList;
        public ArrayList<LookupCategory> tempUsertypeList;
        public ArrayList<LookupCategory> tempUserstatusList;
        public ArrayList<LookupCategory> tempvalidity_type;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;


        public PostClassLookUpData(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();


            tempCategorylist = new ArrayList<LookupCategory>();
            tempPackingList = new ArrayList<LookupCategory>();
            tempBrandList = new ArrayList<LookupCategory>();
            tempDispatchfromList = new ArrayList<LookupCategory>();
            tempProductList = new ArrayList<LookupCategory>();
            tempUsertypeList = new ArrayList<LookupCategory>();
            tempUserstatusList = new ArrayList<LookupCategory>();
            tempvalidity_type = new ArrayList<LookupCategory>();


        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");

                    /*JSONObject jsonObject_page_module = new JSONObject();

                    try {
                        jsonObject_page_module.put("page_module", MODULE_ORDERS);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody body = RequestBody.create(mediaType, jsonObject_page_module.toString());

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();*/

                    RequestBody body = RequestBody.create(null, new byte[]{});

                    // RequestBody body = RequestBody.create(mediaType, jsonObject_page_module.toString());

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .addHeader("page_module", MODULE_COMMON)
                            // .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();


                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONObject category = jObject.getJSONObject("data");
                            JSONArray jsonCategory = category.getJSONArray("category");
                            System.out.println("======Lookup_data=========" + Lookup_data);
                            /*tempCategorylist.add(new LookupCategory("",
                                  "","Select Category"));*/

                            for (int i = 0; i < jsonCategory.length(); i++) {
                                JSONObject jsonObject = jsonCategory.getJSONObject(i);
                                tempCategorylist.add(new LookupCategory(jsonObject.getString("lookup_type"),
                                        jsonObject.getString("code"), jsonObject.getString("description")));

                            }
                            Categorylist.addAll(tempCategorylist);


                            JSONArray jsonArraypacking = category.getJSONArray("packing");
                            for (int i = 0; i < jsonArraypacking.length(); i++) {
                                JSONObject jsonPacking = jsonArraypacking.getJSONObject(i);
                                tempPackingList.add(new LookupCategory(jsonPacking.getString("lookup_type"),
                                        jsonPacking.getString("code"), jsonPacking.getString("description")));

                            }
                            packingList.addAll(tempPackingList);

                            JSONArray jsonArrayBrand = category.getJSONArray("brand");
                            for (int i = 0; i < jsonArrayBrand.length(); i++) {
                                JSONObject jsonBrand = jsonArrayBrand.getJSONObject(i);
                                tempBrandList.add(new LookupCategory(jsonBrand.getString("lookup_type"),
                                        jsonBrand.getString("code"), jsonBrand.getString("description")));

                            }
                            brandList.addAll(tempBrandList);
                            JSONArray jsonArraydispatchfrom = category.getJSONArray("dispatchfrom");
                            for (int i = 0; i < jsonArraydispatchfrom.length(); i++) {
                                JSONObject jsondispatchfrom = jsonArraydispatchfrom.getJSONObject(i);
                                tempDispatchfromList.add(new LookupCategory(jsondispatchfrom.getString("lookup_type"),
                                        jsondispatchfrom.getString("code"), jsondispatchfrom.getString("description")));

                            }
                            dispatchfromList.addAll(tempDispatchfromList);
                            JSONArray jsonArrayproduct = category.getJSONArray("product");
                            for (int i = 0; i < jsonArrayproduct.length(); i++) {
                                JSONObject jsonproduct = jsonArrayproduct.getJSONObject(i);
                                tempProductList.add(new LookupCategory(jsonproduct.getString("lookup_type"),
                                        jsonproduct.getString("code"), jsonproduct.getString("description")));

                            }
                            productList.addAll(tempProductList);

                            JSONArray jsonArrayUsertype = category.getJSONArray("user_type");
                            for (int i = 0; i < jsonArrayUsertype.length(); i++) {
                                JSONObject jsonUsertype = jsonArrayUsertype.getJSONObject(i);
                                tempUsertypeList.add(new LookupCategory(jsonUsertype.getString("lookup_type"),
                                        jsonUsertype.getString("code"), jsonUsertype.getString("description")));

                            }
                            user_typeList.addAll(user_typeList);
                            JSONArray jsonArrayUserstatus = category.getJSONArray("user_status");
                            for (int i = 0; i < jsonArrayUserstatus.length(); i++) {
                                JSONObject jsonUserstatus = jsonArrayUserstatus.getJSONObject(i);
                                tempUserstatusList.add(new LookupCategory(jsonUserstatus.getString("lookup_type"),
                                        jsonUserstatus.getString("code"), jsonUserstatus.getString("description")));

                            }
                            userstatusList.addAll(userstatusList);

                            JSONArray jsonArrayValiditytype = category.getJSONArray("validity_type");
                            for (int i = 0; i < jsonArrayValiditytype.length(); i++) {
                                JSONObject Validitytype = jsonArrayValiditytype.getJSONObject(i);
                                tempvalidity_type.add(new LookupCategory(Validitytype.getString("lookup_type"),
                                        Validitytype.getString("code"), Validitytype.getString("description")));

                            }
                            validity_type.addAll(tempvalidity_type);


                            Log.d("===dispatchList.size===", "=====" + tempDispatchfromList.size());
                            Log.d("===product.size===", "=====" + tempProductList.size());

                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
              /*  spinTradeadapter = new SpinCalegory_Adapter(getActivity(), tempCategorylist);
                spnCategory.setAdapter(spinTradeadapter);*/

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }
    }

    public class SpinnerDialogAdapter extends BaseAdapter {
        Context context;
        List<LookupCategory> rowItems;
        String Title = "";


        public SpinnerDialogAdapter(Context context, List<LookupCategory> items, String title) {
            this.context = context;
            this.rowItems = items;
            this.Title = title;
            Log.d("===rowItems.zize===", "" + rowItems.size());


        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            SpinnerDialogAdapter.ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new SpinnerDialogAdapter.ViewHolder();
                holder.textName = convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (SpinnerDialogAdapter.ViewHolder) convertView.getTag();

            }

            holder.textName.setText(rowItems.get(position).getDESCRIPTION());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String meaning = rowItems.get(position).getDESCRIPTION();
                    switch (Title) {
                        case "Packing Type":
                            builder3.dismiss();
                            tvPackingType.setText(meaning);

                            break;
                        case "Brand":
                            builder3.dismiss();
                            tvBrand.setText(meaning);
                            brandCode = rowItems.get(position).getCODE();
                            new PostClassMidalman(context, rowItems.get(position).getCODE()).execute();

                            break;
                        case "Product":
                            builder3.dismiss();
                            tvProduct.setText(meaning);
                            break;
                        case "Dispatch From":
                            builder3.dismiss();
                            tvDispatch.setText(meaning);
                            break;
                        case "Category":
                            builder3.dismiss();
                            tvCategory.setText(meaning);
                            break;
                    }


                }
            });
            return convertView;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;

        }


    }

    public class PostClassMidalman extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";
        String Id = "";
        String Usertype = "";


        public PostClassMidalman(Context c, String id) {
            this.context = c;
            this.Id = id;
            MiddleManList.clear();


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("org_id", Id);
                    // jsonObject.put("page_module", MODULE_ORDERS);

                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());
                    Request request = new Request.Builder()
                            .post(body)
                            .url(Middle_man)

                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_ORDERS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===Middle_man===", "=======" + Middle_man);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            JSONArray jsonData = jObject.getJSONArray("data");

                            for (int i = 0; i < jsonData.length(); i++) {
                                JSONObject jCustomer = jsonData.getJSONObject(i);
                                MiddleManList.add(new BeanMiddleman(jCustomer.getString("vendor_id"),
                                        jCustomer.getString("vendor_name"), jCustomer.getString("inactive_date"),
                                        jCustomer.getString("vendor_site_code")));

                            }
                            Log.d("===DiList==", "=======" + MiddleManList.size());
                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyCode");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                // Toast.makeText(context, "Okk", Toast.LENGTH_SHORT).show();


            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }
    }

    public class CustomerSearchAdapter extends BaseAdapter {
        Context context;
        List<CustomerSearch> rowItems;
        String Title = "";


        public CustomerSearchAdapter(Context context, List<CustomerSearch> items) {
            this.context = context;
            this.rowItems = items;
            Log.d("===rowItems.zize===", "" + rowItems.size());
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            CustomerSearchAdapter.ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new CustomerSearchAdapter.ViewHolder();
                holder.textName = (TextView) convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (CustomerSearchAdapter.ViewHolder) convertView.getTag();

            }
            Log.d("=====search list=====", "" + rowItems.get(position).getAddress());

            holder.textName.setText(rowItems.get(position).getName() + rowItems.get(position).getAddress());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customerId = rowItems.get(position).getId();
                    City = rowItems.get(position).getCity();
                    State = rowItems.get(position).getState();
                    final String meaning = rowItems.get(position).getName();
                    final String address = rowItems.get(position).getAddress();
                    final String code = rowItems.get(position).getId();
                    tvCustomer.setText(meaning);
                    tvConsignee.setText("");
                    edAddress.setText(address);
                    builder3.dismiss();

                    new PostClassConsingeeSearch(context, "", "c", code).execute();

                }
            });
            return convertView;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;
        }

    }

    public class PostClassConsingeeSearch extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";
        String Keyword = "";
        String Usertype = "";
        String CustomerCode = "";


        public PostClassConsingeeSearch(Context c, String keyword, String usertype, String customerCode) {
            this.context = c;
            this.Keyword = keyword;
            this.Usertype = usertype;
            this.CustomerCode = customerCode;
            consineeList.clear();

            Log.d("====keyword=====", "" + keyword);
            Log.d("====usertype=====", "" + usertype);

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("keyword", Keyword);
                    jsonObject.put("user_type", "C");
                    jsonObject.put("customer_code", CustomerCode);
                    jsonObject.put("page_module", MODULE_ORDERS);

                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());
                    Request request = new Request.Builder()
                            .post(body)
                            .url(ConsineeSearchearchUrl)
                            .addHeader("content-type", "application/json")
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("==ConsineeUrl==", "=======" + ConsineeSearchearchUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONArray jsonData = jObject.getJSONArray("data");

                            for (int i = 0; i < jsonData.length(); i++) {
                                JSONObject jCustomer = jsonData.getJSONObject(i);
                                consineeList.add(new CustomerSearch(jCustomer.getString("id"),
                                        jCustomer.getString("name"), jCustomer.getString("city"),
                                        jCustomer.getString("state"), jCustomer.getString("address")));

                            }


                            Log.d("===customerSearchList==", "=======" + consineeList.size());
                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context, jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();
                //showConsigneeDialog(consineeList, "Select Category");
            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }
    }

    public class ConsinggeAdapter extends BaseAdapter {
        Context context;
        List<CustomerSearch> rowItems;
        String Title = "";


        public ConsinggeAdapter(Context context, List<CustomerSearch> items) {
            this.context = context;
            this.rowItems = items;
            Log.d("===rowItems.zize===", "" + rowItems.size());
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ConsinggeAdapter.ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new ConsinggeAdapter.ViewHolder();
                holder.textName = (TextView) convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (ConsinggeAdapter.ViewHolder) convertView.getTag();

            }
            Log.d("=====search list=====", "" + rowItems.get(position).getAddress());

            holder.textName.setText(rowItems.get(position).getAddress());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvConsignee.setText(rowItems.get(position).getName());
                    consigneeId = rowItems.get(position).getId();
                    builder3.dismiss();


                }
            });
            return convertView;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;
        }

    }

    public class MiddleManAdapter extends BaseAdapter {
        Context context;
        List<BeanMiddleman> rowItems;
        String Title = "";


        public MiddleManAdapter(Context context, List<BeanMiddleman> items) {
            this.context = context;
            this.rowItems = items;
            Log.d("===rowItems.zize===", "" + rowItems.size());
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            MiddleManAdapter.ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new MiddleManAdapter.ViewHolder();
                holder.textName = (TextView) convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (MiddleManAdapter.ViewHolder) convertView.getTag();

            }
            Log.d("=====search list=====", "" + rowItems.get(position).getVendor_name());

            holder.textName.setText(rowItems.get(position).getVendor_name());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvMiddleName.setText(rowItems.get(position).getVendor_name());
                    vendor_id = rowItems.get(position).getVendor_id();
                    builder3.dismiss();


                }
            });
            return convertView;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;
        }

    }


}
