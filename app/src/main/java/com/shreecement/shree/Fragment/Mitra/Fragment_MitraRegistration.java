package com.shreecement.shree.Fragment.Mitra;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.github.chuross.library.ExpandableLayout;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Documents.Fragment_DocUpload;
import com.shreecement.shree.Fragment.Mitra.PostClass.PostClassCreateMitra;
import com.shreecement.shree.ModalClass.BeanOrderList;
import com.shreecement.shree.ModalClass.ListContainsBean;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;
import com.shreecement.shree.Utlility.StorePrefs;
import com.shreecement.shree.Utlility.Utils;
import com.shreecement.shree.networkUtils.BackGroundTask;
import com.shreecement.shree.networkUtils.OnTaskCompleted;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.Citylist;
import static com.shreecement.shree.Utlility.Constant.Districtlist;
import static com.shreecement.shree.Utlility.Constant.Lookup_data;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.MitraDetail;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.Statelist;
import static com.shreecement.shree.Utlility.Constant.Talukalist;
import static com.shreecement.shree.Utlility.Constant.ValidateAadhaarPhone;
import static com.shreecement.shree.Utlility.Constant.VerifyMitraMobile;
import static com.shreecement.shree.Utlility.Constant.VerifyMitraMobileOtp;
import static com.shreecement.shree.Utlility.Constant.orders_listUrl;
import static com.shreecement.shree.Utlility.Constant.showToastMessage;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_MitraRegistration extends Fragment implements View.OnClickListener {
    TextView tvCategory, tvBrand, tvCustomer, tvConsingee, tvAddress, tvProduct, tvPacking, tvRs, tvValid, tvDispatch, tvMiddleName,
            tvQty, tvRemark;
    BeanOrderList beanOrderList;

    TextView tvTitle, tvAadhaarTitle, spnGender, tvDateOfBirthMitra, tvMitraType, tvState, tvDistrict,
            tvTaluka, tvCity, tvLocality, tvMaritalStatus, tvAnniversary,
            tvChildOneDob, tvChildTwoDob, tvAverageAnnualIncome, tvBloodType, tvAadhaarNumber, tvMobileNumber, tvTitleMitraId,tvMitraID;

    EditText  edFisrtName, edMiddleName, edLastName, edEmail, edAddressLineOne,
            edAddressLineTwo,edAddressLineThree,edPostal,
             edPan, edSpouseName, edChildOneName,edDrivingLicence,edVoterId,edGstNumber,
            edChildTwoName;

    String  first_name, middle_names,last_name,email_address,mobile_number,sex_code,blood_type_code,date_of_birth,mitra_number,
            adhaar_number,voter_id,pan,driving_licence_number,gst_number,marital_status_code,
            spouse_name,date_of_wedding,child1_name,date_of_birth_child1,child2_name,date_of_birth_child2,
            address_line1,address_line2,address_line3,state,district,taluka,city,postal_code;

    String record = "";

    Boolean isAadhaarMandatory = false;

    String dateButtonType = "";
    String title = "";
    String mitraCode = "";
    String titleCode = "";
    String bloodCode = "";
    String stateCode = "";
    String genderCode = "";
    String districtCode = "";
    String talukaCode = "";
    String maritalCode = "";
    String cityCode = "";
    String date_dobOfMitra= "";
    String date_Anniversary= "";
    String date_dobChildOne= "";
    String date_dobChildTwo = "";
    String mobile_number_verified_flag = "";
    String otpCode ="";
    String decodedCode ="";
    String mitraIdSendForOtp = "";
    String enc_code = "";
    String mitraApproveStatus = "";
    ToggleButton simpleToggleButton1;



    Button btnSave, btnVerifyPhoneNumber;
    TextView dialogTitle;
    ListView listView;
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog builder3;

    public ArrayList<LookupCategory> Titlelist;
    public ArrayList<LookupCategory> GenderList;
    public ArrayList<LookupCategory> MaritalList;
    public ArrayList<LookupCategory> MitraTypeList;
    public ArrayList<LookupCategory> BloodList;
    public ArrayList<LookupCategory> StateList;
    public ArrayList<LookupCategory> DistrictList;
    public ArrayList<LookupCategory> TalukaList;
    public ArrayList<LookupCategory> CityList;

    public SpinnerDialogAdapter spinnerAdapter;

    public static String PageValueReg = "";
    public String mitraId = "";
    public JSONObject jsonObject;
    ArrayList<LookupCategory> mitraList;
    Dialog dialog,dialogForOtpVerify;
    TextView txtHeading;
    Button btnCancelDialogTop , btnDialogOk;
    EditText edInputField;
    ExpandableLayout layout_FamilyInformation, layout_Documments, layout_Address;
    RelativeLayout relFamilyInformation, relDocumentsInformation, relAddresssInformation;

    public String adhaar_imageUrl = "";
    String adhaar_imageUrl_back = "";
    String voter_imageUrl_back = "";
    public String voter_imageUrl = "";
    public String driving_licence_imageUrl = "";
    public String pan_imageUrl = "";
    public String mitra_imageUrl = "";


    public static Fragment_MitraRegistration newInstance(String text) {

        Fragment_MitraRegistration f = new Fragment_MitraRegistration();
        Bundle b = new Bundle();
        b.putString("PageValueReg", text);

        f.setArguments(b);

        return f;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            PageValueReg = bundle.getString("PageValueReg");
            Log.d("--------", "PageValueReg : - " + PageValueReg);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mitra_registration_part1, container, false);
        view.setBackgroundColor(Color.WHITE);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getActivity().setTitle(getActivity().getString(R.string.nav_item_mitras));
        readBundle(getArguments());
        mitraList = new ArrayList<>();
        dialog = new Dialog(getActivity());
        dialogForOtpVerify = new Dialog(getActivity());
        simpleToggleButton1 =   view.findViewById(R.id.simpleToggleButton1);

        layout_FamilyInformation = view.findViewById(R.id.layout_FamilyInformation);
        relFamilyInformation = view.findViewById(R.id.relFamilyInformation);
        relFamilyInformation.setOnClickListener(this);

        layout_Documments = view.findViewById(R.id.layout_Documments);
        relDocumentsInformation = view.findViewById(R.id.relDocumentsInformation);
        relDocumentsInformation.setOnClickListener(this);

        layout_Address = view.findViewById(R.id.layout_Address);
        relAddresssInformation = view.findViewById(R.id.relAddresssInformation);
        relAddresssInformation.setOnClickListener(this);

        MitraTypeList = new ArrayList<LookupCategory>();
        GenderList = new ArrayList<LookupCategory>();
        MaritalList = new ArrayList<LookupCategory>();
        Titlelist = new ArrayList<LookupCategory>();
        BloodList = new ArrayList<LookupCategory>();
        StateList = new ArrayList<LookupCategory>();
        DistrictList = new ArrayList<LookupCategory>();
        TalukaList = new ArrayList<LookupCategory>();
        CityList = new ArrayList<LookupCategory>();
        if (PageValueReg.equalsIgnoreCase("edit")){

            new PostClassMitraDetail(getActivity(),mitraId).execute();

            mitraIdSendForOtp = mitraId;

            tvTitleMitraId =  view.findViewById(R.id.tvTitleMitraId);
            tvMitraID =  view.findViewById(R.id.tvMitraID);
            tvTitle =  view.findViewById(R.id.tvTitle);
            tvAadhaarTitle =  view.findViewById(R.id.tvAadhaarTitle);
            spnGender =  view.findViewById(R.id.spnGender);
            tvDateOfBirthMitra =  view.findViewById(R.id.tvDateOfBirthMitra);
            tvMitraType =  view.findViewById(R.id.tvMitraType);
            tvBloodType =  view.findViewById(R.id.tvBloodType);
            tvState =  view.findViewById(R.id.tvState);
            tvDistrict =  view.findViewById(R.id.tvDistrict);
            tvTaluka =  view.findViewById(R.id.tvTaluka);
            tvCity =  view.findViewById(R.id.tvCity);
            tvMaritalStatus =  view.findViewById(R.id.tvMaritalStatus);
            tvAnniversary =  view.findViewById(R.id.tvAnniversary);
            tvChildOneDob =  view.findViewById(R.id.tvChildOneDob);
            tvChildTwoDob =  view.findViewById(R.id.tvChildTwoDob);
            tvAadhaarNumber =  view.findViewById(R.id.tvAadhaarNumber);
            tvMobileNumber =  view.findViewById(R.id.tvMobileNumber);

            edFisrtName =   view.findViewById(R.id.edFisrtName);
            edFisrtName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edMiddleName = view.findViewById(R.id.edMiddleName);
            edMiddleName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edLastName = view.findViewById(R.id.edLastName);
            edLastName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

            edEmail =   view.findViewById(R.id.edEmail);
            edEmail.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edAddressLineOne =  view.findViewById(R.id.edAddressLineOne);
            edAddressLineOne.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edAddressLineTwo =  view.findViewById(R.id.edAddressLineTwo);
            edAddressLineTwo.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edAddressLineThree =  view.findViewById(R.id.edAddressLineThree);
            edAddressLineThree.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edPostal =  view.findViewById(R.id.edPostal);

            edPan =  view.findViewById(R.id.edPan);
            edPan.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edSpouseName =   view.findViewById(R.id.edSpouseName);
            edSpouseName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edChildOneName =  view.findViewById(R.id.edChildOneName);
            edChildOneName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edDrivingLicence = view.findViewById(R.id.edDrivingLicence);
            edDrivingLicence.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edVoterId =  view.findViewById(R.id.edVoterId);
            edVoterId.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edGstNumber =   view.findViewById(R.id.edGstNumber);
            edGstNumber.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edChildTwoName =   view.findViewById(R.id.edChildTwoName);
            edChildTwoName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});


            simpleToggleButton1 =   view.findViewById(R.id.simpleToggleButton1);

            btnSave = view.findViewById(R.id.btnSave);
            btnSave.setOnClickListener(this);
            btnVerifyPhoneNumber = view.findViewById(R.id.btnVerifyPhoneNumber);
            btnVerifyPhoneNumber.setOnClickListener(this);
            tvAadhaarNumber.setOnClickListener(this);
            tvMobileNumber.setOnClickListener(this);
            tvTitle.setOnClickListener(this);
            spnGender.setOnClickListener(this);
            tvDateOfBirthMitra.setOnClickListener(this);
            tvMitraType.setOnClickListener(this);
            tvState.setOnClickListener(this);
            tvDistrict.setOnClickListener(this);
            tvTaluka.setOnClickListener(this);
            tvCity.setOnClickListener(this);
            tvMaritalStatus.setOnClickListener(this);
            tvAnniversary.setOnClickListener(this);
            tvChildOneDob.setOnClickListener(this);
            tvChildTwoDob.setOnClickListener(this);
            tvBloodType.setOnClickListener(this);

            MitraTypeList = new ArrayList<LookupCategory>();
            GenderList = new ArrayList<LookupCategory>();
            MaritalList = new ArrayList<LookupCategory>();
            Titlelist = new ArrayList<LookupCategory>();
            BloodList = new ArrayList<LookupCategory>();
            StateList = new ArrayList<LookupCategory>();
            DistrictList = new ArrayList<LookupCategory>();
            TalukaList = new ArrayList<LookupCategory>();
            CityList = new ArrayList<LookupCategory>();

            new PostClassLookUpData(getActivity()).execute();
            new PostClassStateList(getActivity()).execute();

            tvTitleMitraId.setVisibility(View.VISIBLE);
            tvMitraID.setVisibility(View.VISIBLE);
            tvMitraID.setText(mitraId);

        }
        else if (PageValueReg.equalsIgnoreCase("add")) {

            tvTitleMitraId =  view.findViewById(R.id.tvTitleMitraId);
            tvMitraID =  view.findViewById(R.id.tvMitraID);
            tvTitle =  view.findViewById(R.id.tvTitle);
            tvAadhaarTitle =  view.findViewById(R.id.tvAadhaarTitle);
            spnGender =  view.findViewById(R.id.spnGender);
            tvDateOfBirthMitra =  view.findViewById(R.id.tvDateOfBirthMitra);
            tvMitraType =  view.findViewById(R.id.tvMitraType);
            tvBloodType =  view.findViewById(R.id.tvBloodType);
            tvState =  view.findViewById(R.id.tvState);
            tvDistrict =  view.findViewById(R.id.tvDistrict);
            tvTaluka =  view.findViewById(R.id.tvTaluka);
            tvCity =  view.findViewById(R.id.tvCity);
            tvMaritalStatus =  view.findViewById(R.id.tvMaritalStatus);
            tvAnniversary =  view.findViewById(R.id.tvAnniversary);
            tvChildOneDob =  view.findViewById(R.id.tvChildOneDob);
            tvChildTwoDob =  view.findViewById(R.id.tvChildTwoDob);
            tvAadhaarNumber =  view.findViewById(R.id.tvAadhaarNumber);
            tvMobileNumber =  view.findViewById(R.id.tvMobileNumber);

            edFisrtName = view.findViewById(R.id.edFisrtName);
            edFisrtName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edMiddleName = view.findViewById(R.id.edMiddleName);
            edMiddleName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edLastName = view.findViewById(R.id.edLastName);
            edLastName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

            edEmail = view.findViewById(R.id.edEmail);
            edEmail.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edAddressLineOne = view.findViewById(R.id.edAddressLineOne);
            edAddressLineOne.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edAddressLineTwo = view.findViewById(R.id.edAddressLineTwo);
            edAddressLineTwo.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edAddressLineThree = view.findViewById(R.id.edAddressLineThree);
            edAddressLineThree.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edPostal = view.findViewById(R.id.edPostal);

            edPan = view.findViewById(R.id.edPan);
            edPan.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edSpouseName = view.findViewById(R.id.edSpouseName);
            edSpouseName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edChildOneName = view.findViewById(R.id.edChildOneName);
            edChildOneName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edDrivingLicence = view.findViewById(R.id.edDrivingLicence);
            edDrivingLicence.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edVoterId = view.findViewById(R.id.edVoterId);
            edVoterId.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edGstNumber = view.findViewById(R.id.edGstNumber);
            edGstNumber.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edChildTwoName = view.findViewById(R.id.edChildTwoName);
            edChildTwoName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});


            simpleToggleButton1 =  view.findViewById(R.id.simpleToggleButton1);
            btnSave = view.findViewById(R.id.btnSave);
            btnSave.setOnClickListener(this);
            tvAadhaarNumber.setOnClickListener(this);
            tvMobileNumber.setOnClickListener(this);
            tvTitle.setOnClickListener(this);
            spnGender.setOnClickListener(this);
            tvDateOfBirthMitra.setOnClickListener(this);
            tvMitraType.setOnClickListener(this);
            tvState.setOnClickListener(this);
            tvDistrict.setOnClickListener(this);
            tvTaluka.setOnClickListener(this);
            tvCity.setOnClickListener(this);
            tvMaritalStatus.setOnClickListener(this);
            tvAnniversary.setOnClickListener(this);
            tvChildOneDob.setOnClickListener(this);
            tvChildTwoDob.setOnClickListener(this);
            tvBloodType.setOnClickListener(this);
            tvAadhaarNumber.setOnClickListener(this);

            MitraTypeList = new ArrayList<LookupCategory>();
            GenderList = new ArrayList<LookupCategory>();
            MaritalList = new ArrayList<LookupCategory>();
            Titlelist = new ArrayList<LookupCategory>();
            BloodList = new ArrayList<LookupCategory>();
            StateList = new ArrayList<LookupCategory>();
            DistrictList = new ArrayList<LookupCategory>();
            TalukaList = new ArrayList<LookupCategory>();
            CityList = new ArrayList<LookupCategory>();

            new PostClassLookUpData(getActivity()).execute();
            new PostClassStateList(getActivity()).execute();

            tvTitleMitraId.setVisibility(View.GONE);
            tvMitraID.setVisibility(View.GONE);
        }

        else
        {
            tvTitleMitraId =  view.findViewById(R.id.tvTitleMitraId);
            tvMitraID =  view.findViewById(R.id.tvMitraID);
            tvTitle =  view.findViewById(R.id.tvTitle);
            tvAadhaarTitle =  view.findViewById(R.id.tvAadhaarTitle);
            spnGender =  view.findViewById(R.id.spnGender);
            tvDateOfBirthMitra =  view.findViewById(R.id.tvDateOfBirthMitra);
            tvMitraType =  view.findViewById(R.id.tvMitraType);
            tvBloodType =  view.findViewById(R.id.tvBloodType);
            tvState =  view.findViewById(R.id.tvState);
            tvDistrict =  view.findViewById(R.id.tvDistrict);
            tvTaluka =  view.findViewById(R.id.tvTaluka);
            tvCity =  view.findViewById(R.id.tvCity);
            tvMaritalStatus =  view.findViewById(R.id.tvMaritalStatus);
            tvAnniversary =  view.findViewById(R.id.tvAnniversary);
            tvChildOneDob =  view.findViewById(R.id.tvChildOneDob);
            tvChildTwoDob =  view.findViewById(R.id.tvChildTwoDob);
            tvAadhaarNumber =  view.findViewById(R.id.tvAadhaarNumber);
            tvMobileNumber =  view.findViewById(R.id.tvMobileNumber);

            edFisrtName = view.findViewById(R.id.edFisrtName);
            edFisrtName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edMiddleName = view.findViewById(R.id.edMiddleName);
            edMiddleName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edLastName = view.findViewById(R.id.edLastName);
            edLastName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

            edEmail = view.findViewById(R.id.edEmail);
            edEmail.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edAddressLineOne = view.findViewById(R.id.edAddressLineOne);
            edAddressLineOne.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edAddressLineTwo = view.findViewById(R.id.edAddressLineTwo);
            edAddressLineTwo.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edAddressLineThree = view.findViewById(R.id.edAddressLineThree);
            edAddressLineThree.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edPostal = view.findViewById(R.id.edPostal);

            edPan = view.findViewById(R.id.edPan);
            edPan.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edSpouseName = view.findViewById(R.id.edSpouseName);
            edSpouseName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edChildOneName = view.findViewById(R.id.edChildOneName);
            edChildOneName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edDrivingLicence = view.findViewById(R.id.edDrivingLicence);
            edDrivingLicence.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edVoterId = view.findViewById(R.id.edVoterId);
            edVoterId.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edGstNumber = view.findViewById(R.id.edGstNumber);
            edGstNumber.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
            edChildTwoName = view.findViewById(R.id.edChildTwoName);
            edChildTwoName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

            simpleToggleButton1 =   view.findViewById(R.id.simpleToggleButton1);
            btnSave = view.findViewById(R.id.btnSave);
            btnSave.setOnClickListener(this);
            tvAadhaarNumber.setOnClickListener(this);
            tvMobileNumber.setOnClickListener(this);
            tvTitle.setOnClickListener(this);
            spnGender.setOnClickListener(this);
            tvDateOfBirthMitra.setOnClickListener(this);
            tvMitraType.setOnClickListener(this);
            tvState.setOnClickListener(this);
            tvDistrict.setOnClickListener(this);
            tvTaluka.setOnClickListener(this);
            tvCity.setOnClickListener(this);
            tvMaritalStatus.setOnClickListener(this);
            tvAnniversary.setOnClickListener(this);
            tvChildOneDob.setOnClickListener(this);
            tvChildTwoDob.setOnClickListener(this);
            tvBloodType.setOnClickListener(this);
            tvAadhaarNumber.setOnClickListener(this);

            MitraTypeList = new ArrayList<LookupCategory>();
            GenderList = new ArrayList<LookupCategory>();
            MaritalList = new ArrayList<LookupCategory>();
            Titlelist = new ArrayList<LookupCategory>();
            BloodList = new ArrayList<LookupCategory>();
            StateList = new ArrayList<LookupCategory>();
            DistrictList = new ArrayList<LookupCategory>();
            TalukaList = new ArrayList<LookupCategory>();
            CityList = new ArrayList<LookupCategory>();

            new PostClassLookUpData(getActivity()).execute();
            new PostClassStateList(getActivity()).execute();

            tvTitleMitraId.setVisibility(View.GONE);
            tvMitraID.setVisibility(View.GONE);
        }

        if(simpleToggleButton1.isChecked()){
            mitraApproveStatus = "A";
            //simpleToggleButton1.setTextOn("APPROVED");
        }
        else{
            mitraApproveStatus = "N";
           // simpleToggleButton1.setTextOn("NOT-APPROVED");
        }
        /*edPan.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 10) {
                    String s = editable.toString(); // get your editext value here
                    Pattern pattern = Pattern.compile("[a-z]{5}[0-9]{4}[a-z]{1}");
                    Matcher matcher = pattern.matcher(s);
                    // Check if pattern matches
                    if (matcher.matches()) {
                     String   panNumber = editable.toString();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.plz_enter_your_correct_pan_num), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
        });*/

        /*tvAadhaarNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable text) {
                // TODO Auto-generated method stub
                if (text.length() == 12) {
                 // new  PostClassCheckAddhaarPhoneValidate(getActivity(),tvAadhaarNumber.getText().toString(),"adhaar").execute();
                }
            }
        });*/


        simpleToggleButton1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(simpleToggleButton1.isChecked()){
                    //Button is ON
                    // Do Something

                    if(adhaar_imageUrl != null && adhaar_imageUrl.trim().length() > 0)
                    {
                        // String str is not NULL or not Empty

                        mitraApproveStatus = "A";
                        //simpleToggleButton1.setTextOn("APPROVED");
                        Log.e("value ","Not Null");
                        simpleToggleButton1.setTextOn("APPROVED");
                        simpleToggleButton1.setChecked(true);

                    }

                    else
                    {
                        // String str is NULL or Empty
                        //showToastMessage(getActivity(),"Please upload the Aadhaar Image.");
                        simpleToggleButton1.setTextOff("NOT-APPROVED");
                        Log.e("value "," Null");
                        simpleToggleButton1.setChecked(false);

                        android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(
                                getActivity());

                        alert.setTitle("Please upload the Aadhaar Image.");
                        alert.setCancelable(false);
                        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(PageValueReg.equalsIgnoreCase("edit")){

                                    try {

                                        Log.e("Mitra reg toogle","cMitra reg toogle");
                                        Log.e("adhaar_imageUrl ",adhaar_imageUrl);
                                        Log.e("voter_imageUrl_back ",voter_imageUrl_back);
                                        Log.e("voter_imageUrl ",voter_imageUrl);
                                        Log.e("voter_imageUrl_back ",voter_imageUrl_back);
                                        Log.e("driving_imageUrl",driving_licence_imageUrl);
                                        Log.e("pan_imageUrl ",pan_imageUrl);
                                        Log.e("mitra_imageUrl ",mitra_imageUrl);


                                       // ((Activity) getActivity()).onBackPressed();

                                        Fragment_DocUpload fragment = Fragment_DocUpload.newInstance("edit");
                                        fragment.NewCreatedMitraId = mitraId;
                                        try {
                                            fragment.adhaar_imageUrl = adhaar_imageUrl;
                                            fragment.adhaar_imageUrl_back = adhaar_imageUrl_back;
                                            fragment.voter_imageUrl = voter_imageUrl;
                                            fragment.voter_imageUrl_back = voter_imageUrl_back;
                                            fragment.driving_licence_imageUrl = driving_licence_imageUrl;
                                            fragment.pan_imageUrl = pan_imageUrl;
                                            fragment.mitra_imageUrl = mitra_imageUrl;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)getActivity()).getSupportFragmentManager().beginTransaction();
                                        fragmentTransaction.replace(R.id.container_body, fragment);
                                        fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();

                                        //((MainActivity) context).showUpButton(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }

                            }
                        });

                        alert.show();

                    }

                }

                else if(simpleToggleButton1.isChecked() == (false)) {
                    //Button is OFF
                    // Do Something
                    mitraApproveStatus = "N";
                    //simpleToggleButton1.setTextOff("NOT-APPROVED");
                    Log.e("value ", "Else part");
                    //simpleToggleButton1.setChecked(false);
                }
            }
        });

        return view;
    }

    @Override
    public void onClick(View view) {

        if (view == tvBrand) {

            // showSpinnerDialog(brandList, "Brand");
            // showSpinnerDialog(packegeTypeeList,"Select package type",tvPackingType);
        }
        if (view == relFamilyInformation) {
            layout_FamilyInformation.expand();// expand with animation
            layout_FamilyInformation.collapse();// collapse with animation

           /* relFamilyInformation.setTranslationX(-(50*100));
            relFamilyInformation.setAlpha(0.5f);
            relFamilyInformation.animate().alpha(1f).translationX(0).setDuration(700).start();*/
        }

        if (view == relDocumentsInformation) {
            layout_Documments.expand();// expand with animation
            layout_Documments.collapse();// collapse with animation
        }

        if (view == relAddresssInformation) {
            layout_Address.expand();// expand with animation
            layout_Address.collapse();// collapse with animation
        }

        if (view == tvAadhaarNumber) {

            dialog.getWindow();

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_with_edittext_and_button);
            dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

            TextView txtHeading;
            Button btnCancelDialogTop , btnDialogOk;
            final EditText edInputField;

            btnCancelDialogTop = dialog.findViewById(R.id.btnCancelDialogTop);
            btnDialogOk = dialog.findViewById(R.id.btnDialogOk);
            txtHeading  = dialog.findViewById(R.id.txtHeading);
            edInputField  = dialog.findViewById(R.id.edInputField);
            txtHeading.setText(R.string.aadhaar_number);


            int maxLength = 12;
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            edInputField.setFilters(FilterArray);

            dialog.show();

            btnCancelDialogTop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });
            btnDialogOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(edInputField.getText().toString().length() == 0 || edInputField.getText().toString().length() < 12) {

                        Toast.makeText(getActivity(), "Please input valid Number", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        new  PostClassCheckAddhaarPhoneValidate(getActivity(),edInputField.getText().toString().trim(),"adhaar").execute();

                    }

                }
            });
        }

        if (view == btnVerifyPhoneNumber) {

            new PostClass_SendOtp(getActivity(),mitraIdSendForOtp).execute();

            dialogForOtpVerify.getWindow();

                        dialogForOtpVerify.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialogForOtpVerify.setContentView(R.layout.dialog_with_edittext_and_button);
                        dialogForOtpVerify.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
                        dialogForOtpVerify.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                        dialogForOtpVerify.setCancelable(false);

                        TextView txtHeading;
                        Button btnCancelDialogTop , btnDialogOk;
                        final EditText edInputField;

                        btnCancelDialogTop = dialogForOtpVerify.findViewById(R.id.btnCancelDialogTop);
                        btnDialogOk = dialogForOtpVerify.findViewById(R.id.btnDialogOk);
                        txtHeading  = dialogForOtpVerify.findViewById(R.id.txtHeading);
                        edInputField  = dialogForOtpVerify.findViewById(R.id.edInputField);
                        txtHeading.setText(R.string.enter_otp);

                        int maxLength = 4;
                        InputFilter[] FilterArray = new InputFilter[1];
                        FilterArray[0] = new InputFilter.LengthFilter(maxLength);
                        edInputField.setFilters(FilterArray);

                        dialogForOtpVerify.show();

                        btnCancelDialogTop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogForOtpVerify.dismiss();
                                edInputField.setText("");
                                //tvMobileNumber.setText("");

                            }
                        });
                        btnDialogOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if(edInputField.getText().toString().length() == 0 || edInputField.getText().toString().length() < 4) {

                                    Toast.makeText(getActivity(), "Please input valid otp", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                   // new  PostClassCheckAddhaarPhoneValidate(getActivity(),edInputField.getText().toString().trim(),"mobile").execute();
                                    funVerifyOtp(mitraIdSendForOtp, edInputField.getText().toString(),enc_code);
                                }

                            }
                        });
        }

        if (view == tvMobileNumber) {

            dialog.getWindow();

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_with_edittext_and_button);
            dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

            TextView txtHeading;
            Button btnCancelDialogTop , btnDialogOk;
            final EditText edInputField;

            btnCancelDialogTop = dialog.findViewById(R.id.btnCancelDialogTop);
            btnDialogOk = dialog.findViewById(R.id.btnDialogOk);
            txtHeading  = dialog.findViewById(R.id.txtHeading);
            edInputField  = dialog.findViewById(R.id.edInputField);
            txtHeading.setText(R.string.mobile_number);


            int maxLength = 10;
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            edInputField.setFilters(FilterArray);

            dialog.show();

            btnCancelDialogTop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });
            btnDialogOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(edInputField.getText().toString().length() == 0 || edInputField.getText().toString().length() < 10) {

                        Toast.makeText(getActivity(), "Please input valid Number", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        new  PostClassCheckAddhaarPhoneValidate(getActivity(),edInputField.getText().toString().trim(),"mobile").execute();

                    }

                }
            });
        }

        else if (view == btnSave) {

            if (isAadhaarMandatory) {
                if (tvAadhaarNumber.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Aadhaar field is required!", Toast.LENGTH_LONG).show();
                } else if (tvMobileNumber.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Phone Number is required!", Toast.LENGTH_LONG).show();

                } else if (titleCode.trim().length() == 0) {
                    Toast.makeText(getActivity(), "Title field is required!", Toast.LENGTH_LONG).show();

                } else if (edFisrtName.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "FirstName field is required!", Toast.LENGTH_LONG).show();

                } else if (mitraCode.trim().length() == 0) {
                    Toast.makeText(getActivity(), "Mitra field is required!", Toast.LENGTH_LONG).show();

                } else if (tvState.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "State field is required!", Toast.LENGTH_LONG).show();

                } else if (tvDistrict.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "District field is required!", Toast.LENGTH_LONG).show();

                } else if (tvTaluka.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Taluka field is required!", Toast.LENGTH_LONG).show();

                } else if (tvCity.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "City field is required!", Toast.LENGTH_LONG).show();

                } else if (edAddressLineOne.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Address field is required!", Toast.LENGTH_LONG).show();

                } else if (genderCode.trim().length() == 0) {
                    Toast.makeText(getActivity(), "Gender field is required!", Toast.LENGTH_LONG).show();

                } else {

                    if (PageValueReg.equalsIgnoreCase("add")) {
                        String mitra_number = "";

                        new PostClassCreateMitra(getActivity(), "add", "", mitra_number,
                                mitraCode.trim(), titleCode.trim(),
                                edFisrtName.getText().toString().trim(), edMiddleName.getText().toString().trim(), edLastName.getText().toString().trim(),
                                edEmail.getText().toString().trim(), tvMobileNumber.getText().toString().trim(),
                                genderCode.trim(), maritalCode.trim(),
                                edSpouseName.getText().toString().trim(), date_dobOfMitra.trim(),
                                bloodCode.trim(), date_Anniversary.trim(),
                                edChildOneName.getText().toString().trim(), date_dobChildOne.trim(),
                                edChildTwoName.getText().toString().trim(), date_dobChildTwo.trim(),
                                edPan.getText().toString().trim(), tvAadhaarNumber.getText().toString().trim(),
                                edDrivingLicence.getText().toString().trim(), edVoterId.getText().toString().trim(),
                                edGstNumber.getText().toString().trim(), edAddressLineOne.getText().toString().trim(),
                                edAddressLineTwo.getText().toString().trim(), edAddressLineThree.getText().toString().trim(),
                                tvCity.getText().toString().trim(), tvTaluka.getText().toString().trim(),
                                tvDistrict.getText().toString().trim(), tvState.getText().toString().trim(),
                                edPostal.getText().toString().trim(), "","","", "", "", "", "",
                                "N"
                        ).execute();

                    }
                    else if (PageValueReg.equalsIgnoreCase("edit")) {
                        new PostClassCreateMitra(getActivity(), "edit", mitraId, mitra_number,
                                mitraCode.trim(), titleCode.trim(),
                                edFisrtName.getText().toString().trim(), edMiddleName.getText().toString().trim(), edLastName.getText().toString().trim(),
                                edEmail.getText().toString().trim(), tvMobileNumber.getText().toString().trim(),
                                genderCode.trim(), maritalCode.trim(),
                                edSpouseName.getText().toString().trim(), date_dobOfMitra.trim(),
                                bloodCode.trim(), date_Anniversary.trim(),
                                edChildOneName.getText().toString().trim(), date_dobChildOne.trim(),
                                edChildTwoName.getText().toString().trim(), date_dobChildTwo.trim(),
                                edPan.getText().toString().trim(), tvAadhaarNumber.getText().toString().trim(),
                                edDrivingLicence.getText().toString().trim(), edVoterId.getText().toString().trim(),
                                edGstNumber.getText().toString().trim(), edAddressLineOne.getText().toString().trim(),
                                edAddressLineTwo.getText().toString().trim(), edAddressLineThree.getText().toString().trim(),
                                tvCity.getText().toString().trim(), tvTaluka.getText().toString().trim(),
                                tvDistrict.getText().toString().trim(), tvState.getText().toString().trim(),
                                edPostal.getText().toString().trim(), adhaar_imageUrl, adhaar_imageUrl_back,voter_imageUrl, voter_imageUrl_back,driving_licence_imageUrl, pan_imageUrl, mitra_imageUrl,
                                mitraApproveStatus
                        ).execute();
                    }
                    else
                     {
                        String mitra_number = "";

                        new PostClassCreateMitra(getActivity(), "add", "", mitra_number,
                                mitraCode.trim(), titleCode.trim(),
                                edFisrtName.getText().toString().trim(), edMiddleName.getText().toString().trim(), edLastName.getText().toString().trim(),
                                edEmail.getText().toString().trim(), tvMobileNumber.getText().toString().trim(),
                                genderCode.trim(), maritalCode.trim(),
                                edSpouseName.getText().toString().trim(), date_dobOfMitra.trim(),
                                bloodCode.trim(), date_Anniversary.trim(),
                                edChildOneName.getText().toString().trim(), date_dobChildOne.trim(),
                                edChildTwoName.getText().toString().trim(), date_dobChildTwo.trim(),
                                edPan.getText().toString().trim(), tvAadhaarNumber.getText().toString().trim(),
                                edDrivingLicence.getText().toString().trim(), edVoterId.getText().toString().trim(),
                                edGstNumber.getText().toString().trim(), edAddressLineOne.getText().toString().trim(),
                                edAddressLineTwo.getText().toString().trim(), edAddressLineThree.getText().toString().trim(),
                                tvCity.getText().toString().trim(), tvTaluka.getText().toString().trim(),
                                tvDistrict.getText().toString().trim(), tvState.getText().toString().trim(),
                                edPostal.getText().toString().trim(), "", "","","", "", "", "",
                                "N"
                        ).execute();

                    }

                }
            } else {


                if (tvMobileNumber.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Phone Number is required!", Toast.LENGTH_LONG).show();

                } else if (titleCode.trim().length() == 0) {
                    Toast.makeText(getActivity(), "Title field is required!", Toast.LENGTH_LONG).show();

                } else if (edFisrtName.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "FirstName field is required!", Toast.LENGTH_LONG).show();

                } else if (mitraCode.trim().length() == 0) {
                    Toast.makeText(getActivity(), "Mitra field is required!", Toast.LENGTH_LONG).show();

                } else if (tvState.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "State field is required!", Toast.LENGTH_LONG).show();

                } else if (tvDistrict.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "District field is required!", Toast.LENGTH_LONG).show();

                } else if (tvTaluka.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Taluka field is required!", Toast.LENGTH_LONG).show();

                } else if (tvCity.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "City field is required!", Toast.LENGTH_LONG).show();

                } else if (edAddressLineOne.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Address field is required!", Toast.LENGTH_LONG).show();

                } else if (genderCode.trim().length() == 0) {
                    Toast.makeText(getActivity(), "Gender field is required!", Toast.LENGTH_LONG).show();

                } else {

                    if (PageValueReg.equalsIgnoreCase("add")) {
                        String mitra_number = "";

                        new PostClassCreateMitra(getActivity(), "add", "", mitra_number,
                                mitraCode.trim(), titleCode.trim(),
                                edFisrtName.getText().toString().trim(), edMiddleName.getText().toString().trim(), edLastName.getText().toString().trim(),
                                edEmail.getText().toString().trim(), tvMobileNumber.getText().toString().trim(),
                                genderCode.trim(), maritalCode.trim(),
                                edSpouseName.getText().toString().trim(), date_dobOfMitra.trim(),
                                bloodCode.trim(), date_Anniversary.trim(),
                                edChildOneName.getText().toString().trim(), date_dobChildOne.trim(),
                                edChildTwoName.getText().toString().trim(), date_dobChildTwo.trim(),
                                edPan.getText().toString().trim(), tvAadhaarNumber.getText().toString().trim(),
                                edDrivingLicence.getText().toString().trim(), edVoterId.getText().toString().trim(),
                                edGstNumber.getText().toString().trim(), edAddressLineOne.getText().toString().trim(),
                                edAddressLineTwo.getText().toString().trim(), edAddressLineThree.getText().toString().trim(),
                                tvCity.getText().toString().trim(), tvTaluka.getText().toString().trim(),
                                tvDistrict.getText().toString().trim(), tvState.getText().toString().trim(),
                                edPostal.getText().toString().trim(), "", "","","", "", "", "",
                                "N"
                        ).execute();

                    }
                   else  if (PageValueReg.equalsIgnoreCase("edit")) {
                        new PostClassCreateMitra(getActivity(), "edit", mitraId, mitra_number,
                                mitraCode.trim(), titleCode.trim(),
                                edFisrtName.getText().toString().trim(), edMiddleName.getText().toString().trim(), edLastName.getText().toString().trim(),
                                edEmail.getText().toString().trim(), tvMobileNumber.getText().toString().trim(),
                                genderCode.trim(), maritalCode.trim(),
                                edSpouseName.getText().toString().trim(), date_dobOfMitra.trim(),
                                bloodCode.trim(), date_Anniversary.trim(),
                                edChildOneName.getText().toString().trim(), date_dobChildOne.trim(),
                                edChildTwoName.getText().toString().trim(), date_dobChildTwo.trim(),
                                edPan.getText().toString().trim(), tvAadhaarNumber.getText().toString().trim(),
                                edDrivingLicence.getText().toString().trim(), edVoterId.getText().toString().trim(),
                                edGstNumber.getText().toString().trim(), edAddressLineOne.getText().toString().trim(),
                                edAddressLineTwo.getText().toString().trim(), edAddressLineThree.getText().toString().trim(),
                                tvCity.getText().toString().trim(), tvTaluka.getText().toString().trim(),
                                tvDistrict.getText().toString().trim(), tvState.getText().toString().trim(),
                                edPostal.getText().toString().trim(), adhaar_imageUrl,adhaar_imageUrl_back ,voter_imageUrl, voter_imageUrl_back,driving_licence_imageUrl, pan_imageUrl, mitra_imageUrl,
                                mitraApproveStatus
                        ).execute();
                    }
                    else
                     {
                        String mitra_number = "";

                        new PostClassCreateMitra(getActivity(), "add", "", mitra_number,
                                mitraCode.trim(), titleCode.trim(),
                                edFisrtName.getText().toString().trim(), edMiddleName.getText().toString().trim(), edLastName.getText().toString().trim(),
                                edEmail.getText().toString().trim(), tvMobileNumber.getText().toString().trim(),
                                genderCode.trim(), maritalCode.trim(),
                                edSpouseName.getText().toString().trim(), date_dobOfMitra.trim(),
                                bloodCode.trim(), date_Anniversary.trim(),
                                edChildOneName.getText().toString().trim(), date_dobChildOne.trim(),
                                edChildTwoName.getText().toString().trim(), date_dobChildTwo.trim(),
                                edPan.getText().toString().trim(), tvAadhaarNumber.getText().toString().trim(),
                                edDrivingLicence.getText().toString().trim(), edVoterId.getText().toString().trim(),
                                edGstNumber.getText().toString().trim(), edAddressLineOne.getText().toString().trim(),
                                edAddressLineTwo.getText().toString().trim(), edAddressLineThree.getText().toString().trim(),
                                tvCity.getText().toString().trim(), tvTaluka.getText().toString().trim(),
                                tvDistrict.getText().toString().trim(), tvState.getText().toString().trim(),
                                edPostal.getText().toString().trim(), "", "","","", "", "", ""
                                ,"N"
                        ).execute();

                    }
                }

            }
        }

        else if (view == tvMitraType) {
            showSpinnerDialog(MitraTypeList, "Mitra Type");

        }
        else if (view == tvTitle) {
              showSpinnerDialog(Titlelist, "Title");
        }
        else if (view == spnGender) {
            showSpinnerDialog(GenderList, "Gender");
        }
        else if (view == tvMaritalStatus) {
            showSpinnerDialog(MaritalList, "Marital Status");
        }
        else if (view == tvState) {
            showSpinnerDialog(StateList, "State");
        }
        else if (view == tvDistrict) {
            showSpinnerDialog(DistrictList, "District");
        }
        else if (view == tvTaluka) {
            showSpinnerDialog(TalukaList, "Taluka");
        }
        else if (view == tvCity) {
            showSpinnerDialog(CityList, "City");
        }
        else if (view == tvBloodType) {
            showSpinnerDialog(BloodList, "Blood Type");

        }
        else if (view == tvDateOfBirthMitra) {
            showDialog();
            dateButtonType = "tvDateOfBirthMitra";
        } else if (view == tvAnniversary) {
            showDialog();
            dateButtonType = "tvAnniversary";
        } else if (view == tvChildOneDob) {
            showDialog();
            dateButtonType = "tvChildOneDob";
        } else if (view == tvChildOneDob) {
            showDialog();
            dateButtonType = "tvChildOneDob";
        } else if (view == tvChildTwoDob) {
            showDialog();
            dateButtonType = "tvChildTwoDob";
        }

    }

    private void showDialog() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        cal.add(Calendar.DAY_OF_MONTH, 1);
        // Create the DatePickerDialog instance
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), datePickerListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.setCancelable(false);
        datePicker.setTitle("Select the date");
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePicker.show();
    }

    private void showSpinnerDialog(ArrayList<LookupCategory> list, String title) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        listView = (ListView) dialogView.findViewById(R.id.listCatecory);
        dialogTitle =  dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerAdapter = new SpinnerDialogAdapter(getActivity(), list, title);
        listView.setAdapter(spinnerAdapter);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }

    public class SpinnerDialogAdapter extends BaseAdapter {
        Context context;
        List<LookupCategory> rowItems;
        String Title = "";


        public SpinnerDialogAdapter(Context context, List<LookupCategory> items, String title) {
            this.context = context;
            this.rowItems = items;
            this.Title = title;
            Log.d("===rowItems.zize===", "" + rowItems.size());


        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;

        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new ViewHolder();
                holder.textName = convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();

            }

            holder.textName.setText(rowItems.get(position).getDESCRIPTION());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String meaning = rowItems.get(position).getDESCRIPTION();
                    switch (Title) {
                        case "Packing Type":
                            builder3.dismiss();
                          //  tvPackingType.setText(meaning);

                            break;
                        case "Brand":
                            builder3.dismiss();
                            tvBrand.setText(meaning);
                           // brandCode = rowItems.get(position).getCODE();
                         //   new PostClassMidalman(context, rowItems.get(position).getCODE()).execute();

                            break;
                        case "Product":
                            builder3.dismiss();
                            tvProduct.setText(meaning);
                            break;

                        case "Mitra Type":
                            builder3.dismiss();
                            tvMitraType.setText(meaning);
                            mitraCode = rowItems.get(position).getCODE();

                            if (mitraCode.equalsIgnoreCase("M")) {
                                isAadhaarMandatory = true;

                                tvAadhaarTitle.setText(R.string.aadhaar_number_red);
                                Log.e("Mitra Type code ", "" + mitraCode);
                            } else if (mitraCode.equalsIgnoreCase("C")) {
                                isAadhaarMandatory = true;

                                tvAadhaarTitle.setText(R.string.aadhaar_number_red);
                                Log.e("Mitra Type code ", "" + mitraCode);

                            } else {
                                isAadhaarMandatory = false;
                                tvAadhaarTitle.setText(R.string.aadhaar_number);
                                tvAadhaarNumber.setText("");
                                Log.e("Mitra Type code ", "" + mitraCode);
                            }

                            break;

                        case "Title":
                            builder3.dismiss();
                            tvTitle.setText(meaning);
                            titleCode = rowItems.get(position).getCODE();
                            break;

                        case "Gender":
                            builder3.dismiss();
                            spnGender.setText(meaning);
                            genderCode = rowItems.get(position).getCODE();
                            break;

                        case "Marital Status":
                            builder3.dismiss();
                            tvMaritalStatus.setText(meaning);
                            maritalCode = rowItems.get(position).getCODE();
                            break;

                        case "State":
                            builder3.dismiss();
                            tvState.setText(meaning);
                            stateCode = rowItems.get(position).getDESCRIPTION();
                            tvDistrict.setText("");
                            districtCode = "";
                            tvTaluka.setText("");
                            talukaCode = "";
                            tvCity.setText("");
                            new PostClassDistrictList(getActivity(), rowItems.get(position).getDESCRIPTION()).execute();
                            break;

                        case "District":
                            builder3.dismiss();
                            tvDistrict.setText(meaning);
                            districtCode = rowItems.get(position).getDESCRIPTION();
                            tvTaluka.setText("");
                            talukaCode = "";
                            tvCity.setText("");
                            new PostClassTalukaList(getActivity(), rowItems.get(position).getDESCRIPTION(),stateCode).execute();
                            break;

                        case "Taluka":
                            builder3.dismiss();
                            tvTaluka.setText(meaning);
                            talukaCode = rowItems.get(position).getDESCRIPTION();
                            tvCity.setText("");
                            new PostClassCityList(getActivity(), rowItems.get(position).getDESCRIPTION(),stateCode,districtCode).execute();
                            break;

                        case "City":
                            builder3.dismiss();
                            tvCity.setText(meaning);
                            break;

                        case "Blood Type":
                            builder3.dismiss();
                            tvBloodType.setText(meaning);
                            bloodCode = rowItems.get(position).getCODE();
                            break;

                    }


                }
            });
            return convertView;
        }


        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }


    }

    public class PostClassLookUpData extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempCategorylist;
        public ArrayList<LookupCategory> tempPackingList;
        public ArrayList<LookupCategory> tempBrandList;
        public ArrayList<LookupCategory> tempDispatchfromList;
        public ArrayList<LookupCategory> tempProductList;
        public ArrayList<LookupCategory> tempUsertypeList;
        public ArrayList<LookupCategory> tempUserstatusList;
        public ArrayList<LookupCategory> tempvalidity_type;
        public ArrayList<LookupCategory> tempMitraTypeList;
        public ArrayList<LookupCategory> tempTitlelist;
        public ArrayList<LookupCategory> tempGenderList;
        public ArrayList<LookupCategory> tempMaritalList;
        public ArrayList<LookupCategory> tempBloodList;

        public PostClassLookUpData(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

            tempCategorylist = new ArrayList<LookupCategory>();
            tempPackingList = new ArrayList<LookupCategory>();
            tempBrandList = new ArrayList<LookupCategory>();
            tempDispatchfromList = new ArrayList<LookupCategory>();
            tempProductList = new ArrayList<LookupCategory>();
            tempUsertypeList = new ArrayList<LookupCategory>();
            tempUserstatusList = new ArrayList<LookupCategory>();
            tempvalidity_type = new ArrayList<LookupCategory>();

            tempMitraTypeList = new ArrayList<LookupCategory>();
            tempTitlelist = new ArrayList<LookupCategory>();
            tempGenderList = new ArrayList<LookupCategory>();
            tempMaritalList = new ArrayList<LookupCategory>();
            tempBloodList = new ArrayList<LookupCategory>();
        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                   /* MediaType mediaType = MediaType.parse("application/json");

                    JSONObject jsonObject_page_module = new JSONObject();

                    try {
                        jsonObject_page_module.put("page_module", MODULE_MITRAS);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody body = RequestBody.create(mediaType, jsonObject_page_module.toString());

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();*/

                    RequestBody body = RequestBody.create(null, new byte[]{});

                    // RequestBody body = RequestBody.create(mediaType, jsonObject_page_module.toString());

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .addHeader("page_module", MODULE_COMMON)
                            //.addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();


                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONObject category = jObject.getJSONObject("data");

                            System.out.println("======Lookup_data=========" + Lookup_data);


                            JSONArray jsonArrayMitraType = category.getJSONArray("mitra_type");
                            for (int i = 0; i < jsonArrayMitraType.length(); i++) {
                                JSONObject jsonPacking = jsonArrayMitraType.getJSONObject(i);
                                tempMitraTypeList.add(new LookupCategory(jsonPacking.getString("lookup_type"),
                                        jsonPacking.getString("code"), jsonPacking.optString("description", "Not Available")));

                            }
                           MitraTypeList.addAll(tempMitraTypeList);

                            JSONArray jsonArrayTitle = category.getJSONArray("title");
                            for (int i = 0; i < jsonArrayTitle.length(); i++) {
                                JSONObject jsonPacking = jsonArrayTitle.getJSONObject(i);
                                tempTitlelist.add(new LookupCategory(jsonPacking.getString("lookup_type"),
                                        jsonPacking.getString("code"), jsonPacking.optString("description","Not Available")));

                            }
                            Titlelist.addAll(tempTitlelist);

                            JSONArray jsonSex = category.getJSONArray("sex");
                            for (int i = 0; i < jsonSex.length(); i++) {
                                JSONObject jsonObject = jsonSex.getJSONObject(i);
                                tempGenderList.add(new LookupCategory(jsonObject.getString("lookup_type"),
                                        jsonObject.getString("code"), jsonObject.getString("description")));

                            }
                            GenderList.addAll(tempGenderList);


                            JSONArray jsonArrayMaritalStatus = category.getJSONArray("marital_status");
                            for (int i = 0; i < jsonArrayMaritalStatus.length(); i++) {
                                JSONObject jsonBrand = jsonArrayMaritalStatus.getJSONObject(i);
                                tempMaritalList.add(new LookupCategory(jsonBrand.getString("lookup_type"),
                                        jsonBrand.getString("code"), jsonBrand.getString("description")));

                            }
                            MaritalList.addAll(tempMaritalList);

                            JSONArray jsonArrayBloodType = category.getJSONArray("blood_type");
                            for (int i = 0; i < jsonArrayBloodType.length(); i++) {
                                JSONObject jsondispatchfrom = jsonArrayBloodType.getJSONObject(i);
                                tempBloodList.add(new LookupCategory(jsondispatchfrom.getString("lookup_type"),
                                        jsondispatchfrom.getString("code"), jsondispatchfrom.getString("description")));

                            }
                            BloodList.addAll(tempBloodList);

                            JSONArray jsonArrayproduct = category.getJSONArray("product");
                            for (int i = 0; i < jsonArrayproduct.length(); i++) {
                                JSONObject jsonproduct = jsonArrayproduct.getJSONObject(i);
                                tempProductList.add(new LookupCategory(jsonproduct.getString("lookup_type"),
                                        jsonproduct.getString("code"), jsonproduct.getString("description")));

                            }
                          //  productList.addAll(tempProductList);

                            JSONArray jsonArrayUsertype = category.getJSONArray("user_type");
                            for (int i = 0; i < jsonArrayUsertype.length(); i++) {
                                JSONObject jsonUsertype = jsonArrayUsertype.getJSONObject(i);
                                tempUsertypeList.add(new LookupCategory(jsonUsertype.getString("lookup_type"),
                                        jsonUsertype.getString("code"), jsonUsertype.getString("description")));

                            }


                            JSONArray jsonArrayValiditytype = category.getJSONArray("validity_type");
                            for (int i = 0; i < jsonArrayValiditytype.length(); i++) {
                                JSONObject Validitytype = jsonArrayValiditytype.getJSONObject(i);
                                tempvalidity_type.add(new LookupCategory(Validitytype.getString("lookup_type"),
                                        Validitytype.getString("code"), Validitytype.getString("description")));

                            }
                           // validity_type.addAll(tempvalidity_type);


                            Log.d("===dispatchList.size===", "=====" + tempDispatchfromList.size());
                            Log.d("===product.size===", "=====" + tempProductList.size());

                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
              /*  spinTradeadapter = new SpinCalegory_Adapter(getActivity(), tempCategorylist);
                spnCategory.setAdapter(spinTradeadapter);*/

                if (PageValueReg.equalsIgnoreCase("edit")) {
                    ListContainsBean listContainsTitle = Constant.checkListContains(Titlelist, Constant.nullCheckFunction(title));
                    if (listContainsTitle.isExist()) {
                       // Toast.makeText(context, "Exists", Toast.LENGTH_SHORT).show();
                        tvTitle.setText(Constant.nullCheckFunction(listContainsTitle.getLookupCategory().getDESCRIPTION()));
                        titleCode = Constant.nullCheckFunction(listContainsTitle.getLookupCategory().getCODE());
                       // Toast.makeText(context, "titleCode  "+titleCode, Toast.LENGTH_SHORT).show();
                        Log.e("titleCode ",titleCode);
                    } else {
                        //Toast.makeText(context, "NOT Exists", Toast.LENGTH_SHORT).show();
                    }

                    ListContainsBean listContainsMitraType = Constant.checkListContains(MitraTypeList, Constant.nullCheckFunction(mitraCode));
                    if (listContainsMitraType.isExist()) {

                        tvMitraType.setText(Constant.nullCheckFunction(listContainsMitraType.getLookupCategory().getDESCRIPTION()));
                        mitraCode = Constant.nullCheckFunction(listContainsMitraType.getLookupCategory().getCODE());
                        //Toast.makeText(context, "mitraCode  "+mitraCode, Toast.LENGTH_SHORT).show();
                        Log.e("mitraCode ",mitraCode);
                    } else {
                       // Toast.makeText(context, "NOT Exists", Toast.LENGTH_SHORT).show();
                    }

                    ListContainsBean listContainsGender = Constant.checkListContains(GenderList, Constant.nullCheckFunction(genderCode));
                    if (listContainsGender.isExist()) {
                       // Toast.makeText(context, "Exists", Toast.LENGTH_SHORT).show();
                        spnGender.setText(Constant.nullCheckFunction(listContainsGender.getLookupCategory().getDESCRIPTION()));
                        genderCode = Constant.nullCheckFunction(listContainsGender.getLookupCategory().getCODE());
                        Log.e("genderCode ",genderCode);
                    } else {
                       // Toast.makeText(context, "NOT Exists", Toast.LENGTH_SHORT).show();
                    }

                    ListContainsBean listContainsMaritalStatus = Constant.checkListContains(MaritalList, Constant.nullCheckFunction(marital_status_code));
                    if (listContainsMaritalStatus.isExist()) {
                     //   Toast.makeText(context, "Exists", Toast.LENGTH_SHORT).show();
                        tvMaritalStatus.setText(Constant.nullCheckFunction(listContainsMaritalStatus.getLookupCategory().getDESCRIPTION()));
                        maritalCode = Constant.nullCheckFunction(listContainsMaritalStatus.getLookupCategory().getCODE());
                        Log.e("maritalCode ",maritalCode);
                    } else {
                      //  Toast.makeText(context, "NOT Exists", Toast.LENGTH_SHORT).show();
                    }

                    ListContainsBean listContainsBloodType = Constant.checkListContains(BloodList, Constant.nullCheckFunction(blood_type_code));
                    if (listContainsBloodType.isExist()) {
                      //  Toast.makeText(context, "Exists", Toast.LENGTH_SHORT).show();
                        tvBloodType.setText(Constant.nullCheckFunction(listContainsBloodType.getLookupCategory().getDESCRIPTION()));
                        bloodCode = Constant.nullCheckFunction(listContainsBloodType.getLookupCategory().getCODE());
                    } else {
                       // Toast.makeText(context, "NOT Exists", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

    class PostClassStateList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        String Keyword;
        boolean showLoader;
        public ArrayList<LookupCategory> tempStateList;

        PostClassStateList(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempStateList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Statelist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Statelist=========" + Statelist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempStateList.add(new LookupCategory("",
                                            "", jsonObject.getString("state")));

                                }
                                StateList.clear();
                                StateList.addAll(tempStateList);


                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


            } else {


            }

        }
    }

    class PostClassDistrictList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        String Keyword;
        boolean showLoader;
        public ArrayList<LookupCategory> tempDistrictList;
        String state;

        PostClassDistrictList(Context c, String state) {
            this.context = c;
            this.state = state;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempDistrictList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("state",state);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Districtlist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======orders_listUrl=========" + orders_listUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempDistrictList.add(new LookupCategory("",
                                            "", jsonObject.getString("district")));

                                }
                                DistrictList.clear();
                                DistrictList.addAll(tempDistrictList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            } else {

            }
        }
    }

    class PostClassTalukaList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        String Keyword;
        boolean showLoader;
        public ArrayList<LookupCategory> tempTalukaList;
        String district;
        String state ;

        PostClassTalukaList(Context c, String district, String state) {
            this.context = c;
            this.district = district;
            this.state = state;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempTalukaList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("district", district);
                    json.put("state", state);

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Talukalist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======orders_listUrl=========" + orders_listUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempTalukaList.add(new LookupCategory("",
                                            "", jsonObject.getString("taluka")));

                                }
                                TalukaList.clear();
                                TalukaList.addAll(tempTalukaList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();




            } else {


            }

        }
    }

    class PostClassCityList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        String Keyword;
        boolean showLoader;
        public ArrayList<LookupCategory> tempCityList;
        String taluka;
        String state, district;


        PostClassCityList(Context c, String taluka, String state, String district) {
            this.context = c;
            this.taluka = taluka;
            this.state = state;
            this.district = district;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempCityList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                   // json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("taluka", taluka);
                    json.put("state", state);
                    json.put("district", district);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Citylist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======orders_listUrl=========" + orders_listUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempCityList.add(new LookupCategory("",
                                            "", jsonObject.getString("city")));

                                }
                                CityList.clear();
                                CityList.addAll(tempCityList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();




            } else {


            }

        }
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            String year = String.valueOf(selectedYear);
            String month = String.valueOf(selectedMonth + 1);
            String day = String.valueOf(selectedDay);
            String time = "";
            try {
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("hh:mm aa");
                time = df.format(cal.getTime());

                // tvDate.setText(time);

            } catch (Exception e) {
                e.printStackTrace();
            }

            String validity = String.valueOf(new StringBuilder().append(day).append("-")
                    .append(month).append("-").append(year));
            DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
            Date date1 = null;
            Calendar cal = Calendar.getInstance();
            try {
                date1 = readFormat1.parse(validity);
                String dateformatt = formatter1.format(date1);

                if (dateButtonType.equalsIgnoreCase("tvDateOfBirthMitra")) {
                    tvDateOfBirthMitra.setText(dateformatt);
                    date_dobOfMitra = validity;
                }
                if (dateButtonType.equalsIgnoreCase("tvAnniversary")) {
                    tvAnniversary.setText(dateformatt);
                    date_Anniversary = validity;
                }
                if (dateButtonType.equalsIgnoreCase("tvChildOneDob")) {
                    tvChildOneDob.setText(dateformatt);
                    date_dobChildOne = validity;
                }
                if (dateButtonType.equalsIgnoreCase("tvChildTwoDob")) {
                    tvChildTwoDob.setText(dateformatt);
                    date_dobChildTwo = validity;
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };

    class PostClassMitraDetail extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempMitraList;
        String id;

        PostClassMitraDetail(Context c, String id) {
            this.context = c;
            this.id = id;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

            tempMitraList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();

                    json.put("id",id );
                    //json.put("page_module", MODULE_MITRAS);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(MitraDetail)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                           // .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======MitraDetail=========" + MitraDetail);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONObject jsonData = jObject.getJSONObject("data");
                            if (jObject != null) {

                                first_name  = Constant.nullCheckFunction(jsonData.getString("first_name"));
                                middle_names = Constant.nullCheckFunction(jsonData.getString("middle_names"));
                                title = Constant.nullCheckFunction(jsonData.getString("title"));
                                mitraCode = Constant.nullCheckFunction(jsonData.getString("mitra_type_code"));
                                genderCode = Constant.nullCheckFunction(jsonData.getString("sex_code"));
                                last_name = Constant.nullCheckFunction(jsonData.getString("last_name"));
                                email_address = Constant.nullCheckFunction(jsonData.getString("email_address"));
                                mobile_number = Constant.nullCheckFunction(jsonData.getString("mobile_number"));
                                sex_code =  Constant.nullCheckFunction(jsonData.getString("sex_code"));
                                blood_type_code = Constant.nullCheckFunction(jsonData.getString("blood_type_code"));
                                date_of_birth =  Constant.dateFuncation( jsonData.getString("date_of_birth"));
                                mitra_number =  Constant.nullCheckFunction(jsonData.getString("mitra_number"));
                                adhaar_number= Constant.nullCheckFunction(jsonData.getString("adhaar_number"));
                                voter_id= Constant.nullCheckFunction(jsonData.getString("voter_id"));
                                pan =  Constant.nullCheckFunction(jsonData.getString("pan"));
                                driving_licence_number =  Constant.nullCheckFunction(jsonData.getString("driving_licence_number"));
                                gst_number=  Constant.nullCheckFunction(jsonData.getString("gst_number"));
                                marital_status_code =  Constant.nullCheckFunction(jsonData.getString("marital_status_code"));
                                spouse_name=  Constant.nullCheckFunction(jsonData.getString("spouse_name"));
                                date_of_wedding=   Constant.dateFuncation( jsonData.getString("date_of_wedding"));
                                child1_name=  Constant.nullCheckFunction(jsonData.getString("child1_name"));
                                date_of_birth_child1= Constant.dateFuncation( jsonData.getString("date_of_birth_child1"));
                                child2_name=  Constant.nullCheckFunction(jsonData.getString("child2_name"));
                                date_of_birth_child2=  Constant.dateFuncation(jsonData.getString("date_of_birth_child2"));
                                address_line1= Constant.nullCheckFunction(jsonData.getString("address_line1"));
                                address_line2= Constant.nullCheckFunction(jsonData.getString("address_line2"));
                                address_line3= Constant.nullCheckFunction(jsonData.getString("address_line3"));
                                state= Constant.nullCheckFunction(jsonData.getString("state"));
                                district= Constant.nullCheckFunction(jsonData.getString("district"));
                                taluka= Constant.nullCheckFunction(jsonData.getString("taluka"));
                                city= Constant.nullCheckFunction(jsonData.getString("city"));
                                mitraApproveStatus= Constant.nullCheckFunction(jsonData.getString("status"));
                                postal_code=  Constant.nullCheckFunction(jsonData.getString("postal_code"));



                                adhaar_imageUrl = Constant.nullCheckFunction(jsonData.getString("adhaar_image"));
                                adhaar_imageUrl_back = Constant.nullCheckFunction(jsonData.getString("adhaar_image_back"));

                                voter_imageUrl = Constant.nullCheckFunction(jsonData.getString("voter_image"));
                                voter_imageUrl_back = Constant.nullCheckFunction(jsonData.getString("voter_image_back"));

                                driving_licence_imageUrl = Constant.nullCheckFunction(jsonData.getString("driving_licence_image"));
                                pan_imageUrl = Constant.nullCheckFunction(jsonData.getString("pan_image"));
                                mitra_imageUrl = Constant.nullCheckFunction(jsonData.getString("mitra_image"));


                                mobile_number_verified_flag = Constant.nullCheckFunction(jsonData.getString("mobile_number_verified_flag"));


                            }
                           // mitraList.addAll(tempMitraList);


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

              /*
                sex_code =  jsonData.getString("sex_code");
                blood_type_code = jsonData.getString("blood_type_code"
                         =  Constant.dateFuncation( jsonData.getS
                                mitra_number =  jsonData.getString("mitra_number")*/

                edFisrtName.setText(first_name);
                edMiddleName.setText(middle_names);
                edLastName.setText(last_name);
                edEmail.setText(email_address);
                tvMobileNumber.setText(mobile_number);
                tvDateOfBirthMitra.setText(date_of_birth);
                tvAadhaarNumber.setText(adhaar_number);
                edVoterId.setText(voter_id);
                edPan.setText(pan);
                edDrivingLicence.setText(driving_licence_number);
                edGstNumber.setText(gst_number);
                edSpouseName.setText(spouse_name);
                tvAnniversary.setText(date_of_wedding);
                edChildOneName.setText(child1_name);
                tvChildOneDob.setText(date_of_birth_child1);
                edChildTwoName.setText(child2_name);
                tvChildTwoDob.setText(date_of_birth_child2);
                edAddressLineOne.setText(address_line1);
                edAddressLineTwo.setText(address_line2);
                edAddressLineThree.setText(address_line3);
                tvState.setText(state);
                tvDistrict.setText(district);
                tvTaluka.setText(taluka);
                tvCity.setText(city);
                edPostal.setText(postal_code);


                if(mitraApproveStatus.equalsIgnoreCase("N")){
                    simpleToggleButton1.setChecked(false);

                }
                if(mitraApproveStatus.equalsIgnoreCase("")){
                    simpleToggleButton1.setChecked(false);

                }
                if(mitraApproveStatus.equalsIgnoreCase("A")){
                    simpleToggleButton1.setChecked(true);

                }

                if(mobile_number_verified_flag.equalsIgnoreCase("y")){
                    btnVerifyPhoneNumber.setVisibility(View.GONE);

                }
                else if(mobile_number_verified_flag.equalsIgnoreCase("n")){
                    btnVerifyPhoneNumber.setVisibility(View.VISIBLE);
                }
                else if(mobile_number_verified_flag.equalsIgnoreCase("null")){
                    btnVerifyPhoneNumber.setVisibility(View.VISIBLE);
                }
                else if(mobile_number_verified_flag.equalsIgnoreCase("")){
                    btnVerifyPhoneNumber.setVisibility(View.VISIBLE);
                }

                if (mitraCode.equalsIgnoreCase("M")) {
                    isAadhaarMandatory = true;

                    tvAadhaarTitle.setText(R.string.aadhaar_number_red);

                    Log.e("Mitra Type code ", "" + mitraCode);
                } else if (mitraCode.equalsIgnoreCase("C")) {
                    isAadhaarMandatory = true;

                    tvAadhaarTitle.setText(R.string.aadhaar_number_red);
                    Log.e("Mitra Type code ", "" + mitraCode);

                } else {
                    isAadhaarMandatory = false;
                    tvAadhaarTitle.setText(R.string.aadhaar_number);
                    tvAadhaarNumber.setText("");
                    Log.e("Mitra Type code ", "" + mitraCode);
                }

                new PostClassDistrictList(getActivity(),state ).execute();
                new PostClassTalukaList(getActivity(),district, state ).execute();
                new PostClassCityList(getActivity(), taluka, district, state).execute();

            } else {

                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

    class PostClassCheckAddhaarPhoneValidate extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String checkval , type;

        PostClassCheckAddhaarPhoneValidate(Context c, String checkval, String type) {
            this.context = c;
            this.checkval = checkval;
            this.type = type;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();

                    json.put("checkval",checkval );
                    json.put("type",type );
                  //  json.put("page_module", MODULE_COMMON);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(ValidateAadhaarPhone)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======ValidateAadhaarPhone=========" + ValidateAadhaarPhone);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            record = jObject.getString("record");

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {

                //edPostal.setText(postal_code);
                if(record.equalsIgnoreCase("0")) {
                    if (type.equalsIgnoreCase("adhaar")) {
                        dialog.dismiss();
                        tvAadhaarNumber.setText(checkval);
                    }
                    if (type.equalsIgnoreCase("mobile")) {
                        dialog.dismiss();
                        tvMobileNumber.setText(checkval);

                        //Verify Otp
                       /* dialogForOtpVerify.getWindow();

                        dialogForOtpVerify.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        dialogForOtpVerify.setContentView(R.layout.dialog_with_edittext_and_button);
                        dialogForOtpVerify.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
                        dialogForOtpVerify.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                        dialogForOtpVerify.setCancelable(false);

                        TextView txtHeading;
                        Button btnCancelDialogTop , btnDialogOk;
                        final EditText edInputField;

                        btnCancelDialogTop = dialogForOtpVerify.findViewById(R.id.btnCancelDialogTop);
                        btnDialogOk = dialogForOtpVerify.findViewById(R.id.btnDialogOk);
                        txtHeading  = dialogForOtpVerify.findViewById(R.id.txtHeading);
                        edInputField  = dialogForOtpVerify.findViewById(R.id.edInputField);
                        txtHeading.setText(R.string.enter_otp);

                        int maxLength = 4;
                        InputFilter[] FilterArray = new InputFilter[1];
                        FilterArray[0] = new InputFilter.LengthFilter(maxLength);
                        edInputField.setFilters(FilterArray);

                        dialogForOtpVerify.show();

                        btnCancelDialogTop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogForOtpVerify.dismiss();
                                edInputField.setText("");
                                tvMobileNumber.setText("");

                            }
                        });
                        btnDialogOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if(edInputField.getText().toString().length() == 0 || edInputField.getText().toString().length() < 4) {

                                    Toast.makeText(getActivity(), "Please input valid otp", Toast.LENGTH_SHORT).show();
                                }
                                else {
                                   // new  PostClassCheckAddhaarPhoneValidate(getActivity(),edInputField.getText().toString().trim(),"mobile").execute();

                                }

                            }
                        });
*/




                    }


                }else {
                    Toast.makeText(context, type+" Number Already exists.", Toast.LENGTH_SHORT).show();
                    if (type.equalsIgnoreCase("adhaar")) {
                        tvAadhaarNumber.setText("");
                    }
                    if (type.equalsIgnoreCase("mobile")) {
                        tvMobileNumber.setText("");
                    }
                }

            } else {

                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

    class PostClass_SendOtp extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String id;

        public PostClass_SendOtp(Context c, String id ) {
            this.context = c;
            this.id = id;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {


                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();

                    try {
                        json.put("id",id );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //json.put("page_module", MODULE_MITRAS);
                    RequestBody body = RequestBody.create(mediaType, json.toString());
                    System.out.println("======Param=========" + json.toString());

                    Request request = new Request.Builder()
                            .url(VerifyMitraMobile)
                            .post(body)
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======VerifyMitraMobile=========" + VerifyMitraMobile);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            enc_code = jObject.getString("data").trim();

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                byte[] messagedata = Base64.decode(enc_code, Base64.NO_WRAP);
                decodedCode = new String(messagedata);
                Log.e("decodedCode",""+decodedCode);

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void funVerifyOtp(String mitraId,String otp, String enc_code){
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("mitra_id", mitraId);
            jsonObject.put("otp", otp);
            jsonObject.put("enc_code", enc_code);


            Log.d("Tag", "Params ----------->" + jsonObject.toString());
            new BackGroundTask.makeServiceCallPOSTWithCommonHeader(getActivity(), VerifyMitraMobileOtp, jsonObject.toString(),
                    new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String response) {
                            // exerciseJsonResponse = response;
                            //parseJsonData(response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                                    //JSONObject jsonData = jsonObject.getJSONObject("data");

                                   // StorePrefs.setDefaults(PREF_DESCRIPTION, jsonData.getString("description"), SplashActivity.this);
                                   // StorePrefs.setDefaults(PREF_DISCLAIMER_DESC, jsonData.getString("disclaimer"), SplashActivity.this);

                                    Utils.displayToastMessage(getActivity(), jsonObject.getString("replyMsg"));
                                    dialogForOtpVerify.dismiss();

                                    getActivity().onBackPressed();

                                } else if (jsonObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                                    Utils.displayToastMessage(getActivity(), jsonObject.getString("replyMsg"));
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String error) {

                        }
                    }).execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
