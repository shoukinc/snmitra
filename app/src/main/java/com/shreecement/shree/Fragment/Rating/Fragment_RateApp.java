package com.shreecement.shree.Fragment.Rating;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.ACTION_ADD;
import static com.shreecement.shree.Utlility.Constant.MODULE_RATINGS;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.RatingCreate;
import static com.shreecement.shree.Utlility.Constant.create_ratingUrl;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_RateApp extends Fragment implements View.OnClickListener{

    Button btnSubmit, btnStar1, btnStar2, btnStar3, btnStar4, btnStar5;
    String rating="";
    String Description="";
    EditText ediDescription;

    public static Fragment_RateApp newInstance(String text) {

        Fragment_RateApp f = new Fragment_RateApp();
        Bundle b = new Bundle();
        b.putString("edit", text);

        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_write_a_review,container, false);

        getActivity().setTitle(getActivity().getString(R.string.nav_item_rate_app));
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        btnSubmit = view.findViewById(R.id.btnSubmit);
        ediDescription = view.findViewById(R.id.ediDescription);
        btnSubmit.setOnClickListener(this);

        btnStar1 = view.findViewById(R.id.btnStar1);
        btnStar1.setOnClickListener(this);

        btnStar2 = view.findViewById(R.id.btnStar2);
        btnStar2.setOnClickListener(this);

        btnStar3 = view.findViewById(R.id.btnStar3);
        btnStar3.setOnClickListener(this);

        btnStar4 = view.findViewById(R.id.btnStar4);
        btnStar4.setOnClickListener(this);

        btnStar5 = view.findViewById(R.id.btnStar5);
        btnStar5.setOnClickListener(this);

        return view;

    }

    @Override
    public void onClick(View view) {
        if (view == btnSubmit){
            Description=ediDescription.getText().toString().trim();
            if(Description.length()==0){
                Toast.makeText(getActivity(), "Please write review", Toast.LENGTH_SHORT).show();
            }
            else if (rating.length()==0)
            {
                Toast.makeText(getActivity(), "Please give rating ", Toast.LENGTH_SHORT).show();}
            else {
                Log.d("rating","=============="+rating);
                new PostClass_Rating(getActivity(),Description,rating).execute();
            }

        }
        if (view == btnStar1){
            btnStar1.setBackgroundResource(R.drawable.star_red);
            btnStar2.setBackgroundResource(R.drawable.star_white);
            btnStar3.setBackgroundResource(R.drawable.star_white);
            btnStar4.setBackgroundResource(R.drawable.star_white);
            btnStar5.setBackgroundResource(R.drawable.star_white);
            rating="1";

        }
        if (view == btnStar2){
            btnStar1.setBackgroundResource(R.drawable.star_red);
            btnStar2.setBackgroundResource(R.drawable.star_red);
            btnStar3.setBackgroundResource(R.drawable.star_white);
            btnStar4.setBackgroundResource(R.drawable.star_white);
            btnStar5.setBackgroundResource(R.drawable.star_white);
            rating="2";
        }
        if (view == btnStar3){
            btnStar1.setBackgroundResource(R.drawable.star_red);
            btnStar2.setBackgroundResource(R.drawable.star_red);
            btnStar3.setBackgroundResource(R.drawable.star_red);
            btnStar4.setBackgroundResource(R.drawable.star_white);
            btnStar5.setBackgroundResource(R.drawable.star_white);
            rating="3";
        }
        if (view == btnStar4){
            btnStar1.setBackgroundResource(R.drawable.star_red);
            btnStar2.setBackgroundResource(R.drawable.star_red);
            btnStar3.setBackgroundResource(R.drawable.star_red);
            btnStar4.setBackgroundResource(R.drawable.star_red);
            btnStar5.setBackgroundResource(R.drawable.star_white);
            rating="4";
        }
        if (view == btnStar5){
            btnStar1.setBackgroundResource(R.drawable.star_red);
            btnStar2.setBackgroundResource(R.drawable.star_red);
            btnStar3.setBackgroundResource(R.drawable.star_red);
            btnStar4.setBackgroundResource(R.drawable.star_red);
            btnStar5.setBackgroundResource(R.drawable.star_red);
            rating="5";
        }
    }
    class PostClass_Rating extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String Description="";
        String Rating="";



        PostClass_Rating(Context c, String description, String rating) {
            this.context = c;
            this.Description = description;
            this.Rating = rating;
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {
            System.out.println("Rating===============" + Rating);

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                   // json.put("page_module", MODULE_RATINGS);
                    json.put("org_id", ORG_ID);
                    json.put("rating", Rating);
                    json.put("long_description", Description);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(RatingCreate)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_RATINGS)
                            .addHeader("action", ACTION_ADD)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======create_ratingUrl=========" + create_ratingUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
              // Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

                ediDescription.setText("");
                rating="";
                Description="";

                btnStar1.setBackgroundResource(R.drawable.star_white);
                btnStar2.setBackgroundResource(R.drawable.star_white);
                btnStar3.setBackgroundResource(R.drawable.star_white);
                btnStar4.setBackgroundResource(R.drawable.star_white);
                btnStar5.setBackgroundResource(R.drawable.star_white);

                //Toast.makeText(getActivity(), "Thank you for give valuable review.", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder alert = new AlertDialog.Builder(
                        context);
                alert.setTitle(jsonreplyMsg);
                alert.setCancelable(false);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /*Fragment fragment = new Fragment_Rating_list();
                        FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_left);
                        fragmentTransaction.replace(R.id.container_body, fragment);
                        fragmentTransaction.commit();*/
                        getActivity().onBackPressed();

                    }
                });

                alert.show();

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }}
