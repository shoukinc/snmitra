package com.shreecement.shree.Fragment.Tips.Bean;

/**
 * Created by kamesh on 2/1/18.
 */

public class BeanTips_Item {

    private String tip_id, tip_number, title, short_description, application_id, created_by,
            creation_date, long_description;

    public BeanTips_Item(String tip_id, String tip_number, String title, String short_description, String application_id,
                         String created_by, String creation_date, String long_description) {
        this.tip_id = tip_id;
        this.tip_number = tip_number;
        this.title = title;
        this.short_description = short_description;
        this.application_id = application_id;
        this.created_by = created_by;
        this.creation_date = creation_date;
        this.long_description = long_description;
    }

    public String getTip_id() {
        return tip_id;
    }

    public void setTip_id(String tip_id) {
        this.tip_id = tip_id;
    }

    public String getTip_num() {
        return tip_number;
    }

    public void setTip_num(String tip_number) {
        this.tip_number = tip_number;
    }

    public String getTitle() {
        return title;
    }

    public void getTitle(String title) {
        this.title = title;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getApplication_id() {
        return application_id;
    }

    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getLong_description() {
        return long_description;
    }

    public void setLong_description(String long_description) {
        this.long_description = long_description;
    }

}
