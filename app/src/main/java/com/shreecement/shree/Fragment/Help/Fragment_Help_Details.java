package com.shreecement.shree.Fragment.Help;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Fragment.Help.Bean.BeanHelp_Item;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;


/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Help_Details extends Fragment {




    TextView tvHelpQues,tvHelpAnswer;
    Button btn_HelpYes,btn_HelpNo;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_help_details,container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        tvHelpQues = view.findViewById(R.id.tvHelpQues);
        tvHelpAnswer = view.findViewById(R.id.tvHelpAnswer);
        getActivity().setTitle(getActivity().getString(R.string.help_detail));


        Gson gson = new Gson();
        String json = StorePrefs.getDefaults("beanHelp_item", getActivity());
        BeanHelp_Item beanHelp_item = gson.fromJson(json, BeanHelp_Item.class);

       /* tvNameNumber.setText( beanCustomer_item.getCust_account_id()   + " & "+beanCustomer_item.getParty_name());
        tvCityState.setText(beanCustomer_item.getCity()+ " & "+ beanCustomer_item.getState());*/

        tvHelpQues.setText( beanHelp_item.getTitle());
        tvHelpAnswer.setText(beanHelp_item.getLong_description());


         return view;
    }



}
