package com.shreecement.shree.Fragment.NotificationHeader.Bean;


public class BeanNotificationHeader {
    String notification_id, notification,notification_type, creation_date, status, source_type, sender_name,
            unixtimestring, timeagodate, status_text;

    public BeanNotificationHeader() {
    }

    public BeanNotificationHeader(String notification_id, String notification, String notification_type, String creation_date, String status, String source_type, String sender_name, String unixtimestring, String timeagodate, String status_text) {
        this.notification_id = notification_id;
        this.notification = notification;
        this.notification_type = notification_type;
        this.creation_date = creation_date;
        this.status = status;
        this.source_type = source_type;
        this.sender_name = sender_name;
        this.unixtimestring = unixtimestring;
        this.timeagodate = timeagodate;
        this.status_text = status_text;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSource_type() {
        return source_type;
    }

    public void setSource_type(String source_type) {
        this.source_type = source_type;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getUnixtimestring() {
        return unixtimestring;
    }

    public void setUnixtimestring(String unixtimestring) {
        this.unixtimestring = unixtimestring;
    }

    public String getTimeagodate() {
        return timeagodate;
    }

    public void setTimeagodate(String timeagodate) {
        this.timeagodate = timeagodate;
    }

    public String getStatus_text() {
        return status_text;
    }

    public void setStatus_text(String status_text) {
        this.status_text = status_text;
    }
}
