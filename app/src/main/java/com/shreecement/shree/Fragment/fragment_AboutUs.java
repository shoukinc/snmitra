package com.shreecement.shree.Fragment;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

import static com.shreecement.shree.Utlility.Constant.PREF_DESCRIPTION;
import static com.shreecement.shree.Utlility.Constant.PREF_FACEBOOk;
import static com.shreecement.shree.Utlility.Constant.PREF_TWITTER;
import static com.shreecement.shree.Utlility.Constant.PREF_YOUTUBE;


/**
 * Created by Choudhary on 6/29/2017.
 */

public class fragment_AboutUs extends Fragment {
    String strDiscription = "";
    String strFacebook = "";
    String strTwitter = "";
    String strYoutube = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Title of page
        getActivity().setTitle(getActivity().getString(R.string.nav_item_about_us));

        strDiscription = StorePrefs.getDefaults(PREF_DESCRIPTION, getActivity());
        strFacebook = StorePrefs.getDefaults(PREF_FACEBOOk, getActivity());
        strTwitter = StorePrefs.getDefaults(PREF_TWITTER, getActivity());
        strYoutube = StorePrefs.getDefaults(PREF_YOUTUBE, getActivity());

        //Toast.makeText(getActivity(),strDiscription,Toast.LENGTH_SHORT).show();
        Log.e("FB = > ", StorePrefs.getDefaults(PREF_FACEBOOk, getActivity()));
        Log.e("Twitter = > ", StorePrefs.getDefaults(PREF_TWITTER, getActivity()));
        Log.e("Youtube = > ", StorePrefs.getDefaults(PREF_YOUTUBE, getActivity()));


        Element googlePlus = new Element();
        googlePlus.setTitle("Follow us on Google Plus");

        String url = "https://plus.google.com/u/0/101019839616154889886/posts";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        googlePlus.setIntent(i);
        googlePlus.setIconDrawable(R.drawable.google_plus);

        Element linkedIn = new Element();
        linkedIn.setTitle("Follow us on LinkedIn");

        String url1 = "https://www.linkedin.com/company/shree-cement-ltd";
        Intent j = new Intent(Intent.ACTION_VIEW);
        j.setData(Uri.parse(url1));
        linkedIn.setIntent(j);
        linkedIn.setIconDrawable(R.drawable.ic_linkedin);

    /*    return new AboutPage(getContext())
                .isRTL(false)
                .setDescription(strDiscription)
                *//*.setDescription("Shree is a rapidly growing and one of the most efficient and" +
                        " environment friendly Companies in India. Currently, its manufacturing operations are spread" +
                        " over North and East India across six states. \n Company's high corporate governance and" +
                        " social performance together with consistent financial performance makes it a truly sustainable" +
                        " Company.")*//*
                .setImage(R.drawable.ic_shree_jung_rodhak)
                .addGroup("Connect with us")
                .addWebsite("http://www.shreecement.com/")
                .addFacebook("shreecement")
                .addTwitter("shreecementltd")
                .addItem(googlePlus)
                .addItem(linkedIn)
                .addYoutube("UCvLnAQ7R1F2LmmuEfyEZ8bA")
                .addPlayStore("com.shreecement.employee")
                .create();*/

        return new AboutPage(getContext())
                .isRTL(false)
                .setDescription(strDiscription)
                /*.setDescription("Shree is a rapidly growing and one of the most efficient and" +
                        " environment friendly Companies in India. Currently, its manufacturing operations are spread" +
                        " over North and East India across six states. \n Company's high corporate governance and" +
                        " social performance together with consistent financial performance makes it a truly sustainable" +
                        " Company.")*/
                .setImage(R.drawable.shreelogo_without_background)
                .addGroup("Connect with us")
                .addWebsite("http://www.shreecement.com/")
                .addFacebook(strFacebook)
                .addTwitter(strTwitter)
                .addItem(googlePlus)
                .addItem(linkedIn)
                .addYoutube(strYoutube)
                .addPlayStore("com.shreecement.employee")
                .create();

    }

}
