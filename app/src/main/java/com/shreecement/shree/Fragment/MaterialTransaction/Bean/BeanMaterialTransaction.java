package com.shreecement.shree.Fragment.MaterialTransaction.Bean;

/*
 * Created by digvijay on 3/16/18.
 */

public class BeanMaterialTransaction {

    private String DELIVERY_ID, EGP_NO, EGP_DATE, PRODUCT, INVENTORY_ITEM_ID, SOURCE_ORGANIZATION_ID,
            DESTINATION_ORGANIZATION_ID, SOURCE_ORGANIZATION, DESTINATION_ORGANIZATION, RECEIPT_DATE,
            ACCEPT_QTY, SHORT_QTY, CUT_QTY, DAMAGE_QTY, TRUCK_NO, CREATED_BY, LAST_UPDATED_BY,
            LAST_UPDATE_LOGIN, CREATION_DATE, LAST_UPDATE_DATE;

    public BeanMaterialTransaction(String DELIVERY_ID, String EGP_NO, String EGP_DATE, String PRODUCT,
                                   String INVENTORY_ITEM_ID, String SOURCE_ORGANIZATION_ID,
                                   String DESTINATION_ORGANIZATION_ID, String SOURCE_ORGANIZATION,
                                   String DESTINATION_ORGANIZATION, String RECEIPT_DATE, String ACCEPT_QTY,
                                   String SHORT_QTY, String CUT_QTY, String DAMAGE_QTY, String TRUCK_NO,
                                   String CREATED_BY, String LAST_UPDATED_BY, String LAST_UPDATE_LOGIN,
                                   String CREATION_DATE, String LAST_UPDATE_DATE) {
        this.DELIVERY_ID = DELIVERY_ID;
        this.EGP_NO = EGP_NO;
        this.EGP_DATE = EGP_DATE;
        this.PRODUCT = PRODUCT;
        this.INVENTORY_ITEM_ID = INVENTORY_ITEM_ID;
        this.SOURCE_ORGANIZATION_ID = SOURCE_ORGANIZATION_ID;
        this.DESTINATION_ORGANIZATION_ID = DESTINATION_ORGANIZATION_ID;
        this.SOURCE_ORGANIZATION = SOURCE_ORGANIZATION;
        this.DESTINATION_ORGANIZATION = DESTINATION_ORGANIZATION;
        this.RECEIPT_DATE = RECEIPT_DATE;
        this.ACCEPT_QTY = ACCEPT_QTY;
        this.SHORT_QTY = SHORT_QTY;
        this.CUT_QTY = CUT_QTY;
        this.DAMAGE_QTY = DAMAGE_QTY;
        this.TRUCK_NO = TRUCK_NO;
        this.CREATED_BY = CREATED_BY;
        this.LAST_UPDATED_BY = LAST_UPDATED_BY;
        this.LAST_UPDATE_LOGIN = LAST_UPDATE_LOGIN;
        this.CREATION_DATE = CREATION_DATE;
        this.LAST_UPDATE_DATE = LAST_UPDATE_DATE;
    }

    public String getDELIVERY_ID() {
        return DELIVERY_ID;
    }

    public void setDELIVERY_ID(String DELIVERY_ID) {
        this.DELIVERY_ID = DELIVERY_ID;
    }

    public String getEGP_NO() {
        return EGP_NO;
    }

    public void setEGP_NO(String EGP_NO) {
        this.EGP_NO = EGP_NO;
    }

    public String getEGP_DATE() {
        return EGP_DATE;
    }

    public void setEGP_DATE(String EGP_DATE) {
        this.EGP_DATE = EGP_DATE;
    }

    public String getPRODUCT() {
        return PRODUCT;
    }

    public void setPRODUCT(String PRODUCT) {
        this.PRODUCT = PRODUCT;
    }

    public String getINVENTORY_ITEM_ID() {
        return INVENTORY_ITEM_ID;
    }

    public void setINVENTORY_ITEM_ID(String INVENTORY_ITEM_ID) {
        this.INVENTORY_ITEM_ID = INVENTORY_ITEM_ID;
    }

    public String getSOURCE_ORGANIZATION_ID() {
        return SOURCE_ORGANIZATION_ID;
    }

    public void setSOURCE_ORGANIZATION_ID(String SOURCE_ORGANIZATION_ID) {
        this.SOURCE_ORGANIZATION_ID = SOURCE_ORGANIZATION_ID;
    }

    public String getDESTINATION_ORGANIZATION_ID() {
        return DESTINATION_ORGANIZATION_ID;
    }

    public void setDESTINATION_ORGANIZATION_ID(String DESTINATION_ORGANIZATION_ID) {
        this.DESTINATION_ORGANIZATION_ID = DESTINATION_ORGANIZATION_ID;
    }

    public String getSOURCE_ORGANIZATION() {
        return SOURCE_ORGANIZATION;
    }

    public void setSOURCE_ORGANIZATION(String SOURCE_ORGANIZATION) {
        this.SOURCE_ORGANIZATION = SOURCE_ORGANIZATION;
    }

    public String getDESTINATION_ORGANIZATION() {
        return DESTINATION_ORGANIZATION;
    }

    public void setDESTINATION_ORGANIZATION(String DESTINATION_ORGANIZATION) {
        this.DESTINATION_ORGANIZATION = DESTINATION_ORGANIZATION;
    }

    public String getRECEIPT_DATE() {
        return RECEIPT_DATE;
    }

    public void setRECEIPT_DATE(String RECEIPT_DATE) {
        this.RECEIPT_DATE = RECEIPT_DATE;
    }

    public String getACCEPT_QTY() {
        return ACCEPT_QTY;
    }

    public void setACCEPT_QTY(String ACCEPT_QTY) {
        this.ACCEPT_QTY = ACCEPT_QTY;
    }

    public String getSHORT_QTY() {
        return SHORT_QTY;
    }

    public void setSHORT_QTY(String SHORT_QTY) {
        this.SHORT_QTY = SHORT_QTY;
    }

    public String getCUT_QTY() {
        return CUT_QTY;
    }

    public void setCUT_QTY(String CUT_QTY) {
        this.CUT_QTY = CUT_QTY;
    }

    public String getDAMAGE_QTY() {
        return DAMAGE_QTY;
    }

    public void setDAMAGE_QTY(String DAMAGE_QTY) {
        this.DAMAGE_QTY = DAMAGE_QTY;
    }

    public String getTRUCK_NO() {
        return TRUCK_NO;
    }

    public void setTRUCK_NO(String TRUCK_NO) {
        this.TRUCK_NO = TRUCK_NO;
    }

    public String getCREATED_BY() {
        return CREATED_BY;
    }

    public void setCREATED_BY(String CREATED_BY) {
        this.CREATED_BY = CREATED_BY;
    }

    public String getLAST_UPDATED_BY() {
        return LAST_UPDATED_BY;
    }

    public void setLAST_UPDATED_BY(String LAST_UPDATED_BY) {
        this.LAST_UPDATED_BY = LAST_UPDATED_BY;
    }

    public String getLAST_UPDATE_LOGIN() {
        return LAST_UPDATE_LOGIN;
    }

    public void setLAST_UPDATE_LOGIN(String LAST_UPDATE_LOGIN) {
        this.LAST_UPDATE_LOGIN = LAST_UPDATE_LOGIN;
    }

    public String getCREATION_DATE() {
        return CREATION_DATE;
    }

    public void setCREATION_DATE(String CREATION_DATE) {
        this.CREATION_DATE = CREATION_DATE;
    }

    public String getLAST_UPDATE_DATE() {
        return LAST_UPDATE_DATE;
    }

    public void setLAST_UPDATE_DATE(String LAST_UPDATE_DATE) {
        this.LAST_UPDATE_DATE = LAST_UPDATE_DATE;
    }
}
