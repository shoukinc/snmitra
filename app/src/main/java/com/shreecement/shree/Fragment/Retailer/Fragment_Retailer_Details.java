package com.shreecement.shree.Fragment.Retailer;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Fragment.Retailer.Bean.BeanRetailer_Item;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;


/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Retailer_Details extends Fragment {

    TextView tvRetailerNumber,tvRetailerCity,tvRetailerName,tvRetailerState,tvRetailerAddress;
    BeanRetailer_Item beanRetailer_item;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_retailer_details,container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        tvRetailerNumber = view.findViewById(R.id.tvRetailerNumber);
        tvRetailerCity = view.findViewById(R.id.tvRetailerCity);
        tvRetailerName = view.findViewById(R.id.tvRetailerName);
        tvRetailerState = view.findViewById(R.id.tvRetailerState);
        tvRetailerAddress = view.findViewById(R.id.tvRetailerAddress);



        Gson gson = new Gson();
        String json = StorePrefs.getDefaults("beanRetailer_item", getActivity());
        beanRetailer_item = gson.fromJson(json, BeanRetailer_Item.class);


        tvRetailerNumber.setText( beanRetailer_item.getCust_account_id());
        tvRetailerName.setText(beanRetailer_item.getParty_name());
        tvRetailerCity.setText(beanRetailer_item.getCity());
        tvRetailerState.setText(beanRetailer_item.getState());
        tvRetailerAddress.setText(beanRetailer_item.getAddress());


         return view;
    }


}
