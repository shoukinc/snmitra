package com.shreecement.shree.Fragment.RelailerLocater;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Fragment.Referral.Bean.BeanReferral;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.shreecement.shree.Utlility.Constant.Dealerslocators;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Dealer_retailer_Locater_list extends Fragment {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    DealerRetailerLocator_Adapter referralAdapter;
    ArrayList<BeanReferral> ReferralList;
    FloatingActionButton btnFabAddBtn;

    String keyword = "";
    int page = 1;

    String state ="";
    String district ="";
    String taluka ="";
    String city ="";
    String dealerOrRetailer ="";
    TextView tv_no_record_found;
    TextView tv_count_item;

    int total_record = 0;
    int recordCount = 0;
    boolean isPagination = false;

    public static Fragment_Dealer_retailer_Locater_list newInstance(String text) {

        Fragment_Dealer_retailer_Locater_list f = new Fragment_Dealer_retailer_Locater_list();
        Bundle b = new Bundle();
        b.putString("edit", text);

        f.setArguments(b);
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_common_list, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //setHasOptionsMenu(true);

        tv_no_record_found = view.findViewById(R.id.tv_no_record_found);
        tv_count_item = view.findViewById(R.id.tv_count_item);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        btnFabAddBtn = view.findViewById(R.id.btnFabAddBtn);
        btnFabAddBtn.setVisibility(View.GONE);

        getActivity().setTitle(getActivity().getString(R.string.delear_or_retailer_locator));
        keyword = "";

        ReferralList = new ArrayList<>();
        referralAdapter = new DealerRetailerLocator_Adapter(getActivity(), ReferralList);
        recyclerView.setAdapter(referralAdapter);
        referralAdapter.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();

       // new PostClassReferralList(getActivity(), String.valueOf(++page), state, district, taluka, city,dealerOrRetailer, true).execute();


/*
        btnFabAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);
                fragmentTransaction.replace(R.id.container_body, Fragment_Referral.newInstance("add")).addToBackStack(null);
                fragmentTransaction.commit();

                ((MainActivity) getActivity()).showUpButton(true);

            }
        });*/

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    loadMoreItems();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            public void onRefresh() {
                refreshItems();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
             //   new PostClassReferralList(getActivity(), "1", keyword, true).execute();

               new PostClassReferralList(getActivity(), String.valueOf(+page), "",state, district, taluka, city,dealerOrRetailer, true).execute();

            }
        }, 450);

    /*    RealmResults<BeanOrderList> results = realm.where(BeanOrderList.class).findAll();

        if (results.size() > 0)
        {
            ReferralList.addAll(results);
        }
        else {

        }*/


        return view;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu, menu);


        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                ReferralList.clear();
                page = 1;
                keyword = "";
              //  new PostClassReferralList(getActivity(), "1", "", true).execute();


                return false;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                keyword = query;
                ReferralList.clear();
              //  new PostClassReferralList(getActivity(), "", keyword, true).execute();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
             /*   if (newText.length()==0){
                    keyword = "0";
                    new PostClassReferralList(getActivity(), "1", keyword,true).execute();
                }*/

                return false;
            }
        });
    }


    private void loadMoreItems() {
        isPagination = true;

       // new PostClassReferralList(getActivity(), String.valueOf(++page), keyword, true).execute();
        new PostClassReferralList(getActivity(), String.valueOf(++page), "",state, district, taluka, city,dealerOrRetailer, true).execute();


    }

    private void refreshItems() {
        page = 1;
        ReferralList.clear();
        swipeRefreshLayout.setRefreshing(false);
        // Load items
        // ...
      //  new PostClassReferralList(getActivity(), String.valueOf(page), keyword, true).execute();
        new PostClassReferralList(getActivity(), String.valueOf(page), "", state, district, taluka, city,dealerOrRetailer, true).execute();

    }

    class PostClassReferralList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        String Keyword;
        boolean showLoader;
        String district;
        String taluka;
        String city;
        String user_type,state;

        PostClassReferralList(Context c, String pageNo, String keyword,String state, String district, String taluka, String city, String user_type,  boolean showLoader) {
            this.context = c;
            this.pageNo = pageNo;
            this.Keyword = keyword;
            this.state = state;
            this.district= district;
            this.taluka= taluka;
            this.city= city;
            this.user_type= user_type;
            this.showLoader = showLoader;

            Log.d("-----Keyword------", "" + Keyword);
            Log.d("-----state------", "" + state);
            Log.d("-----district------", "" + district);
            Log.d("-----taluka------", "" + taluka);
            Log.d("-----city------", "" + city);
            Log.d("-----user_type------", "" + user_type);
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("page", pageNo);
                    json.put("keyword", Keyword);
                    json.put("state", state);
                    json.put("district", district);
                    json.put("taluka", taluka);
                    json.put("city", city);
                    json.put("user_type", user_type);


                    RequestBody body = RequestBody.create(mediaType, json.toString());
                    System.out.println("Paremters===============" + json.toString());

                    Request request = new Request.Builder()
                            .url(Dealerslocators)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                          //  .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Dealerslocators=========" + Dealerslocators);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");


                            total_record = Integer.parseInt(jObject.getString("total_record"));
                            recordCount = Integer.parseInt(jObject.getString("recordCount"));
                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {

                                if(jsonArraydata.length() == 0) {
                                    --page;
                                }

                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    ReferralList.add(new BeanReferral(
                                            jsonObject.getString("party_id"),
                                            jsonObject));
                                }

                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                page--;
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


                referralAdapter.notifyDataSetChanged();

            } else {
                page--;
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            }
            referralAdapter.notifyDataSetChanged();

            recordCount = ReferralList.size();
            if (recordCount==0){
                tv_no_record_found.setVisibility(View.VISIBLE);

                tv_count_item.setText("Showing  "+recordCount+"   of  "+total_record);
            }else {
                tv_no_record_found.setVisibility(View.GONE);
                tv_count_item.setText("Showing  "+recordCount+"   of  "+total_record);
            }


        }
    }

}
