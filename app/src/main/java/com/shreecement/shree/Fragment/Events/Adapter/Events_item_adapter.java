package com.shreecement.shree.Fragment.Events.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Events.BeanEvents.BeanEvents;
import com.shreecement.shree.Fragment.Events.Fragment_Events_Details;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import java.util.ArrayList;


/**
 * Created by TIA on 12-08-2016.
 */

public class Events_item_adapter extends RecyclerView.Adapter<Events_item_adapter.ViewHolder> {
    Context context;
    ArrayList<BeanEvents> rowItems;

    public Events_item_adapter(Context context, ArrayList<BeanEvents> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_event_row_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String rowItem = rowItems.get(position).getTitle();

        holder.tvEventName.setText(rowItems.get(position).getTitle());
        holder.tvEventDescription.setText(rowItems.get(position).getShort_description());
        holder.tvEventVilidity.setText(rowItems.get(position).getStart_date());

        String s=rowItem.substring(0,1).toUpperCase();
        holder.tvFirst.setText(s);


        holder.tvFirst.setBackgroundResource(R.drawable.rounder_date_red_image);



    }

    @Override
    public int getItemCount() {
        return rowItems.size();

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvEventName,tvEventDescription,tvEventVilidity,tvEventLink;
        TextView tvFirst;
        ViewHolder(View convertView) {
            super(convertView);
            tvEventName = convertView.findViewById(R.id.tvEventName);
            tvEventDescription = convertView.findViewById(R.id.tvEventDescription);
            tvEventVilidity = convertView.findViewById(R.id.tvEventVilidity);
            tvEventLink = convertView.findViewById(R.id.tvEventLink);
            tvFirst = convertView.findViewById(R.id.tvFirst);
            convertView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {


            StorePrefs.setDefaults("event_id",rowItems.get(getPosition()).getEvent_id(),context);
            BeanEvents beanEvents = rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanEvents);
            StorePrefs.setDefaults("beanEvents", json, context);
            Fragment_Events_Details fragment = new Fragment_Events_Details();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            ((MainActivity) context).showUpButton(true);

            ((MainActivity) context).showUpButton(true);

        }
    }


}