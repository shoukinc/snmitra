package com.shreecement.shree.Fragment.Events.BeanEvents;

public class BeanEvents {

    String event_id,event_number,event_type_id,location_id,long_description,short_description,start_date,title;
    String event_type;

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public BeanEvents() {
    }

    public BeanEvents(String event_id, String event_number, String event_type_id, String location_id, String long_description, String short_description, String start_date, String title) {
        this.event_id = event_id;
        this.event_number = event_number;
        this.event_type_id = event_type_id;
        this.location_id = location_id;
        this.long_description = long_description;
        this.short_description = short_description;
        this.start_date = start_date;
        this.title = title;
    }

    public BeanEvents(String event_id, String event_number,String event_type, String long_description, String short_description, String start_date, String title) {
        this.event_id = event_id;
        this.event_number = event_number;
        this.event_type = event_type;
        this.long_description = long_description;
        this.short_description = short_description;
        this.start_date = start_date;
        this.title = title;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getEvent_number() {
        return event_number;
    }

    public void setEvent_number(String event_number) {
        this.event_number = event_number;
    }

    public String getEvent_type_id() {
        return event_type_id;
    }

    public void setEvent_type_id(String event_type_id) {
        this.event_type_id = event_type_id;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getLong_description() {
        return long_description;
    }

    public void setLong_description(String long_description) {
        this.long_description = long_description;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
