package com.shreecement.shree.Fragment.Query;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Common.Fragment_Common_Query_Feedback_list;
import com.shreecement.shree.Fragment.FeedBack.Spin_Feedback_adapter;
import com.shreecement.shree.ModalClass.BeanCustomer_Item;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.ACTION_ADD;
import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.Create_Query;
import static com.shreecement.shree.Utlility.Constant.Lookup_data;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.MODULE_QUERYS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;


/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Query extends Fragment {

    Spinner spin_query;
    Spin_Feedback_adapter spin_feedback_adapter;

    List<LookupCategory>listFeedbak;
    EditText edComment;
    TextView tvTitle;
    Button btnSave;
    String message="";

    String QueryCode="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_query,container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setHasOptionsMenu(true);
        spin_query =   view.findViewById(R.id.spin_query);
        tvTitle = view.findViewById(R.id.tvTitle);
        edComment=view.findViewById(R.id.edComment);
        btnSave=view.findViewById(R.id.btnSave);
        tvTitle.setText(R.string.query);

        listFeedbak=new ArrayList<>();
        spin_feedback_adapter = new Spin_Feedback_adapter(getActivity(), listFeedbak );
        spin_query.setAdapter(spin_feedback_adapter);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message=edComment.getText().toString();
                if (message.length()==0){

                    Toast.makeText(getActivity(),"Please Add Message",Toast.LENGTH_LONG).show();
                }else {
                    new PostClass_CreateQuery(getActivity(),message).execute();
                }
            }
        });
        spin_query.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                QueryCode=listFeedbak.get(i).getCODE();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        new  PostClass_LookUpData(getActivity()).execute();
        getActivity().setTitle(getActivity().getResources().getString(R.string.query));

        return view;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_common, menu);

        MenuItem item = menu.findItem(R.id.action_button_basket);
        item.setIcon(R.drawable.ic_list);
        View view1 = MenuItemCompat.getActionView(item);
        final Bundle bundle=new Bundle();
        bundle.putString("frag","query");
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Fragment fragment=new  Fragment_Common_Query_Feedback_list();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.addToBackStack(null);
                fragment.setArguments(bundle);

                fragmentTransaction.replace(R.id.container_body, fragment).commit();
                ((MainActivity) getActivity()).showUpButton(true);


                return false;
            }
        });


    }
    class PostClass_LookUpData extends AsyncTask<String, Void, String> {

        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;


        boolean showLoader;
        List<BeanCustomer_Item> tempcustomerlist;

        PostClass_LookUpData(Context c) {
            this.context = c;



        }

        protected void onPreExecute() {
            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            listFeedbak = new ArrayList<>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();


                    RequestBody body = RequestBody.create(null, new byte[]{});

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Lookup_data=========" + Lookup_data);

                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONObject jsonObjectData=jObject.getJSONObject("data");
                            JSONArray jsonArraycategory = jsonObjectData.getJSONArray("query_type");
                            System.out.println("======jsonArraycategory=========" + jsonArraycategory);
                            if (jsonArraycategory != null) {
                                JSONObject jsonObject;
                                for (int i = 0; i < jsonArraycategory.length(); i++) {
                                    jsonObject = jsonArraycategory.getJSONObject(i);
                                    System.out.println("======jsonObject=========" + jsonObject);

                                    listFeedbak.add(new LookupCategory(
                                            jsonObject.getString("lookup_type"),
                                            jsonObject.getString("code"),
                                            jsonObject.getString("description")
                                    ));

                                }

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {

                spin_feedback_adapter = new  Spin_Feedback_adapter(getActivity(), listFeedbak);
                spin_query.setAdapter(spin_feedback_adapter);

            } else {


            }
            spin_feedback_adapter.notifyDataSetChanged();
        }

    }


    ////////////--------------------- Create FeedBack -------------------////////////////////////////
    class PostClass_CreateQuery extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String Message="";

        PostClass_CreateQuery(Context c, String message) {
            this.context = c;
            this.Message = message;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {
            System.out.println("Message===============" + Message);

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    // json.put("page_module", MODULE_RATINGS);

                    json.put("query", Message);
                    json.put("title", QueryCode);


                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Create_Query)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("action", ACTION_ADD)
                            .addHeader("page_module", MODULE_QUERYS)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Create_QueryUrl=========" + Create_Query);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        return e.toString();
                    }

                } catch (IOException e) {

                    e.printStackTrace();
                    return e.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                    return e.toString();
                }
            } else {
                return "Please Check Internet Connection";
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {


                edComment.setText("");
                message="";


                final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(
                        context);
                alert.setTitle(jsonreplyMsg);
                alert.setCancelable(false);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alert.show();

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            }
        }
    }
}


