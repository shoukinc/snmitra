package com.shreecement.shree.Fragment.Mitra.Bean;

public class BeanMitra {

    String mitra_id,mitra_number,full_name,email_address,mobile_number,adhaar_number,city,state;

    public BeanMitra() {
    }

    public BeanMitra(String mitra_id, String mitra_number, String full_name, String email_address, String mobile_number, String adhaar_number, String city, String state) {
        this.mitra_id = mitra_id;
        this.mitra_number = mitra_number;
        this.full_name = full_name;
        this.email_address = email_address;
        this.mobile_number = mobile_number;
        this.adhaar_number = adhaar_number;
        this.city = city;
        this.state = state;
    }

    public String getMitra_id() {
        return mitra_id;
    }

    public void setMitra_id(String mitra_id) {
        this.mitra_id = mitra_id;
    }

    public String getMitra_number() {
        return mitra_number;
    }

    public void setMitra_number(String mitra_number) {
        this.mitra_number = mitra_number;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getAdhaar_number() {
        return adhaar_number;
    }

    public void setAdhaar_number(String adhaar_number) {
        this.adhaar_number = adhaar_number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
