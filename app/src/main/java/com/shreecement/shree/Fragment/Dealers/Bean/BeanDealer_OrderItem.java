package com.shreecement.shree.Fragment.Dealers.Bean;

/**
 * Created by kamesh on 3/17/18.
 */

public class BeanDealer_OrderItem {
    private String order_date,
            customer_name, consignee_name, mm_name, brand_name,
            category_name, packing_type_name, product_name, order_id, category,
            customer_code, product, brand, packingtype, bagtype, quantity, consignee,
            state, district, taluka, city, rate, dispatch_from, validity, address,
            employee_code, transfer_flag, remark, last_updated_by, last_update_login,
            last_update_date, order_date_new;


    public BeanDealer_OrderItem(String order_date, String customer_name, String consignee_name,
                                  String mm_name, String brand_name, String category_name, String
                                          packing_type_name, String product_name, String order_id,
                                  String category, String customer_code, String product, String brand,
                                  String packingtype, String bagtype, String quantity, String consignee, String state, String district, String taluka, String city, String rate, String dispatch_from, String validity, String address, String employee_code, String transfer_flag, String remark, String last_updated_by,
                                  String last_update_login, String last_update_date, String order_date_new) {
        this.order_date = order_date;
        this.customer_name = customer_name;
        this.consignee_name = consignee_name;
        this.mm_name = mm_name;
        this.brand_name = brand_name;
        this.category_name = category_name;
        this.packing_type_name = packing_type_name;
        this.product_name = product_name;
        this.order_id = order_id;
        this.category = category;
        this.customer_code = customer_code;
        this.product = product;
        this.brand = brand;
        this.packingtype = packingtype;
        this.bagtype = bagtype;
        this.quantity = quantity;
        this.consignee = consignee;
        this.state = state;
        this.district = district;
        this.taluka = taluka;
        this.city = city;
        this.rate = rate;
        this.dispatch_from = dispatch_from;
        this.validity = validity;
        this.address = address;
        this.employee_code = employee_code;
        this.transfer_flag = transfer_flag;
        this.remark = remark;
        this.last_updated_by = last_updated_by;
        this.last_update_login = last_update_login;
        this.last_update_date = last_update_date;
        this.order_date_new = order_date_new;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getConsignee_name() {
        return consignee_name;
    }

    public void setConsignee_name(String consignee_name) {
        this.consignee_name = consignee_name;
    }

    public String getMm_name() {
        return mm_name;
    }

    public void setMm_name(String mm_name) {
        this.mm_name = mm_name;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getPacking_type_name() {
        return packing_type_name;
    }

    public void setPacking_type_name(String packing_type_name) {
        this.packing_type_name = packing_type_name;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPackingtype() {
        return packingtype;
    }

    public void setPackingtype(String packingtype) {
        this.packingtype = packingtype;
    }

    public String getBagtype() {
        return bagtype;
    }

    public void setBagtype(String bagtype) {
        this.bagtype = bagtype;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getTaluka() {
        return taluka;
    }

    public void setTaluka(String taluka) {
        this.taluka = taluka;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDispatch_from() {
        return dispatch_from;
    }

    public void setDispatch_from(String dispatch_from) {
        this.dispatch_from = dispatch_from;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmployee_code() {
        return employee_code;
    }

    public void setEmployee_code(String employee_code) {
        this.employee_code = employee_code;
    }

    public String getTransfer_flag() {
        return transfer_flag;
    }

    public void setTransfer_flag(String transfer_flag) {
        this.transfer_flag = transfer_flag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLast_updated_by() {
        return last_updated_by;
    }

    public void setLast_updated_by(String last_updated_by) {
        this.last_updated_by = last_updated_by;
    }

    public String getLast_update_login() {
        return last_update_login;
    }

    public void setLast_update_login(String last_update_login) {
        this.last_update_login = last_update_login;
    }

    public String getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(String last_update_date) {
        this.last_update_date = last_update_date;
    }

    public String getOrder_date_new() {
        return order_date_new;
    }

    public void setOrder_date_new(String order_date_new) {
        this.order_date_new = order_date_new;
    }
}
