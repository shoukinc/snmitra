package com.shreecement.shree.Fragment.Retailer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Fragment.Retailer.Adapter.Retailers_Adapter;
import com.shreecement.shree.Fragment.Retailer.Bean.BeanRetailer_Item;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.MODULE_RETAILERS;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.retailersUrl;

/*
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Retailers extends Fragment {
    RecyclerView recyclerView;
    Retailers_Adapter retailers_adapter;

    ArrayList<BeanRetailer_Item> retailerList;
    String keyword = "";
    int page = 1;
    SwipeRefreshLayout swipeRefreshLayout;
    boolean isPagination = false;

    TextView tv_no_record_found;
    TextView tv_count_item;

    int total_record = 0;
    int recordCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_retailers, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        recyclerView = view.findViewById(R.id.recyclerview);
        tv_no_record_found = view.findViewById(R.id.tv_no_record_found);
        tv_count_item = view.findViewById(R.id.tv_count_item);

        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        setHasOptionsMenu(true);

        retailerList = new ArrayList<>();
        keyword = "";

        retailers_adapter = new Retailers_Adapter(getActivity(), retailerList);
        recyclerView.setAdapter(retailers_adapter);
        retailers_adapter.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();

        getActivity().setTitle(getActivity().getString(R.string.nav_item_retailers));

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    loadMoreItems();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new PostClass_Retailer(getActivity(), "1", keyword, true).execute();
            }
        }, 425);


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        // super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.clearFocus();

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                retailerList.clear();
                page = 1;
                keyword = "";
                new PostClass_Retailer(getActivity(), "1", "", true).execute();

                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                keyword = query;
                retailerList.clear();

                new PostClass_Retailer(getActivity(), "", keyword, true).execute();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

              /*  if (newText.length() == 0){
                    keyword = newText;
                    new PostClass_Retailer(getActivity(), "1",keyword, true).execute();

                }*/
                return false;
            }

        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                keyword="";
                new PostClass_Retailer(getActivity(), "", keyword, true).execute();


                return false;
            }
        });


    }


    private void loadMoreItems() {
        isPagination = true;

        new PostClass_Retailer(getActivity(), String.valueOf(++page), keyword, true).execute();
    }

    private void refreshItems() {
        page = 1;
        retailerList.clear();
        swipeRefreshLayout.setRefreshing(false);
        // Load items
        // ...
        new PostClass_Retailer(getActivity(), String.valueOf(page), keyword, true).execute();


    }

    class PostClass_Retailer extends AsyncTask<String, Void, String> {

        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;

        String pageNo;
        String Keyword;
        boolean showLoader;
        List<BeanRetailer_Item> tempretailerlist;

        PostClass_Retailer(Context c, String pageNo, String keyword, boolean showLoader) {
            this.context = c;
            this.pageNo = pageNo;
            this.Keyword = keyword;
            this.showLoader = showLoader;
            Log.d("====keyword=====", "" + Keyword);
        }

        protected void onPreExecute() {
            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempretailerlist = new ArrayList<>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("keyword", Keyword);
                    json.put("page", pageNo);
                    json.put("user_type", StorePrefs.getDefaults(PREF_USER_TYPE, context));
                    //json.put("page_module", MODULE_RETAILERS);

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(retailersUrl)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_RETAILERS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))

                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======retailersUrl=========" + retailersUrl);

                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");


                            total_record = Integer.parseInt(jObject.getString("total_record"));
                            recordCount = Integer.parseInt(jObject.getString("recordCount"));
                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            System.out.println("======jsonArraydata=========" + jsonArraydata);
                            if (jsonArraydata != null) {
                                JSONObject jsonObject;


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    jsonObject = jsonArraydata.getJSONObject(i);
                                    System.out.println("======jsonObject=========" + jsonObject);

                                    tempretailerlist.add(new BeanRetailer_Item(
                                            jsonObject.getString("party_id"),
                                            jsonObject.getString("party_name"),
                                            jsonObject.getString("cust_account_id"),
                                            jsonObject.getString("account_number"),
                                            jsonObject.getString("city"),
                                            jsonObject.getString("postal_code"),
                                            jsonObject.getString("state"),
                                            jsonObject.getString("province"),
                                            jsonObject.getString("country"),
                                            jsonObject.getString("address")));

                                }

                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                // Toast.makeText(context, jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


                if (!isPagination) {
                    retailerList.clear();
                    retailerList.addAll(tempretailerlist);
                    System.out.println("=!isPagination=====retailerList=========" + retailerList.size());
                } else {

                    if (tempretailerlist.size() > 0) {
                        retailerList.addAll(tempretailerlist);
                        System.out.println("======tempretailerlist=========" + tempretailerlist.size());
                        System.out.println("======retailerList=========" + retailerList.size());
                    } else {
                        Toast.makeText(context, "This is last page", Toast.LENGTH_SHORT).show();
                    }
                }


            } else {
                page--;
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }
            retailers_adapter.notifyDataSetChanged();

            recordCount = retailerList.size();
            if (recordCount==0){
                tv_no_record_found.setVisibility(View.VISIBLE);

                tv_count_item.setText("Showing  "+recordCount+"   of  "+total_record);
            }else {
                tv_no_record_found.setVisibility(View.GONE);
                tv_count_item.setText("Showing  "+recordCount+"   of  "+total_record);
            }
        }


    }

}
