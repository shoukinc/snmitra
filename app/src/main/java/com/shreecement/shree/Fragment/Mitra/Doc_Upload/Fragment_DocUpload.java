package com.shreecement.shree.Fragment.Mitra.Doc_Upload;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.shreecement.shree.Fragment.Fragment_ChangePassword;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.shreecement.shree.Utlility.Constant.PERMISSION_ALL;
import static com.shreecement.shree.Utlility.Constant.ProfileChangeUrl;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.baseUrl;
import static com.shreecement.shree.Utlility.Constant.hasPermissions;


/**
 * Created by Choudhary on 6/30/2017.
 */

public class Fragment_DocUpload extends BottomSheetDialogFragment {

    TextView tvName, tvPersonId, tvUserName, tvProEmail, tv_empId, tv_Address, tv_state, tv_city, tv_prof_fullname;
    FloatingActionButton fab;
    LinearLayout layoutEmp;
    ImageView ProImg, imgBackground;
    private static final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    private final int PICK_IMAGE = 12345;
    private final int TAKE_PICTURE = 6352;
    RelativeLayout layout_main;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doc_upload, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // FloatingActionButton..........
        fab = view.findViewById(R.id.fab);
        //TextView........................
        tvName = view.findViewById(R.id.tvName);
        tvUserName = view.findViewById(R.id.tvUserName);
        tvProEmail = view.findViewById(R.id.tvEmail);
        tvPersonId = view.findViewById(R.id.tvPersonId);
        tv_empId = view.findViewById(R.id.tvEmployeId);
        tv_city = view.findViewById(R.id.tvCITY);
        tv_state = view.findViewById(R.id.tvState);
        tv_Address = view.findViewById(R.id.tvAddress);
        layout_main = view.findViewById(R.id.layout_main);
        tv_prof_fullname = view.findViewById(R.id.tv_prof_fullname);
        layoutEmp = view.findViewById(R.id.layoutEmp);
        //ImageView......................
        ProImg = view.findViewById(R.id.prof_img);
        getActivity().setTitle(getActivity().getString(R.string.my_profile));
        setHasOptionsMenu(true);
        imgBackground = view.findViewById(R.id.img_background);

        //Set profile details....................

        // Fac Click Listener ...................
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,};
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                }
                //Dialog
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_gallery);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View view = inflater.inflate(R.layout.dialog_gallery, null);
                TextView tvCamera, tvGallery;
                tvCamera = (TextView) view.findViewById(R.id.tv_camera);
                tvGallery = (TextView) view.findViewById(R.id.tv_gallery);
                dialog.setContentView(view);

                tvCamera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivityForResult(takePicture, TAKE_PICTURE);
                        }
                        dialog.dismiss();
                    }
                });

                tvGallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                        dialog.dismiss();

                    }
                });
                dialog.show();
            }
        });

        // Upload Image set ...............
        tv_prof_fullname.setText(StorePrefs.getDefaults("NAME", getActivity()));
        tvName.setText(StorePrefs.getDefaults("NAME", getActivity()));
        tvUserName.setText(StorePrefs.getDefaults("USER_NAME", getActivity()));
        tvPersonId.setText(StorePrefs.getDefaults("PERSON_ID", getActivity()));
        tvProEmail.setText(StorePrefs.getDefaults("EMAIL", getActivity()));
        tv_empId.setText(StorePrefs.getDefaults("EMPLOYEE_ID", getActivity()));
        tv_city.setText(StorePrefs.getDefaults("CITY", getActivity()));
        tv_state.setText(StorePrefs.getDefaults("STATE", getActivity()));
        tv_Address.setText(StorePrefs.getDefaults("ADDRESS", getActivity()));

        if(!StorePrefs.getDefaults("USER_TYPE", getActivity()).equals("E")) {
            layoutEmp.setVisibility(View.GONE);
        }


        Log.d("=====StorePrefs======", "" + baseUrl + StorePrefs.getDefaults("IMAGE", getActivity()));



/*

       Picasso.with(getActivity()).load(StorePrefs.getDefaults("imageUrl",getActivity()))
                .transform(new BlurTransformation(getActivity()))
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        imgBackground.setBackground(new BitmapDrawable(getActivity().getResources(), bitmap));
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
*/


        Glide.with(getActivity()).load(baseUrl + StorePrefs.getDefaults("IMAGE", getActivity()))
                .asBitmap()
                .centerCrop()
                .into(new BitmapImageViewTarget(ProImg) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        ProImg.setImageDrawable(circularBitmapDrawable);
                    }
                });

        // Back Button CLick listener.................

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_cp, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem myActionMenuItem = menu.findItem(R.id.changePassword);

        myActionMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.changePassword:
                        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                                R.anim.enter_from_right, R.anim.exit_to_left);
                        fragmentTransaction.replace(R.id.container_body, new Fragment_ChangePassword()).commit();
                }

                return false;
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {

                try {
                    Bitmap bitmap;
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    File filesDir = getActivity().getFilesDir();
                    //create a file to write bitmap data
                    File file = new File(filesDir, "Presto_" + System.currentTimeMillis() + ".jpg");
                    file.createNewFile();
                    //Convert bitmap to byte array
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();
                    //write the bytes in file
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                    Log.d("-----------------", "" + file.getAbsolutePath());
                    new PostClassProfile_ImageUpdate(getActivity(), file.getAbsolutePath()).execute();


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == TAKE_PICTURE) {

            if (resultCode == Activity.RESULT_OK) {
                try {
                    // Log.d("camera","=====filepath========"+filepath);
                    Bundle extras = data.getExtras();
                    Bitmap bitmap = (Bitmap) extras.get("data");
                    File filesDir = getActivity().getFilesDir();
                    //create a file to write bitmap data
                    File file = new File(filesDir, "Shree_" + System.currentTimeMillis() + ".jpg");
                    file.createNewFile();
                    //Convert bitmap to byte array
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();
                    //write the bytes in file
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                    Log.d("-----------------", "" + file.getAbsolutePath());
                    new PostClassProfile_ImageUpdate(getActivity(), file.getAbsolutePath()).execute();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    //Check Permission ....................
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, TAKE_PICTURE);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    class PostClassProfile_ImageUpdate extends AsyncTask<String, Void, String> {
        Context context;
        String Id;
        String Service_id;
        ProgressDialog progress;
        JSONObject jObject;
        String Files;
        String jsonreplyMsg = "";


        public PostClassProfile_ImageUpdate(Context mcontext, String Files) {
            this.context = mcontext;
            this.Files = Files;
        }

        protected void onPreExecute() {
            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MultipartBuilder buildernew = new MultipartBuilder().type(MultipartBuilder.FORM);
                    String path = Files.replaceAll(" ", "%20");
                    File uploadFile = new File(path);
                    String name1 = uploadFile.getName();
                    System.out.println("=========FILE===============" + uploadFile);

                    RequestBody requestBody = null;
                    if (uploadFile.exists()) {
                        final MediaType MEDIA_TYPE_PNG = MediaType.parse("application/pdf");
                        requestBody = new MultipartBuilder()
                                .type(MultipartBuilder.FORM)
                                .addFormDataPart("FileInput", name1,
                                        RequestBody.create(MEDIA_TYPE_PNG, uploadFile))

                                .build();
                    }

                    Request request = new Request.Builder()
                            .url(ProfileChangeUrl)
                            .post(requestBody)
                            .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("Postman-Token", "2c126362-1839-818d-1b67-d0b1eb53719e")
                            .build();

                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    jObject = new JSONObject(reqBody);
                    try {


                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            String ImgUrl = jObject.getString("image");
                            StorePrefs.setDefaults("IMAGE", ImgUrl, context);
                            jsonreplyMsg = jObject.getString("replyMsg");

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                Toast.makeText(context, jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();
                Intent intent = getActivity().getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

}



