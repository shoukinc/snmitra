package com.shreecement.shree.Fragment.Referral.Bean;


import org.json.JSONObject;

public class BeanReferral {

   String referral_name, mobile_number, mitra_type_code, comments;


   String referral_id;

    JSONObject jsonObject;

    public BeanReferral() {
    }

    public BeanReferral(String referral_name, String mobile_number, String mitra_type_code, String comments) {
        this.referral_name = referral_name;
        this.mobile_number = mobile_number;
        this.mitra_type_code = mitra_type_code;
        this.comments = comments;
    }



    public BeanReferral(String referral_id, JSONObject jsonObject) {
        this.referral_id = referral_id;
        this.jsonObject = jsonObject;
    }

    public String getReferral_id() {
        return referral_id;
    }

    public void setReferral_id(String referral_id) {
        this.referral_id = referral_id;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getReferral_name() {
        return referral_name;
    }

    public void setReferral_name(String referral_name) {
        this.referral_name = referral_name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getMitra_type_code() {
        return mitra_type_code;
    }

    public void setMitra_type_code(String mitra_type_code) {
        this.mitra_type_code = mitra_type_code;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
