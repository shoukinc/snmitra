package com.shreecement.shree.Fragment.Tips;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Fragment.Tips.Bean.BeanTips_Item;
import com.shreecement.shree.Fragment.Tips.Test.URLImageParser;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;


/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Tips_Details extends Fragment  {

    TextView tvTipsTitle,tvTipsDetails;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_tips_details,container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        tvTipsTitle = view.findViewById(R.id.tvTipsTitle);
        tvTipsDetails = view.findViewById(R.id.tvTipsDetails);
        getActivity().setTitle(getActivity().getString(R.string.tips_details));


        Gson gson = new Gson();
        String json = StorePrefs.getDefaults("beanTips_item", getActivity());
        BeanTips_Item beanTips_item = gson.fromJson(json, BeanTips_Item.class);

        tvTipsTitle.setText( beanTips_item.getTitle());
       // tvTipsDetails.setText(beanTips_item.getLong_description());
        tvTipsDetails.setText(Html.fromHtml(beanTips_item.getLong_description(), new URLImageParser(tvTipsDetails, getActivity()), null));
        //String code =beanTips_item.getLong_description();




       /* Spanned spanned = Html.fromHtml(code, (ImageGetter) getActivity(),null);
        tvTipsDetails.setText(spanned);
        tvTipsDetails.setTextSize(16);*/
        //tvTipsDetails. setText(Html.fromHtml((beanTips_item.getLong_description())));



         return view;
    }


   /* @Override
    public Drawable getDrawable(String s) {
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = getResources().getDrawable(R.drawable.icn_happy);
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

        new LoadImage().execute(s, d);

        return d;
    }

    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            Log.d("Tag", "doInBackground " + source);
            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d("Tag", "onPostExecute drawable " + mDrawable);
            Log.d("Tag", "onPostExecute bitmap " + bitmap);
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                mDrawable.setLevel(1);
                // i don't know yet a better way to refresh TextView
                // mTv.invalidate() doesn't work as expected
                CharSequence t = tvTipsDetails.getText();
                tvTipsDetails.setText(t);
            }
        }
    }*/
}

