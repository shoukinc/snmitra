package com.shreecement.shree.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Adapter.OrderRequirement_Adapter;
import com.shreecement.shree.ModalClass.BeanOrderList;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.MODULE_ORDERS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.orders_listUrl;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Orders extends Fragment {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    OrderRequirement_Adapter orderRequirementAdapter;
    ArrayList<BeanOrderList> orderRequirementList;
    FloatingActionButton btnOorderRequirement;
    String keyword = "";
    int page = 1;

    TextView tv_no_record_found;
    TextView tv_count_item;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_requirement, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setHasOptionsMenu(true);
        tv_no_record_found = view.findViewById(R.id.tv_no_record_found);
        tv_count_item = view.findViewById(R.id.tv_count_item);

        btnOorderRequirement = view.findViewById(R.id.btnOorderRequirement);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        getActivity().setTitle(getActivity().getString(R.string.orders));
        keyword = "";

        orderRequirementList = new ArrayList<>();
        orderRequirementAdapter = new OrderRequirement_Adapter(getActivity(), orderRequirementList);
        recyclerView.setAdapter(orderRequirementAdapter);
        orderRequirementAdapter.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();


        btnOorderRequirement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);
                fragmentTransaction.replace(R.id.container_body, Fragment_Add_Order.newInstance("add")).addToBackStack(null);
                fragmentTransaction.commit();

                ((MainActivity) getActivity()).showUpButton(true);

            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    loadMoreItems();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            public void onRefresh() {
                refreshItems();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new PostClassOrderList(getActivity(), "1", keyword, true).execute();
            }
        }, 450);

    /*    RealmResults<BeanOrderList> results = realm.where(BeanOrderList.class).findAll();

        if (results.size() > 0)
        {
            orderRequirementList.addAll(results);
        }
        else {

        }*/


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu, menu);


        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                orderRequirementList.clear();
                page = 1;
                keyword = "";
                new PostClassOrderList(getActivity(), "1", "", true).execute();

                return false;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                keyword = query;
                orderRequirementList.clear();
                new PostClassOrderList(getActivity(), "", keyword, true).execute();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
             /*   if (newText.length()==0){
                    keyword = "0";
                    new PostClassOrderList(getActivity(), "1", keyword,true).execute();
                }*/

                return false;
            }
        });
    }


    private void loadMoreItems() {


        new PostClassOrderList(getActivity(), String.valueOf(++page), keyword, true).execute();
    }

    private void refreshItems() {
        page = 1;
        orderRequirementList.clear();
        swipeRefreshLayout.setRefreshing(false);
        // Load items
        // ...
        new PostClassOrderList(getActivity(), String.valueOf(page), keyword, true).execute();

    }

    class PostClassOrderList extends AsyncTask<String, Void, String> {
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        String Keyword;
        boolean showLoader;
        private Context context;


        PostClassOrderList(Context c, String pageNo, String keyword, boolean showLoader) {
            this.context = c;
            this.pageNo = pageNo;
            this.Keyword = keyword;
            this.showLoader = showLoader;
            Log.d("-----Keyword------", "" + Keyword);
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("page", pageNo);
                    json.put("keyword", Keyword);
                    // json.put("page_module", MODULE_ORDERS);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(orders_listUrl)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_ORDERS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======orders_listUrl=========" + orders_listUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    orderRequirementList.add(new BeanOrderList(
                                            jsonObject.getString("order_date"),
                                            jsonObject.getString("customer_name"),
                                            jsonObject.getString("consignee_name"),
                                            jsonObject.getString("vendor_id"),
                                            jsonObject.getString("dispatch_from"),
                                            jsonObject.getString("mm_name"),
                                            jsonObject.getString("brand_name"),
                                            jsonObject.getString("category_name"),
                                            jsonObject.getString("packing_type_name"),
                                            jsonObject.getString("product_name"),
                                            jsonObject.getString("order_id"),
                                            jsonObject.getString("category"),
                                            jsonObject.getString("customer_code"),
                                            jsonObject.getString("product"),
                                            jsonObject.getString("brand"),
                                            jsonObject.getString("packingtype"),
                                            jsonObject.getString("bagtype"),
                                            jsonObject.getString("quantity"),
                                            jsonObject.getString("consignee"),
                                            jsonObject.getString("state"),
                                            jsonObject.getString("district"),
                                            jsonObject.getString("taluka"),
                                            jsonObject.getString("city"),
                                            jsonObject.getString("rate"),
                                            jsonObject.getString("validity"),
                                            jsonObject.getString("address"),
                                            jsonObject.getString("employee_code"),
                                            jsonObject.getString("transfer_flag"),
                                            jsonObject.getString("remark"),
                                            jsonObject.getString("last_updated_by"),
                                            jsonObject.getString("last_update_login"),
                                            jsonObject.getString("last_update_date"),
                                            jsonObject.getString("order_date_new")));

                                }




                             /*   realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(orderRequirementList);
                                    }
                                });*/

                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                page--;
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


                orderRequirementAdapter.notifyDataSetChanged();

            } else {
                page--;
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }
            int  count=0;

            count=  orderRequirementList.size();
            if (count==0){
                tv_no_record_found.setVisibility(View.VISIBLE);
                tv_count_item.setText(count+"  Order");
            }else {
                tv_no_record_found.setVisibility(View.GONE);
                tv_count_item.setText(count+"  Orders");
            }

        }
    }


}
