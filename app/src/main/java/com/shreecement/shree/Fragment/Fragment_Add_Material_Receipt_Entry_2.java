package com.shreecement.shree.Fragment;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.shreecement.shree.ModalClass.BeanReceiptDetail;
import com.shreecement.shree.PostClass.PostClassAddMaterialrecepit;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import java.text.SimpleDateFormat;
import java.util.Calendar;



/*
 * Created by Choudhary on 02-01-2018
 */

public class Fragment_Add_Material_Receipt_Entry_2 extends Fragment {
    Button btnSave;
    EditText edRecQty, edShortQty, edCutqty, edDamQuantity;
    TextView tvRecdate, tvDiQty, tvTotalQty;
    String addEdit = "";
    String organization_id, delivery_id, transpoter, tuck, tvpackingType, egpdate, product, bagtype, diQty,
            egpNo;
    Handler handler;
    Runnable runnable;

    int a1, a2, a3, a4, sum, x;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_material_receipt_entery2, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        btnSave = view.findViewById(R.id.btnSave);
        tvDiQty = view.findViewById(R.id.tvDiQty);
        edRecQty = view.findViewById(R.id.edRecQty);
        edShortQty = view.findViewById(R.id.edShortQty);
        edCutqty = view.findViewById(R.id.edCutqty);
        edDamQuantity = view.findViewById(R.id.edDamQuantity);
        tvTotalQty = view.findViewById(R.id.tvTotalQty);
        tvRecdate = view.findViewById(R.id.tvRecdate);

        a1 = a2 = a3 = a4 = 0;
        tvDiQty.setBackgroundColor(Color.parseColor("#e6e3e3"));
        tvTotalQty.setBackgroundColor(Color.parseColor("#e6e3e3"));


        edRecQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                x = charSequence.toString().equals("") ? 0 : Integer.valueOf(charSequence.toString());

                if (x == 0)
                    sum -= a1;
                else {
                    if (x > a1)
                        sum += (x - a1);
                    else
                        sum -= (a1 - x);
                }
                a1 = x;
            }

            @Override
            public void afterTextChanged(Editable editable) {

                tvTotalQty.setText(String.valueOf(sum));
            }
        });
        edShortQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                x = charSequence.toString().equals("") ? 0 : Integer.valueOf(charSequence.toString());

                if (x == 0)
                    sum -= a2;
                else {
                    if (x > a2)
                        sum += (x - a2);
                    else
                        sum -= (a2 - x);
                }
                a2 = x;
            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvTotalQty.setText(String.valueOf(sum));
            }
        });
        edCutqty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                x = charSequence.toString().equals("") ? 0 : Integer.valueOf(charSequence.toString());

                if (x == 0)
                    sum -= a3;
                else {
                    if (x > a3)
                        sum += (x - a3);
                    else
                        sum -= (a3 - x);
                }
                a3 = x;
            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvTotalQty.setText(String.valueOf(sum));
            }
        });
        edDamQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                x = charSequence.toString().equals("") ? 0 : Integer.valueOf(charSequence.toString());

                if (x == 0)
                    sum -= a4;
                else {
                    if (x > a4)
                        sum += (x - a4);
                    else
                        sum -= (a4 - x);
                }
                a4 = x;
            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvTotalQty.setText(String.valueOf(sum));
            }
        });


        transpoter = getArguments().getString("transpoter");
        tuck = getArguments().getString("tuck");
        tvpackingType = getArguments().getString("tvpackingType");
        bagtype = getArguments().getString("bagtype");
        product = getArguments().getString("product");

        egpdate = getArguments().getString("egpDate");
        egpNo = getArguments().getString("egpNo");
        delivery_id = getArguments().getString("delivery_id");
        addEdit = getArguments().getString("addEdit");
        diQty = getArguments().getString("diQty");
        tvDiQty.setText(diQty);


        showDialog();


        if (addEdit.equals("edit")) {
            Gson gson = new Gson();
            String json = StorePrefs.getDefaults("receiptDetail", getActivity());
            BeanReceiptDetail beanReceiptDetail = gson.fromJson(json, BeanReceiptDetail.class);


            int totalQty = Integer.parseInt(beanReceiptDetail.getAccept_qty()) +
                    Integer.parseInt(beanReceiptDetail.getShort_qty()) +
                    Integer.parseInt(beanReceiptDetail.getCut_qty()) +
                    Integer.parseInt(beanReceiptDetail.getDamage_qty());

            Log.d("-----date-------", "" + beanReceiptDetail.getReceipt_date());
            tvDiQty.setText(String.valueOf(totalQty));
            edRecQty.setText(beanReceiptDetail.getAccept_qty());
            tvTotalQty.setText(String.valueOf(totalQty));
            tvRecdate.setText(beanReceiptDetail.getReceipt_date());

            edShortQty.setText(beanReceiptDetail.getShort_qty());
            edCutqty.setText(beanReceiptDetail.getCut_qty());
            edDamQuantity.setText(beanReceiptDetail.getDamage_qty());
            organization_id = beanReceiptDetail.getDestination_organization_id();
            delivery_id = beanReceiptDetail.getDelivery_id();
            transpoter = beanReceiptDetail.getTransporter();
            tuck = beanReceiptDetail.getTruck_no();
            tvpackingType = beanReceiptDetail.getPacking_type();
            egpdate = beanReceiptDetail.getEgp_date();
            product = beanReceiptDetail.getProduct();
            bagtype = beanReceiptDetail.getBag_type();
            egpNo = beanReceiptDetail.getEgp_no();
            getActivity().setTitle(getActivity().getString(R.string.update_material_recepit_entry));

        } else {
            getActivity().setTitle(getActivity().getString(R.string.add_material_recepit_entry));

        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvDiQty.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Di Quantity field is required!", Toast.LENGTH_LONG).show();

                } else if (edRecQty.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Recevied Quantity field is required!", Toast.LENGTH_LONG).show();

                } else if (edShortQty.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Short Quantity field is required!", Toast.LENGTH_LONG).show();

                } else if (edCutqty.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Cut Quantity field is required!", Toast.LENGTH_LONG).show();

                } else if (edDamQuantity.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Damaged Quantity field is required!", Toast.LENGTH_LONG).show();

                } else if (tvTotalQty.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Total Quantity field is required!", Toast.LENGTH_LONG).show();

                } else if (!tvDiQty.getText().toString().equals(tvTotalQty.getText().toString())) {
                    Toast.makeText(getActivity(), "Total Quantity Not Matching DI Quantity!", Toast.LENGTH_LONG).show();
                } else
                    new PostClassAddMaterialrecepit(getActivity(), addEdit.equals("edit") ? "edit" : "", StorePrefs.getDefaults("organization_id", getActivity()), delivery_id, transpoter,
                            tvDiQty.getText().toString(), tuck, edRecQty.getText().toString(), tvpackingType,
                            edShortQty.getText().toString(), egpdate, edCutqty.getText().toString(), product,
                            edDamQuantity.getText().toString(), bagtype, tvTotalQty.getText().toString(),
                            egpNo, tvRecdate.getText().toString()).execute();
            }
        });


        return view;

    }

    private void showDialog() {

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 1000);
                try {
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                    String time = df.format(cal.getTime());
                    tvRecdate.setText(time);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 0);


    }
}