package com.shreecement.shree.Fragment.Mitra;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.shreecement.shree.Common.Common_Adapter_for_details;
import com.shreecement.shree.Fragment.Documents.Fragment_DocUpload;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.MitraDetail;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;


/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Mitra_Details extends Fragment {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Common_Adapter_for_details commonAdapterForDetails;
    ArrayList<LookupCategory> mitraList;

    public static String PageValue = "";

    public String mitraId = "";
    String adhaar_imageUrl = "";
    String adhaar_imageUrl_back = "";
    String voter_imageUrl = "";
    String voter_imageUrl_back = "";
    String driving_licence_imageUrl = "";
    String pan_imageUrl = "";
    String mitra_imageUrl = "";

    public static Fragment_Mitra_Details newInstance(String PageValue) {

        Fragment_Mitra_Details f = new Fragment_Mitra_Details();
        Bundle b = new Bundle();
        b.putString("pageValue", PageValue);
        f.setArguments(b);

        return f;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            PageValue = bundle.getString("pageValue");
            Log.d("--------", "pageValue : - " + PageValue);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_common_details_list, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getActivity().setTitle(getActivity().getString(R.string.mitra_detail));
        setHasOptionsMenu(true);
        readBundle(getArguments());


        /*Gson gson = new Gson();
        String json = StorePrefs.getDefaults(PREF_BEAN_MITRA_ITEM, getActivity());
        BeanCommon beanMitra_item = gson.fromJson(json, BeanCommon.class);*/

       // Toast.makeText(getActivity(), ""+beanMitra_item.getId(), Toast.LENGTH_SHORT).show();


        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);

        mitraList = new ArrayList<>();
        commonAdapterForDetails = new Common_Adapter_for_details(getActivity(), mitraList);
        recyclerView.setAdapter(commonAdapterForDetails);
        commonAdapterForDetails.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();

        //new PostClassMitraDetail(getActivity(),beanMitra_item.getId()).execute();
        new PostClassMitraDetail(getActivity(),mitraId).execute();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        // super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_doc, menu);

        MenuItem documents = menu.findItem(R.id.documents);

        documents.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {


                Fragment_DocUpload fragment = Fragment_DocUpload.newInstance("view");
                fragment.NewCreatedMitraId = mitraId;
                try {
                    fragment.adhaar_imageUrl = adhaar_imageUrl;
                    fragment.adhaar_imageUrl_back = adhaar_imageUrl_back;
                    fragment.voter_imageUrl = voter_imageUrl;
                    fragment.voter_imageUrl_back = voter_imageUrl_back;
                    fragment.driving_licence_imageUrl = driving_licence_imageUrl;
                    fragment.pan_imageUrl = pan_imageUrl;
                    fragment.mitra_imageUrl = mitra_imageUrl;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

              //  ((MainActivity) getActivity()).showUpButton(true);

                return false;
            }
        });


    }

    class PostClassMitraDetail extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempMitraList;
        String id;

        PostClassMitraDetail(Context c, String id) {
            this.context = c;
            this.id = id;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

            tempMitraList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();

                    json.put("id",id );
                   // json.put("page_module", MODULE_MITRAS);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(MitraDetail)
                            .post(body)
                            .addHeader("content-type", "application/json")
                           // .addHeader("page_module", MODULE_MITRAS)
                            .addHeader("page_module", MODULE_COMMON)
                           // .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======MitraDetail=========" + MitraDetail);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONObject jsonData = jObject.getJSONObject("data");
                            if (jObject != null) {

                                tempMitraList.add(new LookupCategory("Mitra ID",
                                        "",Constant.nullCheckFunction( id)));

                                tempMitraList.add(new LookupCategory("Mitra Type",
                                        "",Constant.nullCheckFunction( jsonData.getString("mitra_type_meaning"))));

                                tempMitraList.add(new LookupCategory("Aadhaar Number",
                                        "",Constant.nullCheckFunction(  jsonData.getString("adhaar_number"))));

                                tempMitraList.add(new LookupCategory("Mobile Number",
                                        "",Constant.nullCheckFunction(  jsonData.getString("mobile_number"))));

                                tempMitraList.add(new LookupCategory("Full Name",
                                        "",Constant.nullCheckFunction(  jsonData.getString("full_name"))));


                                tempMitraList.add(new LookupCategory("Gender",
                                        "", Constant.nullCheckFunction(jsonData.getString("sex_code_meaning"))));

                                tempMitraList.add(new LookupCategory("Date Of Birth",
                                        "", Constant.dateFuncation( jsonData.getString("date_of_birth"))));


                                tempMitraList.add(new LookupCategory("State",
                                        "",Constant.nullCheckFunction(  jsonData.getString("state"))));

                                tempMitraList.add(new LookupCategory("District",
                                        "",Constant.nullCheckFunction(  jsonData.getString("district"))));

                                tempMitraList.add(new LookupCategory("Taluka",
                                        "",Constant.nullCheckFunction(  jsonData.getString("taluka"))));

                                tempMitraList.add(new LookupCategory("City",
                                        "",Constant.nullCheckFunction(  jsonData.getString("city"))));

                                tempMitraList.add(new LookupCategory("Address",
                                        "",Constant.nullCheckFunction(  jsonData.getString("address_line1"))));

                                tempMitraList.add(new LookupCategory("Pin Code",
                                        "",Constant.nullCheckFunction(  jsonData.getString("postal_code"))));

                                tempMitraList.add(new LookupCategory("Email Address",
                                        "",Constant.nullCheckFunction(  jsonData.getString("email_address"))));

                                tempMitraList.add(new LookupCategory("Marital Status",
                                        "",Constant.nullCheckFunction(  jsonData.getString("marital_status_meaning"))));

                                tempMitraList.add(new LookupCategory("Blood Type",
                                        "",Constant.nullCheckFunction(  jsonData.getString("blood_type_meaning"))));

                                tempMitraList.add(new LookupCategory("Spouse Name",
                                        "",Constant.nullCheckFunction(  jsonData.getString("spouse_name"))));

                                tempMitraList.add(new LookupCategory("Anniversary",
                                        "", Constant.dateFuncation( jsonData.getString("date_of_wedding"))));

                                tempMitraList.add(new LookupCategory("Child 1 Name",
                                        "",Constant.nullCheckFunction(  jsonData.getString("child1_name"))));

                                tempMitraList.add(new LookupCategory("Child 1 Date Of Birth",
                                        "", Constant.dateFuncation( jsonData.getString("date_of_birth_child1"))));

                                tempMitraList.add(new LookupCategory("Child 2 Name",
                                        "",Constant.nullCheckFunction(  jsonData.getString("child2_name"))));

                                tempMitraList.add(new LookupCategory("Child 2 Date Of Birth",
                                        "",  Constant.dateFuncation(jsonData.getString("date_of_birth_child2"))));


                               /* tempMitraList.add(new LookupCategory("Mitra Number",
                                        "", jsonData.getString("mitra_number")));*/

                                tempMitraList.add(new LookupCategory("PAN Card",
                                        "",Constant.nullCheckFunction(  jsonData.getString("pan"))));

                                tempMitraList.add(new LookupCategory("Driving Licence",
                                        "", Constant.nullCheckFunction( jsonData.getString("driving_licence_number"))));

                                tempMitraList.add(new LookupCategory("Voter Id",
                                        "",Constant.nullCheckFunction(  jsonData.getString("voter_id"))));

                                tempMitraList.add(new LookupCategory("GST Number",
                                        "",Constant.nullCheckFunction( jsonData.getString("gst_number"))));


                                /*tempMitraList.add(new LookupCategory("Address Line 2",
                                        "", jsonData.getString("address_line2")));

                                tempMitraList.add(new LookupCategory("Address Line 3",
                                        "", jsonData.getString("address_line3")));*/

                                try{
                                adhaar_imageUrl = Constant.nullCheckFunction(jsonData.getString("adhaar_image"));
                                adhaar_imageUrl_back = Constant.nullCheckFunction(jsonData.getString("adhaar_image_back"));
                                voter_imageUrl = Constant.nullCheckFunction(jsonData.getString("voter_image"));
                                voter_imageUrl_back = Constant.nullCheckFunction(jsonData.getString("voter_image_back"));
                                driving_licence_imageUrl = Constant.nullCheckFunction(jsonData.getString("driving_licence_image"));
                                pan_imageUrl = Constant.nullCheckFunction(jsonData.getString("pan_image"));
                                mitra_imageUrl = Constant.nullCheckFunction(jsonData.getString("mitra_image"));
                                }catch (Exception e){

                                }



                            }
                            mitraList.addAll(tempMitraList);


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

                commonAdapterForDetails.notifyDataSetChanged();

            } else {

                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

}
