package com.shreecement.shree.Fragment.Help;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.shreecement.shree.Fragment.Help.Adapter.Help_Adapter;
import com.shreecement.shree.Fragment.Help.Bean.BeanHelp_Item;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;
import com.shreecement.shree.Utlility.StorePrefs;
import com.shreecement.shree.Utlility.Utils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.CheckInternet;
import static com.shreecement.shree.Utlility.Constant.MODULE_HELP;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.help_listUrl;



/*
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Help extends Fragment {
    RecyclerView recyclerView;
    Help_Adapter help_adapter;

    ArrayList<BeanHelp_Item> helpList;
    String keyword = "";
    int page = 1;
    SwipeRefreshLayout swipeRefreshLayout;


    boolean isPagination = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_help, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);
       // setHasOptionsMenu(true);

        helpList = new ArrayList<>();
        keyword = "";

        help_adapter = new Help_Adapter(getActivity(), helpList);
        recyclerView.setAdapter(help_adapter);
        help_adapter.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();

        getActivity().setTitle(getActivity().getString(R.string.helps));


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new PostClass_Help(getActivity(), "1", keyword, true).execute();
            }
        }, 425);


        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        // super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.clearFocus();

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                helpList.clear();
                page = 1;
                keyword = "";
                new PostClass_Help(getActivity(), "1", "", true).execute();

                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                keyword = query;
                helpList.clear();

                new PostClass_Help(getActivity(), "", keyword, true).execute();
              /*  helpList.clear();
                for (int x = 0; x < tempList1.size(); x++) {
                    if(tempList1.get(x).getParty_name().contains(query.toUpperCase()))
                        helpList.add(tempList1.get(x));
                }
                customers_adapter.notifyDataSetChanged();*/
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

              /*  if (newText.length() == 0){
                    keyword = newText;
                    new PostClass_Help(getActivity(), "1",keyword, true).execute();

                }*/
                return false;
            }
        });


    }


    private void loadMoreItems() {
        isPagination = true;

        new PostClass_Help(getActivity(), String.valueOf(++page), keyword, true).execute();
    }

    private void refreshItems() {
        page = 1;
        helpList.clear();
        swipeRefreshLayout.setRefreshing(false);
        // Load items
        // ...
        new PostClass_Help(getActivity(), String.valueOf(page), keyword, true).execute();


    }

    class PostClass_Help extends AsyncTask<String, Void, String> {

        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;

        String pageNo;
        String Keyword;
        boolean showLoader;
        List<BeanHelp_Item> temphelpList;

        PostClass_Help(Context c, String pageNo, String keyword, boolean showLoader) {
            this.context = c;
            this.pageNo = pageNo;
            this.Keyword = keyword;
            this.showLoader = showLoader;
            Log.d("====keyword=====", "" + Keyword);
        }

        protected void onPreExecute() {
            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            temphelpList = new ArrayList<>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("keyword", Keyword);
                    json.put("page", pageNo);
                    //json.put("page_module", MODULE_HELP);
                    json.put("org_id", ORG_ID);

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(help_listUrl)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_HELP)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))

                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======help_listUrl=========" + help_listUrl);

                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(Constant.RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            System.out.println("======jsonArraydata=========" + jsonArraydata);
                            if (jsonArraydata != null) {
                                JSONObject jsonObject;


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    jsonObject = jsonArraydata.getJSONObject(i);
                                    System.out.println("======jsonObject=========" + jsonObject);

                                    temphelpList.add(new BeanHelp_Item(
                                            jsonObject.getString("help_topic_id"),
                                            jsonObject.getString("help_topic_number"),
                                            jsonObject.getString("title"),
                                            jsonObject.getString("short_description"),
                                            jsonObject.getString("long_description")));

                                }

                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                /*((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });*/

                return Utils.Please_Check_Internet_Connection;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                // Toast.makeText(context, jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


                if (!isPagination) {
                    helpList.clear();
                    helpList.addAll(temphelpList);
                    System.out.println("=!isPagination=====helpList=========" + helpList.size());
                } else {

                    if (temphelpList.size() > 0) {
                        helpList.addAll(temphelpList);
                        System.out.println("======temphelpList=========" + temphelpList.size());
                        System.out.println("======helpList=========" + helpList.size());
                    } else {
                        Toast.makeText(context, "This is last page", Toast.LENGTH_SHORT).show();
                    }
                }


            }
            else if (s.equalsIgnoreCase(Utils.Please_Check_Internet_Connection)) {
                page--;
                progress.dismiss();
                CheckInternet(getContext());
            }
            else {
                page--;
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }
            help_adapter.notifyDataSetChanged();
        }


    }

}
