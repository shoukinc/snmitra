package com.shreecement.shree.Fragment.Mitra;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Common.BeanCommon;
import com.shreecement.shree.Fragment.Mitra.Adapter.Mitra_Adapter;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.Citylist;
import static com.shreecement.shree.Utlility.Constant.Districtlist;
import static com.shreecement.shree.Utlility.Constant.Lookup_data;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.MODULE_MITRAS;
import static com.shreecement.shree.Utlility.Constant.Mitralist;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_STATE;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.Statelist;
import static com.shreecement.shree.Utlility.Constant.Talukalist;


/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Mitras extends Fragment implements View.OnClickListener {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Mitra_Adapter mitraAdapter;
    ArrayList<BeanCommon> mitraList;
    FloatingActionButton btnMitraRegistration;
    String keyword = "";
    String key = "";
    String filterby_status = "";
    String filterby_verify = "";
    String filterby_city = "";
    String filterby_taluka = "";
    String filterby_state = "";
    String filterby_district = "";
    String filterby_mitra_type = "";
    String filterby_Value = "";


    int page = 1;
    int total_record = 0;
    int recordCount = 0;
    //boolean isPagination = false;

    TextView tvStatus,tvcategory,tvRegion,tvmobile;
    LinearLayout lL_Status,lL_category,lL_Region,lL_mobile;


    TextView dialogTitle;
    ListView listView;
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog.Builder dialogFilter;
     public AlertDialog builder2,builderFilter;
     public AlertDialog builderRegion,builder3;

    public ArrayList<LookupCategory> StatusList;
    public ArrayList<LookupCategory> CategoryList;
    public ArrayList<LookupCategory> MobileList;

    public ArrayList<LookupCategory> StateList;
    public ArrayList<LookupCategory> DistrictList;
    public ArrayList<LookupCategory> TalukaList;
    public ArrayList<LookupCategory> CityList;

    public SpinnerDialogAdapter spinnerAdapter;
    public SpinnerFilterAdapter spinnerFilterAdapter;


    String stateCode = "";
    String districtCode = "";
    String talukaCode = "";
    String cityCode = "";

    String statusCode = "";
    String CategoryCode = "";
    String MobileCode = "";

    TextView tvState,tvDistrict,tvtaluka,tvcity;
    Button btnapply;
    ImageView img_filter;

    TextView tv_no_record_found;
    TextView tv_count_item;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mitra_list, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        img_filter = view.findViewById(R.id.img_filter);
        tv_no_record_found = view.findViewById(R.id.tv_no_record_found);
        tv_count_item = view.findViewById(R.id.tv_count_item);
        setHasOptionsMenu(true);

        btnMitraRegistration = view.findViewById(R.id.btnMitraRegistration);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);

        getActivity().setTitle(getActivity().getString(R.string.mitra));
        keyword = "";

        mitraList = new ArrayList<>();
        mitraAdapter = new Mitra_Adapter(getActivity(), mitraList);
        recyclerView.setAdapter(mitraAdapter);
        mitraAdapter.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();


        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialogFilter("Filter");

            }
        }); btnMitraRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              // getActivity().onBackPressed();

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);
                fragmentTransaction.replace(R.id.container_body, Fragment_MitraRegistration.newInstance("add")).addToBackStack(null);
                fragmentTransaction.commit();

               // ((MainActivity) getActivity()).showUpButton(true); i commented this

            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    loadMoreItems();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            public void onRefresh() {

                refreshItems();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new PostClassMitraList(getActivity(), "1", keyword, "","","","","","",
                        "",true,false).execute();
            }
        }, 450);

    /*    RealmResults<BeanOrderList> results = realm.where(BeanOrderList.class).findAll();

        if (results.size() > 0)
        {
            mitraList.addAll(results);
        }
        else {

        }*/
        StatusList = new ArrayList<LookupCategory>();
        CategoryList = new ArrayList<LookupCategory>();
        MobileList = new ArrayList<LookupCategory>();

        StateList = new ArrayList<LookupCategory>();
        DistrictList = new ArrayList<LookupCategory>();
        TalukaList = new ArrayList<LookupCategory>();
        CityList = new ArrayList<LookupCategory>();

                // Status list
        new PostClassLookUpData(getActivity()).execute();
        new PostClassStateList(getActivity()).execute();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
       // menu.clear();
        //super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu, menu);


        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);

        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.clearFocus();


        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                mitraList.clear();
                page = 1;
                keyword = "";
               // new PostClassMitraList(getActivity(), "1", "", true).execute();
                new PostClassMitraList(getActivity(), "1", keyword, "","","","","","",
                        "",true,false).execute();
                return false;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                keyword = query;
                mitraList.clear();
               // new PostClassMitraList(getActivity(), "", keyword, true).execute();
                new PostClassMitraList(getActivity(), "1", keyword, "","","","","","",
                        "",true,true).execute();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
             /*   if (newText.length()==0){
                    keyword = "0";
                    new PostClassMitraList(getActivity(), "1", keyword,true).execute();
                }*/
                if (newText.length() == 0){
                    keyword = newText;
                  //  new PostClassMitraList(getActivity(), "1",keyword, true).execute();

                    new PostClassMitraList(getActivity(), "1", keyword, "","","","","","",
                            "",true,true).execute();
                }

                return false;
            }
        });
        //super.onCreateOptionsMenu(menu, inflater);
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                keyword="";
               // new PostClassMitraList(getActivity(), "", keyword, true).execute();
                new PostClassMitraList(getActivity(), "1", keyword, "","","","","","",
                        "",true,false).execute();
                return false;
            }
        });
    }



    private void loadMoreItems() {
       // isPagination = true;

        //new PostClassMitraList(getActivity(), String.valueOf(++page), keyword, true).execute();
        if (filterby_Value.equalsIgnoreCase("status")){
            new PostClassMitraList(getActivity(), String.valueOf(++page), keyword, statusCode,"","","","","",
                    "",true,true).execute();

        }
       else if (filterby_Value.equalsIgnoreCase("Category")){

            new PostClassMitraList(getActivity(), String.valueOf(++page), keyword, "","","","","","",
                    CategoryCode,true,true).execute();
        }
        else  if (filterby_Value.equalsIgnoreCase("Region Filter")){

            new PostClassMitraList(getActivity(), String.valueOf(++page), keyword, "","",cityCode,talukaCode,stateCode,districtCode,
                    "",true,true).execute();
        }
        else  if (filterby_Value.equalsIgnoreCase("Mobile")){

            new PostClassMitraList(getActivity(), String.valueOf(++page), keyword, "",MobileCode,"","","","",
                    "",true,true).execute();
        }
        else {
            filterby_Value = "";

            new PostClassMitraList(getActivity(), String.valueOf(++page), keyword, "", "", "", "", "", "",
                    "", true,true).execute();
        }
    }

    private void refreshItems() {

        page = 1;

        keyword="";
        statusCode="";
        CategoryCode="";
        cityCode="";
        talukaCode="";
        stateCode="";
        districtCode="";
        MobileCode="";

        mitraList.clear();
        swipeRefreshLayout.setRefreshing(false);

        // Load items
        // ...
      //  new PostClassMitraList(getActivity(), String.valueOf(page), keyword, true).execute();

        filterby_Value = "";

        new PostClassMitraList(getActivity(), "1", keyword, "","","","","","",
                "",true,false).execute();

    }


    private void showDialogFilter( String title) {
        dialogFilter = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_mitra_filter, null);
        dialogFilter.setView(dialogView);
        lL_Status =  dialogView.findViewById(R.id.lL_Status);
        lL_category =  dialogView.findViewById(R.id.lL_category);
        lL_Region =  dialogView.findViewById(R.id.lL_Region);
        lL_mobile =  dialogView.findViewById(R.id.lL_mobile);
        dialogTitle =   dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);

        builderFilter = dialogFilter.create();
        builderFilter.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builderFilter.show();

        lL_Status.setOnClickListener(this);
        lL_category.setOnClickListener(this);
        lL_Region.setOnClickListener(this);
        lL_mobile.setOnClickListener(this);
    }

    private void dialogRegionFilter( String title) {
        dialogFilter = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_mitra_filter_region, null);
        dialogFilter.setView(dialogView);
        tvState =  dialogView.findViewById(R.id.tvState);
        tvDistrict =  dialogView.findViewById(R.id.tvDistrict);
        tvtaluka =  dialogView.findViewById(R.id.tvtaluka);
        tvcity =  dialogView.findViewById(R.id.tvcity);

        btnapply =  dialogView.findViewById(R.id.btnapply);
        dialogTitle =   dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);

        builderRegion = dialogFilter.create();
        builderRegion.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builderRegion.show();

        tvState.setOnClickListener(this);
        tvDistrict.setOnClickListener(this);
        tvtaluka.setOnClickListener(this);
        tvcity.setOnClickListener(this);
        btnapply.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {
        if (view == lL_Status) {
            showFilterSpinnerDialog(StatusList, "Status");
        }
        else if (view == lL_category) {
            showFilterSpinnerDialog(CategoryList, "Category");
        }
        else if (view == lL_Region) {
            dialogRegionFilter( "Region Filter");
        }
        else if (view == lL_mobile) {
            showFilterSpinnerDialog(MobileList, "Mobile");
        }
       else if (view == tvState) {
            showSpinnerDialog(StateList, "State");
        }
        else if (view == tvDistrict) {
            showSpinnerDialog(DistrictList, "District");
        }
        else if (view == tvtaluka) {
            showSpinnerDialog(TalukaList, "Taluka");
        }
        else if (view == tvcity) {
            showSpinnerDialog(CityList, "City");
        }

        else if (view==btnapply){
            builderRegion.dismiss();
            //keyword=stateCode;
            filterby_Value = "Region Filter";
            talukaCode=tvtaluka.getText().toString();
            cityCode=tvcity.getText().toString();
            districtCode=tvDistrict.getText().toString();
            stateCode=tvState.getText().toString();

            mitraList.clear();

            new PostClassMitraList(getActivity(), String.valueOf(page), keyword, "","",cityCode,talukaCode,stateCode,districtCode,
                    "",true,true).execute();

            Log.d("talukaCode","======="+talukaCode);
            Log.d("cityCode","======="+cityCode);


           /* if (districtCode.length()>0){
               // keyword=stateCode+","+districtCode;
                page=1;
               // new PostClassMitraList(getActivity(), String.valueOf(page), keyword, true).execute();
                new PostClassMitraList(getActivity(), String.valueOf(page), keyword, "","","","",stateCode,districtCode,
                        "",true).execute();

            }else if (talukaCode.length()>0){
              //  keyword=stateCode+","+districtCode+","+talukaCode;
                page=1;
               // new PostClassMitraList(getActivity(), String.valueOf(page), keyword, true).execute();
                new PostClassMitraList(getActivity(), String.valueOf(page), keyword, "","","",talukaCode,stateCode,districtCode,
                        "",true).execute();

            }else if (cityCode.length()>0){
              //  keyword=stateCode+","+districtCode+","+talukaCode+","+cityCode;
                page=1;
                //new PostClassMitraList(getActivity(), String.valueOf(page), keyword, true).execute();
                new PostClassMitraList(getActivity(), String.valueOf(page), keyword, "","",cityCode,talukaCode,stateCode,districtCode,
                        "",true).execute();

            }*/


        }


        builderFilter.dismiss();


    }
    private void showFilterSpinnerDialog(ArrayList<LookupCategory > list, String title) {
        dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        listView =  dialogView.findViewById(R.id.listCatecory);
        dialogTitle =   dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerFilterAdapter = new SpinnerFilterAdapter(getActivity(), list, title);
        listView.setAdapter(spinnerFilterAdapter);
        builder2 = dialogBuilder.create();
        builder2.requestWindowFeature(Window.FEATURE_NO_TITLE);

        builder2.show();
    }

    public class SpinnerFilterAdapter extends BaseAdapter {
        Context context;
        List<LookupCategory> rowItems;
        String Title = "";


        public SpinnerFilterAdapter(Context context, List<LookupCategory> items, String title) {
            this.context = context;
            this.rowItems = items;
            this.Title = title;
            Log.d("===rowItems.zize===", "" + rowItems.size());


        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;

        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            SpinnerFilterAdapter.ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new SpinnerFilterAdapter.ViewHolder();
                holder.textName = convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (SpinnerFilterAdapter.ViewHolder) convertView.getTag();

            }

            holder.textName.setText(rowItems.get(position).getDESCRIPTION());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    page=1;
                    final String meaning = rowItems.get(position).getCODE();
                    switch (Title) {

                        case "Status":
                            builder2.dismiss();
                           // keyword=meaning;
                            statusCode = meaning;
                            filterby_Value = "Status";
                            mitraList.clear();
                           // new PostClassMitraList(getActivity(), String.valueOf(page), keyword, true).execute();
                            new PostClassMitraList(getActivity(), String.valueOf(page), keyword, statusCode,"","","","","",
                                    "",true,true).execute();

                            break;
                        case "Category":
                            builder2.dismiss();
                           // keyword=meaning;
                            CategoryCode = meaning;
                            filterby_Value = "Category";
                            mitraList.clear();
                           // new PostClassMitraList(getActivity(), String.valueOf(page), keyword, true).execute();
                            new PostClassMitraList(getActivity(), String.valueOf(page), keyword, "","","","","","",
                                    CategoryCode,true,true).execute();

                            break;

                        case "Mobile":
                            builder2.dismiss();
                            // keyword=meaning;
                            MobileCode = meaning;
                            filterby_Value = "Mobile";
                            mitraList.clear();
                            Log.e("MobileCode> = ",MobileCode);
                           // new PostClassMitraList(getActivity(), String.valueOf(page), keyword, true).execute();
                            new PostClassMitraList(getActivity(), String.valueOf(page), keyword, "",MobileCode,"","","","",
                                    "",true,true).execute();

                            break;

                    }


                }
            });
            return convertView;
        }


        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }


    }


    class PostClassMitraList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        String Keyword;
        boolean showLoader;

        List<BeanCommon> tempMitralist = new ArrayList<>();
        boolean   isPagination ;

        String filterby_status ;
        String filterby_verify ;
        String filterby_city ;
        String filterby_taluka;
        String filterby_state ;
        String filterby_district;
        String filterby_mitra_type ;


        PostClassMitraList(Context c, String pageNo, String keyword,
                           String filterby_status, String filterby_verify,
                           String filterby_city, String filterby_taluka,
                           String filterby_state, String filterby_district,
                           String filterby_mitra_type,
                           boolean showLoader,boolean   isPagination) {
            this.context = c;
            this.pageNo = pageNo;
            this.Keyword = keyword;
            this.filterby_status = filterby_status;
            this.filterby_verify = filterby_verify;
            this.filterby_city = filterby_city;
            this.filterby_taluka = filterby_taluka;
            this.filterby_state = filterby_state;
            this.filterby_district = filterby_district;
            this.filterby_mitra_type = filterby_mitra_type;
            this.showLoader = showLoader;
            this.isPagination = isPagination;

            Log.d("-----Keyword------", "" + Keyword);
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempMitralist.clear();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            System.out.println("Keyword===============" + Keyword);
            System.out.println("pageNo===============" + pageNo);


            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("page", pageNo);
                    json.put("keyword", Keyword);
                    json.put("org_id",ORG_ID );
                    json.put("state", StorePrefs.getDefaults(PREF_MITRA_STATE, context));
                    json.put("user_type", StorePrefs.getDefaults(PREF_USER_TYPE, context));
                    json.put("filterby_status", filterby_status);
                    json.put("filterby_verify", filterby_verify);
                    json.put("filterby_city", filterby_city);
                    json.put("filterby_taluka", filterby_taluka);
                    json.put("filterby_state", filterby_state);
                    json.put("filterby_district", filterby_district);
                    json.put("filterby_mitra_type", filterby_mitra_type);


                    //json.put("page_module", MODULE_MITRAS);
                    RequestBody body = RequestBody.create(mediaType, json.toString());
                    System.out.println("Param===============" + json.toString());
                    Request request = new Request.Builder()
                            .url(Mitralist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_MITRAS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Mitralist=========" + Mitralist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");
                             total_record = Integer.parseInt(jObject.getString("total_record"));
                            recordCount = Integer.parseInt(jObject.getString("recordCount"));

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                  /*  tempMitralist.add(new BeanCommon(
                                            jsonObject.getString("mitra_id"),
                                            jsonObject, jsonObject.getString("full_name")));*/
                                    tempMitralist.add(new BeanCommon(
                                            jsonObject.getString("mitra_id"),
                                            jsonObject, jsonObject.getString("full_name")));
                                }


                             /*   realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.copyToRealmOrUpdate(mitraList);
                                    }
                                });*/

                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progress.dismiss();
        if (s == null) {
            //page--;
            --page;
            Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
            return;
        } else if (s.equals("success")) {


            if (!isPagination) {
                mitraList.clear();
                mitraList.addAll(tempMitralist);

            } else {

                if (tempMitralist.size() > 0) {
                   // mitraList.clear();
                    mitraList.addAll(tempMitralist);

                } else {
                    Toast.makeText(context, "This is last page", Toast.LENGTH_SHORT).show();
                }
            }

            mitraAdapter.notifyDataSetChanged();

        } else {
            //page--;
            --page;
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

        }
            mitraAdapter.notifyDataSetChanged();

            recordCount= mitraList.size();
            if (recordCount==0){
                tv_no_record_found.setVisibility(View.VISIBLE);

                //tv_count_item.setText("Showing "+recordCount+" of "+total_record);
            }else {
                tv_no_record_found.setVisibility(View.GONE);
                //tv_count_item.setText("Showing "+recordCount+" of "+total_record);
            }

        }
}


/////////////////////////////////////===---------------- Filter Region wise---------------------------------- ///////////////////////////////



    private void showSpinnerDialog(ArrayList<LookupCategory> list, String title) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        listView =  dialogView.findViewById(R.id.listCatecory);
        dialogTitle = (TextView) dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerAdapter = new SpinnerDialogAdapter(getActivity(), list, title);
        listView.setAdapter(spinnerAdapter);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }

    public class SpinnerDialogAdapter extends BaseAdapter {
        Context context;
        List<LookupCategory> rowItems;
        String Title = "";


        public SpinnerDialogAdapter(Context context, List<LookupCategory> items, String title) {
            this.context = context;
            this.rowItems = items;
            this.Title = title;
            Log.d("===rowItems.zize===", "" + rowItems.size());


        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;

        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            SpinnerDialogAdapter.ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new SpinnerDialogAdapter.ViewHolder();
                holder.textName = convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (SpinnerDialogAdapter.ViewHolder) convertView.getTag();

            }

            holder.textName.setText(rowItems.get(position).getDESCRIPTION());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String meaning = rowItems.get(position).getDESCRIPTION();
                    switch (Title) {

                        case "State":
                            builder3.dismiss();
                            tvState.setText(meaning);
                            stateCode = rowItems.get(position).getDESCRIPTION();
                            new PostClassDistrictList(getActivity(), stateCode).execute();
                            break;
                        case "District":
                            builder3.dismiss();
                            tvDistrict.setText(meaning);
                            districtCode = rowItems.get(position).getDESCRIPTION();
                            new PostClassTalukaList(getActivity(), stateCode,districtCode).execute();
                            break;
                        case "Taluka":
                            builder3.dismiss();
                            tvtaluka.setText(meaning);
                            talukaCode = rowItems.get(position).getDESCRIPTION();
                            new PostClassCityList(getActivity(), stateCode,districtCode,talukaCode).execute();
                            break;
                        case "City":
                            builder3.dismiss();
                            tvcity.setText(meaning);
                            cityCode=meaning;
                            break;

                    }


                }
            });
            return convertView;
        }


        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }


    }


    class PostClassStateList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempStateList;

        PostClassStateList(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempStateList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Statelist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            //.addHeader("token", StorePrefs.getDefaults("token", context))
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Statelist=========" + Statelist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempStateList.add(new LookupCategory("",
                                            "", jsonObject.getString("state")));


                                }
                                StateList.clear();
                                StateList.addAll(tempStateList);

                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            } else {


            }

        }
    }

    class PostClassDistrictList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempDistrictList;
        String state;


        PostClassDistrictList(Context c, String state) {
            this.context = c;
            this.state = state;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempDistrictList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("state",state);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Districtlist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Districtlist=========" + Districtlist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempDistrictList.add(new LookupCategory("",
                                            "", jsonObject.getString("district")));

                                }
                                DistrictList.clear();
                                DistrictList.addAll(tempDistrictList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


            } else {


            }

        }
    }

    class PostClassTalukaList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempTalukaList;
        String state="";
        String district="";

        PostClassTalukaList(Context c, String state, String district) {
            this.context = c;
            this.state = state;
            this.district = district;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempTalukaList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    // json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("state", state);
                    json.put("district", district);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Talukalist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Talukalist=========" + Talukalist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {

                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempTalukaList.add(new LookupCategory("",
                                            "", jsonObject.getString("taluka")));

                                }
                                TalukaList.clear();
                                TalukaList.addAll(tempTalukaList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            } else {


            }

        }
    }

    class PostClassCityList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempCityList;
        String taluka="";
        String state="";
        String district="";

        PostClassCityList(Context c, String state, String district, String taluka) {
            this.context = c;
            this.state = state;
            this.district = district;
            this.taluka = taluka;
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempCityList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("state", state);
                    json.put("district", district);
                    json.put("taluka", taluka);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Citylist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Citylist=========" + Citylist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempCityList.add(new LookupCategory("",
                                            "", jsonObject.getString("city")));

                                }
                                CityList.clear();
                                CityList.addAll(tempCityList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                return "Please Check Internet Connection";

            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(context,s, Toast.LENGTH_LONG).show();

            }

        }
    }


    //lookup
    public class PostClassLookUpData extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;

        public PostClassLookUpData(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();


        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                   /* MediaType mediaType = MediaType.parse("application/json");

                    JSONObject jsonObject_page_module = new JSONObject();

                    try {
                        jsonObject_page_module.put("page_module", MODULE_MITRAS);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody body = RequestBody.create(mediaType, jsonObject_page_module.toString());

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();*/

                    RequestBody body = RequestBody.create(null, new byte[]{});

                    // RequestBody body = RequestBody.create(mediaType, jsonObject_page_module.toString());

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .addHeader("page_module", MODULE_COMMON)
                            //.addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();


                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONObject category = jObject.getJSONObject("data");

                            System.out.println("======Lookup_data=========" + Lookup_data);


                            JSONArray jsonArrayMitraType = category.getJSONArray("mitra_status");
                            for (int i = 0; i < jsonArrayMitraType.length(); i++) {
                                JSONObject jsonStatus = jsonArrayMitraType.getJSONObject(i);
                                StatusList.add(new LookupCategory(jsonStatus.getString("lookup_type"),
                                        jsonStatus.getString("code"), jsonStatus.optString("description" )));
                                }

                            JSONArray jsonArraycategory = category.getJSONArray("mitra_type");
                            for (int i = 0; i < jsonArraycategory.length(); i++) {
                                JSONObject jsonCategory = jsonArraycategory.getJSONObject(i);
                                CategoryList.add(new LookupCategory(jsonCategory.getString("lookup_type"),
                                        jsonCategory.getString("code"), jsonCategory.getString("description")));

                            }

                            JSONArray jsonArrayMobile = category.getJSONArray("verification_type");
                            for (int i = 0; i < jsonArrayMobile.length(); i++) {
                                JSONObject jsonMobile = jsonArrayMobile.getJSONObject(i);
                                MobileList.add(new LookupCategory(jsonMobile.getString("lookup_type"),
                                        jsonMobile.getString("code"), jsonMobile.getString("description")));

                            }

                            Log.d("===StatusList.size===", "=====" + StatusList.size());
                            Log.d("===CategoryList.size===", "=====" + CategoryList.size());
                            Log.d("===MobileList.size===", "=====" + MobileList.size());

                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
              /*  spinTradeadapter = new SpinCalegory_Adapter(getActivity(), tempCategorylist);
                spnCategory.setAdapter(spinTradeadapter);*/


            }
        }

    }
    }
