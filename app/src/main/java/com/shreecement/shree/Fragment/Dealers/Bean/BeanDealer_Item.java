package com.shreecement.shree.Fragment.Dealers.Bean;

/**
 * Created by kamesh on 2/1/18.
 */

public class BeanDealer_Item {

    private String party_id, party_name, cust_account_id, account_number, city, postal_code, state, province, country, address;

    public BeanDealer_Item(String party_id, String party_name, String cust_account_id, String account_number, String city, String postal_code, String state, String province, String country, String address) {
        this.party_id = party_id;
        this.party_name = party_name;
        this.cust_account_id = cust_account_id;
        this.account_number = account_number;
        this.city = city;
        this.postal_code = postal_code;
        this.state = state;
        this.province = province;
        this.country = country;
        this.address = address;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getParty_name() {
        return party_name;
    }

    public void setParty_name(String party_name) {
        this.party_name = party_name;
    }

    public String getCust_account_id() {
        return cust_account_id;
    }

    public void setCust_account_id(String cust_account_id) {
        this.cust_account_id = cust_account_id;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
