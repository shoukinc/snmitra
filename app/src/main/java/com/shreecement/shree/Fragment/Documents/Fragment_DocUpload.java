package com.shreecement.shree.Fragment.Documents;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.Headers;
import com.bumptech.glide.signature.StringSignature;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.github.ybq.android.spinkit.style.CubeGrid;
import com.shreecement.shree.Common.HeadersClass;
import com.shreecement.shree.Fragment.Fragment_ChangePassword;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.CommonUtils;
import com.shreecement.shree.Utlility.Constant;
import com.shreecement.shree.Utlility.StorePrefs;
import com.shreecement.shree.Utlility.Utils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import static com.shreecement.shree.Utlility.Constant.ACTION_ADD;
import static com.shreecement.shree.Utlility.Constant.ACTION_EDIT;
import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.AadhaarImage;
import static com.shreecement.shree.Utlility.Constant.AadhaarImage_back;
import static com.shreecement.shree.Utlility.Constant.CheckInternet;
import static com.shreecement.shree.Utlility.Constant.DrivingLicence;
import static com.shreecement.shree.Utlility.Constant.MODULE_MITRAS;
import static com.shreecement.shree.Utlility.Constant.MitraDocuments;
import static com.shreecement.shree.Utlility.Constant.PERMISSION_ALL;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_ID;
import static com.shreecement.shree.Utlility.Constant.PanCard;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.UploadNewMitraDocuments;
import static com.shreecement.shree.Utlility.Constant.VoterId;
import static com.shreecement.shree.Utlility.Constant.baseUrlForImage;
import static com.shreecement.shree.Utlility.Constant.hasPermissions;
import static com.shreecement.shree.Utlility.StorePrefs.getDefaults;


/**
 * Created by Abhishek punia on 6/30/2017.
 */

public class Fragment_DocUpload extends BottomSheetDialogFragment  {

    FloatingActionButton fabAadhaar, fabAadhaar_back, fabPanCard,
            fabDrivingLicence, fabVoterId, fabVoterId_back, fabMitraPicture;

    ImageView prof_img0, prof_img1, prof_img1_back, imgBackground, prof_img2, prof_img3, prof_img4, prof_img4_back;

    Button btnClose;
    private static final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    private final int PICK_IMAGE = 12345;
    private final int TAKE_PICTURE = 6352;
    RelativeLayout layout_main;
    String idType = "";
    String UrlForDocuments = "";
    public String NewCreatedMitraId = "";
    public JSONObject jsonObjectDataOfUser = new JSONObject();
    String PageValueDocUpload = "";

    public String adhaar_imageUrl = "";
    public String adhaar_imageUrl_back = "";
    public String voter_imageUrl = "";
    public String voter_imageUrl_back = "";
    public String driving_licence_imageUrl = "";
    public String pan_imageUrl = "";
    public String mitra_imageUrl = "";
    public String action = "";

    public static String imageFilePath;//Made it static as need to override the original image with compressed image.
    Dialog dialog;
    String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,};
    Headers headers;
    public static String TokenTOPass = "" ;

    public static Fragment_DocUpload newInstance(String text) {

        Fragment_DocUpload f = new Fragment_DocUpload();
        Bundle b = new Bundle();
        b.putString("PageValueDocUpload", text);

        f.setArguments(b);

        return f;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            PageValueDocUpload = bundle.getString("PageValueDocUpload");
            Log.d("YUP!!PageValueDocUpload", "PageValueDocUpload : - " + PageValueDocUpload);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fresco.initialize(getActivity());
        View view = inflater.inflate(R.layout.fragment_doc_upload, container, false);
        view.setBackgroundColor(Color.WHITE);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        imageFilePath = CommonUtils.getFilename();
        Log.d("Image Path===", imageFilePath);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder(); StrictMode.setVmPolicy(builder.build());
        dialog = new Dialog(getActivity());

        // FloatingActionButton..........
        fabAadhaar = view.findViewById(R.id.fabAadhaar);
        fabAadhaar_back = view.findViewById(R.id.fabAadhaar_back);
        fabPanCard = view.findViewById(R.id.fabPanCard);
        fabDrivingLicence = view.findViewById(R.id.fabDrivingLicence);
        fabVoterId = view.findViewById(R.id.fabVoterId);
        fabVoterId_back = view.findViewById(R.id.fabVoterId_back);
        fabMitraPicture = view.findViewById(R.id.fabMitraPicture);
        //TextView........................

        btnClose = view.findViewById(R.id.btnClose);

        layout_main = view.findViewById(R.id.layout_main);

        prof_img0 = view.findViewById(R.id.prof_img0);
        prof_img1 = view.findViewById(R.id.prof_img1);
        prof_img1_back = view.findViewById(R.id.prof_img1_back);
        prof_img2 = view.findViewById(R.id.prof_img2);
        prof_img3 = view.findViewById(R.id.prof_img3);
        prof_img4 = view.findViewById(R.id.prof_img4);
        prof_img4_back = view.findViewById(R.id.prof_img4_back);

        readBundle(getArguments());

      //TokenTOPass =  StorePrefs.getDefaults("token", getActivity());
        Log.e("InHeader",Constant.token_from_server);

        if (PageValueDocUpload.equalsIgnoreCase("add")) {

            UrlForDocuments = UploadNewMitraDocuments + NewCreatedMitraId;
            btnClose.setVisibility(View.VISIBLE);
            action = ACTION_ADD;
            Log.e("add","add");

        }
        if (PageValueDocUpload.equalsIgnoreCase("edit")) {

            UrlForDocuments = UploadNewMitraDocuments + NewCreatedMitraId;
            action = ACTION_EDIT;
            btnClose.setVisibility(View.VISIBLE);

            Log.e("edit","edit");

            Log.e("Edit ", "adhaar_image : - " + baseUrlForImage + adhaar_imageUrl);
            Log.e("Edit ", "adhaar_image_back : - " + baseUrlForImage + adhaar_imageUrl_back);
            Log.e("Edit", "pan_imageUrl : - " + baseUrlForImage + pan_imageUrl);
            Log.e("Edit", "driving_licence_imageUrl : - " + baseUrlForImage + driving_licence_imageUrl);
            Log.e("Edit", "mitra_imageUrl : - " + baseUrlForImage + mitra_imageUrl);
            Log.e("Edit", "voter_imageUrl : - " + baseUrlForImage + voter_imageUrl);
            Log.e("Edit", "voter_imageUrl_back : - " + baseUrlForImage + voter_imageUrl_back);

            setImage(mitra_imageUrl,adhaar_imageUrl, adhaar_imageUrl_back, pan_imageUrl,driving_licence_imageUrl,voter_imageUrl,voter_imageUrl_back);

           /* Glide.with(getActivity()).load(baseUrlForImage + mitra_imageUrl)
                    .thumbnail(1.0f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(prof_img0);

            Glide.with(getActivity()).load(baseUrlForImage + adhaar_imageUrl)
                    .thumbnail(1.0f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(prof_img1);


            Glide.with(getActivity()).load(baseUrlForImage + pan_imageUrl)
                    .thumbnail(1.0f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(prof_img2);


            Glide.with(getActivity()).load(baseUrlForImage + driving_licence_imageUrl)
                    .thumbnail(1.0f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(prof_img3);

            Glide.with(getActivity()).load(baseUrlForImage + voter_imageUrl)
                    .thumbnail(1.0f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(prof_img4);*/


        }

        if (PageValueDocUpload.equalsIgnoreCase("view")) {

            btnClose.setVisibility(View.VISIBLE);
            fabAadhaar.setVisibility(View.GONE);
            fabAadhaar_back.setVisibility(View.GONE);
            fabPanCard.setVisibility(View.GONE);
            fabDrivingLicence.setVisibility(View.GONE);
            fabVoterId.setVisibility(View.GONE);
            fabVoterId_back.setVisibility(View.GONE);
            fabMitraPicture.setVisibility(View.GONE);

            Log.e("view","view");

            Log.e("View", "mitra_imageUrl : - " + baseUrlForImage + mitra_imageUrl);
            Log.e("View ", "adhaar_image : - " + baseUrlForImage + adhaar_imageUrl);
            Log.e("View ", "adhaar_image_back : - " + baseUrlForImage + adhaar_imageUrl_back);
            Log.e("View", "pan_imageUrl : - " + baseUrlForImage + pan_imageUrl);
            Log.e("View", "driving_licence_imageUrl : - " + baseUrlForImage + driving_licence_imageUrl);
            Log.e("View", "voter_imageUrl : - " + baseUrlForImage + voter_imageUrl);
            Log.e("View", "voter_imageUrl_back : - " + baseUrlForImage + voter_imageUrl_back);

            setImage(mitra_imageUrl,adhaar_imageUrl, adhaar_imageUrl_back, pan_imageUrl,driving_licence_imageUrl,voter_imageUrl,voter_imageUrl_back);


            prof_img0.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showImageDialog(getString(R.string.mitra_image),mitra_imageUrl);

                }
            });
            prof_img1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showImageDialog(getString(R.string.aadhar),adhaar_imageUrl);

                }
            });
            prof_img1_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showImageDialog(getString(R.string.aadhar_back),adhaar_imageUrl_back);

                }
            });
            prof_img2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showImageDialog(getString(R.string.pan_card),pan_imageUrl);

                }
            });
            prof_img3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showImageDialog(getString(R.string.driving_licence),driving_licence_imageUrl);

                }
            });
            prof_img4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showImageDialog(getString(R.string.voter_id),voter_imageUrl);

                }
            });
            prof_img4_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showImageDialog(getString(R.string.voter_id_back),voter_imageUrl_back);

                }
            });


        }
        if (PageValueDocUpload.equalsIgnoreCase("SelfEdit")) {
            //UrlForDocuments = UploadDocuments;
            Log.e("SelfEdit","SelfEdit");

            UrlForDocuments = UploadNewMitraDocuments + StorePrefs.getDefaults(PREF_MITRA_ID, getActivity());

            Log.e("SelfEdit", "mitra_imageUrl : - " + baseUrlForImage + mitra_imageUrl);
            Log.e("SelfEdit ", "adhaar_image : - " + baseUrlForImage + adhaar_imageUrl);
            Log.e("SelfEdit ", "adhaar_image_back : - " + baseUrlForImage + adhaar_imageUrl_back);
            Log.e("SelfEdit", "pan_imageUrl : - " + baseUrlForImage + pan_imageUrl);
            Log.e("SelfEdit", "driving_licence_imageUrl : - " + baseUrlForImage + driving_licence_imageUrl);
            Log.e("SelfEdit", "driving_licence_imageUrl : - " + baseUrlForImage + voter_imageUrl);
            Log.e("SelfEdit", "driving_licence_imageUrl : - " + baseUrlForImage + voter_imageUrl_back);

            btnClose.setVisibility(View.GONE);

             //new PostClassLoadImage(getActivity()).execute();
            setImage(mitra_imageUrl,adhaar_imageUrl, adhaar_imageUrl_back, pan_imageUrl,driving_licence_imageUrl,voter_imageUrl,voter_imageUrl_back);

        /*    Glide.with(getActivity()).load(baseUrlForImage + StorePrefs.getDefaults(AadhaarImage, getActivity()))
                    .thumbnail(1.0f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(prof_img1);

            Glide.with(getActivity()).load(baseUrlForImage + StorePrefs.getDefaults(AadhaarImage_back, getActivity()))
                    .thumbnail(1.0f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(prof_img1_back);

            Glide.with(getActivity()).load(baseUrlForImage + StorePrefs.getDefaults(PanCard, getActivity()))
                    .thumbnail(1.0f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(prof_img2);


            Glide.with(getActivity()).load(baseUrlForImage + StorePrefs.getDefaults(DrivingLicence, getActivity()))
                    .thumbnail(1.0f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(prof_img3);

            Glide.with(getActivity()).load(baseUrlForImage + StorePrefs.getDefaults(VoterId, getActivity()))
                    .thumbnail(1.0f)
                    .crossFade()
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(prof_img4);*/

        }

        getActivity().setTitle(getActivity().getString(R.string.nav_item_my_documments));

        imgBackground = view.findViewById(R.id.img_background);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

           /*     FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.container_body, new Fragment_Mitras());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
               getActivity().onBackPressed();*/

                getActivity().onBackPressed();


               /* FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);
                fragmentTransaction.replace(R.id.container_body, new Fragment_Mitras()).addToBackStack(null);
                fragmentTransaction.commit();*/

            }
        });

        // Fac Click Listener ...................
        fabAadhaar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,};
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                } else {
                    idType = "adhaar_image";
                    //Dialog
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_gallery);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View view = inflater.inflate(R.layout.dialog_gallery, null);
                    TextView tvCamera, tvGallery;
                    tvCamera = (TextView) view.findViewById(R.id.tv_camera);
                    tvGallery = (TextView) view.findViewById(R.id.tv_gallery);
                    dialog.setContentView(view);

                    tvCamera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            startIntent(false);
                   /* //    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                       // if (takePicture.resolveActivity(getActivity().getPackageManager()) != null) {
                           // startActivityForResult(takePicture, TAKE_PICTURE);
                            File imageFile = new File(imageFilePath);
                            Uri imageFileUri = Uri.fromFile(imageFile); // convert path to Uri
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);   // set the image file name
                            startActivityForResult(intent, TAKE_PICTURE);
.
                       // }*/
                            dialog.dismiss();
                        }
                    });

                    tvGallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                }
            }
        });

        // Fac Click Listener ...................
        fabAadhaar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,};
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                } else {
                    idType = "adhaar_image_back";
                    //Dialog
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_gallery);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View view = inflater.inflate(R.layout.dialog_gallery, null);
                    TextView tvCamera, tvGallery;
                    tvCamera = (TextView) view.findViewById(R.id.tv_camera);
                    tvGallery = (TextView) view.findViewById(R.id.tv_gallery);
                    dialog.setContentView(view);

                    tvCamera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            startIntent(false);

                            dialog.dismiss();
                        }
                    });

                    tvGallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                }
            }
        });

        // Fac Click Listener ...................
        fabPanCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,};
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                } else {
                    idType = "pan_image";
                    //Dialog
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_gallery);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View view = inflater.inflate(R.layout.dialog_gallery, null);
                    TextView tvCamera, tvGallery;
                    tvCamera = (TextView) view.findViewById(R.id.tv_camera);
                    tvGallery = (TextView) view.findViewById(R.id.tv_gallery);
                    dialog.setContentView(view);

                    tvCamera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        /*Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivityForResult(takePicture, TAKE_PICTURE);
                        }*/
                            startIntent(false);
                            dialog.dismiss();
                        }
                    });

                    tvGallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                }
            }
        });

        // fabDrivingLicence Click Listener ...................
        fabDrivingLicence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,};
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                } else {
                    idType = "driving_licence_image";
                    //Dialog
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_gallery);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View view = inflater.inflate(R.layout.dialog_gallery, null);
                    TextView tvCamera, tvGallery;
                    tvCamera = (TextView) view.findViewById(R.id.tv_camera);
                    tvGallery = (TextView) view.findViewById(R.id.tv_gallery);
                    dialog.setContentView(view);

                    tvCamera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        /*Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivityForResult(takePicture, TAKE_PICTURE);
                        }*/
                            startIntent(false);
                            dialog.dismiss();
                        }
                    });

                    tvGallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                }
            }
        });

        // fabMitraImage Click Listener ...................
        fabMitraPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                } else {
                    idType = "mitra_image";
                    //Dialog
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_gallery);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View view = inflater.inflate(R.layout.dialog_gallery, null);
                    TextView tvCamera, tvGallery;
                    tvCamera = (TextView) view.findViewById(R.id.tv_camera);
                    tvGallery = (TextView) view.findViewById(R.id.tv_gallery);
                    dialog.setContentView(view);

                    tvCamera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        /*Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivityForResult(takePicture, TAKE_PICTURE);
                        }*/
                            startIntent(false);
                            dialog.dismiss();
                        }
                    });

                    tvGallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                }
            }
        });

        // fabVoterId Click Listener ...................
        fabVoterId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,};
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                } else {
                    idType = "voter_image";
                    //Dialog
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_gallery);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View view = inflater.inflate(R.layout.dialog_gallery, null);
                    TextView tvCamera, tvGallery;
                    tvCamera = (TextView) view.findViewById(R.id.tv_camera);
                    tvGallery = (TextView) view.findViewById(R.id.tv_gallery);
                    dialog.setContentView(view);

                    tvCamera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        /*Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivityForResult(takePicture, TAKE_PICTURE);
                        }*/
                            startIntent(false);
                            dialog.dismiss();
                        }
                    });

                    tvGallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                }
            }
        });

        // fabVoterId Click Listener ...................
        fabVoterId_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,};
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                } else {
                    idType = "voter_image_back";
                    //Dialog
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_gallery);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View view = inflater.inflate(R.layout.dialog_gallery, null);
                    TextView tvCamera, tvGallery;
                    tvCamera = (TextView) view.findViewById(R.id.tv_camera);
                    tvGallery = (TextView) view.findViewById(R.id.tv_gallery);
                    dialog.setContentView(view);

                    tvCamera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        /*Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivityForResult(takePicture, TAKE_PICTURE);
                        }*/
                            startIntent(false);
                            dialog.dismiss();
                        }
                    });

                    tvGallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                            dialog.dismiss();

                        }
                    });
                    dialog.show();
                }
            }
        });

        // Upload Image set ...............

        // Log.d("=====StorePrefs======", "" + baseUrlForImage + StorePrefs.getDefaults(AadhaarImage, getActivity()));

        // Back Button CLick listener.................

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_cp, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem myActionMenuItem = menu.findItem(R.id.changePassword);

        myActionMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.changePassword:
                        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                                R.anim.enter_from_right, R.anim.exit_to_left);
                        fragmentTransaction.replace(R.id.container_body, new Fragment_ChangePassword()).commit();
                }

                return false;
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                } else {
                    try {
                        Bitmap bitmap;
                        InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                        bitmap = BitmapFactory.decodeStream(inputStream);
                        File filesDir = getActivity().getFilesDir();
                        //create a file to write bitmap data
                        File file = new File(filesDir, "Shree_" + System.currentTimeMillis() + ".jpg");
                        file.createNewFile();
                        //Convert bitmap to byte array
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85 /*ignored for PNG*/, bos);
                        byte[] bitmapdata = bos.toByteArray();
                        //write the bytes in file
                        FileOutputStream fos = new FileOutputStream(file);
                        fos.write(bitmapdata);
                        fos.flush();
                        fos.close();
                        Log.d("-----------------", "" + file.getAbsolutePath());

                        new PostClassProfile_ImageUpdate(getActivity(), file.getAbsolutePath()).execute();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (requestCode == TAKE_PICTURE) {

            if (resultCode == Activity.RESULT_OK) {
                try {

                    if (!hasPermissions(getActivity(), PERMISSIONS)) {
                        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                    } else {
                        new ImageCompression().execute(imageFilePath);
                    }

                      /* // Log.d("camera","=====filepath========"+filepath);
                    Bundle extras = data.getExtras();
                    Bitmap bitmap = (Bitmap) extras.get("data");
                    File filesDir = getActivity().getFilesDir();

                    //create a file to write bitmap data
                    File file = new File(filesDir, "Shree_" + System.currentTimeMillis() + ".jpg");
                    file.createNewFile();

                    //Convert bitmap to byte array
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100 *//*ignored for PNG*//*, bos);
                    byte[] bitmapdata = bos.toByteArray();
                    .
                    //write the bytes in file
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                    Log.d("-----------------", "" + file.getAbsolutePath());*/

                    // new PostClassProfile_ImageUpdate(getActivity(),imageFilePath).execute();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    //Check Permission ....................
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, TAKE_PICTURE);
                }
                else {
                    //checkSelfPermission(Manifest.permission.READ_CONTACTS);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    class PostClassProfile_ImageUpdate extends AsyncTask<String, Void, String> {
        Context context;
        String Id;
        String Service_id;
        ProgressDialog progress;
        JSONObject jObject ;
        String Files;
        String jsonreplyMsg = "";

        public PostClassProfile_ImageUpdate(Context mcontext, String Files) {
            this.context = mcontext;
            this.Files = Files;

        }

        protected void onPreExecute() {
            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            jObject = new JSONObject();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();


                    client.setConnectTimeout(25, TimeUnit.SECONDS);
                    client.setReadTimeout(25, TimeUnit.SECONDS);

                    MultipartBuilder buildernew = new MultipartBuilder().type(MultipartBuilder.FORM);
                    String path = Files.replaceAll(" ", "%20");
                    File uploadFile = new File(path);
                    String name1 = uploadFile.getName();
                    System.out.println("=========FILE===============" + uploadFile);
                    System.out.println("=========idType===============" + idType);

                    RequestBody requestBody = null;
                    if (uploadFile.exists()) {
                        final MediaType MEDIA_TYPE_PNG = MediaType.parse("application/pdf");
                        requestBody = new MultipartBuilder()
                                .type(MultipartBuilder.FORM)
                                .addFormDataPart(idType, uploadFile.getName(),
                                        RequestBody.create(MEDIA_TYPE_PNG, uploadFile))
                                //.addFormDataPart("page_module", MODULE_MITRAS)
                                .build();
                    }

                    Request request = new Request.Builder()
                            .url(UrlForDocuments)
                            .post(requestBody)
                            .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                            .addHeader("token", getDefaults("token", context))
                            .addHeader("page_module", MODULE_MITRAS)
                           // .addHeader("action", ACTION_VIEW)
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("Postman-Token", "2c126362-1839-818d-1b67-d0b1eb53719e")
                            .build();

                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    System.out.println("======UrlForDocuments=========" + UrlForDocuments);
                    jObject = new JSONObject(reqBody);
                    try {

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            /*String ImgUrl = jObject.getString("image");
                            StorePrefs.setDefaults("IMAGE", ImgUrl, context);*/
                            jsonreplyMsg = jObject.getString("replyMsg");

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                /*((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });*/
                return Utils.Please_Check_Internet_Connection;

            }
            return null;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                //Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();

                try {
                    if (jObject.getString("replyMsg") != null) {
                        if (jObject.has("replyMsg")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(
                                    context);
                            alert.setTitle(jObject.getString("replyMsg"));
                            alert.setCancelable(false);
                            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            alert.show();
                        }
                    }
                    else {
                        Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return;
            } else if (s.equals("success")) {
                Toast.makeText(context, jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();
                /*Intent intent = getActivity().getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);*/

                try {
                    if (jObject.has("image_type") || jObject.has("filepath")) {
                        if (jObject.getString("image_type").equalsIgnoreCase("mitra_image")) {

                            CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getActivity());
                            //circularProgressDrawable.strokeWidth = 5f;
                            circularProgressDrawable.setStrokeWidth(5f) ;
                            //circularProgressDrawable.centerRadius = 30f;
                            circularProgressDrawable.setCenterRadius(30f);
                            circularProgressDrawable.start();

                            Log.e("Mitra image url","Response"+baseUrlForImage + jObject.getString("filepath"));
                           /* Glide.with(getActivity())
                                    .load(baseUrlForImage + jObject.getString("filepath"))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img0);*/

                            Glide.with(getActivity())
                                    // .load(baseUrlForImage + mitraImage)
                                    .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + jObject.getString("filepath")))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img0);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (jObject.has("image_type") || jObject.has("filepath")) {
                        if (jObject.getString("image_type").equalsIgnoreCase("adhaar_image")) {

                            /*Glide.with(getActivity())
                                    .load(baseUrlForImage + jObject.getString("filepath"))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img1);*/

                            Glide.with(getActivity())
                                    // .load(baseUrlForImage + mitraImage)
                                    .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + jObject.getString("filepath")))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img1);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (jObject.has("image_type") || jObject.has("filepath")) {
                        if (jObject.getString("image_type").equalsIgnoreCase("adhaar_image_back")) {

                           /* Glide.with(getActivity())
                                    .load(baseUrlForImage + jObject.getString("filepath"))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img1_back);*/

                            Glide.with(getActivity())
                                    // .load(baseUrlForImage + mitraImage)
                                    .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + jObject.getString("filepath")))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img1_back);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (jObject.has("image_type") || jObject.has("filepath")) {
                        if (jObject.getString("image_type").equalsIgnoreCase("pan_image")) {

                            /*Glide.with(getActivity())
                                    .load(baseUrlForImage + jObject.getString("filepath"))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img2);*/

                            Glide.with(getActivity())
                                    // .load(baseUrlForImage + mitraImage)
                                    .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + jObject.getString("filepath")))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img2);

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (jObject.has("image_type") || jObject.has("filepath")) {
                        if (jObject.getString("image_type").equalsIgnoreCase("driving_licence_image")) {

                           /* Glide.with(getActivity())
                                    .load(baseUrlForImage + jObject.getString("filepath"))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img3);*/


                            Glide.with(getActivity())
                                    // .load(baseUrlForImage + mitraImage)
                                    .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + jObject.getString("filepath")))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img3);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (jObject.has("image_type") || jObject.has("filepath")) {
                        if (jObject.getString("image_type").equalsIgnoreCase("voter_image")) {

                           /* Glide.with(getActivity())
                                    .load(baseUrlForImage + jObject.getString("filepath"))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img4);*/

                            Glide.with(getActivity())
                                    // .load(baseUrlForImage + mitraImage)
                                    .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + jObject.getString("filepath")))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img4);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    if (jObject.has("image_type") || jObject.has("filepath")) {
                        if (jObject.getString("image_type").equalsIgnoreCase("voter_image_back")) {

                            /*Glide.with(getActivity())
                                    .load(baseUrlForImage + jObject.getString("filepath"))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img4_back);*/

                            Glide.with(getActivity())
                                    // .load(baseUrlForImage + mitraImage)
                                    .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + jObject.getString("filepath")))
                                    .thumbnail(1.0f)
                                    .placeholder(R.drawable.profile_icon).dontAnimate()
                                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                                    .crossFade()
                                    .centerCrop()
                                    .into(prof_img4_back);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else if (s.equalsIgnoreCase(Utils.Please_Check_Internet_Connection)) {
                progress.dismiss();
                CheckInternet(getContext());
            }


            else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

    public class PostClassLoadImage extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;

        public PostClassLoadImage(Context c) {
            this.context = c;


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                   /* try {
                        jsonObject.put("page_module", MODULE_MITRAS);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());*/

                    RequestBody body = RequestBody.create(null, new byte[]{});

                    Request request = new Request.Builder()
                            .post(body)
                            .url(MitraDocuments)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_MITRAS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===MitraDocuments===", "=======" + MitraDocuments);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            JSONObject jsonData = jObject.getJSONObject("data");

                            System.out.println("ADHAAR_IMAGE===============" + jsonData.getString("ADHAAR_IMAGE"));
                            System.out.println("ADHAAR_IMAGE===============" + jsonData.getString("ADHAAR_IMAGE_BACK"));
                            System.out.println("VOTER_IMAGE===============" + jsonData.getString("VOTER_IMAGE"));
                            System.out.println("DRIVING_LICENCE_IMAGE===============" + jsonData.getString("DRIVING_LICENCE_IMAGE"));
                            System.out.println("PAN_IMAGE===============" + jsonData.getString("PAN_IMAGE"));

                            if (jsonData != null) {
                                if (jsonData.has("ADHAAR_IMAGE")) {
                                    StorePrefs.setDefaults(AadhaarImage, jsonData.getString("ADHAAR_IMAGE"), getActivity());
                                }
                                if (jsonData.has("ADHAAR_IMAGE_BACK")) {
                                    StorePrefs.setDefaults(AadhaarImage_back, jsonData.getString("ADHAAR_IMAGE_BACK"), getActivity());
                                }
                                if (jsonData.has("VOTER_IMAGE")) {
                                    StorePrefs.setDefaults(VoterId, jsonData.getString("VOTER_IMAGE"), getActivity());
                                }
                                if (jsonData.has("DRIVING_LICENCE_IMAGE")) {
                                    StorePrefs.setDefaults(DrivingLicence, jsonData.getString("DRIVING_LICENCE_IMAGE"), getActivity());
                                }
                                if (jsonData.has("PAN_IMAGE")) {
                                    StorePrefs.setDefaults(PanCard, jsonData.getString("PAN_IMAGE"), getActivity());
                                }
                            }
                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyCode");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                // Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();

                android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(
                        context);
                try {
                    alert.setTitle(jObject.getString("replyMsg"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                alert.setCancelable(false);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alert.show();

                return;
            } else if (s.equals("success")) {


            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

    public void setImage(String mitraImage,String aadhaarImage,String aadhaarImage_back, String panImage, String DrivingImage,String voterId, String voterId_back){

        Log.e("mitraImage",baseUrlForImage + mitraImage);
        Log.e("aadhaarImage",baseUrlForImage + aadhaarImage);
        Log.e("aadhaarImage_back",baseUrlForImage + aadhaarImage_back);
        Log.e("panImage",baseUrlForImage + panImage);
        Log.e("DrivingImage",baseUrlForImage + DrivingImage);
        Log.e("voterId",baseUrlForImage + voterId);
        Log.e("voterId_back",baseUrlForImage + voterId_back);


     /*   Glide.with(getActivity()).load("http://api.shreecementltd.com/uploads/mitras/1026/mitra_image.jpg")
                .thumbnail(1.0f)
                .crossFade()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(prof_img0);*/

       // HeadersClass headers = new LazyHeaders().addHeader("token", StorePrefs.getDefaults("token", getActivity()));

       /* LazyHeaders.Builder builder = new LazyHeaders.Builder()
                .addHeader("token", StorePrefs.getDefaults("token", getActivity()));
        GlideUrl glideUrl = new GlideUrl(mitraImage, builder.build());
*/

        Glide.with(getActivity())
               // .load(baseUrlForImage + mitraImage)
               .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + mitraImage))
                .thumbnail(1.0f)
                .placeholder(R.drawable.profile_icon).dontAnimate()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .crossFade()
                .centerCrop()
                .into(prof_img0);

        Glide.with(getActivity())
               // .load(baseUrlForImage + aadhaarImage)
                .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + aadhaarImage))
                .thumbnail(1.0f)
                .placeholder(R.drawable.profile_icon).dontAnimate()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .crossFade()
                .centerCrop()
                .into(prof_img1);

        Glide.with(getActivity())
                // .load(baseUrlForImage + aadhaarImage)
                .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + aadhaarImage_back))
                .thumbnail(1.0f)
                .placeholder(R.drawable.profile_icon).dontAnimate()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .crossFade()
                .centerCrop()
                .into(prof_img1_back);

        Glide.with(getActivity())
               // .load(baseUrlForImage + panImage)
                .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + panImage))
                .thumbnail(1.0f)
                .placeholder(R.drawable.profile_icon).dontAnimate()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .crossFade()
                .centerCrop()
                .into(prof_img2);

        Glide.with(getActivity())
                //.load(baseUrlForImage + DrivingImage)
                .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + DrivingImage))
                .thumbnail(1.0f)
                .placeholder(R.drawable.profile_icon).dontAnimate()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .crossFade()
                .centerCrop()
                .into(prof_img3);


        Glide.with(getActivity())
               // .load(baseUrlForImage + voterId)
                .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + voterId))
                .thumbnail(1.0f)
                .placeholder(R.drawable.profile_icon).dontAnimate()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .crossFade()
                .centerCrop()
                .into(prof_img4);

        Glide.with(getActivity())
                // .load(baseUrlForImage + voterId)
                .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + voterId_back))
                .thumbnail(1.0f)
                .placeholder(R.drawable.profile_icon).dontAnimate()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .crossFade()
                .centerCrop()
                .into(prof_img4_back);

    }

    public class ImageCompression extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            if (strings.length == 0 || strings[0] == null)
                return null;

            return CommonUtils.compressImage(strings[0]);
        }

        protected void onPostExecute(String imagePath) {
            // imagePath is path of new compressed image.
           // prof_img1.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));

            new PostClassProfile_ImageUpdate(getActivity(), new File(imagePath).getAbsolutePath()).execute();

            if (idType.equalsIgnoreCase("mitra_image")) {
                prof_img0.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));
            }

            if (idType.equalsIgnoreCase("adhaar_image")) {
                prof_img1.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));
            }

            if (idType.equalsIgnoreCase("adhaar_image_back")) {
                prof_img1_back.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));
            }

            if (idType.equalsIgnoreCase("pan_image")) {
                prof_img2.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));
            }

            if (idType.equalsIgnoreCase("driving_licence_image")) {
                prof_img3.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));
            }

            if (idType.equalsIgnoreCase("voter_image")) {
                prof_img4.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));
            }
            if (idType.equalsIgnoreCase("voter_image_back")) {
                prof_img4_back.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));
            }
        }
    }

    public void startIntent(boolean isFromGallery) {
        if (!isFromGallery) {

            File imageFile = new File(imageFilePath);
            Uri imageFileUri = Uri.fromFile(imageFile); // convert path to Uri
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);   // set the image file name
            startActivityForResult(intent, TAKE_PICTURE);

        } else if (isFromGallery) {
            File imageFile = new File(imageFilePath);
            Uri imageFileUri = Uri.fromFile(imageFile); // convert path to Uri
            Intent intent = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileUri);   // set the image file name
            startActivityForResult(
                    Intent.createChooser(intent, "Select File"),
                    PICK_IMAGE);
        }
    }

    public void showImageDialog(String txtDesc, String urlOfImage) {
        Log.e("Desc : -->> ",txtDesc);
        Log.e("urlOfImage : -->> ",urlOfImage);

        dialog.getWindow();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_with_imageview);
        dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.WRAP_CONTENT;
        Constant.hideKeyboard(getActivity());

      final   ProgressBar progressBar = (ProgressBar)dialog.findViewById(R.id.spin_kit);
         CubeGrid wave = new CubeGrid();
        progressBar.setIndeterminateDrawable(wave);


        TextView txtHeading;
        Button btnCancelDialogTop, btnDialogOk;
        ImageView imageView;

        btnCancelDialogTop = (Button) dialog.findViewById(R.id.btnCancelDialogTop);
        btnDialogOk = (Button) dialog.findViewById(R.id.btnDialogOk);
        txtHeading = dialog.findViewById(R.id.txtHeading);
        imageView = dialog.findViewById(R.id.ImageToPriview1);
        txtHeading.setText(txtDesc);

        /*Uri imageUri = Uri.parse(baseUrlForImage + urlOfImage);
        Log.e("imageUri",""+imageUri);
        SimpleDraweeView draweeView = (SimpleDraweeView) dialog.findViewById(R.id.ImageToPriview);
        draweeView.setImageURI(imageUri);*/

       /* Glide.with(getActivity())
                .load(baseUrlForImage + urlOfImage)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target,
                                                   boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageView);*/

        Glide.with(getActivity())
               // .load(baseUrlForImage + urlOfImage)
                .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + urlOfImage))
                .into(imageView);

        dialog.show();

        btnCancelDialogTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        btnDialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });

    }

   /* static class HeadersClass {

       // private static final String AUTHORIZATION = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzaWQiOjI0MDcsImlhdCI6MTUyNzU5NTQzOCwiZXhwIjoxNTI4NDU5NDM4fQ.tJdQ1tdrQrpuJJ-OS9lzlEUbWhOGqJMDE-vUV2eaCyA";


        static GlideUrl getUrlWithHeaders(String url){
            return new GlideUrl(url, new LazyHeaders.Builder()
                    .addHeader("token", Constant.token_from_server)
                    .build());
        }
    }*/


  public void  hideFabButton(){

      fabAadhaar.setVisibility(View.GONE);
      fabAadhaar_back.setVisibility(View.GONE);
      fabPanCard.setVisibility(View.GONE);
      fabDrivingLicence.setVisibility(View.GONE);
      fabVoterId.setVisibility(View.GONE);
      fabVoterId_back.setVisibility(View.GONE);
      fabMitraPicture.setVisibility(View.GONE);

  }

}



