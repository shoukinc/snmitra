package com.shreecement.shree.Fragment.Referral.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shreecement.shree.Fragment.Referral.Bean.BeanReferral;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TIA on 12-08-2016.
 */

public class Referral_Adapter extends RecyclerView.Adapter<Referral_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanReferral> rowItems;

    public Referral_Adapter(Context context, ArrayList<BeanReferral> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_mitra_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        BeanReferral beanReferral = rowItems.get(position);
        JSONObject jsonObject = beanReferral.getJsonObject();

        try {
            holder.tvMobileNumber.setText(Constant.nullCheckFunction(jsonObject.getString("mobile_number")));
            holder.tvName.setText(Constant.nullCheckFunction(jsonObject.getString("referral_name")).toUpperCase());
            holder.tvEmail.setText(Constant.nullCheckFunction(jsonObject.getString("comments")));
            holder.tvFirst.setText(Constant.nullCheckFunction(jsonObject.getString("referral_name")).substring(0,1).toUpperCase());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.tvFirst.setBackgroundResource(R.drawable.rounder_date_red_image);

    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public void add(List<BeanReferral> items) {
        int previousDataSize = this.rowItems.size();
        this.rowItems.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvMobileNumber, tvName, tvFirst ,tvEmail;

        ViewHolder(View convertView) {
            super(convertView);
            convertView.setOnClickListener(this);

            tvMobileNumber =  convertView.findViewById(R.id.tvMobileNumber);
            tvName =  convertView.findViewById(R.id.tvMitraName);
            tvEmail =  convertView.findViewById(R.id.tvMitraEmail);
            tvFirst =  convertView.findViewById(R.id.tvFirst);

        }

        @Override
        public void onClick(View v) {



            /*BeanMitra beanMitra_item = rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanMitra_item);
            StorePrefs.setDefaults(PREF_BEAN_MITRA_ITEM, json, context);

            Fragment_Mitra_Details fragment = new Fragment_Mitra_Details();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            ((MainActivity) context).showUpButton(true);*/


        }


    }
}
