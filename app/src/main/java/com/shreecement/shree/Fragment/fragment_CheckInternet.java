package com.shreecement.shree.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

import static com.shreecement.shree.Utlility.Constant.PREF_DESCRIPTION;
import static com.shreecement.shree.Utlility.Constant.PREF_FACEBOOk;
import static com.shreecement.shree.Utlility.Constant.PREF_TWITTER;
import static com.shreecement.shree.Utlility.Constant.PREF_YOUTUBE;


/**
 * Created by Choudhary on 6/29/2017.
 */

public class fragment_CheckInternet extends Fragment {

    TextView tvheader;
    TextView btn_tryAgain;
    String Internet="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_check_internet_connection, container, false);

        tvheader=view.findViewById(R.id.tvheader);
        btn_tryAgain=view.findViewById(R.id.btn_tryAgain);
        Internet="Oops!\n" +
                "No Internet";
        tvheader.setText(Internet);
        btn_tryAgain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();

                if (netInfo != null && netInfo.isConnected()) {
                    getActivity().onBackPressed();
                }
            }
        });


        return view;
    }








}