package com.shreecement.shree.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.ChangePasswordUrl;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_ChangePassword extends Fragment implements View.OnClickListener{
    Button btnSend, btnUpdatePhone;

    EditText currentPassword, newPassword, confirmPassword;
    String currpassword = "";
    String newpassword = "";
    String confirmpassword = "";
    String user_mail = "";
    View view;
    Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_change_password, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getActivity().setTitle(getActivity().getResources().getString(R.string.change_password));
        dialog = new Dialog(getActivity());

        btnSend = (Button) view.findViewById(R.id.btnSend);
        btnUpdatePhone = (Button) view.findViewById(R.id.btnUpdatePhone);
        currentPassword = (EditText) view.findViewById(R.id.et_currentPassword);
        newPassword = (EditText) view.findViewById(R.id.ed_newPassword);
        confirmPassword = (EditText) view.findViewById(R.id.ed_confirmPassword);

        btnUpdatePhone.setOnClickListener(this);


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newpassword = newPassword.getText().toString();
                if (currentPassword.getText().toString().length() == 0) {

                    Toast.makeText(getActivity(), "Current Password field is required!", Toast.LENGTH_LONG).show();
                } else if (newPassword.getText().toString().length() == 0) {

                    Toast.makeText(getActivity(), "New password length at least 6 ", Toast.LENGTH_LONG).show();

                } else if (newPassword.getText().toString().length() <= 5) {

                    Toast.makeText(getActivity(), "Enter password length at least 6 ", Toast.LENGTH_LONG).show();

                } else if (newPassword.getText().toString().length() == 0) {

                    Toast.makeText(getActivity(), "New password field is required! ", Toast.LENGTH_LONG).show();

                } else if (!confirmPassword.getText().toString().trim().equals(newpassword)) {
                    Toast.makeText(getActivity(), "Conform password is doesn't match ", Toast.LENGTH_SHORT).show();
                } else {
                    currpassword = currentPassword.getText().toString();
                    confirmpassword = confirmPassword.getText().toString();
                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    user_mail = StorePrefs.getDefaults("EMAIL", getActivity());

                    new PostClass_ChangePassword(getActivity(), currpassword, newpassword, confirmpassword, user_mail).execute();

                }

            }
        });

        return view;
    }

    @Override
    public void onClick(View view) {
        if (view == btnUpdatePhone){

            dialog.getWindow();

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_with_edittext_and_button);
            dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

            TextView txtHeading;
            Button btnCancelDialogTop , btnDialogOk;
            final EditText edInputField, edInputField2;

            btnCancelDialogTop = (Button) dialog.findViewById(R.id.btnCancelDialogTop);
            btnDialogOk = (Button) dialog.findViewById(R.id.btnDialogOk);
            txtHeading  = dialog.findViewById(R.id.txtHeading);
            edInputField  = dialog.findViewById(R.id.edInputField);
            edInputField2  = dialog.findViewById(R.id.edInputField2);

            txtHeading.setText(R.string.update_phone_number);
            btnDialogOk.setText(R.string.submit);

            edInputField.setHint(R.string.enterotp);
            edInputField2.setVisibility(View.VISIBLE);
            edInputField2.setHint(R.string.new_phone);

            int maxLength = 10;
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            edInputField.setFilters(FilterArray);

            dialog.show();

            btnCancelDialogTop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();

                }
            });
            btnDialogOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(edInputField.getText().toString().length() == 0 || edInputField.getText().toString().length() < 4) {

                        Toast.makeText(getActivity(), "Please input valid otp", Toast.LENGTH_SHORT).show();
                    }
                   else if(edInputField2.getText().toString().length() == 0 || edInputField2.getText().toString().length() < 10) {

                        Toast.makeText(getActivity(), "Please input valid Number", Toast.LENGTH_SHORT).show();
                    }
                    else {
                      // new  PostClassCheckAddhaarPhoneValidate(getActivity(),edInputField.getText().toString().trim(),"adhaar").execute();

                    }

                }
            });

        }
    }


    class PostClass_ChangePassword extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String Curr_password = "";
        String Password = "";
        String Cpassword;
        String User_mail;
        String jsonreplyMsg;


        public PostClass_ChangePassword(Context c, String curr_password, String password, String cpassword, String user_mail) {
            this.context = c;
            this.Curr_password = curr_password;
            this.Password = password;
            this.Cpassword = cpassword;
            this.User_mail = user_mail;
            Log.d("===Curr_password===", Curr_password);
            Log.d("===Password===", Password);
            Log.d("===Cpassword===", Cpassword);
            Log.d("===user_mail===", User_mail);


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {

                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("curr_password", Curr_password);
                    jsonObject.put("password", newpassword);
                    jsonObject.put("cpassword", confirmpassword);
                    jsonObject.put("user_mail", user_mail);
                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());

                    Request request = new Request.Builder()
                            .url(ChangePasswordUrl)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .addHeader("page_module", MODULE_COMMON)
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======ChangePasswordUrl=========" + ChangePasswordUrl);

                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {

                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                Toast.makeText(context, jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();
                Fragment fragment = new Fragment_Dashboard();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

}
