package com.shreecement.shree.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.shreecement.shree.ModalClass.BeanDeport;
import com.shreecement.shree.ModalClass.BeanReceiptDetail;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.Deport;
import static com.shreecement.shree.Utlility.Constant.MODULE_RECEIPTS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Material_Receipt_View extends Fragment {
    TextView to, truckNumber, diNumber, diQuantity, packingType, date, shortQuantity, from,
            cutQuantity, product, damagedQuantity, egpNumber, transporter, totalQuantity, bagType,
            receivedQuantity;
    BeanReceiptDetail beanReceiptDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_material_receipt_view, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getActivity().setTitle(getActivity().getString(R.string.material_receipt_view));

        to = view.findViewById(R.id.to);
        truckNumber = view.findViewById(R.id.truckNumber);
        diNumber = view.findViewById(R.id.diNumber);
        diQuantity = view.findViewById(R.id.diQuantity);
        packingType = view.findViewById(R.id.packingType);
        date = view.findViewById(R.id.date);
        shortQuantity = view.findViewById(R.id.shortQuantity);
        from = view.findViewById(R.id.from);
        cutQuantity = view.findViewById(R.id.cutQuantity);
        product = view.findViewById(R.id.product);
        damagedQuantity = view.findViewById(R.id.damagedQuantity);
        egpNumber = view.findViewById(R.id.egpNumber);
        transporter = view.findViewById(R.id.transporter);
        totalQuantity = view.findViewById(R.id.totalQuantity);
        bagType = view.findViewById(R.id.bagType);
        receivedQuantity = view.findViewById(R.id.receivedQuantity);


        Gson gson = new Gson();
        String json = StorePrefs.getDefaults("receiptDetail", getActivity());
        beanReceiptDetail = gson.fromJson(json, BeanReceiptDetail.class);

        truckNumber.setText(beanReceiptDetail.getTruck_no());
        diNumber.setText(beanReceiptDetail.getDelivery_id());

        int totalQty = Integer.parseInt(beanReceiptDetail.getAccept_qty()) +
                Integer.parseInt(beanReceiptDetail.getShort_qty()) +
                Integer.parseInt(beanReceiptDetail.getCut_qty()) +
                Integer.parseInt(beanReceiptDetail.getDamage_qty());

        diQuantity.setText(String.valueOf(totalQty));
        packingType.setText(beanReceiptDetail.getPacking_type());

        shortQuantity.setText(beanReceiptDetail.getShort_qty());


        Log.d("--date1--", "======" + beanReceiptDetail.getReceipt_date());
        DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        Date date1 = null;
        Calendar cal = Calendar.getInstance();
        try {
            date1 = readFormat1.parse(beanReceiptDetail.getReceipt_date());
            String dateformatt = formatter1.format(date1);

            date.setText(dateformatt);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        DateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        Date date = null;
        Calendar cal1 = Calendar.getInstance();
        try {
            date = readFormat.parse(beanReceiptDetail.getCreation_date());
            String dateformat = formatter.format(date);
            Log.d("--dateformat--", "======" + beanReceiptDetail.getCreation_date());
            from.setText(dateformat);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        cutQuantity.setText(beanReceiptDetail.getCut_qty());
        product.setText(beanReceiptDetail.getProduct());
        damagedQuantity.setText(beanReceiptDetail.getDamage_qty());
        egpNumber.setText(beanReceiptDetail.getEgp_no());
        transporter.setText(beanReceiptDetail.getTransporter());
        totalQuantity.setText(String.valueOf(totalQty));
        bagType.setText(beanReceiptDetail.getBag_type());
        receivedQuantity.setText(beanReceiptDetail.getAccept_qty());

        new PostClassDeport(getContext()).execute();
        return view;
    }


    class PostClassDeport extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";
        ArrayList<BeanDeport> tempDeportList = new ArrayList<>();

        PostClassDeport(Context c) {
            this.context = c;

            tempDeportList.clear();


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");

                    JSONObject jsonObject = new JSONObject();

                  /*  try {
                        jsonObject.put("page_module", MODULE_RECEIPTS);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    RequestBody body = RequestBody.create(mediaType,  jsonObject.toString());*/
                    RequestBody body = RequestBody.create(null, new byte[]{});

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Deport)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_RECEIPTS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===Deport===", "=======" + Deport);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONArray jsonData = jObject.getJSONArray("deportOrganizationData");

                            for (int i = 0; i < jsonData.length(); i++) {
                                JSONObject jCustomer = jsonData.getJSONObject(i);
                                tempDeportList.add(new BeanDeport(jCustomer.getString("name"),
                                        jCustomer.getString("organization_id")));
                            }

                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                to.setText(tempDeportList.get(0).getName());

            }

        }

    }
}
