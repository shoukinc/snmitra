package com.shreecement.shree.Fragment.Dealers.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Dealers.Bean.BeanDealer_Item;
import com.shreecement.shree.Fragment.Dealers.Fragment_Dealer_Tab;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import java.util.ArrayList;


/**
 * Created by TIA on 12-08-2016.
 */

public class Dealers_Adapter extends RecyclerView.Adapter<Dealers_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanDealer_Item> rowItems;

    public Dealers_Adapter(Context context, ArrayList<BeanDealer_Item> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_dealer_row_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String rowItem = rowItems.get(position).getParty_name();

       // holder.tvCustomerId.setText(rowItems.get(position).getAccount_number());
        holder.tvDealerName.setText(rowItem+"  ("+rowItems.get(position).getAccount_number()+")");
        holder.tvDealer.setText(rowItems.get(position).getAddress());
        String s=rowItem.substring(0,1);
        holder.tvFirst.setText(s);
        holder.tvFirst.setText(s);
        holder.tvFirst.setBackgroundResource(R.drawable.rounder_dateimage_red);


    }

    @Override
    public int getItemCount() {
        return rowItems.size();

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvDealerName,tvDealer,tvDealerId;
        TextView tvFirst;
        ViewHolder(View convertView) {
            super(convertView);
            tvDealer = convertView.findViewById(R.id.tvDealer);
            tvDealerId = convertView.findViewById(R.id.tvDealerId);
            tvFirst = convertView.findViewById(R.id.tvFirst);
            tvDealerName = convertView.findViewById(R.id.tvDealerName);
            tvDealerId.setVisibility(View.GONE);
            convertView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {

            StorePrefs.setDefaults("customer_code",rowItems.get(getPosition()).getParty_id(),context);
            BeanDealer_Item beanDealer_item = rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanDealer_item);
            StorePrefs.setDefaults("beanDealer_item", json, context);
            Fragment_Dealer_Tab fragment = new Fragment_Dealer_Tab();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            ((MainActivity) context).showUpButton(true);

        }
    }


}