package com.shreecement.shree.Fragment.Events;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Fragment.Events.BeanEvents.BeanEvents;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

/*
 * Created by Choudhary on 11-04-2018.
 */

public class Fragment_Events_Details extends Fragment {


    TextView tv_event_title,tv_event_number,tv_event_type_id,
            tv_location_id,tv_short_description,tv_long_description,tv_start_date;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragmentevent_details, container, false);
        tv_event_title=view.findViewById(R.id.tv_event_title);

        tv_event_number=view.findViewById(R.id.tv_event_number);
        tv_event_type_id=view.findViewById(R.id.tv_event_type_id);
        tv_location_id=view.findViewById(R.id.tv_location_id);
        tv_short_description=view.findViewById(R.id.tv_short_description);
        tv_long_description=view.findViewById(R.id.tv_long_description);
        tv_start_date=view.findViewById(R.id.tv_start_date);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setHasOptionsMenu(true);
        getActivity().setTitle(getActivity().getString(R.string.eventsDetails));


        Gson gson = new Gson();
        String json = StorePrefs.getDefaults("beanEvents", getActivity());
        BeanEvents beanEvents = gson.fromJson(json, BeanEvents.class);

        tv_event_title.setText(beanEvents.getTitle());
        tv_event_number.setText(beanEvents.getEvent_number());
        tv_event_type_id.setText(beanEvents.getEvent_type());
        tv_location_id.setText(beanEvents.getLocation_id());
        tv_short_description.setText(beanEvents.getShort_description());
        tv_long_description.setText(beanEvents.getLong_description());
        tv_start_date.setText(beanEvents.getStart_date());

        return view;
    }


}
