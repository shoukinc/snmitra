package com.shreecement.shree.Fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.shreecement.shree.ModalClass.BeanDeport;
import com.shreecement.shree.ModalClass.BeanDi;
import com.shreecement.shree.ModalClass.BeanReceiptDetail;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.Deport;
import static com.shreecement.shree.Utlility.Constant.Di;
import static com.shreecement.shree.Utlility.Constant.MODULE_RECEIPTS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;

/*
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Add_Material_Receipt_Entry extends Fragment {
    public String className = "Fragment_Add_Materila_Receipt_Entery";
    public SpinnerDeportAdapter spinnerAdapter;
    public TextView tvDeport;
    public ArrayList<BeanDi> DiList;
    public TextView tvDiLable, tvDi;
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog builder1;
    public ListView CategoryList;
    public SpinnerDIAdapter spinnerDIAdapter;
    Button btnNext;
    ArrayList<BeanDeport> deportList;
    TextView dialogTitle;
    String egpDate = "";

    String delivery_id = "";
    String egpNo = "";
    String addEdit = "";
    String diQty;
    String org_id = "";
    TextView tvtranspoter, tvTruck, tvPackingType, tvedpDate, tvProduct, tvBagtype, tvInvoice;

    public static Fragment_Add_Material_Receipt_Entry newInstance() {

        Fragment_Add_Material_Receipt_Entry f = new Fragment_Add_Material_Receipt_Entry();
        Bundle b = new Bundle();
        b.putString("edit", "edit");

        f.setArguments(b);

        return f;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            addEdit = bundle.getString("edit");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_material_receipt_entery, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        btnNext = view.findViewById(R.id.btnNext);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        tvDeport = view.findViewById(R.id.spnDeport);

        tvDiLable = view.findViewById(R.id.tvDiLable);
        readBundle(getArguments());

        tvtranspoter = view.findViewById(R.id.tvtranspoter);
        tvTruck = view.findViewById(R.id.tvTruck);
        tvPackingType = view.findViewById(R.id.tvPackingType);
        tvedpDate = view.findViewById(R.id.tvedpDate);
        tvProduct = view.findViewById(R.id.tvProduct);
        tvBagtype = view.findViewById(R.id.tvBagtype);
        tvInvoice = view.findViewById(R.id.tvInvoice);
        tvDi = view.findViewById(R.id.tvDi);

        tvtranspoter.setBackgroundColor(Color.parseColor("#e6e3e3"));
        tvTruck.setBackgroundColor(Color.parseColor("#e6e3e3"));
        tvPackingType.setBackgroundColor(Color.parseColor("#e6e3e3"));
        tvedpDate.setBackgroundColor(Color.parseColor("#e6e3e3"));
        tvProduct.setBackgroundColor(Color.parseColor("#e6e3e3"));
        tvBagtype.setBackgroundColor(Color.parseColor("#e6e3e3"));
        tvInvoice.setBackgroundColor(Color.parseColor("#e6e3e3"));

        deportList = new ArrayList<>();
        DiList = new ArrayList<BeanDi>();
        new PostClassDeport(getActivity()).execute();

        // On Click listener
        tvDeport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSpinnerDialog(deportList, "Deport", tvDeport);
            }
        });
        tvDi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSpinnerDIDialog(DiList, "DI", tvDi);
            }
        });

        if (addEdit.equals("edit")) {
            Gson gson = new Gson();
            String json = StorePrefs.getDefaults("receiptDetail", getActivity());
            BeanReceiptDetail beanReceiptDetail = gson.fromJson(json, BeanReceiptDetail.class);
            tvDi.setText(beanReceiptDetail.getDelivery_id());
            tvDeport.setText(beanReceiptDetail.getDestination_organization());
            tvtranspoter.setText(beanReceiptDetail.getTransporter());
            tvTruck.setText(beanReceiptDetail.getTruck_no());
            tvPackingType.setText(beanReceiptDetail.getPacking_type());

            Log.d("--date1--", "======" + beanReceiptDetail.getReceipt_date());
            DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
            Date date1 = null;
            Calendar cal = Calendar.getInstance();
            try {
                date1 = readFormat1.parse(beanReceiptDetail.getEgp_date());
                String dateformatt = formatter1.format(date1);

                tvedpDate.setText(dateformatt);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            tvProduct.setText(beanReceiptDetail.getProduct());
            tvBagtype.setText(beanReceiptDetail.getBag_type());
            tvInvoice.setText(beanReceiptDetail.getEgp_no());
            egpDate = beanReceiptDetail.getEgp_date();
            egpNo = beanReceiptDetail.getEgp_no();
            delivery_id = beanReceiptDetail.getDelivery_id();
            getActivity().setTitle(getActivity().getString(R.string.update_material_recepit_entry));


        } else {
            getActivity().setTitle(getActivity().getString(R.string.add_material_recepit_entry));
        }

        btnNext.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                if (tvDeport.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Deport field is required!", Toast.LENGTH_LONG).show();

                } else if (tvDi.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Di field is required!", Toast.LENGTH_LONG).show();

                } else if (tvtranspoter.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Transpoter field is required!", Toast.LENGTH_LONG).show();

                } else if (tvPackingType.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Packing Type field is required!", Toast.LENGTH_LONG).show();

                } else if (tvedpDate.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Egp field is required!", Toast.LENGTH_LONG).show();

                } else if (tvProduct.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Product field is required!", Toast.LENGTH_LONG).show();

                } else if (tvBagtype.getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Bag type field is required!", Toast.LENGTH_LONG).show();
                } else {
                    delivery_id = tvDi.getText().toString();
                    String transpoter = tvtranspoter.getText().toString();
                    String tuck = tvTruck.getText().toString();
                    String tvpackingType = tvPackingType.getText().toString();
                    String product = tvProduct.getText().toString();
                    String bagtype = tvBagtype.getText().toString();

                    Fragment_Add_Material_Receipt_Entry_2 fragment = new Fragment_Add_Material_Receipt_Entry_2();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                    Bundle bundle = new Bundle();
                    bundle.putString("transpoter", transpoter);
                    bundle.putString("tuck", tuck);
                    bundle.putString("tvpackingType", tvpackingType);
                    bundle.putString("product", product);
                    bundle.putString("bagtype", bagtype);
                    bundle.putString("egpDate", egpDate);
                    bundle.putString("egpNo", egpNo);
                    bundle.putString("delivery_id", delivery_id);
                    bundle.putString("addEdit", addEdit);
                    bundle.putString("diQty", diQty);
                    fragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }
            }
        });


        return view;
    }


    private void showSpinnerDialog(ArrayList<BeanDeport> list, String title, TextView spnType) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        CategoryList = (ListView) dialogView.findViewById(R.id.listCatecory);
        dialogTitle = (TextView) dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerAdapter = new SpinnerDeportAdapter(getActivity(), list, title);
        CategoryList.setAdapter(spinnerAdapter);
        builder1 = dialogBuilder.create();
        builder1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder1.show();
    }


    private void showSpinnerDIDialog(ArrayList<BeanDi> list, String title, TextView spnType) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        CategoryList = (ListView) dialogView.findViewById(R.id.listCatecory);
        dialogTitle = (TextView) dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerDIAdapter = new SpinnerDIAdapter(getActivity(), list, title);
        CategoryList.setAdapter(spinnerDIAdapter);
        builder1 = dialogBuilder.create();
        builder1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder1.show();
    }


    class PostClassDeport extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";
        ArrayList<BeanDeport> tempDeportList = new ArrayList<>();

        PostClassDeport(Context c) {
            this.context = c;

            tempDeportList.clear();
            deportList.clear();


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();

                   /* try {
                        jsonObject.put("page_module", MODULE_RECEIPTS);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    RequestBody body = RequestBody.create(mediaType,  jsonObject.toString());*/
                    RequestBody body = RequestBody.create(null, new byte[]{});

                    Request request = new Request.Builder()
                            .post(body)
                            .url(Deport)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_RECEIPTS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===Deport===", "=======" + Deport);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONArray jsonData = jObject.getJSONArray("deportOrganizationData");

                            for (int i = 0; i < jsonData.length(); i++) {
                                JSONObject jCustomer = jsonData.getJSONObject(i);
                                tempDeportList.add(new BeanDeport(jCustomer.getString("name"),
                                        jCustomer.getString("organization_id")));
                            }
                            deportList.addAll(tempDeportList);

                            Log.d("===deportList==", "=======" + deportList.size());
                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                if (!deportList.isEmpty()) {
                    if (addEdit.equals("edit")) {
                      /*  organization_id = deportList.get(0).getOrganization_id();
                        StorePrefs.setDefaults("organization_id", organization_id, context);
*/
                    }
                }

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }
    }

    public class SpinnerDeportAdapter extends BaseAdapter {
        public String code = "";
        Context context;
        List<BeanDeport> rowItems;
        String Title = "";


        public SpinnerDeportAdapter(Context context, List<BeanDeport> items, String title) {
            this.context = context;
            this.rowItems = items;
            this.Title = title;
            Log.d("===rowItems.zize===", "" + rowItems.size());


        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            SpinnerDeportAdapter.ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new SpinnerDeportAdapter.ViewHolder();
                holder.textName = (TextView) convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (SpinnerDeportAdapter.ViewHolder) convertView.getTag();

            }

            holder.textName.setText(rowItems.get(position).getName());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String meaning = rowItems.get(position).getName();
                    org_id = rowItems.get(position).getOrganization_id();
                    StorePrefs.setDefaults("organization_id", org_id, context);
                    tvDeport.setText(meaning);
                    builder1.dismiss();
                    new PostClassDi(context, org_id).execute();
                }
            });
            return convertView;
        }

        public String getCode() {
            return code;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;

        }

    }

    public class PostClassDi extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";
        String Organization_id = "";
        String Usertype = "";


        public PostClassDi(Context c, String organization_id) {
            this.context = c;
            this.Organization_id = organization_id;
            DiList.clear();


        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("organization_id", Organization_id);
                    //jsonObject.put("page_module", MODULE_RECEIPTS);

                    RequestBody body = RequestBody.create(mediaType, jsonObject.toString());
                    Request request = new Request.Builder()
                            .post(body)
                            .url(Di)

                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_RECEIPTS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===Di===", "=======" + Di);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            JSONArray jsonData = jObject.getJSONArray("diData");

                            for (int i = 0; i < jsonData.length(); i++) {
                                JSONObject jCustomer = jsonData.getJSONObject(i);
                                DiList.add(new BeanDi(jCustomer.getString("delivery_id"),
                                        jCustomer.getString("egp_no"), jCustomer.getString("egp_date"),
                                        jCustomer.getString("product"), jCustomer.getString("ship_quantity"),
                                        jCustomer.getString("packing_type"), jCustomer.getString("bag_type"), jCustomer.getString("truck_no"), jCustomer.getString("transporter")));

                            }
                            Log.d("===DiList==", "=======" + DiList.size());
                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyCode");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {


            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }


    }

    public class SpinnerDIAdapter extends BaseAdapter {
        public String code = "";
        Context context;
        List<BeanDi> rowItems;
        String Title = "";


        public SpinnerDIAdapter(Context context, List<BeanDi> items, String title) {
            this.context = context;
            this.rowItems = items;
            this.Title = title;
            Log.d("===rowItems.zize===", "" + rowItems.size());


        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            SpinnerDIAdapter.ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new SpinnerDIAdapter.ViewHolder();
                holder.textName = (TextView) convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (SpinnerDIAdapter.ViewHolder) convertView.getTag();

            }

            holder.textName.setText(rowItems.get(position).getDelivery_id());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvDi.setText(rowItems.get(position).getDelivery_id());
                    tvtranspoter.setText(rowItems.get(position).getTransporter());
                    tvTruck.setText(rowItems.get(position).getTruck_no());
                    tvPackingType.setText(rowItems.get(position).getPacking_type());


                    DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
                    Date date1 = null;
                    Calendar cal = Calendar.getInstance();
                    try {
                        date1 = readFormat1.parse(rowItems.get(position).getEgp_date());
                        String dateformatt = formatter1.format(date1);

                        tvedpDate.setText(dateformatt);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    tvProduct.setText(rowItems.get(position).getProduct());
                    tvBagtype.setText(rowItems.get(position).getBag_type());
                    egpDate = rowItems.get(position).getEgp_date();
                    delivery_id = rowItems.get(position).getDelivery_id();
                    egpNo = rowItems.get(position).getEgp_no();
                    diQty = rowItems.get(position).getShip_quantity();
                    tvBagtype.setText(rowItems.get(position).getBag_type());
                    tvInvoice.setText(rowItems.get(position).getEgp_no());
                    builder1.dismiss();
                }
            });
            return convertView;
        }

        public String getCode() {
            return code;
        }

        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;

        }
    }


}
