package com.shreecement.shree.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Adapter.MaterialReceipt_Adapter;
import com.shreecement.shree.ModalClass.BeanReceipt;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.MODULE_RECEIPTS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.receiptsURL;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Receipts extends Fragment {

    RecyclerView recyclerView;
    MaterialReceipt_Adapter materialReceipt_adapter;
    ArrayList<BeanReceipt> materiaList = new ArrayList<>();


    FloatingActionButton addMaterilView;
    String keyword = "";

    int page = 1;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView tv_no_record_found;
    TextView tv_count_item;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_material_receipt, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setHasOptionsMenu(true);
        tv_no_record_found = view.findViewById(R.id.tv_no_record_found);
        tv_count_item = view.findViewById(R.id.tv_count_item);

        addMaterilView = view.findViewById(R.id.addMaterilView);
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        getActivity().setTitle(getActivity().getString(R.string.material_receipt));

        addMaterilView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = new Fragment_Add_Material_Receipt_Entry();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                ((MainActivity) getActivity()).showUpButton(true);
            }
        });


        materialReceipt_adapter = new MaterialReceipt_Adapter(getActivity(), materiaList);
        recyclerView.setAdapter(materialReceipt_adapter);
        materialReceipt_adapter.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new PostClass_Receipt(getActivity(), "1", keyword, true).execute();
            }
        }, 450);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    //loadMoreItems();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });


        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();


        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                materiaList.clear();
                keyword = "";
                page = 1;
                new PostClass_Receipt(getActivity(), "1", "", true).execute();

                return false;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                keyword = query;
                materiaList.clear();
                new PostClass_Receipt(getActivity(), "", keyword, true).execute();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
               /* if (newText.length() == 0) {
                    keyword = "";
                    new PostClass_Receipt(getActivity(), "1", keyword, true).execute();
                }*/
                return true;
            }
        });

    }


    private void refreshItems() {

        page = 1;
        materiaList.clear();
        swipeRefreshLayout.setRefreshing(false);
        // Load items
        // ...
        new PostClass_Receipt(getActivity(), String.valueOf(page), keyword, true).execute();


    }

    private void loadMoreItems() {
        new PostClass_Receipt(getActivity(), String.valueOf(++page), keyword, true).execute();

    }


    class PostClass_Receipt extends AsyncTask<String, Void, String> {

        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        boolean showLoader;
        String Keyword = "";
        private Context context;


        public PostClass_Receipt(Context c, String pageNo, String keyword, boolean showLoader) {
            this.context = c;
            this.pageNo = pageNo;
            this.Keyword = keyword;
            this.showLoader = showLoader;
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            materiaList.clear();


        }

        @Override
        protected String doInBackground(String... params) {

            System.out.println("pageNo===============" + pageNo);


            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("keyword", Keyword);
                    json.put("page", pageNo);
                    //json.put("page_module", MODULE_RECEIPTS);

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(receiptsURL)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_RECEIPTS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response=R=============" + reqBody);
                    response.message();
                    System.out.println("======receiptsURL=R=======" + receiptsURL);

                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArrayData = jObject.getJSONArray("data");
                            if (jsonArrayData != null) {
                                JSONObject jsonObject;

                                for (int i = 0; i < jsonArrayData.length(); i++) {
                                    jsonObject = jsonArrayData.getJSONObject(i);
                                    System.out.println("======jsonObject=========" + jsonObject);

                                    materiaList.add(new BeanReceipt(
                                            jsonObject.getString("DELIVERY_ID"),
                                            jsonObject.getString("EGP_NO"),
                                            jsonObject.getString("EGP_DATE"),
                                            jsonObject.getString("PRODUCT"),
                                            jsonObject.getString("INVENTORY_ITEM_ID"),
                                            jsonObject.getString("SOURCE_ORGANIZATION_ID"),
                                            jsonObject.getString("DESTINATION_ORGANIZATION_ID"),
                                            jsonObject.getString("SOURCE_ORGANIZATION"),
                                            jsonObject.getString("DESTINATION_ORGANIZATION"),
                                            jsonObject.getString("RECEIPT_DATE"),
                                            jsonObject.getString("ACCEPT_QTY"),
                                            jsonObject.getString("SHORT_QTY"),
                                            jsonObject.getString("CUT_QTY"),
                                            jsonObject.getString("DAMAGE_QTY"),
                                            jsonObject.getString("TRUCK_NO"),
                                            jsonObject.getString("CREATED_BY"),
                                            jsonObject.getString("LAST_UPDATED_BY"),
                                            jsonObject.getString("LAST_UPDATE_LOGIN"),
                                            jsonObject.getString("CREATION_DATE"),
                                            jsonObject.getString("LAST_UPDATE_DATE")));
                                }

                            }
                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {

            } else {

                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }
            materialReceipt_adapter.notifyDataSetChanged();

            int  count=0;

            count=  materiaList.size();
            if (count==0){
                tv_no_record_found.setVisibility(View.VISIBLE);
                tv_count_item.setText(count+"  Receipt");
            }else {
                tv_no_record_found.setVisibility(View.GONE);
                tv_count_item.setText(count+"  Receipts");
            }

        }

    }
}
