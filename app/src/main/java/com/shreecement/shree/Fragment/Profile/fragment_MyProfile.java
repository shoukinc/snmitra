package com.shreecement.shree.Fragment.Profile;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.shreecement.shree.Common.Common_Adapter_for_details;
import com.shreecement.shree.Common.HeadersClass;
import com.shreecement.shree.Fragment.Documents.Fragment_DocUpload;
import com.shreecement.shree.Fragment.Fragment_ChangePassword;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.PERMISSION_ALL;
import static com.shreecement.shree.Utlility.Constant.PREF_CUSTOMER_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_EMPLOYEE_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_MITRA_ID;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;
import static com.shreecement.shree.Utlility.Constant.ProfileChangeUrl;
import static com.shreecement.shree.Utlility.Constant.ProfileDetail;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.baseUrlForImage;
import static com.shreecement.shree.Utlility.Constant.hasPermissions;


/**
 * Created by Choudhary on 6/30/2017.
 */

public class fragment_MyProfile extends BottomSheetDialogFragment {

    private static final int REQUEST_CAMERA_ACCESS_PERMISSION = 5674;
    private final int PICK_IMAGE = 12345;
    private final int TAKE_PICTURE = 6352;
    TextView tvName, tvPersonId, tvUserName, tvProEmail, tv_empId, tv_Address, tv_state, tv_city, tv_prof_fullname;
    FloatingActionButton fab;
    LinearLayout layoutEmp;
    ImageView ProImg, imgBackground;
    RelativeLayout layout_main;
    Button btnEditProfile;

    // Shwoing data in listview Start
    RecyclerView recyclerView;
    Common_Adapter_for_details commonAdapterForDetails;
    ArrayList<LookupCategory> mitraList;
    String UserType = "";
    //Showing data on listView details End

    public String mitraId = "";

    String adhaar_imageUrl = "";
    String adhaar_imageUrl_back = "";
    String voter_imageUrl = "";
    String driving_licence_imageUrl = "";
    String pan_imageUrl = "";
    String mitra_imageUrl = "";

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myprofile, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // FloatingActionButton..........
        fab = view.findViewById(R.id.fab);

        //TextView........................
        tvName = view.findViewById(R.id.tvName);
        tvUserName = view.findViewById(R.id.tvUserName);
        tvProEmail = view.findViewById(R.id.tvEmail);
        tvPersonId = view.findViewById(R.id.tvPersonId);
        tv_empId = view.findViewById(R.id.tvEmployeId);
        tv_city = view.findViewById(R.id.tvCITY);
        tv_state = view.findViewById(R.id.tvState);
        tv_Address = view.findViewById(R.id.tvAddress);
        layout_main = view.findViewById(R.id.layout_main);
        tv_prof_fullname = view.findViewById(R.id.tv_prof_fullname);
        layoutEmp = view.findViewById(R.id.layoutEmp);

        //ImageView......................
        ProImg = view.findViewById(R.id.prof_img);
        getActivity().setTitle(getActivity().getString(R.string.my_profile));

        setHasOptionsMenu(true);

        imgBackground = view.findViewById(R.id.img_background);

        btnEditProfile = view.findViewById(R.id.btnEditProfile);



        //Set profile details....................

        //Showing data on listView details Start
        recyclerView = view.findViewById(R.id.recyclerview);
        mitraList = new ArrayList<>();
        commonAdapterForDetails = new Common_Adapter_for_details(getActivity(), mitraList);
        recyclerView.setAdapter(commonAdapterForDetails);
        commonAdapterForDetails.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();
        //Showing data on listView details End

        try {
            Log.e("Token", StorePrefs.getDefaults("token", getActivity()));
            Log.e("PREF_USER_TYPE", StorePrefs.getDefaults(PREF_USER_TYPE, getActivity()));
            Log.e("PREF_CUSTOMER_ID", StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity()));
            Log.e("employee_id", StorePrefs.getDefaults(PREF_EMPLOYEE_ID, getActivity()));
            Log.e("IMAGE", StorePrefs.getDefaults("IMAGE", getActivity()));

        } catch (Exception e) {
            Log.e("log error ", "" + e);
        }


        UserType = StorePrefs.getDefaults(PREF_USER_TYPE, getActivity());

        //Toast.makeText(getActivity(), ""+StorePrefs.getDefaults(PREF_MITRA_ID,getActivity()), Toast.LENGTH_SHORT).show();
        if (UserType.equalsIgnoreCase("M")) {
            //new PostClassProfileDetail(getActivity(), StorePrefs.getDefaults(PREF_MITRA_ID, getActivity()), MitraDetail).execute();
            new PostClassProfileDetail(getActivity(), StorePrefs.getDefaults(PREF_MITRA_ID, getActivity()), ProfileDetail).execute();
            mitraId = StorePrefs.getDefaults(PREF_MITRA_ID,getActivity());
        }
        if (UserType.equalsIgnoreCase("R")) {
           // new PostClassProfileDetail(getActivity(), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity()), RetailerDetail).execute();
            new PostClassProfileDetail(getActivity(), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity()), ProfileDetail).execute();
            mitraId = StorePrefs.getDefaults(PREF_CUSTOMER_ID,getActivity());
        }
        if (UserType.equalsIgnoreCase("D")) {
            //new PostClassProfileDetail(getActivity(), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity()), DelearDetail).execute();
            new PostClassProfileDetail(getActivity(), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity()), ProfileDetail).execute();
            mitraId = StorePrefs.getDefaults(PREF_CUSTOMER_ID,getActivity());
        }
        if (UserType.equalsIgnoreCase("E")) {
            //new PostClassProfileDetail(getActivity(), StorePrefs.getDefaults(PREF_CUSTOMER_ID, getActivity()), DelearDetail).execute();
            new PostClassProfileDetail(getActivity(), StorePrefs.getDefaults(PREF_EMPLOYEE_ID, getActivity()), ProfileDetail).execute();
            mitraId = StorePrefs.getDefaults(PREF_EMPLOYEE_ID,getActivity());
        }

        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.container_body, new Fragment_MitrtaProfileUpdate()).commit();

            }
        });

        // Fac Click Listener ...................
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.SYSTEM_ALERT_WINDOW,};
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                }
                //Dialog
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_gallery);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View view = inflater.inflate(R.layout.dialog_gallery, null);
                TextView tvCamera, tvGallery;
                tvCamera = (TextView) view.findViewById(R.id.tv_camera);
                tvGallery = (TextView) view.findViewById(R.id.tv_gallery);
                dialog.setContentView(view);

                tvCamera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePicture.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivityForResult(takePicture, TAKE_PICTURE);
                        }
                        dialog.dismiss();
                    }
                });

                tvGallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                        dialog.dismiss();

                    }
                });
                dialog.show();
            }
        });

        // Upload Image set ...............
        tv_prof_fullname.setText(StorePrefs.getDefaults("NAME", getActivity()));
        tvName.setText(StorePrefs.getDefaults("NAME", getActivity()));
        tvUserName.setText(StorePrefs.getDefaults("USER_NAME", getActivity()));
        tvPersonId.setText(StorePrefs.getDefaults("PERSON_ID", getActivity()));
        tvProEmail.setText(StorePrefs.getDefaults("EMAIL", getActivity()));
        tv_empId.setText(StorePrefs.getDefaults("EMPLOYEE_ID", getActivity()));
        tv_city.setText(StorePrefs.getDefaults("CITY", getActivity()));
        tv_state.setText(StorePrefs.getDefaults("STATE", getActivity()));
        tv_Address.setText(StorePrefs.getDefaults("ADDRESS", getActivity()));

        if (!StorePrefs.getDefaults("USER_TYPE", getActivity()).equals("E")) {
            layoutEmp.setVisibility(View.GONE);
        }


        // Log.d("=====StorePrefs======", "" + baseUrl + StorePrefs.getDefaults("IMAGE", getActivity()));



/*

       Picasso.with(getActivity()).load(StorePrefs.getDefaults("imageUrl",getActivity()))
                .transform(new BlurTransformation(getActivity()))
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        imgBackground.setBackground(new BitmapDrawable(getActivity().getResources(), bitmap));
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
*/

    Glide.with(getActivity())
           // .load(baseUrlForImage + StorePrefs.getDefaults("IMAGE", getActivity()))
            .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + StorePrefs.getDefaults("IMAGE", getActivity())))
            .asBitmap()
                .centerCrop()
                .into(new BitmapImageViewTarget(ProImg) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        ProImg.setImageDrawable(circularBitmapDrawable);
                    }
                });


     /*   Glide.with(getActivity()).load(baseUrlForImage + StorePrefs.getDefaults("IMAGE", getActivity()))
                .asBitmap()
                .centerCrop()
                .into(new BitmapImageViewTarget(ProImg) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        ProImg.setImageDrawable(circularBitmapDrawable);
                    }
                });*/

       /* Glide.with(getActivity())
                // .load(baseUrlForImage + voterId)
                .load(HeadersClass.getUrlWithHeaders(baseUrlForImage + StorePrefs.getDefaults("IMAGE", getActivity())))
                .thumbnail(1.0f)
                .placeholder(R.drawable.profile_icon).dontAnimate()
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                .crossFade()
                .centerCrop()
                .into(ProImg);*/


        // Back Button CLick listener.................

        return view;
    }

    /* @Override
     public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
         inflater.inflate(R.menu.menu_cp, menu);
         super.onCreateOptionsMenu(menu, inflater);

         MenuItem myActionMenuItem = menu.findItem(R.id.changePassword);

         myActionMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
             @Override
             public boolean onMenuItemClick(MenuItem item) {
                 switch (item.getItemId()) {

                     case R.id.changePassword:
                         FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                         fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                                 R.anim.enter_from_right, R.anim.exit_to_left);
                         fragmentTransaction.replace(R.id.container_body, new Fragment_ChangePassword()).commit();
                 }

                 return false;
             }
         });
     }*/
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        // super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_profile_options, menu);

        MenuItem doc = menu.findItem(R.id.doc);
        MenuItem keyPassword = menu.findItem(R.id.keyPassword);

        if (UserType.equalsIgnoreCase("M")){
            doc.setVisible(true);
            btnEditProfile.setVisibility(View.VISIBLE);
        }
        else {
            doc.setVisible(false);
            btnEditProfile.setVisibility(View.GONE);
        }

        doc.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

               /* FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.container_body, new Fragment_DocUpload()).commit();*/


                Fragment_DocUpload fragment = Fragment_DocUpload.newInstance("SelfEdit");

                fragment.NewCreatedMitraId = mitraId;
                try {
                    fragment.adhaar_imageUrl = adhaar_imageUrl;
                    fragment.adhaar_imageUrl_back = adhaar_imageUrl_back;
                    fragment.voter_imageUrl = voter_imageUrl;
                    fragment.driving_licence_imageUrl = driving_licence_imageUrl;
                    fragment.pan_imageUrl = pan_imageUrl;
                    fragment.mitra_imageUrl = mitra_imageUrl;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e("adhaar_imageUrl","----"+adhaar_imageUrl);
                Log.e("adhaar_imageUrl_back","----"+adhaar_imageUrl_back);
                Log.e("voter_imageUrl","----"+voter_imageUrl);
                Log.e("driving_li_imageUrl","----"+driving_licence_imageUrl);
                Log.e("pan_imageUrl","----"+pan_imageUrl);
                Log.e("mitra_imageUrl","----"+mitra_imageUrl);

                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                return false;
            }
        });

        keyPassword.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.container_body, new Fragment_ChangePassword()).commit();
                return false;
            }
        });


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {

                try {
                    Bitmap bitmap;
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    File filesDir = getActivity().getFilesDir();
                    //create a file to write bitmap data
                    File file = new File(filesDir, "Presto_" + System.currentTimeMillis() + ".jpg");
                    file.createNewFile();
                    //Convert bitmap to byte array
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();
                    //write the bytes in file
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                    Log.e("----PATH--------", "" + file.getAbsolutePath());
                    new PostClassProfile_ImageUpdate(getActivity(), file.getAbsolutePath()).execute();
                    //   AttatchedFile(file.getAbsolutePath());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == TAKE_PICTURE) {

            if (resultCode == Activity.RESULT_OK) {
                try {
                    // Log.d("camera","=====filepath========"+filepath);
                    Bundle extras = data.getExtras();
                    Bitmap bitmap = (Bitmap) extras.get("data");
                    File filesDir = getActivity().getFilesDir();
                    //create a file to write bitmap data
                    File file = new File(filesDir, "Shree_" + System.currentTimeMillis() + ".jpg");
                    file.createNewFile();
                    //Convert bitmap to byte array
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();
                    //write the bytes in file
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                    Log.d("-----------------", "" + file.getAbsolutePath());
                    new PostClassProfile_ImageUpdate(getActivity(), file.getAbsolutePath()).execute();
                    // AttatchedFile(file.getAbsolutePath());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    //Check Permission ....................
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, TAKE_PICTURE);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    class PostClassProfile_ImageUpdate extends AsyncTask<String, Void, String> {
        Context context;
        String Id;
        String Service_id;
        ProgressDialog progress;
        JSONObject jObject;
        String Files;
        String jsonreplyMsg = "";


        public PostClassProfile_ImageUpdate(Context mcontext, String Files) {
            this.context = mcontext;
            this.Files = Files;
        }

        protected void onPreExecute() {
            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            System.out.println("=========FILE===IN-PRE" + "IN-PRE");

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MultipartBuilder buildernew = new MultipartBuilder().type(MultipartBuilder.FORM);
                    String path = Files.replaceAll(" ", "%20");
                    File uploadFile = new File(path);
                    String name1 = uploadFile.getName();
                    System.out.println("=========FILE===============" + uploadFile);
                    System.out.println("=========FILE===IN-Background" + "IN-PRE");

                    RequestBody requestBody = null;
                    if (uploadFile.exists()) {
                        final MediaType MEDIA_TYPE_PNG = MediaType.parse("application/pdf");
                        requestBody = new MultipartBuilder()
                                .type(MultipartBuilder.FORM)
                                .addFormDataPart("FileInput", name1,
                                        RequestBody.create(MEDIA_TYPE_PNG, uploadFile))

                                .build();
                    }

                    Request request = new Request.Builder()
                            .url(ProfileChangeUrl)
                            .post(requestBody)
                            .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .addHeader("Cache-Control", "no-cache")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("Postman-Token", "2c126362-1839-818d-1b67-d0b1eb53719e")
                            .build();

                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("ProfileChangeUrl response===============" + reqBody);
                    jObject = new JSONObject(reqBody);
                    try {


                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            String ImgUrl = jObject.getString("image");
                            StorePrefs.setDefaults("IMAGE", ImgUrl, context);
                            jsonreplyMsg = jObject.getString("replyMsg");

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                Toast.makeText(context, jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();
                Intent intent = getActivity().getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

    class PostClassProfileDetail extends AsyncTask<String, Void, String> {
        public ArrayList<LookupCategory> tempMitraList;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String id;
        String urlToPass;
        String PageModule = "";
        private Context context;


        PostClassProfileDetail(Context c, String id, String urlToPass) {
            this.context = c;
            this.id = id;
            this.urlToPass = urlToPass;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

            tempMitraList = new ArrayList<LookupCategory>();

            if (UserType.equalsIgnoreCase("R")) {
                //PageModule = MODULE_RETAILERS;
                PageModule = MODULE_COMMON;
            }
            if (UserType.equalsIgnoreCase("D")) {
                //PageModule = MODULE_DEALERS;
                PageModule = MODULE_COMMON;
            }
            if (UserType.equalsIgnoreCase("M")) {
               // PageModule = MODULE_MITRAS;
                PageModule = MODULE_COMMON;
            }
            else
                PageModule = MODULE_COMMON;

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();

                    json.put("id", id);
                    json.put("user_type",  StorePrefs.getDefaults(PREF_USER_TYPE, context));
                    //json.put("page_module", MODULE_COMMON);
                    RequestBody body = RequestBody.create(mediaType, json.toString());
                    System.out.println("Parameters sent:==> " + json.toString());

                    Request request = new Request.Builder()
                            .url(urlToPass)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", PageModule)
                            //.addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("MyProfilePage=====urlToPass=========" + urlToPass);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONObject jsonData = jObject.getJSONObject("data");
                            if (jObject != null) {

                                if (UserType.equalsIgnoreCase("M")) {
                                    tempMitraList.add(new LookupCategory("Full Name",
                                            "", Constant.nullCheckFunction(jsonData.getString("full_name"))));

                                    tempMitraList.add(new LookupCategory("Email Address",
                                            "", Constant.nullCheckFunction(jsonData.getString("email_address"))));

                                    tempMitraList.add(new LookupCategory("Mobile Number",
                                            "", Constant.nullCheckFunction(jsonData.getString("mobile_number"))));

                                    tempMitraList.add(new LookupCategory("Gender",
                                            "", Constant.nullCheckFunction(jsonData.getString("sex_code"))));

//                                    tempMitraList.add(new LookupCategory("Blood Type",
//                                            "", Constant.nullCheckFunction(jsonData.getString("blood_type_code"))));

                                    tempMitraList.add(new LookupCategory("Date Of Birth",
                                            "", Constant.dateFuncation(jsonData.getString("date_of_birth"))));

                                    tempMitraList.add(new LookupCategory("Mitra Number",
                                            "",Constant.nullCheckFunction( jsonData.getString("mitra_number"))));

                                    tempMitraList.add(new LookupCategory("Aadhaar Number",
                                            "", Constant.nullCheckFunction(jsonData.getString("adhaar_number"))));

                                    tempMitraList.add(new LookupCategory("Voter Id",
                                            "", Constant.nullCheckFunction(jsonData.getString("voter_id"))));

                                    tempMitraList.add(new LookupCategory("PAN Card",
                                            "", Constant.nullCheckFunction(jsonData.getString("pan"))));

                                    tempMitraList.add(new LookupCategory("Driving Licence",
                                            "", Constant.nullCheckFunction(jsonData.getString("driving_licence_number"))));

                                    tempMitraList.add(new LookupCategory("GST Number",
                                            "", Constant.nullCheckFunction(jsonData.getString("gst_number"))));

                                    tempMitraList.add(new LookupCategory("Marital Status",
                                            "", Constant.nullCheckFunction(jsonData.getString("marital_status_code"))));

                                    tempMitraList.add(new LookupCategory("Spouse Name",
                                            "", Constant.nullCheckFunction(jsonData.getString("spouse_name"))));

                                    tempMitraList.add(new LookupCategory("Anniversary",
                                            "", Constant.dateFuncation(jsonData.getString("date_of_wedding"))));

                                    tempMitraList.add(new LookupCategory("Child 1 Name",
                                            "", Constant.nullCheckFunction(jsonData.getString("child1_name"))));

                                    tempMitraList.add(new LookupCategory("Date Of Birth",
                                            "", Constant.dateFuncation(jsonData.getString("date_of_birth_child1"))));

                                    tempMitraList.add(new LookupCategory("Child 2 Name",
                                            "", Constant.nullCheckFunction(jsonData.getString("child2_name"))));

                                    tempMitraList.add(new LookupCategory("Date Of Birth",
                                            "", Constant.dateFuncation(jsonData.getString("date_of_birth_child2"))));

                                    tempMitraList.add(new LookupCategory("Address",
                                            "", Constant.nullCheckFunction(jsonData.getString("address_line1"))));

                                  /*  tempMitraList.add(new LookupCategory("Address Line 2",
                                            "", jsonData.getString("address_line2")));

                                    tempMitraList.add(new LookupCategory("Address Line 3",
                                            "", jsonData.getString("address_line3")));*/

                                    tempMitraList.add(new LookupCategory("City",
                                            "",Constant.nullCheckFunction( jsonData.getString("city"))));

                                    tempMitraList.add(new LookupCategory("Taluka",
                                            "",Constant.nullCheckFunction( jsonData.getString("taluka"))));

                                    tempMitraList.add(new LookupCategory("District",
                                            "", Constant.nullCheckFunction(jsonData.getString("district"))));

                                    tempMitraList.add(new LookupCategory("State",
                                            "", Constant.nullCheckFunction(jsonData.getString("state"))));


                                    if (UserType.equalsIgnoreCase("M")) {

                                        adhaar_imageUrl = Constant.nullCheckFunction(jsonData.getString("adhaar_image"));
                                        adhaar_imageUrl_back = Constant.nullCheckFunction(jsonData.getString("adhaar_image_back"));
                                        voter_imageUrl = Constant.nullCheckFunction(jsonData.getString("voter_image"));
                                        driving_licence_imageUrl = Constant.nullCheckFunction(jsonData.getString("driving_licence_image"));
                                        pan_imageUrl = Constant.nullCheckFunction(jsonData.getString("pan_image"));
                                        mitra_imageUrl = Constant.nullCheckFunction(jsonData.getString("mitra_image"));

                                    }


                                }
                                else if (UserType.equalsIgnoreCase("E")) {
                                    tempMitraList.add(new LookupCategory("Full Name",
                                            "", Constant.nullCheckFunction(jsonData.getString("name"))));

                                    tempMitraList.add(new LookupCategory("Email Address",
                                            "", Constant.nullCheckFunction(jsonData.getString("email"))));

                                 /*   tempMitraList.add(new LookupCategory("State",
                                            "", Constant.nullCheckFunction(jsonData.getString("state"))));

                                    tempMitraList.add(new LookupCategory("Address",
                                            "", Constant.nullCheckFunction(jsonData.getString("address"))));*/


                                }

                                else {
                                    tempMitraList.add(new LookupCategory("Party Name",
                                            "", Constant.nullCheckFunction(jsonData.getString("party_name"))));

                                    tempMitraList.add(new LookupCategory("Contact Person",
                                            "", Constant.nullCheckFunction(jsonData.getString("contact_person_name"))));

                                    tempMitraList.add(new LookupCategory("Email Address",
                                            "", Constant.nullCheckFunction(jsonData.getString("customer_email_address"))));

                                    tempMitraList.add(new LookupCategory("Phone",
                                            "",Constant.nullCheckFunction( jsonData.getString("customer_mobile_number"))));

                                    tempMitraList.add(new LookupCategory("Address",
                                            "", Constant.nullCheckFunction(jsonData.getString("address"))));

                                    tempMitraList.add(new LookupCategory("City",
                                            "", Constant.nullCheckFunction(jsonData.getString("city"))));

                                    tempMitraList.add(new LookupCategory("Taluka",
                                            "", Constant.nullCheckFunction(jsonData.getString("taluka"))));

                                    tempMitraList.add(new LookupCategory("District",
                                            "",Constant.nullCheckFunction( jsonData.getString("district"))));

                                    tempMitraList.add(new LookupCategory("State",
                                            "", Constant.nullCheckFunction(jsonData.getString("state"))));

                                    tempMitraList.add(new LookupCategory("Postal",
                                            "", Constant.nullCheckFunction(jsonData.getString("postal_code"))));


                                    if (UserType.equalsIgnoreCase("M")) {

                                        adhaar_imageUrl = Constant.nullCheckFunction(jsonData.getString("adhaar_image"));
                                        adhaar_imageUrl_back = Constant.nullCheckFunction(jsonData.getString("adhaar_image_back"));
                                        voter_imageUrl = Constant.nullCheckFunction(jsonData.getString("voter_image"));
                                        driving_licence_imageUrl = Constant.nullCheckFunction(jsonData.getString("driving_licence_image"));
                                        pan_imageUrl = Constant.nullCheckFunction(jsonData.getString("pan_image"));
                                        mitra_imageUrl = Constant.nullCheckFunction(jsonData.getString("mitra_image"));

                                    }


                                    Log.e("adhaar_imageUrl","----"+adhaar_imageUrl);
                                    Log.e("adhaar_imageUrl_back","----"+adhaar_imageUrl_back);
                                    Log.e("voter_imageUrl","----"+voter_imageUrl);
                                    Log.e("driving_li_imageUrl","----"+driving_licence_imageUrl);
                                    Log.e("pan_imageUrl","----"+pan_imageUrl);
                                    Log.e("mitra_imageUrl","----"+mitra_imageUrl);


                                }

                                Log.e("adhaar_imageUrl","--1111111111--"+adhaar_imageUrl);
                                Log.e("adhaar_imageUrl_back","--111111111--"+adhaar_imageUrl_back);
                                Log.e("voter_imageUrl","----"+voter_imageUrl);
                                Log.e("driving_li_imageUrl","----"+driving_licence_imageUrl);
                                Log.e("pan_imageUrl","----"+pan_imageUrl);
                                Log.e("mitra_imageUrl","--111111111--"+mitra_imageUrl);

                            }
                            mitraList.addAll(tempMitraList);


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


                commonAdapterForDetails.notifyDataSetChanged();

            } else {

                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }

        }
    }

}



