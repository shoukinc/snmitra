package com.shreecement.shree.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.shreecement.shree.Adapter.CustomersOrderDetails_Adapter;
import com.shreecement.shree.ModalClass.BeanCustomer_Item;
import com.shreecement.shree.ModalClass.BeanCustomer_OrderItem;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.MODULE_CUSTOMERS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.customerOrdersUrl;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Customers_Order_Orders extends Fragment {

    RecyclerView recyclerView;
    CustomersOrderDetails_Adapter customersOrderDetailsAdapter;
    ArrayList<BeanCustomer_OrderItem> customerOrderList;


    TextView tvCityState, tvNameNumber;
    //  TextView txtDispatchDetails,txtConsignee,txtOrderDetails;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customers_order_orders, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        tvCityState = view.findViewById(R.id.tvCityState);
        tvNameNumber = view.findViewById(R.id.tvNameNumber);
        recyclerView = view.findViewById(R.id.recyclerview);
        getActivity().setTitle(getActivity().getString(R.string.customer_details));



        customerOrderList = new ArrayList<>();
        Gson gson = new Gson();
        String json = StorePrefs.getDefaults("beanCustomer_item", getActivity());
        BeanCustomer_Item beanCustomer_item = gson.fromJson(json, BeanCustomer_Item.class);

        tvNameNumber.setText(beanCustomer_item.getCust_account_id() + " & " + beanCustomer_item.getParty_name());
        tvCityState.setText(beanCustomer_item.getCity() + " & " + beanCustomer_item.getState());


        new PostClassCustomerOrder(getActivity(), beanCustomer_item.getParty_id()).execute();

        customerOrderList.clear();


        return view;
    }


    class PostClassCustomerOrder extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg = "";
        String CustomerCode = "";

        PostClassCustomerOrder(Context c, String customerCode) {
            this.context = c;
            this.CustomerCode = customerCode;

            Log.d("====CustomerCode=====", "" + CustomerCode);

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.setCancelable(false);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();


                    RequestBody body = new FormEncodingBuilder()
                            .add("customer_code", CustomerCode)
                            //.add("page_module", MODULE_CUSTOMERS)
                            .build();
                    Request request = new Request.Builder()
                            .post(body)
                            .url(customerOrdersUrl)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_CUSTOMERS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token", StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    Log.d("===customerOrdersUrl===", "=======" + customerOrdersUrl);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONArray jsonData = jObject.getJSONArray("data");

                            for (int i = 0; i < jsonData.length(); i++) {
                                JSONObject jCustomer = jsonData.getJSONObject(i);
                                customerOrderList.add(new BeanCustomer_OrderItem(
                                        jCustomer.getString("order_date"),
                                        jCustomer.getString("customer_name"),
                                        jCustomer.getString("consignee_name"),
                                        jCustomer.getString("mm_name"),
                                        jCustomer.getString("brand_name"),
                                        jCustomer.getString("category_name"),
                                        jCustomer.getString("packing_type_name"),
                                        jCustomer.getString("product_name"),
                                        jCustomer.getString("order_id"),
                                        jCustomer.getString("category"),
                                        jCustomer.getString("customer_code"),
                                        jCustomer.getString("product"),
                                        jCustomer.getString("brand"),
                                        jCustomer.getString("packingtype"),
                                        jCustomer.getString("bagtype"),
                                        jCustomer.getString("quantity"),
                                        jCustomer.getString("consignee"),
                                        jCustomer.getString("state"),
                                        jCustomer.getString("district"),
                                        jCustomer.getString("taluka"),
                                        jCustomer.getString("city"),
                                        jCustomer.getString("rate"),
                                        jCustomer.getString("dispatch_from"),
                                        jCustomer.getString("validity"),
                                        jCustomer.getString("address"),
                                        jCustomer.getString("employee_code"),
                                        jCustomer.getString("transfer_flag"),
                                        jCustomer.getString("remark"),
                                        jCustomer.getString("last_updated_by"),
                                        jCustomer.getString("last_update_login"),
                                        jCustomer.getString("last_update_date"),
                                        jCustomer.getString("order_date_new")));

                            }
                            Log.d("===customerOrderList==", "=======" + customerOrderList.size());
                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {

                if (!customerOrderList.isEmpty()) {
                    customersOrderDetailsAdapter = new CustomersOrderDetails_Adapter(getActivity(), customerOrderList);
                    recyclerView.setAdapter(customersOrderDetailsAdapter);
                }

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }




        }
    }
}
