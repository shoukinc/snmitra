package com.shreecement.shree.Fragment.Enquiry;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Fragment.Referral.Bean.BeanReferral;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.shreecement.shree.Utlility.Constant.ACTION_EDIT;
import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.Enquiry_comment;
import static com.shreecement.shree.Utlility.Constant.Enquiry_list;
import static com.shreecement.shree.Utlility.Constant.MODULE_ENQUIRIES;
import static com.shreecement.shree.Utlility.Constant.MODULE_PAGE;
import static com.shreecement.shree.Utlility.Constant.PREF_ROLE_TYPE;
import static com.shreecement.shree.Utlility.Constant.PREF_USER_TYPE;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.nullCheckFunction;
import static com.shreecement.shree.Utlility.Utils.Please_Check_Internet_Connection;

/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Enquiry_List extends Fragment {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    Result_Adapter referralAdapter;
    ArrayList<BeanReferral> equeryList;

    int total_record = 0;
    int recordCount = 0;

    String keyword = "";
    String type = "";
    String Comment = "";
    int page = 1;
    String Url="";
    String frag="";
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog builder3;
    EditText edDialodComment;
    TextView tv_no_record_found;
    TextView tv_count_item;
    String enquiry_id = "", enquiry_name = "", mobile_number = ""
            , state = "", district = "",
            taluka = "", city = "", address = "",
            inventory_item_id = "",
            required_quantity = "",
            required_date = "",
            enquiry = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_referral_list, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        tv_no_record_found = view.findViewById(R.id.tv_no_record_found);
        tv_count_item = view.findViewById(R.id.tv_count_item);

        setHasOptionsMenu(true);
             getActivity().setTitle(getActivity().getString(R.string.enquiry_list));




        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);

        keyword = "";

        equeryList = new ArrayList<>();
        referralAdapter = new Result_Adapter(getActivity(), equeryList,frag);
        recyclerView.setAdapter(referralAdapter);
        referralAdapter.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    loadMoreItems();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            public void onRefresh() {
                refreshItems();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new PostClasseEnquiryList(getActivity(), "1", keyword, true).execute();
            }
        }, 450);


        return view;
    }

    private void loadMoreItems() {

        new PostClasseEnquiryList(getActivity(), String.valueOf(++page), keyword, false).execute();
    }

    private void refreshItems() {
        page = 1;
        equeryList.clear();
        swipeRefreshLayout.setRefreshing(false);
        // Load items
        // ...
        new PostClasseEnquiryList(getActivity(), String.valueOf(page), keyword, false).execute();

    }
    public class Result_Adapter extends RecyclerView.Adapter<Result_Adapter.ViewHolder> {
        Context context;
        String Type;
        ArrayList<BeanReferral> rowItems;

        public Result_Adapter(Context context, ArrayList<BeanReferral> rowItem, String type) {
            this.context = context;
            this.rowItems = rowItem;
            this.Type = type;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_enquiry_row_item, parent, false));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            BeanReferral beanReferral = rowItems.get(position);

            String Enquiry = "";
            String mobile = "";
            String Product = "";
            String title = "";
            JSONObject jsonObject = beanReferral.getJsonObject();


            try {

                Product = jsonObject.getString("product");
                Enquiry = jsonObject.getString("required_quantity");
                mobile = jsonObject.getString("mobile_number");
                title = jsonObject.getString("name");

                Comment = jsonObject.getString("comments");
                holder.tvtitle.setText(nullCheckFunction(title.toUpperCase()));

                holder.tvMobile.setText(nullCheckFunction(mobile));
                holder.tvProduct.setText("Product : " + nullCheckFunction(Product));
                holder.tvEnquiry.setText(nullCheckFunction(Enquiry));
                holder.tvComment.setText(nullCheckFunction(Comment));
                holder.tvFirst.setText(nullCheckFunction(jsonObject.getString("name").substring(0, 1).toUpperCase()));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            holder.tvFirst.setBackgroundResource(R.drawable.rounder_date_red_image);


        }

        @Override
        public int getItemCount() {
            return rowItems.size();
        }

        public void add(List<BeanReferral> items) {
            int previousDataSize = this.rowItems.size();
            this.rowItems.addAll(items);
            notifyItemRangeInserted(previousDataSize, items.size());
        }


        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView tvEnquiry, tvProduct, tvMobile,
                    tvtitle, tvFirst, tvComment;

            ViewHolder(View convertView) {
                super(convertView);
                 convertView.setOnClickListener(this);

                tvFirst = convertView.findViewById(R.id.tvFirst);

                tvtitle = convertView.findViewById(R.id.tvtitle);
                tvMobile = convertView.findViewById(R.id.tvMobile);

                tvProduct = convertView.findViewById(R.id.tvProduct);
                tvEnquiry = convertView.findViewById(R.id.tvEnquiry);
                tvComment = convertView.findViewById(R.id.tvComment);


            }

            @Override
            public void onClick(View v) {
                String UserType = StorePrefs.getDefaults(PREF_USER_TYPE, context);



                if (UserType.equalsIgnoreCase("E")) {



                    try {
                        BeanReferral beanReferral = rowItems.get(getAdapterPosition());

                        JSONObject jsonObject = beanReferral.getJsonObject();

                        enquiry_id = nullCheckFunction(jsonObject.getString("enquiry_id"));
                        enquiry_name =nullCheckFunction( jsonObject.getString("name"));
                        mobile_number = nullCheckFunction(jsonObject.getString("mobile_number"));
                        state =nullCheckFunction( jsonObject.getString("state"));
                        district =nullCheckFunction( jsonObject.getString("district"));
                        taluka = jsonObject.getString("taluka");
                        city = nullCheckFunction(jsonObject.getString("city"));
                        address = nullCheckFunction(jsonObject.getString("address"));
                        inventory_item_id = nullCheckFunction(jsonObject.getString("inventory_item_id"));
                        required_quantity = nullCheckFunction(jsonObject.getString("required_quantity"));
                        required_date = nullCheckFunction(jsonObject.getString("required_date"));
                        enquiry =nullCheckFunction( jsonObject.getString("enquiry"));



                        dialogBuilder = new AlertDialog.Builder(getActivity());
                        final TextView dialogTitle;
                        Button btnOk, btnCancle;


                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        final View dialogView = inflater.inflate(R.layout.dialog_write_comment, null);
                        dialogBuilder.setView(dialogView);
                        edDialodComment = dialogView.findViewById(R.id.edComment);
                        btnOk = dialogView.findViewById(R.id.btnOk);
                        btnCancle = dialogView.findViewById(R.id.btnCancle);
                        dialogTitle = dialogView.findViewById(R.id.dialogTitle);
                        dialogTitle.setText("Write  Comment");

                        btnOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Comment = edDialodComment.getText().toString();
                                if (Comment.length() == 0) {
                                    Toast.makeText(context, "Comment is Required!", Toast.LENGTH_LONG).show();

                                } else {


                                    new PostClass_CommentEnquiry(getActivity(), enquiry_id, enquiry_name, mobile_number
                                            , state, district,
                                            taluka, city, address,
                                            inventory_item_id,
                                            required_quantity,
                                            required_date,
                                            enquiry, Comment).execute();
                                }


                            }
                        });
                        btnCancle.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                builder3.dismiss();
                            }
                        });
                        builder3 = dialogBuilder.create();
                        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        builder3.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Comment=tvComment.getText().toString();
                    nullCheckFunction(Comment);
                    if (Comment.length()>0){
                    dialogBuilder = new AlertDialog.Builder(getActivity());
                    TextView tvComment, dialogTitle;
                    Button btnOk;

                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.dialog_comment, null);
                    dialogBuilder.setView(dialogView);
                    tvComment = dialogView.findViewById(R.id.tvComment);
                    btnOk = dialogView.findViewById(R.id.btnOk);
                    dialogTitle = dialogView.findViewById(R.id.dialogTitle);
                    dialogTitle.setText("View Comment");
                    tvComment.setText(nullCheckFunction(Comment));

                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            builder3.dismiss();
                        }
                    });
                    builder3 = dialogBuilder.create();
                    builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    builder3.show();

                    }else {
                        Toast.makeText(context,"No Comments Available!",Toast.LENGTH_LONG).show();
                    }
                }

            }


        }
    }

            class PostClasseEnquiryList extends AsyncTask<String, Void, String> {
                private Context context;
                ProgressDialog progress;
                JSONObject jObject;
                String jsonreplyMsg;
                String pageNo;
                String Keyword;
                boolean showLoader;

                List<BeanReferral> tempequeryList = new ArrayList<>();

                PostClasseEnquiryList(Context c, String pageNo, String keyword, boolean showLoader) {
                    this.context = c;
                    this.pageNo = pageNo;
                    this.Keyword = keyword;
                    this.showLoader = showLoader;
                    Log.d("-----Keyword------", "" + Keyword);
                }

                protected void onPreExecute() {

                    progress = ProgressDialog.show(context, null, null);
                    progress.setTitle("Loading...");
                    Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
                    d.setAlpha(200);
                    progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    progress.getWindow().setBackgroundDrawable(d);
                    progress.setContentView(R.layout.progress_dialog);
                    progress.show();
                    tempequeryList = new ArrayList<>();

                }

                @Override
                protected String doInBackground(String... params) {

                    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = cm.getActiveNetworkInfo();
                    /*Realm realm = Realm.getDefaultInstance();*/
                    String User_type = StorePrefs.getDefaults(PREF_USER_TYPE, context);
                    String User_role = StorePrefs.getDefaults(PREF_ROLE_TYPE, context);
                    if (netInfo != null && netInfo.isConnected()) {
                        try {
                            OkHttpClient client = new OkHttpClient();
                            MediaType mediaType = MediaType.parse("application/json");
                            JSONObject json = new JSONObject();
                            json.put("user_type", User_type);
                            json.put("role", User_role);
                            RequestBody body = RequestBody.create(mediaType, json.toString());

                            Request request = new Request.Builder()
                                    .url(Enquiry_list)
                                    .post(body)
                                    .addHeader("content-type", "application/json")
                                    .addHeader(MODULE_PAGE, MODULE_ENQUIRIES)
                                    .addHeader("action", ACTION_VIEW)
                                    .addHeader("token", StorePrefs.getDefaults("token", context))
                                    .build();
                            Response response = client.newCall(request).execute();
                            String reqBody = response.body().string();
                            System.out.println("response===============" + reqBody);
                            response.message();
                            System.out.println("======EnquiryListUrl=========" + Enquiry_list);

                            try {
                                jObject = new JSONObject(reqBody);
                                if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                                    jsonreplyMsg = jObject.getString("replyMsg");


                                    total_record = Integer.parseInt(jObject.getString("total_record"));
                                    recordCount = Integer.parseInt(jObject.getString("recordCount"));


                                    JSONArray jsonArraydata = jObject.getJSONArray("data");
                                    if (jsonArraydata != null) {


                                        for (int i = 0; i < jsonArraydata.length(); i++) {
                                            JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                            tempequeryList.add(new BeanReferral(
                                                    jsonObject.getString("enquiry_id"),
                                                    jsonObject));
                                        }

                                    }


                                    return jObject.getString("replyCode");

                                } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                                    return jObject.getString("replyMsg");
                                }

                            } catch (JSONException e) {
                                progress.dismiss();
                                e.printStackTrace();
                                return e.toString();
                            }

                        } catch (IOException e) {

                            e.printStackTrace();
                            return e.toString();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            return e.toString();

                        }
                    } else {


                        return Please_Check_Internet_Connection;
                    }


                    return null;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    progress.dismiss();
                    if (s == null) {
                        page--;
                        Toast.makeText(context, "Some thing wrong on Web end.", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (s.equalsIgnoreCase("success")) {
                        Log.d("tempequeryList", "========" + equeryList.size());
                        equeryList.clear();
                        Log.d("tempequeryList", "========" + tempequeryList.size());
                        Log.d("tempequeryList", "========" + tempequeryList.size());
                        equeryList.addAll(tempequeryList);
                        referralAdapter.notifyDataSetChanged();

                    } else if (s.equals(Please_Check_Internet_Connection)) {
                        page--;
                        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

                    } else {
                        page--;
                        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

                    }
                    referralAdapter.notifyDataSetChanged();
                    recordCount = equeryList.size();
                    if (recordCount == 0) {
                        tv_no_record_found.setVisibility(View.VISIBLE);

                        tv_count_item.setText("Showing " + recordCount + "   of " + total_record);
                    } else {
                        tv_no_record_found.setVisibility(View.GONE);
                        tv_count_item.setText("Showing " + recordCount + "   of " + total_record);
                    }

                }
            }


            ////////////--------------------- Comment FeedBack -------------------////////////////////////////
            class PostClass_CommentEnquiry extends AsyncTask<String, Void, String> {
                private Context context;
                ProgressDialog progress;
                JSONObject jObject;
                String jsonreplyMsg;

                String enquiry_id = "";
                String enquiry_name = "";
                String mobile_number = "";
                String state = "";
                String district = "";
                String taluka = "";
                String city = "";
                String address = "";
                String inventory_item_id = "";
                String required_quantity = "";
                String required_date = "";
                String enquiry = "";
                String comments = "";

                public PostClass_CommentEnquiry(Context context, String enquiry_id, String enquiry_name, String mobile_number,
                                                String state, String district, String taluka, String city, String address,
                                                String inventory_item_id, String required_quantity, String required_date, String enquiry,
                                                String comments) {
                    this.context = context;
                    this.enquiry_id = enquiry_id;
                    this.enquiry_name = enquiry_name;
                    this.mobile_number = mobile_number;
                    this.state = state;
                    this.district = district;
                    this.taluka = taluka;
                    this.city = city;
                    this.address = address;
                    this.inventory_item_id = inventory_item_id;
                    this.required_quantity = required_quantity;
                    this.required_date = required_date;
                    this.enquiry = enquiry;
                    this.comments = comments;
                }

                protected void onPreExecute() {

                    progress = ProgressDialog.show(context, null, null);
                    progress.setTitle("Loading...");
                    Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
                    d.setAlpha(200);
                    progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    progress.getWindow().setBackgroundDrawable(d);
                    progress.setContentView(R.layout.progress_dialog);
                    progress.show();

                }

                @Override
                protected String doInBackground(String... params) {

                    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = cm.getActiveNetworkInfo();

                    if (netInfo != null && netInfo.isConnected()) {
                        try {
                            OkHttpClient client = new OkHttpClient();
                            MediaType mediaType = MediaType.parse("application/json");
                            JSONObject json = new JSONObject();
                            json.put("id", enquiry_id);
                            json.put("enquiry_name", enquiry_name);
                            json.put("mobile_number", mobile_number);
                            json.put("state", state);
                            json.put("district", district);
                            json.put("taluka", taluka);
                            json.put("city", city);
                            json.put("address", address);
                            json.put("inventory_item_id", inventory_item_id);
                            json.put("required_quantity", required_quantity);
                            json.put("required_date", required_date);
                            json.put("enquiry", enquiry);
                            json.put("comments", Comment);

                            System.out.println("Param===============" + json.toString());

                            RequestBody body = RequestBody.create(mediaType, json.toString());

                            Request request = new Request.Builder()
                                    .url(Enquiry_comment)
                                    .post(body)
                                    .addHeader("content-type", "application/json")
                                    .addHeader("page_module", MODULE_ENQUIRIES)
                                    .addHeader("action", ACTION_EDIT)
                                    .addHeader("token", StorePrefs.getDefaults("token", context))
                                    .build();
                            Response response = client.newCall(request).execute();
                            String reqBody = response.body().string();
                            System.out.println("response===============" + reqBody);
                            response.message();
                            System.out.println("======Enquiry_commentUrl=========" + Enquiry_comment);

                            try {
                                jObject = new JSONObject(reqBody);
                                if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                                    jsonreplyMsg = jObject.getString("replyMsg");

                                    return jObject.getString("replyCode");

                                } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                                    return jObject.getString("replyMsg");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                return e.toString();
                            }

                        } catch (IOException e) {

                            e.printStackTrace();
                            return e.toString();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            return e.toString();
                        }
                    } else {
                        return  Please_Check_Internet_Connection;
                    }


                    return null;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    progress.dismiss();
                    if (s == null) {

                        Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (s.equals("success")) {
                        builder3.dismiss();
                        edDialodComment.setText("");
                        Toast.makeText(context, jsonreplyMsg, Toast.LENGTH_LONG).show();
                        new PostClasseEnquiryList(getActivity(), "1", keyword, true).execute();

                    } else {
                        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                    }
                }
            }

}
