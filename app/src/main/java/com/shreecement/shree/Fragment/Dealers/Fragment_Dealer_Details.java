package com.shreecement.shree.Fragment.Dealers;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Fragment.Dealers.Bean.BeanDealer_Item;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;


/**
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Dealer_Details extends Fragment {

    TextView tvDealerNumber,tvDealerCity,tvDealerName,tvDealerState,tvDealerAddress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_dealer_details,container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        tvDealerNumber = view.findViewById(R.id.tvDealerNumber);
        tvDealerCity = view.findViewById(R.id.tvDealerCity);
        tvDealerName = view.findViewById(R.id.tvDealerName);
        tvDealerState = view.findViewById(R.id.tvDealerState);
        tvDealerAddress = view.findViewById(R.id.tvDealerAddress);

             getActivity().setTitle(getActivity().getString(R.string.dealer_details));


        Gson gson = new Gson();
        String json = StorePrefs.getDefaults("beanDealer_item", getActivity());
        BeanDealer_Item beanDealer_item = gson.fromJson(json, BeanDealer_Item.class);

        tvDealerNumber.setText( beanDealer_item.getCust_account_id());
        tvDealerName.setText(beanDealer_item.getParty_name());
        tvDealerCity.setText(beanDealer_item.getCity());
        tvDealerState.setText(beanDealer_item.getState());
        tvDealerAddress.setText(beanDealer_item.getAddress());


         return view;
    }


}
