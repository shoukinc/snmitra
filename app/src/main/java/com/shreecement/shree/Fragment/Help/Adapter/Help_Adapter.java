package com.shreecement.shree.Fragment.Help.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Help.Bean.BeanHelp_Item;
import com.shreecement.shree.Fragment.Help.Fragment_Help_Details;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import java.util.ArrayList;


/**
 * Created by TIA on 12-08-2016.
 */

public class Help_Adapter extends RecyclerView.Adapter<Help_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanHelp_Item> rowItems;

    public Help_Adapter(Context context, ArrayList<BeanHelp_Item> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_help, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String rowItem = rowItems.get(position).getTitle();

        // holder.tvCustomerId.setText(rowItems.get(position).getAccount_number());
        holder.tvHelp.setText(rowItem);

    }

    @Override
    public int getItemCount() {
        return rowItems.size();

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvHelp;
        ViewHolder(View convertView) {
            super(convertView);
            tvHelp = convertView.findViewById(R.id.tvHelp);

            convertView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {

            BeanHelp_Item beanHelp_item = rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanHelp_item);

            StorePrefs.setDefaults("beanHelp_item", json, context);
            Fragment_Help_Details fragment = new Fragment_Help_Details();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            ((MainActivity) context).showUpButton(true);

        }
    }


}