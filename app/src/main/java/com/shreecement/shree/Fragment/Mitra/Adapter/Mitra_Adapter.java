package com.shreecement.shree.Fragment.Mitra.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Common.BeanCommon;
import com.shreecement.shree.Fragment.Mitra.Fragment_MitraRegistration;
import com.shreecement.shree.Fragment.Mitra.Fragment_Mitra_Details;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by TIA on 12-08-2016.
 */

public class Mitra_Adapter extends RecyclerView.Adapter<Mitra_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanCommon> rowItems;
    String caltegory;

    public Mitra_Adapter(Context context, ArrayList<BeanCommon> rowItem) {
        this.context = context;
        this.rowItems = rowItem;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_mitra_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        BeanCommon beanMitra = rowItems.get(position);

        JSONObject jsonObject = beanMitra.getJsonObject();

        try {

            String Name = Constant.nullCheckFunction(jsonObject.getString("full_name"));
            String mitraId =  Constant.nullCheckFunction(jsonObject.getString("mitra_id"));
            String mitra_type_meaning =  Constant.nullCheckFunction(jsonObject.getString("mitra_type_meaning"));
            String Registration_source =  Constant.nullCheckFunction(jsonObject.getString("registration_source"));

            holder.tvMitraName.setText(Name +" ("+ mitraId+")");
            holder.tvDescOne.setText(mitra_type_meaning);
            if(Registration_source.equalsIgnoreCase("S")) {
                holder.tvDescTwo.setText("Registration Source : Self");
            }
           else if(Registration_source.equalsIgnoreCase("E")) {
                holder.tvDescTwo.setText("Registration Source : In App");
            }

            holder.tvMobileNumber.setText(Constant.nullCheckFunction(jsonObject.getString("mobile_number")));
           // String checkEmailValue = jsonObject.getString("email_address");

          //  if(!checkEmailValue.equalsIgnoreCase("null")) {
                holder.tvMitraEmail.setText(Constant.nullCheckFunction(jsonObject.getString("adhaar_number")));
          //  }

            String first = jsonObject.getString("first_name");
            if (first !=null) {
                holder.tvFirst.setText(first.substring(0, 1).trim().toUpperCase());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


       /* String first = beanMitra.getFull_name();
        System.out.println("first===============" + first);
        int i = first.indexOf(" ");
        System.out.println("i===============" + i);
        String str = first.substring(i,first.length() - 1);
        System.out.println("str===============" + str);
        String firstChart = str.substring(1, 2);
        System.out.println("firstChart===============" + firstChart);
        holder.tvFirst.setText(firstChart);*/

        holder.tvFirst.setBackgroundResource(R.drawable.rounder_date_red_image);

    }

    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    public void add(List<BeanCommon> items) {
        int previousDataSize = this.rowItems.size();
        this.rowItems.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvMobileNumber, tvMitraName, tvFirst ,tvMitraEmail, tvDescOne,tvDescTwo;

        ViewHolder(View convertView) {
            super(convertView);
            convertView.setOnClickListener(this);

            tvMobileNumber = (TextView) convertView.findViewById(R.id.tvMobileNumber);
            tvMitraName = (TextView) convertView.findViewById(R.id.tvMitraName);
            tvMitraEmail = (TextView) convertView.findViewById(R.id.tvMitraEmail);
            tvDescOne = (TextView) convertView.findViewById(R.id.tvDescOne);

            tvFirst = (TextView) convertView.findViewById(R.id.tvFirst);
            tvDescTwo = (TextView) convertView.findViewById(R.id.tvDescTwo);

            tvDescOne.setVisibility(View.VISIBLE);

        }

        @Override
        public void onClick(View v) {



           /* BeanCommon beanMitra_item = rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanMitra_item);
            StorePrefs.setDefaults(PREF_BEAN_MITRA_ITEM, json, context);*/
            /*final BeanCommon beanMitra_item = rowItems.get(getLayoutPosition());

            Fragment_Mitra_Details fragment = new Fragment_Mitra_Details();
            fragment.mitraId =beanMitra_item.getId();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();*/
            final BeanCommon beanMitra_item = rowItems.get(getLayoutPosition());

            new AlertDialog.Builder(context)
                    .setTitle(context.getResources().getString(R.string.select_your_option))
                    .setPositiveButton("View Details", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {


                            Fragment_Mitra_Details fragment = Fragment_Mitra_Details.newInstance("view");
                            fragment.mitraId =beanMitra_item.getId();
                            FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.container_body, fragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                            ((MainActivity) context).showUpButton(true);
                        }
                    })
                    .setNegativeButton("Update Details", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Fragment_MitraRegistration fragment = Fragment_MitraRegistration.newInstance("edit");
                            fragment.mitraId =beanMitra_item.getId();
                            fragment.jsonObject =beanMitra_item.getJsonObject();
                            FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.container_body, fragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                            ((MainActivity) context).showUpButton(true);
                        }
                    })
                    .show();
        }


    }
}
