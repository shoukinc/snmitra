package com.shreecement.shree.Fragment.Enquiry;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.Enquiry.Fragment_Enquiry_List;
import com.shreecement.shree.ModalClass.LookupCategory;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.shreecement.shree.Utlility.Constant.ACTION_ADD;
import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.CLIENT_KEY;
import static com.shreecement.shree.Utlility.Constant.CLIENT_SECRET;
import static com.shreecement.shree.Utlility.Constant.Citylist;
import static com.shreecement.shree.Utlility.Constant.Create_enquiry;
import static com.shreecement.shree.Utlility.Constant.Districtlist;
import static com.shreecement.shree.Utlility.Constant.Lookup_data;
import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.MODULE_ENQUIRIES;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.Statelist;
import static com.shreecement.shree.Utlility.Constant.Talukalist;


/*
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Customer_add_Enquiry extends Fragment  implements View.OnClickListener{


    EditText edName,edMobileNumber,edAddress,edRequired_quantity,edEnquiry;
    TextView tvState,tvDistrict,tvtaluka,tvcity;
    TextView tvProduct,tvDate ;
    Button btnSave;

    String name = "";
    String mobile = "";
    String stateCode = "";
    String districtCode = "";
    String talukaCode = "";
    String cityCode = "";
    String address = "";
    String productCode = "";
    String date = "";
    String required_quantity = "";
    String enquiry = "";



    TextView dialogTitle;
    ListView listView;
    public AlertDialog.Builder dialogBuilder;
    public AlertDialog builder3;

    public ArrayList<LookupCategory> StateList;
    public ArrayList<LookupCategory> DistrictList;
    public ArrayList<LookupCategory> TalukaList;
    public ArrayList<LookupCategory> CityList;

    public SpinnerDialogAdapter spinnerAdapter;

    private LayerDrawable mBasketIcon;
    public ArrayList<LookupCategory> ProductList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_customer_enquiry, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        edName = view.findViewById(R.id.edName);
        edMobileNumber = view.findViewById(R.id.edMobileNumber);
        edAddress = view.findViewById(R.id.edName);
        edRequired_quantity = view.findViewById(R.id.edRequired_quantity);
        edEnquiry = view.findViewById(R.id.edEnquiry);

    getActivity().setTitle(getActivity().getString(R.string.nav_item_customer_enquiry));
        setHasOptionsMenu(true);


        tvState=view.findViewById(R.id.tvState);
        tvDistrict=view.findViewById(R.id.tvDistrict);
        tvtaluka=view.findViewById(R.id.tvtaluka);
        tvcity=view.findViewById(R.id.tvcity);
        tvProduct=view.findViewById(R.id.tvProduct);
        tvDate=view.findViewById(R.id.tvDate);

        btnSave=view.findViewById(R.id.btnSave);
        tvState.setOnClickListener(this);
        tvDistrict.setOnClickListener(this);
        tvtaluka.setOnClickListener(this);
        tvcity.setOnClickListener(this);
        tvProduct.setOnClickListener(this);
        tvDate.setOnClickListener(this);

        btnSave.setOnClickListener(this);
        ProductList=new ArrayList<>();
        StateList = new ArrayList<LookupCategory>();
        DistrictList = new ArrayList<LookupCategory>();
        TalukaList = new ArrayList<LookupCategory>();
        CityList = new ArrayList<LookupCategory>();

      //  new PostClassStateList(getActivity()).execute();
        new PostClassLookUpData(getActivity()).execute();

        return view;
    }

    private void showDialog() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        // cal.add(Calendar.DAY_OF_YEAR, -7);
        cal.add(Calendar.DAY_OF_MONTH, 1);

        //Date newDate = cal.getTime();
        // Create the DatePickerDialog instance
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), datePickerListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.setCancelable(false);
        datePicker.setTitle("Select the date");
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        // datePicker.getDatePicker().setMinDate(newDate.getTime());
        datePicker.show();


        /*Calendar calendar = Calendar.getInstance();
        calendar.setTime(myDate);
        calendar.add(Calendar.DAY_OF_YEAR, -7);
        Date newDate = calendar.getTime();*/

    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            String year = String.valueOf(selectedYear);
            String month = String.valueOf(selectedMonth + 1);
            String day = String.valueOf(selectedDay);
            String time = "";
            try {
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("hh:mm aa");
                time = df.format(cal.getTime());

                // tvDate.setText(time);

            } catch (Exception e) {
                e.printStackTrace();
            }

            String validity = String.valueOf(new StringBuilder().append(day).append("-")
                    .append(month).append("-").append(year));

            DateFormat readFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yyyy");
            Date date1 = null;
            Calendar cal = Calendar.getInstance();
            try {
                date1 = readFormat1.parse(validity);
                String dateformatt = formatter1.format(date1);


                tvDate.setText(dateformatt);
                date = validity;


            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    };
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_common, menu);
        MenuItem itemBasket = menu.findItem(R.id.action_button_basket);
        mBasketIcon = (LayerDrawable) itemBasket.getIcon();

        itemBasket.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.action_button_basket:
                        Bundle bundle= new Bundle();
                        bundle.putString("frag","enquirylist");

                        Fragment fragment= new Fragment_Enquiry_List();
                        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                                R.anim.enter_from_right, R.anim.exit_to_left);
                        fragment.setArguments(bundle);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.replace(R.id.container_body, fragment).commit();
                        ((MainActivity) getActivity()).showUpButton(true);

                }

                return false;
            }
        });


    }

    @Override
    public void onClick(View view) {
        if (view == tvState) {
            showSpinnerDialog(StateList, "State");
        }
        else if (view == tvDistrict) {
            showSpinnerDialog(DistrictList, "District");
        }
        else if (view == tvtaluka) {
            showSpinnerDialog(TalukaList, "Taluka");
        }
        else if (view == tvcity) {
            showSpinnerDialog(CityList, "City");
        } else if (view == tvProduct) {
            showSpinnerDialog(ProductList, "Product");
        } else if (view == tvDate) {
            showDialog();
        }

        else if (view == btnSave) {
            name=edName.getText().toString();
            mobile=edMobileNumber.getText().toString();
            stateCode=tvState.getText().toString();
            districtCode=tvDistrict.getText().toString();
            talukaCode=tvtaluka.getText().toString();
            cityCode=tvcity.getText().toString();
            address=edAddress.getText().toString();
            //productCode=tvProduct.getText().toString();
           // date=tvDate.getText().toString();
            required_quantity=edRequired_quantity.getText().toString();
            enquiry=edEnquiry.getText().toString();

         if (edName.getText().toString().length() == 0) {
                Toast.makeText(getActivity(), "Name field is required!", Toast.LENGTH_LONG).show();

            } else if (edMobileNumber.getText().toString().length() ==0) {
                Toast.makeText(getActivity(), "Mobile no field is required!", Toast.LENGTH_LONG).show();

            }else if (edMobileNumber.getText().toString().length() <10) {
                Toast.makeText(getActivity(), "Please Enter 10 digit mobile no !", Toast.LENGTH_LONG).show();

            } else if (edAddress.getText().toString().length() == 0) {
                Toast.makeText(getActivity(), "Address field is required!", Toast.LENGTH_LONG).show();

            } else if (tvProduct.getText().toString().length() == 0) {
                Toast.makeText(getActivity(), "Product field is required!", Toast.LENGTH_LONG).show();

            }else if (tvDate.getText().toString().length() == 0) {
                Toast.makeText(getActivity(), "Date field is required!", Toast.LENGTH_LONG).show();

            } else if (edRequired_quantity.getText().toString().length() == 0) {
                Toast.makeText(getActivity(), "Quantity field is required!", Toast.LENGTH_LONG).show();

            } else if (edEnquiry.getText().toString().length() == 0) {
                Toast.makeText(getActivity(), "Enquiry field is required!", Toast.LENGTH_LONG).show();

            }else {

                new PostClass_Enquiry(getActivity(),name, mobile, stateCode, districtCode , talukaCode,
             cityCode , address , productCode , date , required_quantity, enquiry).execute();
         }

        }
    }
    private void showSpinnerDialog(ArrayList<LookupCategory> list, String title) {
        dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listview, null);
        dialogBuilder.setView(dialogView);
        listView =  dialogView.findViewById(R.id.listCatecory);
        dialogTitle =   dialogView.findViewById(R.id.dialogTitle);
        dialogTitle.setText(title);
        spinnerAdapter = new SpinnerDialogAdapter(getActivity(), list, title);
        listView.setAdapter(spinnerAdapter);
        builder3 = dialogBuilder.create();
        builder3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder3.show();
    }
    public class SpinnerDialogAdapter extends BaseAdapter {
        Context context;
        List<LookupCategory> rowItems;
        String Title = "";


        public SpinnerDialogAdapter(Context context, List<LookupCategory> items, String title) {
            this.context = context;
            this.rowItems = items;
            this.Title = title;
            Log.d("===rowItems.zize===", "" + rowItems.size());

        }

        /*private view holder class*/
        public class ViewHolder {
            TextView textName;

        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.spinner_list_row_iteam, null);
                holder = new ViewHolder();
                holder.textName = convertView.findViewById(R.id.txt_title);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();

            }

            holder.textName.setText(rowItems.get(position).getDESCRIPTION());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String meaning = rowItems.get(position).getDESCRIPTION();
                    switch (Title) {

                        case "State":
                            builder3.dismiss();
                            tvState.setText(meaning);
                            stateCode = rowItems.get(position).getDESCRIPTION();
                            new PostClassDistrictList(getActivity(), stateCode).execute();
                            break;
                        case "District":
                            builder3.dismiss();
                            tvDistrict.setText(meaning);
                            districtCode = rowItems.get(position).getDESCRIPTION();
                            new PostClassTalukaList(getActivity(), stateCode,districtCode).execute();
                            break;
                        case "Taluka":
                            builder3.dismiss();
                            tvtaluka.setText(meaning);
                            talukaCode = rowItems.get(position).getDESCRIPTION();
                            new PostClassCityList(getActivity(), stateCode,districtCode,talukaCode).execute();
                            break;
                        case "City":
                            cityCode= rowItems.get(position).getDESCRIPTION();
                            builder3.dismiss();
                            tvcity.setText(meaning);
                            break;
                            case "Product":
                            productCode= rowItems.get(position).getCODE();
                            builder3.dismiss();
                            tvProduct.setText(meaning);
                            break;

                    }


                }
            });
            return convertView;
        }


        @Override
        public int getCount() {
            return rowItems.size();
        }

        @Override
        public Object getItem(int position) {
            return rowItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return rowItems.indexOf(getItem(position));
        }


    }


    class PostClassStateList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempStateList;

        PostClassStateList(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempStateList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Statelist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            //.addHeader("token", StorePrefs.getDefaults("token", context))
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Statelist=========" + Statelist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempStateList.add(new LookupCategory("",
                                            "", jsonObject.getString("state")));

                                }
                                StateList.clear();
                                StateList.addAll(tempStateList);

                            }


                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            } else {


            }

        }
    }

    class PostClassDistrictList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempDistrictList;
        String state;


        PostClassDistrictList(Context c, String state) {
            this.context = c;
            this.state = state;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempDistrictList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("state",state);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Districtlist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Districtlist=========" + Districtlist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempDistrictList.add(new LookupCategory("",
                                            "", jsonObject.getString("district")));

                                }
                                DistrictList.clear();
                                DistrictList.addAll(tempDistrictList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


            } else {


            }

        }
    }

    class PostClassTalukaList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempTalukaList;
        String state="";
        String district="";

        PostClassTalukaList(Context c, String state, String district) {
            this.context = c;
            this.state = state;
            this.district = district;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempTalukaList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    // json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("state", state);
                    json.put("district", district);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Talukalist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Talukalist=========" + Talukalist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {

                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempTalukaList.add(new LookupCategory("",
                                            "", jsonObject.getString("taluka")));

                                }
                                TalukaList.clear();
                                TalukaList.addAll(tempTalukaList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            } else {


            }

        }
    }

    class PostClassCityList extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        public ArrayList<LookupCategory> tempCityList;
        String taluka="";
        String state="";
        String district="";

        PostClassCityList(Context c, String state, String district, String taluka) {
            this.context = c;
            this.state = state;
            this.district = district;
            this.taluka = taluka;
        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            tempCityList = new ArrayList<LookupCategory>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            /*Realm realm = Realm.getDefaultInstance();*/

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    //json.put("page_module", MODULE_COMMON);
                    json.put("org_id", ORG_ID);
                    json.put("state", state);
                    json.put("district", district);
                    json.put("taluka", taluka);
                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Citylist)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_COMMON)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Citylist=========" + Citylist);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            if (jsonArraydata != null) {


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    JSONObject jsonObject = jsonArraydata.getJSONObject(i);

                                    tempCityList.add(new LookupCategory("",
                                            "", jsonObject.getString("city")));

                                }
                                CityList.clear();
                                CityList.addAll(tempCityList);

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();

            } else {

            }

        }
    }


    ////////////--------------------- Create FeedBack -------------------////////////////////////////
    class PostClass_Enquiry extends AsyncTask<String, Void, String> {
        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;

        String Name = "";
        String Mobile = "";
        String StateCode = "";
        String DistrictCode = "";
        String TalukaCode = "";
        String CityCode = "";
        String Address = "";
        String ProductCode = "";
        String Date = "";
        String Required_quantity = "";
        String Enquiry = "";
        PostClass_Enquiry(Context c,String name,
                          String mobile,String stateCode,String districtCode,
                          String talukaCode,String cityCode,String address,
                          String productCode,String date,String required_quantity, String enquiry) {
            this.context = c;
            this.Name = name;
            this.Mobile = mobile;
            this.StateCode = stateCode;
            this.DistrictCode =districtCode;
            this.TalukaCode = talukaCode;
            this.CityCode = cityCode;
            this.Address = address;
            this.ProductCode =productCode;
            this.Date = date;
            this.Required_quantity = required_quantity;
            this.Enquiry = enquiry;



        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();

                    json.put("enquiry_name", Name);
                    json.put("mobile_number", Mobile);
                    json.put("state", StateCode);
                    json.put("district", DistrictCode);
                    json.put("taluka", TalukaCode);
                    json.put("city", CityCode);
                    json.put("address", Address);
                    json.put("inventory_item_id", ProductCode);
                    json.put("required_quantity", Required_quantity);
                    json.put("required_date", Date);
                    json.put("enquiry",Enquiry );


                    System.out.println("json===============" + json.toString());

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(Create_enquiry)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_ENQUIRIES)
                            .addHeader("action", ACTION_ADD)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))
                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======Create_enquiryUrl=========" + Create_enquiry);

                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {

                            jsonreplyMsg = jObject.getString("replyMsg");

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        return e.toString();
                    }

                } catch (IOException e) {

                    e.printStackTrace();
                    return e.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                    return e.toString();
                }
            } else {
                return "Please Check Internet Connection";
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {

                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                Bundle bundle= new Bundle();
                bundle.putString("frag","enquirylist");
                Fragment fragment= new Fragment_Enquiry_List();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                        R.anim.enter_from_right, R.anim.exit_to_left);
                fragment.setArguments(bundle);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, fragment).commit();
                ((MainActivity) getActivity()).showUpButton(true);

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            }
        }
    }

    // Lokup data

    public class PostClassLookUpData extends AsyncTask<String, Void, String> {

        private final Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;



        public PostClassLookUpData(Context c) {
            this.context = c;

        }

        protected void onPreExecute() {

            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
            ProductList=new ArrayList<>();

        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");

                    /*JSONObject jsonObject_page_module = new JSONObject();

                    try {
                        jsonObject_page_module.put("page_module", MODULE_COMMON);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    RequestBody body = RequestBody.create(null, new byte[]{});

                    // RequestBody body = RequestBody.create(mediaType, jsonObject_page_module.toString());
                    Request request = new Request.Builder()
                            .post(body)
                            .url(Lookup_data)
                            .addHeader("content-type", "application/json")
                            .addHeader("client_key", CLIENT_KEY)
                            .addHeader("client_secret", CLIENT_SECRET)
                            .addHeader("page_module", MODULE_COMMON)
                            //.addHeader("action", ACTION_VIEW)
                            .addHeader("token",StorePrefs.getDefaults("token", context))
                            .build();
                    // .addHeader("token", StorePrefs.getDefaults("token", context))
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    System.out.println("Lookup_data===============" + Lookup_data);
                    response.message();


                    try {
                        jObject = new JSONObject(reqBody);
                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");
                            JSONObject category = jObject.getJSONObject("data");
                            JSONArray jsonArrayProduct = category.getJSONArray("product");
                            for (int i = 0; i < jsonArrayProduct.length(); i++) {
                                JSONObject jsonProduct = jsonArrayProduct.getJSONObject(i);
                                ProductList.add(new LookupCategory(jsonProduct.getString("lookup_type"),
                                        jsonProduct.getString("code"), jsonProduct.getString("description")));
                                Log.e("Product", ""+jsonProduct.getString("lookup_type") +" = "+jsonProduct.getString("code") +" = "+
                                        jsonProduct.getString("description") );

                            }


                            return jObject.getString("replyCode");


                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }


                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
               new PostClassStateList(context).execute();

            } else {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }


        }
    }
}
