package com.shreecement.shree.Fragment.BeanSliderImage;


public class BeanSliderImage {

     String scheme_name,scheme_short_description,scheme_long_description,scheme_url;

    public BeanSliderImage() {
    }

    public BeanSliderImage(String scheme_name, String scheme_short_description, String scheme_long_description, String scheme_url) {
        this.scheme_name = scheme_name;
        this.scheme_short_description = scheme_short_description;
        this.scheme_long_description = scheme_long_description;
        this.scheme_url = scheme_url;
    }

    public String getScheme_name() {
        return scheme_name;
    }

    public void setScheme_name(String scheme_name) {
        this.scheme_name = scheme_name;
    }

    public String getScheme_short_description() {
        return scheme_short_description;
    }

    public void setScheme_short_description(String scheme_short_description) {
        this.scheme_short_description = scheme_short_description;
    }

    public String getScheme_long_description() {
        return scheme_long_description;
    }

    public void setScheme_long_description(String scheme_long_description) {
        this.scheme_long_description = scheme_long_description;
    }

    public String getScheme_url() {
        return scheme_url;
    }

    public void setScheme_url(String scheme_url) {
        this.scheme_url = scheme_url;
    }
}
