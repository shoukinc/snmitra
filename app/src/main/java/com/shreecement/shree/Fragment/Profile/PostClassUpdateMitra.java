package com.shreecement.shree.Fragment.Profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;
import android.widget.Toast;

import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.shreecement.shree.Utlility.Constant.MODULE_COMMON;
import static com.shreecement.shree.Utlility.Constant.ORG_ID;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.UpdateMitraProfile;

/**
 * Created by Abhishek Punia on 15-03-2018.
 */


 public class PostClassUpdateMitra extends AsyncTask<String, Void, String> {
    private  Context context;
    ProgressDialog progress;
    String jsonreplyMsg;
    JSONObject jObject;


    String pageValue;
    String mitra_id;
    String mitra_number;
    String mitra_type;
    String title;
    String first_name;
    String middle_name;
    String last_name;
    String email;
    String mobile;
    String sex_code;
    String marital_status;
    String SPOUSE_NAME;
    String type_date;
    String BLOOD_TYPE_CODE;
    String DATE_OF_WEDDING;
    String CHILD1_NAME ;
    String DATE_OF_BIRTH_CHILD1;
    String CHILD2_NAME;
    String DATE_OF_BIRTH_CHILD2;
    String pan;
    String adhaar_number;
    String DRIVING_LICENCE_NUMBER;
    String VOTER_ID;
    String gst_number;
    String address1;
    String address2;
    String address3;
    String city,taluka,district,state,postal,page_module;

    String urlToPass = "";
    String action = "";
    String adhaar_imageUrl = "";
    String voter_imageUrl = "";
    String driving_licence_imageUrl = "";
    String pan_imageUrl = "";
    String mitra_imageUrl = "";

    public PostClassUpdateMitra(Context context, String pageValue, String mitra_id, String mitra_number
            , String mitra_type, String title, String first_name, String middle_name, String last_name,
                                String email, String mobile, String sex_code, String marital_status,
                                String SPOUSE_NAME, String type_date, String BLOOD_TYPE_CODE,
                                String DATE_OF_WEDDING, String CHILD1_NAME, String DATE_OF_BIRTH_CHILD1,
                                String CHILD2_NAME, String DATE_OF_BIRTH_CHILD2, String pan,
                                String adhaar_number, String DRIVING_LICENCE_NUMBER, String VOTER_ID,
                                String gst_number, String address1, String address2, String address3,
                                String city, String taluka, String district, String state, String postal,
                                String adhaar_imageUrl, String voter_imageUrl, String driving_licence_imageUrl, String pan_imageUrl, String mitra_imageUrl) {

        this.context = context;
        this.pageValue = pageValue;
        this.mitra_id = mitra_id;
        this.mitra_number = mitra_number;
        this.mitra_type = mitra_type;
        this.title = title;
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.email = email;
        this.mobile = mobile;
        this.sex_code = sex_code;
        this.marital_status = marital_status;
        this.SPOUSE_NAME = SPOUSE_NAME;
        this.type_date = type_date;
        this.BLOOD_TYPE_CODE = BLOOD_TYPE_CODE;
        this.DATE_OF_WEDDING = DATE_OF_WEDDING;
        this.CHILD1_NAME = CHILD1_NAME;
        this.DATE_OF_BIRTH_CHILD1 = DATE_OF_BIRTH_CHILD1;
        this.CHILD2_NAME = CHILD2_NAME;
        this.DATE_OF_BIRTH_CHILD2 = DATE_OF_BIRTH_CHILD2;
        this.pan = pan;
        this.adhaar_number = adhaar_number;
        this.DRIVING_LICENCE_NUMBER = DRIVING_LICENCE_NUMBER;
        this.VOTER_ID = VOTER_ID;
        this.gst_number = gst_number;
        this.address1 = address1;
        this.address2 = address2;
        this.address3 = address3;
        this.city = city;
        this.taluka = taluka;
        this.district = district;
        this.state = state;
        this.postal = postal;
        this.adhaar_imageUrl = adhaar_imageUrl;
        this.voter_imageUrl = voter_imageUrl;
        this.driving_licence_imageUrl = driving_licence_imageUrl;
        this.pan_imageUrl = pan_imageUrl;
        this.mitra_imageUrl = mitra_imageUrl;
    }



/*Log.d("======OrderId=======",""+OrderId);
        Log.d("======Category=======",""+Category);
        Log.d("======brand=======",""+Brand);
        Log.d("======User_type=======",""+User_type);
        Log.d("======Customer=======",""+Customer);
        Log.d("======Consignee=======",""+Consignee);
        Log.d("======Product=======",""+Product);
        Log.d("======Packing=======",""+Packing);
        Log.d("======Quantity=======",""+Quantity);
        Log.d("======Mm_name=======",""+Mm_name);
        Log.d("======Vendor_id=======",""+Vendor_id);
        Log.d("======Rate=======",""+Rate);
        Log.d("======Validity=======",""+Validity);
        Log.d("====Dispatch_from=====",""+Dispatch_from);
        Log.d("======Remark=======",""+Remark);
        Log.d("======Address=======",""+Address);
        Log.d("======City=======",""+City);
        Log.d("======State=======",""+State);
        Log.d("======District=======",""+District);
*/


    protected void onPreExecute() {

        progress = ProgressDialog.show(context, null, null);
        progress.setTitle("Loading...");
        Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
        d.setAlpha(200);
        progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        progress.getWindow().setBackgroundDrawable(d);
        progress.setContentView(R.layout.progress_dialog);
        progress.show();

         if(pageValue.equalsIgnoreCase("SelfUpdateMitra")){

            urlToPass = UpdateMitraProfile;


        }

    }

    @Override
    protected String doInBackground(String... params) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnected()) {
            try {
                OkHttpClient client = new OkHttpClient();
                MediaType mediaType = MediaType.parse("application/json");
                JSONObject  jsonObject = new JSONObject();


                if(pageValue.equalsIgnoreCase("SelfUpdateMitra")){
                    jsonObject.put("mitra_id", mitra_id);
                }

               // jsonObject.put("org_id", ORG_ID);

                //jsonObject.put("page_module", MODULE_MITRAS);
                jsonObject.put("org_id", ORG_ID);
                jsonObject.put("org_id", ORG_ID);
                jsonObject.put("mitra_number",mitra_number);
                jsonObject.put("mitra_type_code",mitra_type);
                jsonObject.put("title",title);
                jsonObject.put("first_name",first_name);
                jsonObject.put("middle_names",middle_name);
                jsonObject.put("last_name",last_name);
                jsonObject.put("email_address",email);
                jsonObject.put("mobile_number",mobile);
                jsonObject.put("sex_code",sex_code);
                jsonObject.put("marital_status_code",marital_status);
                jsonObject.put("spouse_name",SPOUSE_NAME);
                jsonObject.put("date_of_birth",type_date);
                jsonObject.put("blood_type_code",BLOOD_TYPE_CODE);
                jsonObject.put("date_of_wedding",DATE_OF_WEDDING);
                jsonObject.put("child1_name",CHILD1_NAME);
                jsonObject.put("date_of_birth_child1",DATE_OF_BIRTH_CHILD1);
                jsonObject.put("child2_name",CHILD2_NAME);
                jsonObject.put("date_of_birth_child2",DATE_OF_BIRTH_CHILD2);
                jsonObject.put("pan",pan);
                jsonObject.put("adhaar_number",adhaar_number);
                jsonObject.put("driving_licence_number",DRIVING_LICENCE_NUMBER);
                jsonObject.put("voter_id",VOTER_ID);
                jsonObject.put("gst_number",gst_number);
                jsonObject.put("address_line1",address1);
                jsonObject.put("address_line2",address2);
                jsonObject.put("address_line3",address3);
                jsonObject.put("city",city);
                jsonObject.put("taluka",taluka);
                jsonObject.put("district",district);
                jsonObject.put("state",state);
                jsonObject.put("postal_code",postal);

                RequestBody body = RequestBody.create(mediaType,jsonObject.toString());
                System.out.println("jsonObject.toString()===============" + jsonObject.toString());
                Request request = new Request.Builder()
                        .url(urlToPass)
                        .post(body)
                        .addHeader("content-type", "application/json")
                        .addHeader("page_module", MODULE_COMMON)
                        //.addHeader("action", action)
                        .addHeader("token",  StorePrefs.getDefaults("token", context))
                        .build();
                Response response = client.newCall(request).execute();
                String reqBody = response.body().string();
                System.out.println("response===============" + reqBody);
                response.message();

                try {
                    jObject = new JSONObject(reqBody);

                    if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                        jsonreplyMsg = jObject.getString("replyMsg");

                        return jObject.getString("replyCode");

                    } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                        return jObject.getString("replyMsg");
                    }
                } catch (JSONException e) {
                    progress.dismiss();
                    e.printStackTrace();
                }

            } catch (IOException e) {
                progress.dismiss();
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    progress.dismiss();
                    Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            });
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progress.dismiss();
        if (s == null) {
            Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
            return;
        } else if (s.equals("success")) {
            android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(
                    context);

            if(pageValue.equalsIgnoreCase("SelfUpdateMitra")) {
                try {
                    alert.setTitle(jsonreplyMsg);
                    alert.setMessage("ID - "+jObject.getString("mitra_id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            alert.setCancelable(false);
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {


                    /* Fragment_DocUpload fragment = Fragment_DocUpload.newInstance("add");
                     fragment.NewCreatedMitraId = jObject.getString("mitra_id");
                     FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                     FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                     fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,R.anim.enter_from_right, R.anim.exit_to_left);
                     fragmentTransaction.replace(R.id.container_body, fragment);
                     fragmentTransaction.commit();*/



                            ((Activity) context).onBackPressed();







                    //  ((Activity)context).onBackPressed();
                }
            });

            alert.show();

        } else {
            Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

        }

    }
}
