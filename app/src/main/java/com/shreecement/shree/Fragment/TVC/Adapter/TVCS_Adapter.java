package com.shreecement.shree.Fragment.TVC.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.shreecement.shree.Activity.MainActivity;
import com.shreecement.shree.Fragment.TVC.Bean.BeanVideo;
import com.shreecement.shree.Fragment.TVC.Fragment_TVCSVideoPlayer;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;

import java.util.ArrayList;


/**
 * Created by TIA on 12-08-2016.
 */

public class TVCS_Adapter extends RecyclerView.Adapter<TVCS_Adapter.ViewHolder> {
    Context context;
    ArrayList<BeanVideo> rowItems;

    public TVCS_Adapter(Context context, ArrayList<BeanVideo> rowItem) {
        this.context = context;
        this.rowItems = rowItem;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_tvcs_row_item, parent, false);

        return new ViewHolder(itemView);
    }
    public void updateData(ArrayList<BeanVideo> rowItem) {
        this.rowItems = rowItem;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.tvVideoTitle.setText(rowItems.get(position).getTitle());

        Glide.with(context).load("https://img.youtube.com/vi/" + rowItems.get(position).getYoutube_url()+"/0.jpg")
                .thumbnail(1.0f)
                .crossFade()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imVideoThumb);

    }

    @Override
    public int getItemCount() {

        Log.e("Size of ",String.valueOf(rowItems.size()));
        return rowItems.size();

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvVideoTitle;

        ImageView imVideoThumb;
        ViewHolder(View convertView) {
            super(convertView);

            tvVideoTitle = convertView.findViewById(R.id.tvVideoTitle);
            imVideoThumb  = convertView.findViewById(R.id.imVideoThumb);

            convertView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {


            BeanVideo  beanVideo= rowItems.get(getLayoutPosition());
            Gson gson = new Gson();
            String json = gson.toJson(beanVideo);
            StorePrefs.setDefaults("beanVideo", json, context);

            Fragment_TVCSVideoPlayer fragment = new Fragment_TVCSVideoPlayer();
            android.support.v4.app.FragmentTransaction fragmentTransaction = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            ((MainActivity) context).showUpButton(true);

        }
    }



}