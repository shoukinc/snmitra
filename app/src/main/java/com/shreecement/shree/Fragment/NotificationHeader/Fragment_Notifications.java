package com.shreecement.shree.Fragment.NotificationHeader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


import com.shreecement.shree.Common.BeanCommon;
import com.shreecement.shree.Fragment.NotificationHeader.Adapter.NotificationHeader_Adapter;
import com.shreecement.shree.R;
import com.shreecement.shree.Utlility.StorePrefs;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;



import static com.shreecement.shree.Utlility.Constant.ACTION_VIEW;
import static com.shreecement.shree.Utlility.Constant.MODULE_NOTIFICATIONS;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_ERROR;
import static com.shreecement.shree.Utlility.Constant.RESPONSE_TYPE_SUCCESS;
import static com.shreecement.shree.Utlility.Constant.notifications;

/*
 * Created by TIA on 11-08-2016.
 */

public class Fragment_Notifications extends Fragment {
    RecyclerView recyclerView;
    NotificationHeader_Adapter mitraAdapter;
    ArrayList<BeanCommon> notificationList;
    String keyword = "";
    int page = 1;
    SwipeRefreshLayout swipeRefreshLayout;


    boolean isPagination = false;

    TextView tv_no_record_found;
    TextView tv_count_item;
    int total_record = 0;
    int recordCount = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notifications ,container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getActivity().setTitle(getActivity().getString(R.string.notifications));
        tv_no_record_found = view.findViewById(R.id.tv_no_record_found);
        tv_count_item = view.findViewById(R.id.tv_count_item);

        recyclerView = view.findViewById(R.id.recyclerviewNotifications);
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);
       // setHasOptionsMenu(true);


        notificationList = new ArrayList<>();
        keyword = "";

        mitraAdapter = new NotificationHeader_Adapter(getActivity(), notificationList);
        recyclerView.setAdapter(mitraAdapter);
        mitraAdapter.notifyDataSetChanged();
        LinearLayoutManager llm_recent = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm_recent);
        recyclerView.refreshDrawableState();
        recyclerView.invalidate();



      /*  skeletonScreen = Skeleton.bind(recyclerView)
                .adapter(mitraAdapter)
                .load(R.layout.fragment_notifications)
                .show();*/
        /*skeletonScreen = Skeleton.bind(view)
                .load(R.layout.fragment_notifications)
                .show();*/


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(1)) {
                    loadMoreItems();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new PostClass_NotificationsHeader(getActivity(), "1", keyword, true).execute();
            }
        }, 425);


        return view;
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        // super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.clearFocus();

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                customerList.clear();
                page = 1;
                keyword = "";
                new PostClass_Customer(getActivity(), "1", "", true).execute();

                return false;
            }
        });

      *//*  searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                keyword = query;
                customerList.clear();

                new PostClass_Customer(getActivity(), "", keyword, true).execute();
              *//**//*  customerList.clear();
                for (int x = 0; x < tempList1.size(); x++) {
                    if(tempList1.get(x).getParty_name().contains(query.toUpperCase()))
                        customerList.add(tempList1.get(x));
                }
                customers_adapter.notifyDataSetChanged();*//**//*
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

              *//**//*  if (newText.length() == 0){
                    keyword = newText;
                    new PostClass_Customer(getActivity(), "1",keyword, true).execute();

                }*//**//*
                return false;
            }
        });*//*


    }*/


    private void loadMoreItems() {
        isPagination = true;

        new PostClass_NotificationsHeader(getActivity(), String.valueOf(++page), keyword, true).execute();
    }

    private void refreshItems() {
        page = 1;
        notificationList.clear();
        swipeRefreshLayout.setRefreshing(false);
        // Load items
        // ...
        new PostClass_NotificationsHeader(getActivity(), String.valueOf(page), keyword, true).execute();


    }

    class PostClass_NotificationsHeader extends AsyncTask<String, Void, String> {

        private Context context;
        ProgressDialog progress;
        JSONObject jObject;
        String jsonreplyMsg;
        String pageNo;
        String Keyword;
        boolean showLoader;

        PostClass_NotificationsHeader(Context c, String pageNo, String keyword, boolean showLoader) {
            this.context = c;
            this.pageNo = pageNo;
            this.Keyword = keyword;
            this.showLoader = showLoader;
            Log.d("====keyword=====", "" + Keyword);
        }

        protected void onPreExecute() {
            progress = ProgressDialog.show(context, null, null);
            progress.setTitle("Loading...");
            Drawable d = new ColorDrawable(ContextCompat.getColor(context, R.color.transparent));
            d.setAlpha(200);
            progress.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            progress.getWindow().setBackgroundDrawable(d);
            progress.setContentView(R.layout.progress_dialog);
            progress.show();
          //  shimmerText.startShimmerAnimation();


        }

        @Override
        protected String doInBackground(String... params) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    MediaType mediaType = MediaType.parse("application/json");
                    JSONObject json = new JSONObject();
                    json.put("keyword", Keyword);
                    json.put("page", pageNo);
                    //json.put("page_module", MODULE_NOTIFICATIONS);

                    RequestBody body = RequestBody.create(mediaType, json.toString());

                    Request request = new Request.Builder()
                            .url(notifications)
                            .post(body)
                            .addHeader("content-type", "application/json")
                            .addHeader("page_module", MODULE_NOTIFICATIONS)
                            .addHeader("action", ACTION_VIEW)
                            .addHeader("token",  StorePrefs.getDefaults("token", context))

                            .build();
                    Response response = client.newCall(request).execute();
                    String reqBody = response.body().string();
                    System.out.println("response===============" + reqBody);
                    response.message();
                    System.out.println("======notifications=========" + notifications);

                    try {
                        jObject = new JSONObject(reqBody);

                        if (jObject.getString("replyCode").equals(RESPONSE_TYPE_SUCCESS)) {
                            jsonreplyMsg = jObject.getString("replyMsg");

                            total_record = Integer.parseInt(jObject.getString("total_record"));
                            recordCount = Integer.parseInt(jObject.getString("recordCount"));

                            JSONArray jsonArraydata = jObject.getJSONArray("data");
                            System.out.println("======jsonArraydata=========" + jsonArraydata);
                            if (jsonArraydata != null) {
                                JSONObject jsonObject;


                                for (int i = 0; i < jsonArraydata.length(); i++) {
                                    jsonObject = jsonArraydata.getJSONObject(i);
                                    System.out.println("======jsonObject=========" + jsonObject);

                                   /* tempcustomerlist.add(new BeanNotificationHeader(
                                            jsonObject.getString("notification_id"),
                                            jsonObject.getString("notification"),
                                            jsonObject.getString("notification_type"),
                                            jsonObject.getString("creation_date"),
                                            jsonObject.getString("status"),
                                            jsonObject.getString("source_type"),
                                            jsonObject.getString("sender_name"),
                                            jsonObject.getString("unixtimestring"),
                                            jsonObject.getString("timeagodate"),
                                            jsonObject.getString("status_text")));*/

                                    notificationList.add(new BeanCommon(
                                            jsonObject.getString("notification_id"),
                                            jsonObject));

                                }

                            }

                            return jObject.getString("replyCode");

                        } else if (jObject.getString("replyCode").equals(RESPONSE_TYPE_ERROR)) {

                            return jObject.getString("replyMsg");
                        }
                    } catch (JSONException e) {
                        progress.dismiss();
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    progress.dismiss();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        progress.dismiss();
                        Toast.makeText(context, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.dismiss();
            if (s == null) {
                page--;
                Toast.makeText(context, "Server not responding! Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            } else if (s.equals("success")) {
                //Toast.makeText(context,jsonreplyMsg.toString(), Toast.LENGTH_LONG).show();


                mitraAdapter.notifyDataSetChanged();
              //  shimmerText.stopShimmerAnimation();

            } else {
                page--;
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

            }
            recordCount = notificationList.size();
            if (recordCount==0){
                tv_no_record_found.setVisibility(View.VISIBLE);

             tv_count_item.setText("Showing  "+recordCount+"   of  "+total_record);
            }else {
                tv_no_record_found.setVisibility(View.GONE);
             tv_count_item.setText("Showing  "+recordCount+"   of  "+total_record);
            }


        }
    }
}
